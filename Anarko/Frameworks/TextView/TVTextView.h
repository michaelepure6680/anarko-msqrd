//
//  TextView.h
//  VideoEditor
//
//  Created by Hua Wan on 8/29/12.
//  Copyright (c) 2012 VideoEditor. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CustomTextView.h"

#define MIN_TEXTVIEW_SIZE   CGSizeMake(110, 90)
#define TEXT_OFFSET         20

#define TEXTVIEW_FRAME      CGRectMake(0, 0, 120, 90)

@protocol TVTextViewDelegate;

@interface TVTextView : UIView

@property (nonatomic, retain) CustomTextView *textView;

//! Text view delegate value
@property (nonatomic, assign) id<TVTextViewDelegate> delegate;

//! Text view active flag
@property (nonatomic, getter = isActive, setter = setActive:) BOOL active;

//! Text view proportional resize flag
@property (nonatomic, assign) BOOL proportionalResize;

// Render text properties

//! Text reflection
@property (nonatomic, assign) CGFloat reflection;

//! Typed text color
@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, assign) NSInteger colorIndex;

//! Typed text highlight color
@property (nonatomic, assign) UIColor *highlightColor;

//! Typed text lightness
@property (nonatomic, assign) CGFloat textColorLightness;

//! Typed text highlight lightness
@property (nonatomic, assign) CGFloat highlightColorLightness;

//! Typed text opacity
@property (nonatomic, assign) CGFloat textOpacity;

//! Typed text scale
@property (nonatomic, assign) CGPoint textScale;

//! Typed text size
@property (nonatomic, assign) CGSize textSize;

// TimeLine index
@property (nonatomic, assign) NSInteger timeLineId;

// Font Index
@property (nonatomic, assign) NSInteger fontIndex;

@property (nonatomic, assign) CGFloat fontSize;

- (void)resizePinch:(UIPinchGestureRecognizer *)pinch;

- (void)setTextFontWithName:(NSString *)fontName;
- (UIFont *)textFont;

- (NSLineBreakMode)textLineBreakMode;

- (NSString *)getText;

- (void)showKeyboard;
- (void)dismissKeyboard;

- (UIEdgeInsets)textEdgesInsets;

// Drawning optimization methods
- (void)prepareForPrint;
- (void)prepareForDrawning;

- (CALayer *)layerWithImageRepresentationForSize:(CGSize)videoSize scale:(CGFloat)scale;
- (CALayer *)layerWithImageForSize:(CGSize)videoSize scale:(CGFloat)scale;
- (UIImage *)renderImageForSize:(CGSize)imageSize;

@property (nonatomic, assign) CGSize preservedSize;

@property (nonatomic, assign) CGRect parentFrame;

@end


@protocol TVTextViewDelegate <NSObject>

- (void)textViewWillBecomeActive:(TVTextView *)textView;

- (void)textViewRemovePressed:(TVTextView *)textView;

- (BOOL)textView:(TVTextView *)textView shouldChangeFrame:(CGRect)newFrame;

- (BOOL)textView:(TVTextView *)textView shouldChangeText: (NSString *)newText;

- (void)textViewDidBeginEditing:(TVTextView *)textView;

- (void)textViewDidEndEditing:(TVTextView *)textView;

@end


