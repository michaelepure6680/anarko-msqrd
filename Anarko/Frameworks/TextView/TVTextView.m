//
//  TextView.m
//  VideoEditor
//
//  Created by Hua Wan on 8/29/12.
//  Copyright (c) 2012 VideoEditor. All rights reserved.
//

#import "TVTextView.h"
#import "CustomTextView.h"

#import "UIImage+Resize.h"

#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>

#define MAX_SCALE               7.0
#define MIN_SCALE               0.1

#define REFLECTION_HEIHGT       0.5
#define CONTENT_VIEWS_SIDE      30.0
#define SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define ACTIVE_BORDER_COLOR     [[UIColor whiteColor] CGColor]
#define INACTIVE_BORDER_COLOR   [[UIColor clearColor] CGColor]

#define IS_IOS_7 ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)
#define MAX_FONT_SIZE           500
#define MIN_FONT_SIZE           5

@interface TVTextView () <UITextViewDelegate, UIGestureRecognizerDelegate>
{
    BOOL isDeleting;
}

// Removed unused properties by Luski Timurovich on 04/20/2017.
//@property (nonatomic, retain) UIImageView* rotationImageView;
//@property (nonatomic, retain) UIImageView* resizeImageView;
//@property (nonatomic, retain) UIImageView* removeImageView;
//@property (nonatomic, retain) UITapGestureRecognizer* removeRecognaizer;
//@property (nonatomic, retain) UIPanGestureRecognizer* rotateRecognaizer;
//@property (nonatomic, retain) UIPanGestureRecognizer* resizeRecognaizer;

@property (nonatomic, retain) UIPinchGestureRecognizer* pinchRecognaizer;
@property (nonatomic, retain) UIRotationGestureRecognizer* rotateRecognaizer;

@property (nonatomic, retain) UIView* contentResize;

// Removed unused properties by Luski Timurovich on 04/20/2017.
//@property (nonatomic, retain) UIView* contentRemove;
//@property (nonatomic, retain) UIView* contentRotate;

@property (nonatomic, retain) UIView* recognaizerView;

@property (nonatomic, retain) UIView* borderView;


// For rotating
//
@property (nonatomic, assign) CGFloat lastRotation;
@property (nonatomic, assign) CGPoint lastPoint;


//For resize
//
@property (nonatomic, assign) CGPoint firstCenter;
@property (nonatomic, assign) CGPoint firstPoint;
@property (nonatomic, assign) CGRect  firstFrame;
@property (nonatomic, retain) UIFont* firstFont;

@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, assign) BOOL afterCreation;

// White dots
//
// Removed unused properties by Luski Timurovich on 04/20/2017.
//@property (nonatomic, retain) UIView* leftDotButton;
//@property (nonatomic, retain) UIView* topDotButton;
//@property (nonatomic, retain) UIView* rightDotButton;
//@property (nonatomic, retain) UIView* bottomDotButton;
//@property (nonatomic, retain) UIImageView* tlDotButton;
//@property (nonatomic, retain) UIImageView* trDotButton;
//@property (nonatomic, retain) UIImageView* blDotButton;
@property (nonatomic, retain) UIImageView* brDotButton;

// Removed unused properties by Luski Timurovich on 04/20/2017.
//@property (nonatomic, retain) UIView* leftBottomCornerLargeDot;
//@property (nonatomic, retain) UIPanGestureRecognizer* topResizeGestureRecognizer;
//@property (nonatomic, retain) UIPanGestureRecognizer* bottomResizeGestureRecognizer;
//@property (nonatomic, retain) UIPanGestureRecognizer* leftResizeGestureRecognizer;
//@property (nonatomic, retain) UIPanGestureRecognizer* rightResizeGestureRecognizer;
//@property (nonatomic, retain) UIPanGestureRecognizer* tlResizeGestureRecognizer;
//@property (nonatomic, retain) UIPanGestureRecognizer* trResizeGestureRecognizer;
//@property (nonatomic, retain) UIPanGestureRecognizer* blResizeGestureRecognizer;
@property (nonatomic, retain) UIPanGestureRecognizer* brResizeGestureRecognizer;

// Removed unused properties by Luski Timurovich on 04/20/2017.
//@property (nonatomic, retain) UIPanGestureRecognizer* leftBottomCornerResizeGestureRecognizer;

@end

@implementation TVTextView

@synthesize active = _active;

#pragma mark -
#pragma mark Initialization methods

- (id) init
{
    self = [super init];
    
    if (self)
    {
        self.preservedSize = CGSizeZero;
        self.textColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor clearColor];

        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.layer.contentsScale = [UIScreen mainScreen].scale;
        
        UITapGestureRecognizer* singleTap = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activeView:)] autorelease];
        singleTap.numberOfTapsRequired    = 1;
        [self addGestureRecognizer: singleTap];
        
        UITapGestureRecognizer* doubleTap = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapHandler:)] autorelease];
        doubleTap.numberOfTapsRequired    = 2;
        [self addGestureRecognizer: doubleTap];
        
        
        // Create text view
        //
        self.textView                   = [[CustomTextView new] autorelease];
        self.textView.delegate          = self;
        self.textView.text              = @"";
        self.textView.backgroundColor   = [UIColor clearColor];
        self.textView.textAlignment     = NSTextAlignmentLeft;
        
        CGFloat fontSize = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? 30.0f : 60.0f;
        if (SYSTEM_VERSION_GREATER_THAN(@"5.1.1"))
            self.textView.font          = [UIFont fontWithName: @"BebasNeue" size: fontSize];  //default
        else
            self.textView.font          = [UIFont systemFontOfSize: fontSize];  //default
        _fontSize = fontSize;
        
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textView.editable          = YES;
        self.textView.scrollEnabled     = NO;
        self.textView.autoresizingMask  = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.textView setReturnKeyType:UIReturnKeyDone];
        
        // Create rotation view
        //
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.contentRotate                  = [[UIView new] autorelease];
//        self.contentRotate.autoresizingMask = UIViewAutoresizingNone;
//        self.contentRotate.backgroundColor  = [UIColor clearColor];
        
//        self.rotateRecognaizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(rotate:)] autorelease];
//        [self.contentRotate addGestureRecognizer:self.rotateRecognaizer];
        self.rotateRecognaizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(newRotate:)];
        self.rotateRecognaizer.delegate = self;
        [self addGestureRecognizer:self.rotateRecognaizer];
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.rotationImageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-title-modify-corner"]] autorelease];
//        self.rotationImageView.autoresizingMask = UIViewAutoresizingNone;
        
        
        // Create resize image view
        //
        self.contentResize = [[UIView new] autorelease];
        self.contentResize.autoresizingMask = UIViewAutoresizingNone;
        self.contentResize.backgroundColor = [UIColor clearColor];
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.resizeRecognaizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
//        [self.contentResize addGestureRecognizer:self.resizeRecognaizer];
        self.pinchRecognaizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(newResize:)];
        self.pinchRecognaizer.delegate = self;
        [self addGestureRecognizer:self.pinchRecognaizer];
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.resizeImageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-title-modify-corner"]] autorelease];
//        self.resizeImageView.autoresizingMask = UIViewAutoresizingNone;
        
        
        // Create remove image view
        //
//        self.contentRemove = [[UIView new] autorelease];
//        self.contentRemove.autoresizingMask = UIViewAutoresizingNone;
//        self.contentRemove.backgroundColor = [UIColor clearColor];
//        
//        self.removeRecognaizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
//        [self.contentRemove addGestureRecognizer:self.removeRecognaizer];
        
//        self.removeImageView = [[[UIImageView alloc] initWithImage: [UIImage imageNamed:@"icon-title-modify-corner"]] autorelease];
//        self.removeImageView.autoresizingMask = UIViewAutoresizingNone;
        
        
        // Create dots
        //
        UIImage* smallDotImage = [UIImage imageNamed:@"icon-title-modify-corner"];
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        UIImage* largeDotImage = [UIImage imageNamed:@"icon-title-modify-corner"];
        
//        self.topResizeGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
//        self.rightResizeGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
//        self.bottomResizeGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
//        self.leftResizeGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
//        self.leftBottomCornerResizeGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
//        self.tlResizeGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
//        self.trResizeGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
//        self.blResizeGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
        self.brResizeGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resize:)] autorelease];
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.tlDotButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, TEXT_OFFSET * 2, TEXT_OFFSET * 2)];
//        self.tlDotButton.contentMode = UIViewContentModeCenter;
//        [self.tlDotButton setImage:smallDotImage];
//        self.tlDotButton.userInteractionEnabled = YES;
//        [self.tlDotButton addGestureRecognizer:self.tlResizeGestureRecognizer];
//        self.trDotButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, TEXT_OFFSET * 2, TEXT_OFFSET * 2)];
//        self.trDotButton.contentMode = UIViewContentModeCenter;
//        [self.trDotButton setImage:smallDotImage];
//        self.trDotButton.userInteractionEnabled = YES;
//        [self.trDotButton addGestureRecognizer:self.trResizeGestureRecognizer];
//        self.blDotButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, TEXT_OFFSET * 2, TEXT_OFFSET * 2)];
//        self.blDotButton.contentMode = UIViewContentModeCenter;
//        [self.blDotButton setImage:smallDotImage];
//        self.blDotButton.userInteractionEnabled = YES;
//        [self.blDotButton addGestureRecognizer:self.blResizeGestureRecognizer];
        self.brDotButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, TEXT_OFFSET * 2, TEXT_OFFSET * 2)];
        self.brDotButton.contentMode = UIViewContentModeCenter;
        [self.brDotButton setImage:smallDotImage];
        self.brDotButton.userInteractionEnabled = YES;
        [self.brDotButton addGestureRecognizer:self.brResizeGestureRecognizer];
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        [self addSubview: self.tlDotButton];
//        [self addSubview: self.trDotButton];
//        [self addSubview: self.blDotButton];
        [self addSubview: self.brDotButton];
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
/*
        self.leftDotButton = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, TEXT_OFFSET * 2, TEXT_OFFSET * 2)] autorelease];
        UIImageView* leftDotButtonImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                                              smallDotImage.size.width,
                                                                                              smallDotImage.size.height)] autorelease];
        leftDotButtonImageView.image = smallDotImage;
        leftDotButtonImageView.center = CGPointMake(self.leftDotButton.frame.size.width / 2, self.leftDotButton.frame.size.height / 2);
//        [self.leftDotButton addSubview: leftDotButtonImageView];
        [self.leftDotButton setBackgroundColor:[UIColor redColor]];
        [self.leftDotButton addGestureRecognizer: self.leftResizeGestureRecognizer];
        self.leftDotButton.hidden = YES;
        
        
        
        self.rightDotButton = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, TEXT_OFFSET * 2, TEXT_OFFSET * 2)] autorelease];
        UIImageView* rightDotButtonImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                                               smallDotImage.size.width,
                                                                                               smallDotImage.size.height)] autorelease];
        rightDotButtonImageView.image = smallDotImage;
        rightDotButtonImageView.center = CGPointMake(self.rightDotButton.frame.size.width / 2, self.rightDotButton.frame.size.height / 2);
//        [self.rightDotButton addSubview: rightDotButtonImageView];
        [self.rightDotButton setBackgroundColor:[UIColor redColor]];
        [self.rightDotButton addGestureRecognizer: self.rightResizeGestureRecognizer];
        self.rightDotButton.hidden = YES;
        
        
        
        self.topDotButton = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, TEXT_OFFSET * 2, TEXT_OFFSET * 2)] autorelease];
        UIImageView* topDotButtonImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                                             smallDotImage.size.width,
                                                                                             smallDotImage.size.height)] autorelease];
        topDotButtonImageView.image = smallDotImage;
        topDotButtonImageView.center = CGPointMake(self.topDotButton.frame.size.width / 2, self.topDotButton.frame.size.height / 2);
//        [self.topDotButton addSubview: topDotButtonImageView];
        [self.topDotButton setBackgroundColor:[UIColor redColor]];
        [self.topDotButton addGestureRecognizer: self.topResizeGestureRecognizer];
        self.topDotButton.hidden = YES;
        
*/
/*
        self.bottomDotButton = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, TEXT_OFFSET * 2, TEXT_OFFSET * 2)] autorelease];
        UIImageView* bottomDotButtonImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                                                smallDotImage.size.width ,
                                                                                                smallDotImage.size.height)] autorelease];
        bottomDotButtonImageView.image = smallDotImage;
        bottomDotButtonImageView.center = CGPointMake(self.bottomDotButton.frame.size.width / 2, self.bottomDotButton.frame.size.height / 2);
//        [self.bottomDotButton addSubview: bottomDotButtonImageView];
        [self.bottomDotButton setBackgroundColor:[UIColor redColor]];
        [self.bottomDotButton addGestureRecognizer: self.bottomResizeGestureRecognizer];
        self.bottomDotButton.hidden = YES;
*/
        self.contentMode = UIViewContentModeRedraw;
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
/*
        self.leftBottomCornerLargeDot = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, TEXT_OFFSET * 2, TEXT_OFFSET * 2)] autorelease];
        UIImageView* leftBottomCornerLargeDotImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                                                         largeDotImage.size.width,
                                                                                                         largeDotImage.size.height)] autorelease];
        leftBottomCornerLargeDotImageView.image = largeDotImage;
        leftBottomCornerLargeDotImageView.center = CGPointMake(self.leftBottomCornerLargeDot.frame.size.width / 2, self.leftBottomCornerLargeDot.frame.size.height / 2);
        [self.leftBottomCornerLargeDot addSubview: leftBottomCornerLargeDotImageView];
        [self.leftBottomCornerLargeDot addGestureRecognizer: self.leftBottomCornerResizeGestureRecognizer];
        self.leftBottomCornerLargeDot.hidden = NO;
*/
        self.borderView = [[[UIView alloc] init] autorelease];
        self.borderView.layer.borderWidth = 1.0f;
        self.borderView.layer.borderColor = ACTIVE_BORDER_COLOR;
        
        
        self.textScale = CGPointMake(1.f, 1.f);
        
        self.highlightColor = [UIColor clearColor];
        self.textColorLightness = 1.0f;
        self.highlightColorLightness = 0.0f;
        self.textOpacity = 1;
        
        [self addSubview: self.borderView];
        [self addSubview: self.textView];
        
        [self addSubview: self.contentResize];
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        [self addSubview: self.contentRotate];
//        [self addSubview: self.contentRemove];
        
//        [self addSubview: self.leftDotButton];
//        [self addSubview: self.rightDotButton];
//        [self addSubview: self.topDotButton];
//        [self addSubview: self.bottomDotButton];
//        [self addSubview: self.leftBottomCornerLargeDot];
        
        
//        [self addSubview: self.rotationImageView];
//        [self addSubview: self.resizeImageView];
//        [self addSubview: self.removeImageView];
        //
        
//        self.resizeImageView.frame = CGRectMake(0, 0, self.resizeImageView.image.size.width, self.resizeImageView.image.size.height);
//        self.rotationImageView.frame = CGRectMake(0, 0, self.rotationImageView.image.size.width, self.rotationImageView.image.size.height);
//        self.removeImageView.frame = CGRectMake(0, 0, self.removeImageView.image.size.width, self.removeImageView.image.size.height);
        
//        [self bringSubviewToFront:self.tlDotButton];
//        [self bringSubviewToFront:self.trDotButton];
//        [self bringSubviewToFront:self.blDotButton];
        [self bringSubviewToFront:self.brDotButton];
    }
    
    return self;
}

- (void)dealloc
{
    self.textView           = nil;
    
    // Removed unused properties by Luski Timurovich on 04/20/2017.
//    self.rotationImageView  = nil;
//    self.resizeImageView    = nil;
//    self.removeImageView    = nil;
    self.firstFont          = nil;
    
    // Removed unused properties by Luski Timurovich on 04/20/2017.
//    self.removeRecognaizer  = nil;
//    self.rotateRecognaizer  = nil;
//    self.resizeRecognaizer  = nil;
    self.rotateRecognaizer  = nil;
    self.pinchRecognaizer  = nil;
    
    self.contentResize      = nil;
    
    // Removed unused properties by Luski Timurovich on 04/20/2017.
//    self.contentRemove      = nil;
//    self.contentRotate      = nil;
}

- (void)layoutSubviews
{
    CGAffineTransform transform = self.transform;
    self.transform = CGAffineTransformIdentity;
    [super layoutSubviews];

    self.textView.transform = CGAffineTransformIdentity;

    self.borderView.frame  = CGRectMake(TEXT_OFFSET, TEXT_OFFSET,
            ceilf(self.frame.size.width - TEXT_OFFSET * 2),
            ceilf(self.frame.size.height - TEXT_OFFSET * 2));

    self.textView.frame = self.borderView.frame;

    self.textView.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);

    self.textView.transform = CGAffineTransformMakeScale(self.textScale.x, self.textScale.y);
    CGPoint origin = self.borderView.frame.origin;
    CGSize  size   = self.borderView.frame.size;

    // Removed unused properties by Luski Timurovich on 04/20/2017.
//    self.tlDotButton.center = CGPointMake(origin.x, origin.y);
//    self.trDotButton.center = CGPointMake(CGRectGetMaxX(self.borderView.frame), origin.y);
//    self.blDotButton.center = CGPointMake(origin.x, CGRectGetMaxY(self.borderView.frame));
    self.brDotButton.center = CGPointMake(CGRectGetMaxX(self.borderView.frame), CGRectGetMaxY(self.borderView.frame));
    
    // Removed unused properties by Luski Timurovich on 04/20/2017.
//    self.leftBottomCornerLargeDot.center = CGPointMake(origin.x, origin.y + size.height);


//    self.resizeImageView.center = CGPointMake(CGRectGetMaxX(self.borderView.frame), CGRectGetMaxY(self.borderView.frame));
//    self.rotationImageView.center = CGPointMake(CGRectGetMaxX(self.borderView.frame), origin.y);
//    self.removeImageView.center = CGPointMake(origin.x, origin.y);

    float contentViewsSide = CONTENT_VIEWS_SIDE + 10;

    self.contentResize.frame = CGRectMake(self.frame.size.width - contentViewsSide, self.frame.size.height - contentViewsSide, contentViewsSide, contentViewsSide);
    
    // Removed unused properties by Luski Timurovich on 04/20/2017.
//    self.contentRemove.frame = CGRectMake(0, 0, contentViewsSide, contentViewsSide);
//    self.contentRotate.frame = CGRectMake(self.frame.size.width - contentViewsSide, 0, contentViewsSide, contentViewsSide);

    [self.textView update];

    self.transform = transform;
}


#pragma mark -
#pragma mark UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewDidBeginEditing:)])
        [self.delegate textViewDidBeginEditing:self];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewDidEndEditing:)])
        [self.delegate textViewDidEndEditing:self];
}

- (BOOL)textViewShouldBeginEditing:(UITextView*)textView
{
    self.isEditing = YES;
    
    [self setNeedsLayout];
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView*)textView
{
    textView.text = [textView.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeText:)])
		[self.delegate textView:self shouldChangeText:textView.text];

    self.isEditing = NO;
  
    [self setNeedsLayout];
    
    return YES;
}

- (BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    self.textView.transform = CGAffineTransformIdentity;
    
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    isDeleting = (range.length >= 1 && text.length == 0);
    BOOL shouldChangeText = NO;
    NSString* newText = [self.textView.text stringByReplacingCharactersInRange: range
                                                                    withString: text];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeText:)])
        shouldChangeText = [self.delegate textView: self
                                  shouldChangeText: newText];
    
    
    
    return shouldChangeText;
}
- (void)textViewDidChange:(UITextView *)textView
{
    if (isDeleting)
    {
        [self performSelector:@selector(adjustTextViewFonts:) withObject:@YES];
    }
    else
    {
        [self performSelector:@selector(adjustTextViewFonts:) withObject:@NO];
    }
    
    [self.textView update];
    
    [self setNeedsDisplay];
}


#pragma mark -
#pragma mark Getters

- (BOOL)isActive
{
    return _active;
}

- (UIColor *)getTextColor
{
    return self.textView.textColor;
}

- (void)setFontSize:(CGFloat)fontSize
{
    _fontSize = fontSize;
    
    BOOL shouldChange = YES;
    UIFont* font = self.textView.font;
    
    self.textView.transform = CGAffineTransformIdentity;
    
    self.textView.font = [UIFont fontWithName: font.fontName
                                         size: fontSize];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeText:)])
    {
        shouldChange = [self.delegate textView: self shouldChangeText: self.textView.text];
    }
    
    [self setNeedsLayout];
    [self setNeedsDisplay];
    [self layoutIfNeeded];
}

#pragma mark -
#pragma mark Setters

- (void)setTextSize:(CGSize)textSize
{
    _textSize = textSize;
    
    CGRect frame = self.frame;
    
    frame.size.width = _textSize.width * self.textScale.x + TEXT_OFFSET * 2;
    frame.size.height = _textSize.height * self.textScale.y + TEXT_OFFSET * 2;
    
    self.frame = frame;
    
    [self setNeedsLayout];
}

- (void)setProportionalResize:(BOOL)proportionalResize
{
    _proportionalResize = proportionalResize;
    
    if (proportionalResize)
        self.textScale = CGPointMake(1.0, 1.0);
    
    [self updateResizing];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeText:)])
      [self.delegate textView: self
             shouldChangeText: self.textView.text];
}

- (void)setActive: (BOOL) active
{
    _active = active;
    
    if (active)
    {
        if (!self.textView.editable)
            [self.textView resignFirstResponder];
        
        self.textView.editable = YES;
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.tlDotButton.hidden = NO;
//        self.trDotButton.hidden = NO;
//        self.blDotButton.hidden = NO;
        self.brDotButton.hidden = NO;
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.rotationImageView.hidden   = NO;
//        self.removeImageView.hidden     = NO;
//        self.leftBottomCornerLargeDot.hidden = NO;
        
        self.pinchRecognaizer.enabled = YES;
        self.rotateRecognaizer.enabled  = YES;
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.removeRecognaizer.enabled  = YES;
//        self.leftBottomCornerResizeGestureRecognizer.enabled = YES;
        
        self.borderView.layer.borderColor = ACTIVE_BORDER_COLOR;

        if (self.delegate && [self.delegate respondsToSelector:@selector(textViewWillBecomeActive:)])
            [self.delegate textViewWillBecomeActive: self];
    }
    else
    {
        self.textView.editable = NO;
        
        [self.textView resignFirstResponder];
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.tlDotButton.hidden = YES;
//        self.trDotButton.hidden = YES;
//        self.blDotButton.hidden = YES;
        self.brDotButton.hidden = YES;
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.rotationImageView.hidden   = YES;
//        self.removeImageView.hidden     = YES;
//        self.leftBottomCornerLargeDot.hidden = YES;
        
        self.pinchRecognaizer.enabled = YES;
        self.rotateRecognaizer.enabled  = NO;
        
        // Removed unused properties by Luski Timurovich on 04/20/2017.
//        self.removeRecognaizer.enabled  = NO;
//        self.leftBottomCornerResizeGestureRecognizer.enabled = NO;
        
        self.borderView.layer.borderColor = INACTIVE_BORDER_COLOR;
    }
    
     [self updateResizing];
}

- (void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    
    self.textView.textColor = textColor;
    
    [self.textView update];
}

- (void)setTextColorLightness:(CGFloat)textColorLightness
{
    _textColorLightness = textColorLightness;
    
    [self updateTextColor];
}

- (void)setHighlightColor:(UIColor  *)highlightColor
{
    _highlightColor = highlightColor;
    
    [self updateHiglightColor];
}

- (void)setHighlightColorLightness: (CGFloat) highlightColorLightness
{
    _highlightColorLightness = highlightColorLightness;
    
    [self updateHiglightColor];
}

- (void)setTextOpacity: (CGFloat) textOpacity
{
    _textOpacity = textOpacity;
    
    self.textView.alpha = _textOpacity;
}

- (void)setTextFontWithName: (NSString *)fontName
{
    UIFont *font = [UIFont fontWithName:fontName size:self.textView.font.pointSize];
    self.textView.font = font;
    [self.textView update];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeText:)])
        [self.delegate textView: self
               shouldChangeText: self.textView.text];
}

- (UIFont *)textFont
{
    return self.textView.font;
}

- (NSLineBreakMode) textLineBreakMode
{
    return NSLineBreakByWordWrapping;
}

- (NSString *)getText
{
    return self.textView.text;
}

- (void)setReflection: (CGFloat) refl
{
    _reflection = refl;
    self.textView.reflectionScale = _reflection;
    [self.textView update];
}

- (void)setTextScale:(CGPoint)textScale
{
    _textScale = textScale;
    [self.textView update];
}

#pragma mark -
#pragma mark Gesture Recognizers Handling

- (void)activeView:(UITapGestureRecognizer *)tap
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewWillBecomeActive:)])
        [self.delegate textViewWillBecomeActive: self];
    
    [self setActive: YES];
}

- (void)newRotate:(UIRotationGestureRecognizer *)rotateGesture {
//    if (self.isEditing)
//        return;
//    
//    [self.textView resignFirstResponder];
    
    if([rotateGesture state] == UIGestureRecognizerStateEnded) {
        self.lastRotation = 0.0;
        return;
    }
    
    CGFloat rotation = 0.0 - (self.lastRotation - [rotateGesture rotation]);
    
    CGAffineTransform currentTransform = self.transform;
    CGAffineTransform newTransform = CGAffineTransformRotate(currentTransform, rotation);
    
    [self setTransform:newTransform];
    self.lastRotation = [rotateGesture rotation];
}

- (void)rotate:(UIPanGestureRecognizer *)pan
{
    if (self.isEditing)
        return;
    
    [self.textView resignFirstResponder];
    
    if (pan.state == UIGestureRecognizerStateEnded)
    {
        self.lastRotation = 0.0;
        return;
    }
    
    if (pan.state == UIGestureRecognizerStateBegan)
        self.lastPoint = [pan locationInView: self.superview];
    
    CGFloat newRotation = [self getRotationTo: [pan locationInView: self.superview]];
    CGFloat rotation = (self.lastRotation - newRotation);
    
    CGAffineTransform currentTransform = self.transform;
    CGAffineTransform newTransform = CGAffineTransformRotate(currentTransform,rotation);
    
    [self setTransform: newTransform];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeFrame:)])
    {
        if (![self.delegate textView: self
                  shouldChangeFrame: self.frame])
        {
            [self setTransform: currentTransform];
            
            pan.enabled = NO;
            pan.enabled = YES;
            
            self.lastRotation = 0.0;
            return;
        }
        else
        {
            self.lastRotation = newRotation;
        }
    }
}

// Changed by Luski Timurovich on 04/20/2017 to fix resize error on Pan Gesture.
- (void)resize:(UIPanGestureRecognizer *)pan {
    if (self.isEditing)
        return;
    
    [self.textView resignFirstResponder];
    [self.textView setNeedsDisplay];
    
    
    if (pan.state == UIGestureRecognizerStateBegan)
    {
        CGAffineTransform transform = self.transform;
        self.transform = CGAffineTransformIdentity;
        self.firstFrame = self.bounds;
        self.firstPoint = [pan translationInView: self];
        self.transform = transform;
        self.firstFont  = self.textView.font;
        return;
    }
    
    CGFloat deltaX = ceilf([pan translationInView: self].x - self.firstPoint.x);
    CGFloat deltaY = ceilf([pan translationInView: self].y - self.firstPoint.y);
    
    if (MIN_TEXTVIEW_SIZE.height < ceilf(self.firstFrame.size.height + deltaY) && MIN_TEXTVIEW_SIZE.width < ceilf(self.firstFrame.size.width + deltaX))
    {
        CGRect newFrame = CGRectMake(ceilf(self.firstFrame.origin.x),
                                     ceilf(self.firstFrame.origin.y),
                                     ceilf(self.firstFrame.size.width + deltaX),
                                     ceilf(self.firstFrame.size.height + deltaY));
        
//        CGFloat xScale = (newFrame.size.width - TEXT_OFFSET * 2) / self.textSize.width;
//        CGFloat yScale = (newFrame.size.height - TEXT_OFFSET * 2) / self.textSize.height;
//        
//        if (xScale > MAX_SCALE || xScale < MIN_SCALE)
//            return;
//        
//        if (yScale > MAX_SCALE || yScale < MIN_SCALE)
//            return;
//        
        if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeFrame:)]) {
            if ([self.delegate textView: self shouldChangeFrame: newFrame]) {
                CGAffineTransform transform = self.transform;
                self.transform = CGAffineTransformIdentity;
                self.bounds = newFrame;
                self.transform = transform;
                
                if (deltaX > 0.f && deltaY > 0.f)
                {
                    [self performSelector:@selector(adjustTextViewFonts:) withObject:@YES];
                }
                else
                {
                    [self performSelector:@selector(adjustTextViewFonts:) withObject:@NO];
                }
            }
        }
    }
}
// Commented by Luski Timurovich on 04/20/2017 to fix resize error on Pan Gesture.
/*
- (void)resize:(UIPanGestureRecognizer *)pan
{
    if (self.isEditing)
        return;
    
    [self.textView resignFirstResponder];
    [self.textView setNeedsDisplay];
    
    // static CGRect beginTextViewFrame;
    
    if (pan.state == UIGestureRecognizerStateBegan)
    {
        self.firstPoint = [pan translationInView: nil];
        CGAffineTransform transform = self.transform;
        self.transform = CGAffineTransformIdentity;
        self.firstFrame = self.frame;
        self.transform = transform;
        self.firstFont  = self.textView.font;
    }
    
    CGFloat deltaX = ceilf([pan translationInView: self].x - self.firstPoint.x);
    CGFloat deltaY = ceilf([pan translationInView: self].y - self.firstPoint.y);
    
    CGFloat newWidth = 0;
    CGFloat newHeight = 0;
    CGFloat newX = 0;
    CGFloat newY = 0;
    
    if (pan == self.resizeRecognaizer) {
        newWidth = deltaX;
        newHeight = deltaY;
    }
    else
        if (pan == self.bottomResizeGestureRecognizer)
            newHeight = deltaY;
        else
            if (pan == self.leftResizeGestureRecognizer) {
                newX = deltaX;
                newWidth = -deltaX;
            }
            else
                if (pan == self.leftBottomCornerResizeGestureRecognizer) {
                    newX = deltaX;
                    newWidth = -deltaX;
                    newHeight = deltaY;
                }
                else
                    if (pan == self.topResizeGestureRecognizer) {
                        newY = deltaY;
                        newHeight = -deltaY;
                    }
                    else
                        if (pan == self.rightResizeGestureRecognizer)
                            newWidth = deltaX;
    
    if (MIN_TEXTVIEW_SIZE.height < ceilf(self.firstFrame.size.height + newHeight) && MIN_TEXTVIEW_SIZE.width < ceilf(self.firstFrame.size.width + newWidth))
    {
        CGRect newFrame = CGRectMake(ceilf(self.firstFrame.origin.x + newX),
                                     ceilf(self.firstFrame.origin.y + newY),
                                     ceilf(self.firstFrame.size.width + newWidth),
                                     ceilf(self.firstFrame.size.height + newHeight));
        
        CGFloat xScale = (newFrame.size.width - TEXT_OFFSET * 2) / self.textSize.width;
        CGFloat yScale = (newFrame.size.height - TEXT_OFFSET * 2) / self.textSize.height;
        
        if (xScale > MAX_SCALE || xScale < MIN_SCALE)
            return;
        
        if (yScale > MAX_SCALE || yScale < MIN_SCALE) 
            return;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeFrame:)])
            if ([self.delegate textView: self
                     shouldChangeFrame: newFrame])
            {
                self.frame = newFrame;
                
                if (deltaX > 0.f && deltaY > 0.f)
                {
                    [self performSelector:@selector(adjustTextViewFonts:) withObject:@YES];
                }
                else
                {
                    [self performSelector:@selector(adjustTextViewFonts:) withObject:@NO];
                }
            }
    }
}
*/

// Added by Luski Timurovich on 04/20/2017 to fix resize error on Pan Gesture.
- (void)updateViewBounds:(CGFloat)scale withBound:(CGRect)newBound {
    CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
    CGRect rcTemp = CGRectApplyAffineTransform(newBound, transform);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeFrame:)]) {
        if ([self.delegate textView: self shouldChangeFrame:rcTemp]) {
            CGAffineTransform transform = self.transform;
            self.transform = CGAffineTransformIdentity;
            
            BOOL isPlus = (self.bounds.size.width < rcTemp.size.width) && (self.bounds.size.height < rcTemp.size.height);
            self.bounds = rcTemp;
            self.transform = transform;
            
            [self performSelector:@selector(adjustTextViewFonts:) withObject:[NSNumber numberWithBool:isPlus]];
        }
    }
}

// Added by Luski Timurovich on 04/20/2017 to fix resize error on Pan Gesture.
- (void)newResize:(UIPinchGestureRecognizer *)pinch
{
//    if (self.isEditing)
//        return;
//    
//    [self.textView resignFirstResponder];
//    [self.textView setNeedsDisplay];
    
    if (pinch.state == UIGestureRecognizerStateBegan) {
        [self updateViewBounds:pinch.scale withBound:self.bounds];
    } else if (pinch.state == UIGestureRecognizerStateChanged) {
        [self updateViewBounds:pinch.scale withBound:self.bounds];
    }
    
    pinch.scale = 1.0;
}

- (void)resizePinch:(UIPinchGestureRecognizer *)pinch
{
    if (self.proportionalResize)
        return;
    
    CGAffineTransform transform = self.transform;
    self.transform = CGAffineTransformIdentity;
    if (pinch.state == UIGestureRecognizerStateBegan)
    {
        self.firstCenter = self.center;
        self.firstFrame  = self.frame;
        self.firstFont   = self.textView.font;
    }
    
    CGSize newSize = CGSizeMake(self.firstFrame.size.width * pinch.scale, self.firstFrame.size.height * pinch.scale);
    
    CGFloat deltaX = (NSInteger)newSize.width - (NSInteger)self.firstFrame.size.width;
    CGFloat deltaY = (NSInteger)newSize.height - (NSInteger)self.firstFrame.size.height;
    
    
    
    if (MIN_TEXTVIEW_SIZE.height < ceilf(self.firstFrame.size.height + deltaY) && MIN_TEXTVIEW_SIZE.width < ceilf(self.firstFrame.size.width + deltaX))
    {
        CGRect newFrame = CGRectMake(ceilf(self.firstFrame.origin.x - deltaX / 2),
                                     ceilf(self.firstFrame.origin.y - deltaY / 2),
                                     ceilf(self.firstFrame.size.width + deltaX),
                                     ceilf(self.firstFrame.size.height + deltaY));
        
        CGFloat xScale = (newFrame.size.width - TEXT_OFFSET * 2) / self.textSize.width;
        CGFloat yScale = (newFrame.size.height - TEXT_OFFSET * 2) / self.textSize.height;
        
        if (xScale > MAX_SCALE || xScale < MIN_SCALE)
            return;
        
        if (yScale > MAX_SCALE || yScale < MIN_SCALE)
            return;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeFrame:)])
            if ([self.delegate textView: self
                     shouldChangeFrame: newFrame])
            {
                self.frame = newFrame;
                
                self.textScale = CGPointMake(xScale, yScale);
            }
    }
    
    self.transform = transform;
}

- (void)remove:(UITapGestureRecognizer *)tap
{
    if (self.isEditing)
        return;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewRemovePressed:)])
        [self.delegate textViewRemovePressed: self];
}

- (void)doubleTapHandler:(UITapGestureRecognizer *)tap
{
    // do nothing (for catch double tap)
}

- (void)updateResizing
{
    BOOL available = self.active && !self.proportionalResize;
    
    // Removed unused properties by Luski Timurovich on 04/20/2017.
//    self.resizeImageView.hidden          = !available;
    /*
    self.leftDotButton.hidden            = !available;
    self.topDotButton.hidden             = !available;
    self.rightDotButton.hidden           = !available;
    self.bottomDotButton.hidden          = !available;
    self.leftBottomCornerLargeDot.hidden = !available;
    */
    //available = NO;
    
//    self.resizeRecognaizer.enabled                       = available;
    self.pinchRecognaizer.enabled                        = available;
    self.rotateRecognaizer.enabled = available;
    
    // Removed unused properties by Luski Timurovich on 04/20/2017.
//    self.topResizeGestureRecognizer.enabled              = available;
//    self.bottomResizeGestureRecognizer.enabled           = available;
//    self.leftResizeGestureRecognizer.enabled             = available;
//    self.rightResizeGestureRecognizer.enabled            = available;
//    self.leftBottomCornerResizeGestureRecognizer.enabled = available;
//    self.tlResizeGestureRecognizer.enabled              = available;
//    self.trResizeGestureRecognizer.enabled           = available;
//    self.blResizeGestureRecognizer.enabled             = available;
    self.brResizeGestureRecognizer.enabled            = available;
}




#pragma mark -
#pragma mark Rotate calculations

- (CGFloat) pointPairToBearing: (CGPoint) startingPoint secondPoint: (CGPoint) endingPoint
{
    CGPoint originPoint = CGPointMake(endingPoint.x - startingPoint.x, endingPoint.y - startingPoint.y); // get origin point to origin by subtracting end from start
    float bearingRadians = atan2f(originPoint.y, originPoint.x); // get bearing in radians
    
    return bearingRadians;
}

- (CGFloat) getRotationTo: (CGPoint) toPoint
{
    CGFloat rotation = [self pointPairToBearing: self.center secondPoint: self.lastPoint] - [self pointPairToBearing: self.center secondPoint: toPoint];
    
    return rotation;
}

#pragma mark -
#pragma mark Public methods

- (void)showKeyboard
{
    [self.textView becomeFirstResponder];
}
- (void)dismissKeyboard
{
    [self.textView resignFirstResponder];
}

#pragma mark -
#pragma mark Private methods

- (UIEdgeInsets) textEdgesInsets {
    return UIEdgeInsetsMake(8, 8, 8, 8);
}

- (void)updateTextColor
{
    CGFloat hue;
    CGFloat saturetuion;
    CGFloat brightness;
    
    [_textColor getHue: &hue
            saturation: &saturetuion
            brightness: &brightness
                 alpha: NULL];
    
    brightness = self.textColorLightness;
    
    self.textView.textColor = [UIColor colorWithHue: hue
                                         saturation: saturetuion
                                         brightness: brightness
                                              alpha: 1];
    
    [self.textView update];
}

- (void)updateHiglightColor
{
    if (self.highlightColor == 0)
        self.textView.textBackgroundColor = nil;
    else
    {
        UIColor* highlightColor = self.highlightColor;
        
        CGFloat hue;
        CGFloat saturetuion;
        CGFloat brightness;
        
        [highlightColor getHue: &hue
                    saturation: &saturetuion
                    brightness: &brightness
                         alpha: NULL];
        
        brightness = self.highlightColorLightness;
        
        self.textView.textBackgroundColor = highlightColor;
    }
    
    [self.textView update];
}

#pragma mark -
#pragma mark Drawning optimization methods

- (void)prepareForPrint
{
    self.textView.dynamic = NO;
}

- (void)prepareForDrawning
{
    self.textView.dynamic = NO;
}

- (UIImage *)generateTextImage
{
    UIGraphicsBeginImageContextWithOptions(self.textView.frame.size, NO, 0.0f);
    
    [self.textView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *renderImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return renderImage;
}

- (CALayer *)layerWithImageRepresentationForSize:(CGSize)videoSize scale:(CGFloat)scale
{
    UIView *superView = [self superview];
    NSArray* sortedSubviews = [[superView subviews] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return (NSComparisonResult) (((UIView*)obj1).tag == 510);
    }];
    UIView* imageView = [sortedSubviews lastObject];
    imageView = superView;
    
    BOOL isActive = self.active;
    self.active = NO;
    
    NSData *tempArchiveView = [NSKeyedArchiver archivedDataWithRootObject:self];
    TVTextView *renderTextView = [NSKeyedUnarchiver unarchiveObjectWithData:tempArchiveView];
    renderTextView.center = CGPointMake(renderTextView.center.x - imageView.frame.origin.x, renderTextView.center.y - imageView.frame.origin.y);
    
    UIView *renderView = [[UIView alloc] initWithFrame:imageView.bounds];
    renderView.backgroundColor = [UIColor clearColor];
    [renderView addSubview:renderTextView];
    
    CGFloat screenScale = [UIScreen mainScreen].scale;
    UIGraphicsBeginImageContextWithOptions(renderView.frame.size, NO, screenScale);
    
    [renderView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *renderImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.active = isActive;
    
    renderImage = [renderImage resizedImageToSize:videoSize];
    
    // paste to CALayer
    CALayer* aLayer = [CALayer layer];
    CGRect startFrame = CGRectMake(0.0, 0.0, videoSize.width, videoSize.height);
    aLayer.contents = (id)renderImage.CGImage;
    aLayer.frame = startFrame;

    return aLayer;
}

- (CALayer *)layerWithImageForSize:(CGSize)videoSize scale:(CGFloat)scale
{
    // preserve original metrics
    CGFloat screenScale = [UIScreen mainScreen].scale;
    float xPos = (self.frame.origin.x - self.parentFrame.origin.x + self.borderView.frame.origin.x + self.textView.textContainerInset.left + self.textEdgesInsets.left / 2.0f) * scale;
    float yPos = (self.frame.origin.y - self.parentFrame.origin.y + self.borderView.frame.origin.y + self.textView.textContainerInset.top) * scale;
    
    // capture
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(videoSize.width / screenScale, videoSize.height / screenScale), NO, screenScale);
    
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGRect fullTextViewRect = self.textView.frame;
    CGPoint origin = self.textView.frame.origin;
    CGSize size = self.textView.frame.size;
    if (self.textView.reflectionScale > 0) {
        size.height *= 2;
        size.height += self.textView.reflectionGap;
        yPos -= size.height / 2.0f;
    }

    fullTextViewRect.size = size;
    CGFloat side = MAX(size.width, size.height);
    size = CGSizeMake(side, side);
    CGRect contextRect = CGRectMake(origin.x, origin.y, size.width, size.height);
    
    // move to center
    CGFloat sx = [self xscaleForTransform:self.textView.transform];
    CGFloat sy = [self yscaleForTransform:self.textView.transform];
    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity, sx, sy);

    CGFloat deltaToCenterX = fabs(CGRectGetMidX(contextRect) - CGRectGetMidX(fullTextViewRect));
    CGFloat deltaToCenterY = fabs(CGRectGetMidY(contextRect) - CGRectGetMidY(fullTextViewRect));
    deltaToCenterX /= scale;
    deltaToCenterY /= scale;
    transform = CGAffineTransformTranslate(transform, deltaToCenterX, deltaToCenterY);

    CGContextConcatCTM(context, transform);

    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    UIFont *font = [UIFont fontWithName:self.textFont.fontName size:self.textView.font.pointSize * scale];
    [attributes setObject:font forKey:NSFontAttributeName];
    [attributes setObject:self.textColor forKey:NSForegroundColorAttributeName];
    [[self getText] drawAtPoint:CGPointMake(xPos, yPos) withAttributes:attributes];
    
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSLog(@"%f, %f", resultingImage.size.width, resultingImage.size.height);
    
    // paste to CALayer
    CALayer* aLayer = [CALayer layer];
    aLayer.contents = (id)resultingImage.CGImage;
    aLayer.frame = CGRectMake(0.0, 0.0, videoSize.width, videoSize.height);
    
    return aLayer;
}

- (UIImage *)rotateImage:(UIImage*)src byRadian:(CGFloat)radian
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, src.size.width, src.size.height)] autorelease];
    CGAffineTransform t = CGAffineTransformMakeRotation(radian);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;

    // Create the bitmap context

    UIGraphicsBeginImageContextWithOptions(rotatedSize, NO, [UIScreen mainScreen].scale);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();

    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);

    //   // Rotate the image context
    CGContextRotateCTM(bitmap, radian);

    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-src.size.width / 2, -src.size.height / 2, src.size.width, src.size.height), [src CGImage]);

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)renderImageForSize:(CGSize)imageSize
{
    UIView *superView = [self superview];
    NSArray* sortedSubviews = [[superView subviews] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return (NSComparisonResult) (((UIView*)obj1).tag == 510);
    }];
    UIView* imageView = [sortedSubviews lastObject];
    imageView = superView;
    
    BOOL isActive = self.active;
    self.active = NO;
    
    NSData *tempArchiveView = [NSKeyedArchiver archivedDataWithRootObject:self];
    TVTextView *renderTextView = [NSKeyedUnarchiver unarchiveObjectWithData:tempArchiveView];
    renderTextView.center = CGPointMake(renderTextView.center.x - imageView.frame.origin.x, renderTextView.center.y - imageView.frame.origin.y);
    
    UIView *renderView = [[UIView alloc] initWithFrame:imageView.bounds];
    renderView.backgroundColor = [UIColor clearColor];
    [renderView addSubview:renderTextView];
    
    CGFloat screenScale = [UIScreen mainScreen].scale;
    UIGraphicsBeginImageContextWithOptions(renderView.frame.size, NO, screenScale);
    
    [renderView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *renderImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.active = isActive;
    
    renderImage = [renderImage resizedImageToSize:imageSize];
    
    return renderImage;
}

- (CGFloat) xscaleForTransform:(CGAffineTransform)t
{
    return sqrt(t.a * t.a + t.c * t.c);
}

- (CGFloat) yscaleForTransform:(CGAffineTransform)t
{
    return sqrt(t.b * t.b + t.d * t.d);
}

#pragma mark - Resize Functions

- (BOOL)isBeyondSize:(CGSize)size
{
    if (IS_IOS_7)
    {
        CGFloat ost = _textView.textContainerInset.top + _textView.textContainerInset.bottom;
        
        return size.height + ost > self.textView.frame.size.height;
    }
    else
    {
        return self.textView.contentSize.height > self.textView.frame.size.height;
    }
}

- (BOOL)isBeyondSize:(CGSize)size textView:(UITextView *)textView scale:(CGFloat)scale
{
    if (IS_IOS_7)
    {
        CGFloat ost = textView.textContainerInset.top + textView.textContainerInset.bottom;
        
        return size.height + ost > self.textView.frame.size.height * scale;
    }
    else
    {
        return textView.contentSize.height > textView.frame.size.height * scale;
    }
}

- (CGSize)textSizeWithFont:(CGFloat)font text:(NSString *)string
{
    NSString *text = string ? string : self.textView.text;
    
    CGFloat pO = self.textView.textContainer.lineFragmentPadding * 2;
    CGFloat cW = self.textView.frame.size.width - pO;
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake(cW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [self.textFont fontWithSize:font]} context:nil];
    CGSize tH = rect.size;
    return  tH;
}

- (CGSize)textSizeWithFont:(CGFloat)font text:(NSString *)string scale:(CGFloat)scale
{
    NSString *text = string ? string : self.textView.text;
    
    CGFloat pO = self.textView.textContainer.lineFragmentPadding * 2;
    CGFloat cW = (self.textView.frame.size.width - pO) * scale;
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake(cW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [self.textFont fontWithSize:font]} context:nil];
    CGSize tH = rect.size;
    return  tH;
}

- (CGFloat)fontScaledSize:(CGFloat)scale WithTextView:(UITextView *)textView
{
    CGFloat cFont = textView.font.pointSize;
    CGSize  tSize = IS_IOS_7 ? [self textSizeWithFont:cFont text:nil scale:scale] : CGSizeZero;
    do
    {
        if (IS_IOS_7)
        {
            tSize = [self textSizeWithFont:++cFont text:nil scale:scale];
        }
        else
        {
            [self.textView setFont:[self.textFont fontWithSize:++cFont]];
        }
    }
    while (![self isBeyondSize:tSize textView:textView scale:scale] && cFont < MAX_FONT_SIZE);
    
    cFont = (cFont < MAX_FONT_SIZE) ? cFont : MIN_FONT_SIZE;
    return cFont - 1;
}

- (void)adjustTextViewFonts:(NSNumber *)isPlus
{
    CGFloat cFont = self.textView.font.pointSize;
    CGSize  tSize = IS_IOS_7 ? [self textSizeWithFont:cFont text:nil] : CGSizeZero;
    if ([isPlus boolValue])
    {
        do
        {
            if (IS_IOS_7)
            {
                tSize = [self textSizeWithFont:++cFont text:nil];
            }
            else
            {
                [self.textView setFont:[self.textFont fontWithSize:++cFont]];
            }
        }
        while (![self isBeyondSize:tSize] && cFont < MAX_FONT_SIZE);
        
        cFont = (cFont < MAX_FONT_SIZE) ? cFont : MIN_FONT_SIZE;
        [self.textView setFont:[self.textFont fontWithSize:--cFont]];
    }
    else
    {
        while ([self isBeyondSize:tSize] && cFont > 0)
        {
            if (IS_IOS_7)
            {
                tSize = [self textSizeWithFont:--cFont text:nil];
            }
            else
            {
                [self.textView setFont:[self.textFont fontWithSize:--cFont]];
            }
        }
        
        cFont = (cFont < MAX_FONT_SIZE) ? cFont : MIN_FONT_SIZE;
        [self.textView setFont:[self.textFont fontWithSize:cFont]];
    }
}

#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

@end

