//
//  AppDelegate.swift
//  Anarko
//
//  Created by Hua Wan on 13/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import MagicalRecord
import SVProgressHUD
import UserNotifications


let kAnarkoStoreName = "AKDatabaseModel.sqlite"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var isLoaded = false
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        application.isStatusBarHidden = true

        self.copyDefaultStoreIfNecessary()
        
        MagicalRecord.setLoggingLevel(.verbose)
        MagicalRecord.setupCoreDataStack(withStoreNamed: kAnarkoStoreName)
        
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setBackgroundColor(UIColor.clear)
        SVProgressHUD.setForegroundColor(UIColor.white)
        
        AKMaskManager.sharedInstance.initDownloadTasks()
        
        self.initializeAudioSession()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationAppWillEnterBackground), object: nil)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationAppWillEnterForeground), object: nil)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
                
        guard let _ = AKGlobal.getClientID() else {
            
            self.presentLoginViewController(animated: false)
            return
        }
        
        AKAppManager.sharedInstance.myUser = AKGlobal.getUserData()
        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: - 
    
    func presentLoginViewController(animated: Bool){
        
        let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! AKHomeViewController
        
        let loginNavVC = UINavigationController(rootViewController: loginVC)
        loginNavVC.isNavigationBarHidden = true
        self.window?.makeKeyAndVisible()
        let delayInSeconds: Double = 0.1
        let popTime: DispatchTime = DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: popTime, execute: {
            
            self.window?.rootViewController?.present(loginNavVC, animated: animated, completion: nil)
        })
    }
    
    func generateRefreshToken() {
        
        
        AKAPIManager.sharedManager.generateRefreshToken(params: [:]) { (response) in
            
            switch response.result {
            case .success:
                
                print("Validation Successful")
                
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let user = AKUser(data: json as! NSDictionary)
                    
                    AKAppManager.sharedInstance.myUser = user
                    
                    
                }
            case .failure: break
            }

        }
    }

    
    func copyDefaultStoreIfNecessary() -> Void {
        let fileManager = FileManager.default
        let storeURL = NSPersistentStore.mr_url(forStoreName: kAnarkoStoreName)
        if fileManager.fileExists(atPath: (storeURL?.path)!) {
            let filename = kAnarkoStoreName as NSString
            let defaultStorePath = Bundle.main.path(forResource: filename.deletingPathExtension, ofType: filename.pathExtension)
            
            if defaultStorePath != nil {
                do {
                    try fileManager.copyItem(atPath: defaultStorePath!, toPath: (storeURL?.path)!)
                } catch {
                    print("Failed to install default recipe store")
                }
            }
        }
    }
    
    // MARK: - Audio Session
    
    func initializeAudioSession() {
        
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
        } catch let error {
            
            print("category error --- \(error)")
        }
        
        do {
            
            try session.overrideOutputAudioPort(.speaker)
            
            
        } catch let error{
            
            print("port error --- \(error)")
        }
        
        do {
            try session.setActive(true)
        } catch let error {
            
            print("active error --- \(error)")

        }
        
    }
    
    func registerForPushNotifications() {
        let application = UIApplication.shared
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = String(format: "%@", deviceToken as CVarArg).trimmingCharacters(in: CharacterSet(charactersIn: "<>")).replacingOccurrences(of: " ", with: "")
        print("APNs token retrieved: \(token)")
        UserDefaults.standard.set(token, forKey: "deviceToken")
        UserDefaults.standard.synchronize()
        
        if token != "" {
            //Upload to server
            let paramDic = ["deviceToken" : token]
            AKAPIManager.sharedManager.updateDeviceToken(params: paramDic as [String : AnyObject], completionHandler: { (response) in
                if response != nil {
                    
                }
            })
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo

        print(userInfo)
        
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}
