//
//  AKVideoCamera.h
//  Anarko
//
//  Created by Hua Wan on 23/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AKAbstractCamera.h"

@class AKVideoCamera;

@protocol AKVideoCameraDelegate <NSObject>

#ifdef __cplusplus
// delegate method for processing image frames
- (void)processImage:(cv::Mat&)image;
- (void)finishedRecord:(NSURL *)outputURL;
#endif

@end

@interface AKVideoCamera : AKAbstractCamera <AVCaptureVideoDataOutputSampleBufferDelegate>
{
    AVCaptureVideoDataOutput *videoDataOutput;
    
    dispatch_queue_t videoDataOutputQueue;
    CALayer *customPreviewLayer;
    
    CMTime lastSampleTime;
}

@property (nonatomic, weak) id<AKVideoCameraDelegate> delegate;
@property (nonatomic, assign) BOOL grayscaleMode;

@property (nonatomic, assign) BOOL recordVideo;
@property (nonatomic, assign) BOOL rotateVideo;
@property (nonatomic, strong) AVAssetWriterInput* recordAssetWriterInput;
@property (nonatomic, strong) AVAssetWriterInputPixelBufferAdaptor* recordPixelBufferAdaptor;
@property (nonatomic, strong) AVAssetWriter* recordAssetWriter;
@property (nonatomic, strong) NSString* gravityMode;

- (void)adjustLayoutToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
- (void)layoutPreviewLayer;
- (void)saveVideo;
- (NSURL *)videoFileURL;
- (NSString *)videoFileString;

- (void)startRecord;
- (void)stopRecord;

@end
