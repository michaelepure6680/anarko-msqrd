//
//  AKFaceFeature.m
//  Anarko
//
//  Created by Hua Wan on 23/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import "AKFaceFeature.h"

#import <Foundation/Foundation.h>

#import <opencv2/videoio/cap_ios.h>
#import <opencv2/objdetect/objdetect.hpp>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/imgproc.hpp>
#import <opencv2/imgcodecs/ios.h>

using namespace cv;

NSString* const faceCascadeFilename = @"haarcascade_frontalface_alt2";
NSString* const eyeFairCascadeFilename = @"haarcascade_mcs_eyepair_big";
NSString* const eyesTreeCascadeFilename = @"haarcascade_eye_tree_eyeglasses";
NSString* const mouthCascadeFilename = @"haarcascade_mcs_mouth";
NSString* const frontalCascadeFilename = @"lbpcascade_frontalface";

CascadeClassifier faceCascade;
CascadeClassifier eyeFairCascade;
CascadeClassifier eyesTreeCascade;
CascadeClassifier mouthCascade;

int _width;
int _height;

@implementation AKFaceFeature

@synthesize leftEyeCenter, rightEyeCenter, mouthCenter, chinCenter;
@synthesize angleBetweenEyes, widthBetweenEyes;
@synthesize leftEyeAngle,rightEyeAngle, mouthAngle;
@synthesize isMom;

- (instancetype)init
{
    self = [super init];
    if (self) {
        if (faceCascade.empty()) {
            NSString* cascadePath = [[NSBundle mainBundle] pathForResource:faceCascadeFilename ofType:@"xml"];
            faceCascade.load([cascadePath UTF8String]);
        }
        if (eyeFairCascade.empty()) {
            NSString* cascadePath = cascadePath = [[NSBundle mainBundle] pathForResource:eyeFairCascadeFilename ofType:@"xml"];
            eyeFairCascade.load([cascadePath UTF8String]);
        }
        if (eyesTreeCascade.empty()) {
            NSString* cascadePath = cascadePath = [[NSBundle mainBundle] pathForResource:eyesTreeCascadeFilename ofType:@"xml"];
            eyesTreeCascade.load([cascadePath UTF8String]);
        }
        if (mouthCascade.empty()) {
            NSString* cascadePath = cascadePath = [[NSBundle mainBundle] pathForResource:mouthCascadeFilename ofType:@"xml"];
            mouthCascade.load([cascadePath UTF8String]);
        }
    }
    return self;
}

+ (AKFaceFeature *)faceFeatureFromCIFaceFeature:(CIFaceFeature *)faceFeature
{
    AKFaceFeature *feature = [[AKFaceFeature alloc] init];
    
    feature.leftEyeCenter = CGPointMake(faceFeature.leftEyePosition.x - faceFeature.bounds.origin.x, faceFeature.leftEyePosition.y - faceFeature.bounds.origin.y);
    feature.rightEyeCenter = CGPointMake(faceFeature.rightEyePosition.x - faceFeature.bounds.origin.x, faceFeature.rightEyePosition.y - faceFeature.bounds.origin.y);
    CGPoint centerPosition = CGPointMake(faceFeature.bounds.size.width / 2.0f, faceFeature.bounds.size.height / 2.0f);
    if (faceFeature.hasLeftEyePosition)
    {
        CGPoint leftEye = faceFeature.leftEyePosition;
        
        CGFloat angle = [feature pointPairToBearingRadians:centerPosition secondPoint:leftEye];
        feature.leftEyeAngle = angle;
    }
    if (faceFeature.hasRightEyePosition)
    {
        CGPoint rightEye = faceFeature.rightEyePosition;
        
        CGFloat angle = [feature pointPairToBearingRadians:centerPosition secondPoint:rightEye];
        feature.rightEyeAngle = angle;
    }
    if (faceFeature.hasLeftEyePosition && faceFeature.hasRightEyePosition)
    {
        // Change the positions for 90 degree rotation
        CGPoint leftEyePosition = CGPointMake(faceFeature.leftEyePosition.y, faceFeature.leftEyePosition.x);
        CGPoint rightEyePosition = CGPointMake(faceFeature.rightEyePosition.y, faceFeature.rightEyePosition.x);
        
        CGFloat angle = [feature pointPairToBearingRadians:leftEyePosition secondPoint:rightEyePosition];
        
        feature.widthBetweenEyes = faceFeature.bounds.size.width;
        feature.angleBetweenEyes = angle;
        feature.distanceBetweenEyes = [feature getDistanceBetweenPoint:leftEyePosition secondPoint:rightEyePosition];
    }
    
    return feature;
}

+ (AKFaceFeature *)faceFeatureFromDictionary:(NSDictionary *)faceFeature
{
    AKFaceFeature *feature = [[AKFaceFeature alloc] init];
    
    CGFloat width = [faceFeature[@"Width"] floatValue];
    CGFloat height = [faceFeature[@"Height"] floatValue];
    CGPoint centerPosition = CGPointMake(width / 2.0f, height / 2.0f);
    feature.faceBounds = CGRectMake([faceFeature[@"X"] floatValue], [faceFeature[@"Y"] floatValue], [faceFeature[@"BoundsWidth"] floatValue], [faceFeature[@"BoundsHeight"] floatValue]);
    if (YES)
    {
        CGPoint leftEye = CGPointMake([faceFeature[@"LeftEyeX"] floatValue], [faceFeature[@"LeftEyeY"] floatValue]);
        feature.leftEyeCenter = leftEye;
        
        CGFloat angle = [feature pointPairToBearingRadians:centerPosition secondPoint:leftEye];
        feature.leftEyeAngle = angle;
    }
    if (YES)
    {
        CGPoint rightEye = CGPointMake([faceFeature[@"RightEyeX"] floatValue], [faceFeature[@"RightEyeY"] floatValue]);
        feature.rightEyeCenter = rightEye;
        
        CGFloat angle = [feature pointPairToBearingRadians:centerPosition secondPoint:rightEye];
        feature.rightEyeAngle = angle;
    }
    if (YES)
    {
        // Change the positions for 90 degree rotation
        CGPoint leftEyePosition = CGPointMake([faceFeature[@"LeftEyeX"] floatValue], [faceFeature[@"LeftEyeY"] floatValue]);
        CGPoint rightEyePosition = CGPointMake([faceFeature[@"RightEyeX"] floatValue], [faceFeature[@"RightEyeY"] floatValue]);
        
        CGFloat angle = [feature pointPairToBearingRadians:leftEyePosition secondPoint:rightEyePosition];
        
        feature.widthBetweenEyes = width;
        feature.angleBetweenEyes = angle;
        feature.distanceBetweenEyes = [feature getDistanceBetweenPoint:leftEyePosition secondPoint:rightEyePosition];
    }
    
    return feature;
}

+ (AKFaceFeature *)faceFeatureFromImageBuffer:(CVImageBufferRef)imageBuffer
{
    AKFaceFeature *feature = [[AKFaceFeature alloc] init];
    
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    void *bufferAddress;
    size_t width;
    size_t height;
    size_t bytesPerRow;
    
    int format_opencv;
    
    OSType format = CVPixelBufferGetPixelFormatType(imageBuffer);
    if (format == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) {
        
        format_opencv = CV_8UC1;
        
        bufferAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
        width = CVPixelBufferGetWidthOfPlane(imageBuffer, 0);
        height = CVPixelBufferGetHeightOfPlane(imageBuffer, 0);
        bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0);
        
    } else { // expect kCVPixelFormatType_32BGRA
        
        format_opencv = CV_8UC4;
        
        bufferAddress = CVPixelBufferGetBaseAddress(imageBuffer);
        width = CVPixelBufferGetWidth(imageBuffer);
        height = CVPixelBufferGetHeight(imageBuffer);
        bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
        
    }
    
    cv::Mat image((int)height, (int)width, format_opencv, bufferAddress, bytesPerRow);
    
    Mat grayscaleFrame;
    cvtColor(image, grayscaleFrame, CV_BGR2GRAY);
    equalizeHist(grayscaleFrame, grayscaleFrame);
    
    std::vector<cv::Rect> faces;
    faceCascade.detectMultiScale(grayscaleFrame, faces, 1.4, 4, CV_HAAR_FIND_BIGGEST_OBJECT, cv::Size(60, 60));
    
    if (faces.size() == 0)
        return nil;
    
    if (faces.size() >= 2)
    {
        NSLog(@"");
    }
    
    feature.faceBounds = CGRectMake(faces[0].x, faces[0].y, faces[0].width, faces[0].height);
    for (int i = 0; i < faces.size(); i++)
        //for (int i = 0; i < 1; i++)
    {
        cv::Point pt1(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
        cv::Point pt2(faces[i].x, faces[i].y);
        
        Mat faceROI = grayscaleFrame( faces[i] );
        std::vector<cv::Rect> eyes;
        eyesTreeCascade.detectMultiScale(faceROI, eyes, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, cv::Size(30, 30));
        if (eyes.size() < 2)
            return nil;
        
        NSLog(@"detect eyes");
        feature.leftEyeCenter = CGPointMake(eyes[0].x + eyes[0].width / 2, eyes[0].y + eyes[0].height / 2);
        feature.rightEyeCenter = CGPointMake(eyes[1].x + eyes[1].width / 2, eyes[1].y + eyes[1].height / 2);
        
        cv::Point origin(0, faces[i].height / 2);
        Mat mouthArea = faceROI(cv::Rect(origin, cv::Size(faces[i].width, faces[i].height / 2)));
        std::vector<cv::Rect> mouth;
        mouthCascade.detectMultiScale(mouthArea, mouth, 1.1, 2, CV_HAAR_FIND_BIGGEST_OBJECT, cv::Size(faces[i].width * 0.2, faces[i].height * 0.13));
        if (mouth.size())
            feature.mouthCenter = CGPointMake(mouth[0].x + mouth[0].width / 2, mouth[0].y + mouth[0].height / 2);
    }
    
    feature.distanceBetweenEyes = [feature getDistanceBetweenPoint:feature.leftEyeCenter secondPoint:feature.rightEyeCenter];
    
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
    
    return feature;
}

+ (AKFaceFeature *)faceFeatureFromMatFrame:(Mat)frame
{
    AKFaceFeature *feature = [[AKFaceFeature alloc] init];
    
    Mat grayscaleFrame;
    cvtColor(frame, grayscaleFrame, CV_BGR2GRAY);
    equalizeHist(grayscaleFrame, grayscaleFrame);
    
    std::vector<cv::Rect> faces;
    faceCascade.detectMultiScale(grayscaleFrame, faces, 1.1, 2, CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_ROUGH_SEARCH, cv::Size(120, 120));
    
    if (faces.size() == 0)
        return nil;
    
    feature.faceBounds = CGRectMake(faces[0].x, faces[0].y, faces[0].width, faces[0].height);
    //for (int i = 0; i < faces.size(); i++)
    for (int i = 0; i < 1; i++)
    {
        cv::Point pt1(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
        cv::Point pt2(faces[i].x, faces[i].y);
        
        Mat faceROI = grayscaleFrame(faces[i]);
        Mat eyeArea = faceROI(cv::Rect(0, 0, faces[i].width, faces[i].height / 2));
        std::vector<cv::Rect> eyes;
        eyesTreeCascade.detectMultiScale(eyeArea, eyes, 1.1, 2, CV_HAAR_DO_CANNY_PRUNING | CV_HAAR_SCALE_IMAGE, cv::Size(30, 30));
        if (eyes.size() >= 2)
        {
            NSLog(@"detect eyes");
            feature.leftEyeBounds = CGRectMake(eyes[0].x, eyes[0].y,  + eyes[0].width, eyes[0].height);
            feature.leftEyeCenter = CGPointMake(eyes[0].x + eyes[0].width / 2, eyes[0].y + eyes[0].height / 2);
            feature.rightEyeBounds = CGRectMake(eyes[1].x, eyes[1].y,  + eyes[1].width, eyes[1].height);
            feature.rightEyeCenter = CGPointMake(eyes[1].x + eyes[1].width / 2, eyes[1].y + eyes[1].height / 2);
            
            feature.faceAngle = atan((feature.leftEyeCenter.y - feature.rightEyeCenter.y) / (feature.leftEyeCenter.x - feature.rightEyeCenter.x));
        }
        
        cv::Point origin(0, faces[i].height * 0.33);
        Mat mouthArea = faceROI(cv::Rect(origin, cv::Size(faces[i].width, faces[i].height * 0.66)));
        std::vector<cv::Rect> mouth;
        std::vector<int> num(1);
        mouthCascade.detectMultiScale(mouthArea, mouth, num, 1.1, 2, CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_ROUGH_SEARCH, cv::Size(faces[i].width * 0.2, faces[i].height * 0.13));
        if (mouth.size())
        {
            feature.mouthCenter = CGPointMake(mouth[0].x + mouth[0].width / 2, mouth[0].y + mouth[0].height / 2);
            feature.mouthBounds = CGRectMake(mouth[0].x, mouth[0].y, mouth[0].width, mouth[0].height);
        }
    }
    
    feature.distanceBetweenEyes = [feature getDistanceBetweenPoint:feature.leftEyeCenter secondPoint:feature.rightEyeCenter];
    
    return feature;
}

+ (AKFaceFeature *)faceFeatureFromTrackingData:(FaceData)faceData width:(CGFloat)width height:(CGFloat)height
{
    AKFaceFeature *feature = [[AKFaceFeature alloc] init];
    
    _width = width;
    _height = height;
    
    CGPoint leftEyeLeft = [feature getPoint:3 index:12 faceData:faceData];
    CGPoint leftEyeRight = [feature getPoint:3 index:8 faceData:faceData];
    CGPoint leftEyeUpper = [feature getPoint:3 index:14 faceData:faceData];
    CGPoint leftEyeLower = [feature getPoint:3 index:10 faceData:faceData];
    CGPoint rightEyeLeft = [feature getPoint:3 index:11 faceData:faceData];
    CGPoint rightEyeRight = [feature getPoint:3 index:7 faceData:faceData];
    CGPoint rightEyeUpper = [feature getPoint:3 index:13 faceData:faceData];
    CGPoint rightEyeLower = [feature getPoint:3 index:9 faceData:faceData];
    CGPoint leftBrowRight = [feature getPoint:4 index:2 faceData:faceData];
    CGPoint rightBrowLeft = [feature getPoint:4 index:1 faceData:faceData];
    CGPoint leftBrowLeft = [feature getPoint:4 index:6 faceData:faceData];
    CGPoint rightBrowRight = [feature getPoint:4 index:5 faceData:faceData];
    feature.leftEyeBounds = CGRectMake(leftEyeLeft.x, leftEyeUpper.y, leftEyeRight.x - leftEyeLeft.x, leftEyeLower.y - leftEyeUpper.y);
    feature.leftEyeCenter = CGPointMake(leftEyeLeft.x + feature.leftEyeBounds.size.width / 2.0f, leftEyeUpper.y + feature.leftEyeBounds.size.height / 2.0f);
    feature.rightEyeBounds = CGRectMake(rightEyeLeft.x, rightEyeUpper.y, rightEyeRight.x - rightEyeLeft.x, rightEyeLower.y - rightEyeUpper.y);
    feature.rightEyeCenter = CGPointMake(rightEyeLeft.x + feature.rightEyeBounds.size.width / 2.0f, rightEyeUpper.y + feature.rightEyeBounds.size.height / 2.0f);
    CGPoint mouthUpper = [feature getPoint:8 index:1 faceData:faceData];
    CGPoint mouthLower = [feature getPoint:8 index:2 faceData:faceData];
    CGPoint mouthLeft = [feature getPoint:8 index:4 faceData:faceData];
    CGPoint mouthRight = [feature getPoint:8 index:3 faceData:faceData];
    feature.mouthBounds = CGRectMake(mouthLeft.x, mouthUpper.y, mouthRight.x - mouthLeft.x, mouthLower.y - mouthUpper.y);
    feature.mouthCenter = CGPointMake(mouthLeft.x + feature.mouthBounds.size.width / 2.0f, mouthUpper.y + feature.mouthBounds.size.height / 2.0f);
    feature.distanceBetweenEyes = [feature getDistanceBetweenPoint:feature.leftEyeCenter secondPoint:feature.rightEyeCenter];
    feature.headRotation = faceData.faceRotation[2];
    
    feature.distanceLeftBrow = [feature getDistanceBetweenPoint:leftBrowRight secondPoint:leftEyeRight];
    feature.distanceRightBrow = [feature getDistanceBetweenPoint:rightBrowLeft secondPoint:rightEyeLeft];
    CGFloat distanceLeftLeftBrow = [feature getDistanceBetweenPoint:leftBrowLeft secondPoint:leftEyeLeft];
    CGFloat distanceRightRightBrow = [feature getDistanceBetweenPoint:rightBrowRight secondPoint:rightEyeRight];
    if (feature.distanceLeftBrow > distanceLeftLeftBrow * 1.36 || feature.distanceRightBrow > distanceRightRightBrow * 1.36)
        feature.isRaisingBrow = YES;
    else
        feature.isRaisingBrow = NO;
    
    feature.eyeClosureL = faceData.eyeClosure[0];
    feature.eyeClosureR = faceData.eyeClosure[1];
    
    CGPoint mouthUpperInside = [feature getPoint:2 index:2 faceData:faceData];
    CGPoint mouthLowerInside = [feature getPoint:2 index:3 faceData:faceData];
    feature.heightBetweenMouth = mouthLowerInside.y - mouthUpperInside.y;
    
    return feature;
}

+ (AKFaceFeature *)faceFeatureFromMaskObject:(AKMaskResource *)maskResource
{
    AKFaceFeature *feature = [[AKFaceFeature alloc] init];
    
    if (maskResource == nil)
        return feature;
    
    feature.faceBounds = CGRectMake(maskResource.faceX, maskResource.faceY, maskResource.faceWidth, maskResource.faceHeight);
    if (YES)
    {
        CGPoint leftEye = CGPointMake(maskResource.rightEyeX, maskResource.rightEyeY);
        feature.leftEyeCenter = leftEye;
        
//        CGFloat angle = [feature pointPairToBearingRadians:centerPosition secondPoint:leftEye];
//        feature.leftEyeAngle = angle;
    }
    if (YES)
    {
        CGPoint rightEye = CGPointMake(maskResource.rightEyeX, maskResource.rightEyeY);
        feature.rightEyeCenter = rightEye;
        
//        CGFloat angle = [feature pointPairToBearingRadians:centerPosition secondPoint:rightEye];
//        feature.rightEyeAngle = angle;
    }
    if (YES)
    {
        // Change the positions for 90 degree rotation
        CGPoint leftEyePosition = CGPointMake(maskResource.leftEyeX, maskResource.leftEyeY);
        CGPoint rightEyePosition = CGPointMake(maskResource.rightEyeX, maskResource.rightEyeY);
        
        CGFloat angle = [feature pointPairToBearingRadians:leftEyePosition secondPoint:rightEyePosition];
        
        feature.angleBetweenEyes = angle;
        feature.distanceBetweenEyes = [feature getDistanceBetweenPoint:leftEyePosition secondPoint:rightEyePosition];
    }
    
    return feature;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"leftEyeAngle = %f, rightEyeAngle = %f, mouthAngle = %f", self.leftEyeAngle, self.rightEyeAngle, self.mouthAngle];
}

- (CGFloat)pointPairToBearingRadians:(CGPoint)startingPoint secondPoint:(CGPoint)endingPoint
{
    CGPoint originPoint = CGPointMake(endingPoint.x - startingPoint.x, endingPoint.y - startingPoint.y); // get origin point to origin by subtracting end from start
    
    CGFloat bearingRadians = atan2f(originPoint.y, originPoint.x); // get bearing in radians
    
    return bearingRadians;
}

- (CGPoint)getPoint:(int)group index:(int)index faceData:(FaceData)faceData {
    
    VisageSDK::FeaturePoint point = faceData.featurePoints2D->getFP(group, index);
    CGPoint thisPoint = CGPointMake((point.pos[0]) * _width, (1 - point.pos[1]) * _height);
    return thisPoint;
}

- (double) getDistancePoint:(CGPoint)p1 to:(CGPoint)p2 {
    
    double dx = (p1.x-p2.x);
    double dy = (p1.y-p2.y);
    double dist = sqrt(dx*dx + dy*dy);
    return dist;
}

- (CGFloat)getDistanceBetweenPoint:(CGPoint)p1 secondPoint:(CGPoint)p2
{
    CGFloat xDist = (p2.x - p1.x);
    CGFloat yDist = (p2.y - p1.y);
    CGFloat distance = sqrt((xDist * xDist) + (yDist * yDist));
    
    return distance;
}

@end
