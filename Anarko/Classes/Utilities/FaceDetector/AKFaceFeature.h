//
//  AKFaceFeature.h
//  Anarko
//
//  Created by Hua Wan on 23/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>

#import <opencv2/objdetect/objdetect.hpp>
#import <opencv2/imgproc/imgproc_c.h>

#include "visageVision.h"

#import <UserNotifications/UserNotifications.h>

#import "Anarko-Swift.h"

using namespace cv;
using namespace VisageSDK;

@interface AKFaceFeature : NSObject

@property (nonatomic, assign) CGRect faceBounds, leftEyeBounds, rightEyeBounds, mouthBounds;
@property (nonatomic, assign) CGFloat faceAngle;
@property (nonatomic, assign) CGPoint leftEyeCenter, rightEyeCenter, mouthCenter, chinCenter;
@property (nonatomic, assign) CGFloat angleBetweenEyes, distanceBetweenEyes, heightBetweenMouth;
@property (nonatomic, assign) CGFloat widthBetweenEyes, eyeClosureL, eyeClosureR;

@property (nonatomic, assign) CGFloat leftEyeAngle, rightEyeAngle, mouthAngle, headRotation;
@property (nonatomic, assign) CGFloat distanceLeftBrow, distanceRightBrow;
@property (nonatomic, assign) BOOL isMom, isRaisingBrow;

+ (AKFaceFeature *)faceFeatureFromCIFaceFeature:(CIFaceFeature *)faceFeature;
+ (AKFaceFeature *)faceFeatureFromDictionary:(NSDictionary *)faceFeature;
+ (AKFaceFeature *)faceFeatureFromImageBuffer:(CVImageBufferRef)imageBuffer;
+ (AKFaceFeature *)faceFeatureFromMatFrame:(Mat)frame;
+ (AKFaceFeature *)faceFeatureFromTrackingData:(FaceData)faceData width:(CGFloat)width height:(CGFloat)height;
+ (AKFaceFeature *)faceFeatureFromMaskObject:(AKMaskResource *)maskResource;

@end
