//
//  AnarkoProducts.swift
//  Anarko
//
//  Created by Khalid on 03/04/2017.
//  Copyright © 2017 Oppous. All rights reserved.
//

import Foundation

public struct AnarkoProducts {
  
//    public static let BuggyMan = "com.oppous.57fba91385e57f01eb329677"
//    public static let PeppyVentrilo  = "com.oppous.5818adb898cf263a4c50a924"
//    public static let Mary = "com.oppous.58346b5b1e6ccc3b30c55400"
//    public static let Valkyrie = "com.oppous.5834740a1e6ccc3b30c55414"
//    public static let BozzotheClown = "com.oppous.583478f51e6ccc3b30c55428"
//    public static let ShirleyQuinne = "com.oppous.59024817833b71181a58c237"
//    public static let TheSon = "com.oppous.57fbacf1054b8f0356ed7177"

//    fileprivate static let productIdentifiers: Set<ProductIdentifier> = [AnarkoProducts.BuggyMan,AnarkoProducts.PeppyVentrilo,AnarkoProducts.Mary,AnarkoProducts.Valkyrie,AnarkoProducts.BozzotheClown,AnarkoProducts.ShirleyQuinne,AnarkoProducts.TheSon]
    
    var productIdentifiers: Set<ProductIdentifier> = []

    public var store : IAPHelper?
    
    public init(productIds: Set<ProductIdentifier>) {
        productIdentifiers = productIds
        store = IAPHelper(productIds: productIdentifiers)
    }
    
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}
