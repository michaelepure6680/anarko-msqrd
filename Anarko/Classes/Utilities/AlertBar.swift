//
//  AlertBar.swift
//
//  Created by Jin Sasaki on 2016/01/01.
//  Copyright © 2016年 Jin Sasaki. All rights reserved.
//

import UIKit

public enum AlertBarType {
    case success
    case error
    case notice
    case warning
    case info
    case custom(UIColor, UIColor)
    
    var backgroundColor: UIColor {
        get {
            switch self {
            case .success:
                return AlertBarHelper.UIColorFromRGB(0x4CAF50)
            case .error:
                return AlertBarHelper.UIColorFromRGB(0xf44336)
            case .notice:
                return AlertBarHelper.UIColorFromRGB(0xff0000)
            case .warning:
                return AlertBarHelper.UIColorFromRGB(0xFFC107)
            case .info:
                return AlertBarHelper.UIColorFromRGB(0x009688)
            case .custom(let backgroundColor, _):
                return backgroundColor
            }
        }
    }
    var textColor: UIColor {
        get {
            switch self {
            case .custom(_, let textColor):
                return textColor
            default:
                return AlertBarHelper.UIColorFromRGB(0xFFFFFF)
            }
        }
    }
}

open class AlertBar: UIView {
    open static var textAlignment: NSTextAlignment = .center
    static var alertBars: [AlertBar] = []
    
    let closeButton = UIButton()
    let retryButton = UIButton()
    let messageLabel = UILabel()
    var tapGesture : UITapGestureRecognizer?
    var tempWindows: UIWindow?
    var tempCompletion: (() -> Void)?
    var retryCompletion: (() -> Void)?
    
    func tapAlertBarAction(_ gesture: UITapGestureRecognizer? = nil) {
    
        let duration = 0.5
        let statusBarHeight: CGFloat = 44

        UIView.animate(withDuration: 0.2,
                       delay: duration,
                       options: UIViewAnimationOptions(),
                       animations: { () -> Void in
                        self.transform = CGAffineTransform(translationX: 0, y: -statusBarHeight)
        },
                       completion: { (animated: Bool) -> Void in
                        self.removeFromSuperview()
                        if let index = AlertBar.alertBars.index(of: self) {
                            AlertBar.alertBars.remove(at: index)
                        }
                        // To hold window instance
                        self.tempWindows?.isHidden = true
                        self.tempCompletion?()
        })

        
    }
    func retryBtnTapped(_ sender: UIButton){
        print("tapped");
        let duration = 0.5
        let statusBarHeight: CGFloat = 44
        
        UIView.animate(withDuration: 0.2,
                       delay: duration,
                       options: UIViewAnimationOptions(),
                       animations: { () -> Void in
                        self.transform = CGAffineTransform(translationX: 0, y: -statusBarHeight)
        },
                       completion: { (animated: Bool) -> Void in
                        self.removeFromSuperview()
                        if let index = AlertBar.alertBars.index(of: self) {
                            AlertBar.alertBars.remove(at: index)
                        }
                        // To hold window instance
                        self.tempWindows?.isHidden = true
                        self.retryCompletion?()
        })
    }
    
    func closeBtnTapped(_ sender: UIButton){
        print("tapped");
        self.tapAlertBarAction()

    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        messageLabel.frame = CGRect(x: frame.height - 4, y: 2, width: frame.width - 4, height: frame.height - 4)
        messageLabel.center = self.center
        messageLabel.font = UIFont(name: ANARKO_FONT_GUNPLAY, size: 14)
        //
        retryButton.frame = CGRect(x:10, y: 10, width: frame.height - 20, height: frame.height - 20 )
        retryButton.setImage(UIImage(named:"btn_reupload"), for: .normal)
        retryButton.addTarget(self, action: #selector(AlertBar.retryBtnTapped(_:)), for: .touchUpInside)
        self.addSubview(retryButton)
        
        closeButton.frame = CGRect(x:frame.width - frame.height + 10  , y: 10, width: frame.height-20, height: frame.height-20  )
        closeButton.setImage(UIImage(named:"btn-alert-cancel"), for: .normal)
        closeButton.addTarget(self, action: #selector(AlertBar.closeBtnTapped(_:)), for: .touchUpInside)
        self.addSubview(closeButton)
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(AlertBar.tapAlertBarAction(_:)))
//        messageLabel.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGesture!);
        self.addSubview(messageLabel)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleRotate(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    dynamic fileprivate func handleRotate(_ notification: Notification) {
//        self.removeFromSuperview()
//        AlertBar.alertBars = []
    }
    
    open class func show(_ type: AlertBarType, otherButtonsHidden: Bool? = true, viewInterActionEnabled: Bool? = true, message: String, duration: Double = 2, completion: (() -> Void)? = nil) -> AlertBar {
        
        
        let statusBarHeight: CGFloat = 44
        let alertBar = AlertBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: statusBarHeight))
        alertBar.isUserInteractionEnabled = true
        alertBar.messageLabel.text = message
        alertBar.messageLabel.textAlignment = AlertBar.textAlignment
        alertBar.backgroundColor = type.backgroundColor
        alertBar.messageLabel.textColor = type.textColor
        
        if otherButtonsHidden! {
            alertBar.retryButton.isHidden = true
            alertBar.closeButton.isHidden = true
        }
        else
        {
            alertBar.removeGestureRecognizer(alertBar.tapGesture!)
        }
        AlertBar.alertBars.append(alertBar)
        
        let width = UIScreen.main.bounds.width
        //let height = UIScreen.main.bounds.height
        let height = statusBarHeight

        //let baseView = UIView(frame: UIScreen.main.bounds)
        let baseView = UIView(frame: alertBar.frame)
        baseView.isUserInteractionEnabled = true
        baseView.addSubview(alertBar)

        let window: UIWindow
        let orientation = UIApplication.shared.statusBarOrientation
        if orientation.isLandscape {
            window = UIWindow(frame: CGRect(x: 0, y: 0, width: height, height: width))
            let sign: CGFloat = orientation == .landscapeLeft ? -1 : 1
            let d = fabs(width - height) / 2
            baseView.transform = CGAffineTransform(rotationAngle: sign * CGFloat(M_PI) / 2).translatedBy(x: sign * d, y: sign * d)
        } else {
            window = UIWindow(frame: CGRect(x: 0, y: 0, width: width, height: height))
            if orientation == .portraitUpsideDown {
                baseView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
            }
        }
        window.isUserInteractionEnabled = viewInterActionEnabled!
        window.windowLevel = UIWindowLevelStatusBar + 1 + CGFloat(AlertBar.alertBars.count)
        window.addSubview(baseView)
        window.makeKeyAndVisible()
        
        alertBar.tempWindows = window
        alertBar.tempCompletion = completion
        
        alertBar.transform = CGAffineTransform(translationX: 0, y: -statusBarHeight)
        UIView.animate(withDuration: 0.2,
            animations: { () -> Void in
                alertBar.transform = CGAffineTransform.identity
            }, completion: { _ in
                
                if duration > 0.0 {
                    
                    UIView.animate(withDuration: 0.2,
                                   delay: duration,
                                   options: UIViewAnimationOptions(),
                                   animations: { () -> Void in
                                    alertBar.transform = CGAffineTransform(translationX: 0, y: -statusBarHeight)
                    },
                                   completion: { (animated: Bool) -> Void in
                                    alertBar.removeFromSuperview()
                                    if let index = AlertBar.alertBars.index(of: alertBar) {
                                        AlertBar.alertBars.remove(at: index)
                                    }
                                    // To hold window instance
                                    window.isHidden = true
                                    completion?()
                    })
                }
            })
        
        return alertBar
    }
    
    open class func showError(_ error: NSError, duration: Double = 2, completion: (() -> Void)? = nil) {
        let code = error.code
        let localizedDescription = error.localizedDescription
        _ = self.show(.error, message: "(\(code)) " + localizedDescription)
    }
    
    func hide(completion: (() -> Void)? = nil) -> Void {
        let duration = 0.5
        let statusBarHeight: CGFloat = 44
        
        UIView.animate(withDuration: 0.2,
                       delay: duration,
                       options: UIViewAnimationOptions(),
                       animations: { () -> Void in
                        self.transform = CGAffineTransform(translationX: 0, y: -statusBarHeight)
        },
                       completion: { (animated: Bool) -> Void in
                        self.removeFromSuperview()
                        if let index = AlertBar.alertBars.index(of: self) {
                            AlertBar.alertBars.remove(at: index)
                        }
                        // To hold window instance
                        self.tempWindows?.isHidden = true
                        self.tempCompletion?()
                        completion?()
        })
    }
}

internal class AlertBarHelper {
    class func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
