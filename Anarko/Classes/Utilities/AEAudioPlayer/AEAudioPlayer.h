//
//  AAudioPlayer.h
//  AAudioPlayer
//
//  Created by Luski Timurovich on 4/14/17.
//  Copyright (c) 2017 Luski Timurovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@class AEAudioPlayer;

@protocol AEAudioPlayerDelegate <NSObject>

- (void)audioPlayerDidFinishPlaying:(AEAudioPlayer *)player successfully:(BOOL)flag;
//
//@optional
//- (void)audioPlayerBeginInterruption:(AEAudioPlayer *)player;
//- (void)audioPlayerEndInterruption:(AEAudioPlayer *)player withOptions:(AVAudioSessionInterruptionOptions)flags;
@end


@interface AEAudioPlayer : NSObject

- (instancetype)initWithContentsOfURL:(NSURL *)url error:(NSError **)outError;

- (void)play;
- (void)pause;
- (void)stop;

- (void)enableEffect:(BOOL)enable;
@property (nonatomic, getter = isEffectEnabled) BOOL enableProcessing;

- (void)setEffect:(NSInteger)effectType;

- (NSURL *)applyEffectToFile:(NSURL *)fileURL;

@property (nonatomic,weak) id<AEAudioPlayerDelegate> delegate;

@end
