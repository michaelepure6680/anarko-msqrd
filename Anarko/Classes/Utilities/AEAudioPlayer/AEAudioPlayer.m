//
//  AAudioPlayer.m
//  AAudioPlayer
//
//  Created by Luski Timurovich on 4/14/17.
//  Copyright (c) 2017 Luski Timurovich. All rights reserved.
//

#import "AEAudioPlayer.h"
#import <AVFoundation/AVFoundation.h>

@interface AEAudioPlayer ()

@property (nonatomic, strong) AVAudioEngine *engine;
@property (nonatomic, strong) AVAudioPlayerNode *playerNode;
@property (nonatomic, strong) AVAudioFile *audioFile;
//@property (nonatomic, strong) AVAudioUnitEQ *unitEq;
@property (nonatomic, strong) AVAudioUnitDistortion *unitDistortion;
@property (nonatomic, strong) AVAudioUnitTimePitch *unitPitch;
//@property (nonatomic, strong) AVAudioUnitReverb *unitReverb;
//@property (nonatomic, strong) AVAudioUnitDelay *unitDelay;

@end

@implementation AEAudioPlayer

- (instancetype)initWithContentsOfURL:(NSURL *)url error:(NSError **)outError
{
    self = [super init];
    
    self.engine = [[AVAudioEngine alloc] init];
    
    self.audioFile = [[AVAudioFile alloc] initForReading:url error:outError];
    
    self.enableProcessing = NO;
    
    [self setupPlayer];
//    [self setupEQ];
    [self setupDistortion];
//    [self setupDelay];
    [self setupPitch];
//    [self setupReverb];

    AVAudioMixerNode *mixerNode = [self.engine mainMixerNode];
//    [self.engine connect:self.playerNode to:self.unitEq format:self.audioFile.processingFormat];
//    [self.engine connect:self.unitEq to:self.unitDistortion format:self.audioFile.processingFormat];
//    [self.engine connect:self.unitDistortion to:self.unitDelay format:self.audioFile.processingFormat];
//    [self.engine connect:self.unitDelay to:mixerNode format:self.audioFile.processingFormat];
    [self.engine connect:self.playerNode to:self.unitPitch format:self.audioFile.processingFormat];
    [self.engine connect:self.unitPitch to:self.unitDistortion format:self.audioFile.processingFormat];
//    [self.engine connect:self.unitPitch to:self.unitReverb format:self.audioFile.processingFormat];
//    [self.engine connect:self.unitReverb to:mixerNode format:self.audioFile.processingFormat];
    [self.engine connect:self.unitDistortion to:mixerNode format:self.audioFile.processingFormat];
    
    [self.engine startAndReturnError:outError];
    
    return self;
}

- (void)setupPlayer {
    
    self.playerNode = [[AVAudioPlayerNode alloc] init];
//    self.playerNode.reverbBlend = 0.5;
    [self.engine attachNode:self.playerNode];
}
/*
- (void)setupEQ {
    
    self.unitEq = [[AVAudioUnitEQ alloc] initWithNumberOfBands:2];
    
    AVAudioUnitEQFilterParameters *filterParameters;
    filterParameters = self.unitEq.bands[0];
    filterParameters.filterType = AVAudioUnitEQFilterTypeHighPass;
    filterParameters.frequency = 80;
    
    filterParameters = self.unitEq.bands[1];
    filterParameters.filterType = AVAudioUnitEQFilterTypeParametric;
    filterParameters.frequency = 500;
    filterParameters.bandwidth = 2.0;
    filterParameters.gain = 4.0;
    
    [self.engine attachNode:self.unitEq];
}
*/
- (void)setupDistortion {
    
    self.unitDistortion = [[AVAudioUnitDistortion alloc] init];
/*
    @{@"AVAudioUnitDistortionPresetDrumsBitBrush"      : @0},
    @{@"AVAudioUnitDistortionPresetDrumsBufferBeats"   : @1},
    @{@"AVAudioUnitDistortionPresetDrumsLoFi"          : @2},
    @{@"AVAudioUnitDistortionPresetMultiBrokenSpeaker" : @3},
    @{@"AVAudioUnitDistortionPresetMultiCellphoneConcert": @4},
    @{@"AVAudioUnitDistortionPresetMultiDecimated1"    : @5},
    @{@"AVAudioUnitDistortionPresetMultiDecimated2"    : @6},
    @{@"AVAudioUnitDistortionPresetMultiDecimated3"    : @7},
    @{@"AVAudioUnitDistortionPresetMultiDecimated4"    : @8},
    @{@"AVAudioUnitDistortionPresetMultiDistortedFunk" : @9},
    @{@"AVAudioUnitDistortionPresetMultiDistortedCubed": @10},
    @{@"AVAudioUnitDistortionPresetMultiDistortedSquared": @11},
    @{@"AVAudioUnitDistortionPresetMultiEcho1"         : @12},
    @{@"AVAudioUnitDistortionPresetMultiEcho2"         : @13},
    @{@"AVAudioUnitDistortionPresetMultiEchoTight1"    : @14},
    @{@"AVAudioUnitDistortionPresetMultiEchoTight2"    : @15},
    @{@"AVAudioUnitDistortionPresetMultiEverythingIsBroken": @16},
    @{@"AVAudioUnitDistortionPresetSpeechAlienChatter" : @17},
    @{@"AVAudioUnitDistortionPresetSpeechCosmicInterference": @18},
    @{@"AVAudioUnitDistortionPresetSpeechGoldenPi"     : @19},
    @{@"AVAudioUnitDistortionPresetSpeechRadioTower"   : @20},
    @{@"AVAudioUnitDistortionPresetSpeechWaves"        : @21}
*/
    self.unitDistortion.bypass = !self.enableProcessing;
//    self.unitDistortion.wetDryMix = 0;
    
    [self.engine attachNode:self.unitDistortion];
}
/*
- (void)setupDelay {
    
    self.unitDelay = [[AVAudioUnitDelay alloc] init];
    [self.engine attachNode:self.unitDelay];
}

- (void)setupReverve {
    
    AVAudioEnvironmentNode *environmentNode = [[AVAudioEnvironmentNode alloc] init];
    AVAudioEnvironmentReverbParameters *reverbParameters = environmentNode.reverbParameters;
    reverbParameters.enable = YES;
    [reverbParameters loadFactoryReverbPreset:AVAudioUnitReverbPresetLargeHall];
    self.playerNode.reverbBlend = 0.2;
}
*/
- (void)setupPitch {
    self.unitPitch = [[AVAudioUnitTimePitch alloc] init];
//    self.unitPitch.pitch = 300;
    self.unitPitch.bypass = !self.enableProcessing;
    
    [self.engine attachNode:self.unitPitch];
}
/*
- (void)setupReverb {
    self.unitReverb = [[AVAudioUnitReverb alloc] init];
    [self.unitReverb loadFactoryPreset:AVAudioUnitReverbPresetLargeRoom2];
    
    self.unitReverb.bypass = !self.enableProcessing;
    
    [self.engine attachNode:self.unitReverb];
}
*/

- (void)setEffect:(NSInteger)effectType {
    switch (effectType) {
        case 1: // Doland Trump
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetDrumsBitBrush];
            self.unitDistortion.wetDryMix = 5;
            self.unitPitch.pitch = -300;
            break;
            
        case 2: // Hillory Tipton
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetDrumsLoFi];
            self.unitDistortion.wetDryMix = 2;
            self.unitPitch.pitch = 200;
            break;
            
        case 3: // Kimmy K
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiBrokenSpeaker];
            self.unitDistortion.wetDryMix = 5;
            self.unitPitch.pitch = 300;
            break;
            
        case 4: // Satin
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiCellphoneConcert];
            self.unitDistortion.wetDryMix = 5;
            self.unitPitch.pitch = -200;
            break;
            
        case 5: // Buddhy
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiEcho1];
            self.unitDistortion.wetDryMix = 5;
            self.unitPitch.pitch = -300;
            break;
            
        case 6: // Clyde the Monkey
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiDecimated3];
            self.unitDistortion.wetDryMix = 2;
            self.unitPitch.pitch = 300;
            break;
            
        case 7: // Dollface
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiDistortedCubed];
            self.unitDistortion.wetDryMix = 5;
            self.unitPitch.pitch = 400;
            break;
            
        case 8: // Jerry the Burglar
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiEcho2];
            self.unitDistortion.wetDryMix = 5;
            self.unitPitch.pitch = -600;
            break;
            
        case 9: // Sadie the Sadist
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiDecimated2];
            self.unitDistortion.wetDryMix = 5;
            self.unitPitch.pitch = 300;
            break;
            
        case 10: // The Son
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiDistortedSquared];
            self.unitDistortion.wetDryMix = 5;
            self.unitPitch.pitch = -200;
            break;
            
        case 11: // Peppy Ventrilo
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiEchoTight1];
            self.unitDistortion.wetDryMix = 2;
            self.unitPitch.pitch = 200;
            break;
            
        case 12: // Viking
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiCellphoneConcert];
            self.unitDistortion.wetDryMix = 2;
            self.unitPitch.pitch = 600;
            break;
            
        case 13: // Valkyrie
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiEchoTight2];
            self.unitDistortion.wetDryMix = 5;
            self.unitPitch.pitch = -200;
            break;
            
        case 14: // Bozzo the Clown
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetSpeechRadioTower];
            self.unitDistortion.wetDryMix = 2;
            self.unitPitch.pitch = -200;
            break;
            
        case 15: // Mary
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetMultiEcho1];
            self.unitDistortion.wetDryMix = 1;
            self.unitPitch.pitch = 600;
            break;
            
        case 16: // buggy man
            [self.unitDistortion loadFactoryPreset:AVAudioUnitDistortionPresetSpeechCosmicInterference];
            self.unitDistortion.wetDryMix = 5;
            self.unitPitch.pitch = -200;
            break;
            
        default:
            break;
    }
}

- (void)play {
    if ([self.playerNode isPlaying]) {
        [self.playerNode stop];
    }
    
    [self.playerNode scheduleFile:self.audioFile
                           atTime:nil
                completionHandler:^{
                    
                    // repeat
//                    [self play];
                }];
    
    [self.playerNode play];
}

- (void)pause {
    [self.playerNode pause];
}

- (void)stop {
    [self.playerNode stop];
}

- (void)enableEffect:(BOOL)enable {
    self.enableProcessing = enable;
    
    if (self.unitPitch != nil) {
        self.unitPitch.bypass = !self.enableProcessing;
    }
    
//    if (self.unitReverb != nil) {
//        self.unitReverb.bypass = !self.enableProcessing;
//    }
    
    if (self.unitDistortion != nil) {
        self.unitDistortion.bypass = !self.enableProcessing;
    }
}

- (OSStatus)renderToBufferList:(AudioBufferList *)bufferList
                   writeToFile:(ExtAudioFileRef)audioFile
                  bufferLength:(UInt32)bufferLength
                     timeStamp:(AudioTimeStamp *)timeStamp {
    [self clearBufferList:bufferList];
    AudioUnit outputUnit = self.engine.outputNode.audioUnit;
    OSStatus status = AudioUnitRender(outputUnit, 0, timeStamp, 0, bufferLength, bufferList);
    if (status != noErr) {
        NSLog(@"Can not render audio unit");
        return status;
    }
    timeStamp->mSampleTime += bufferLength;
    status = ExtAudioFileWrite(audioFile, bufferLength, bufferList);
    if (status != noErr)
        NSLog(@"Can not write audio to file");
    return status;
}

- (void)clearBufferList:(AudioBufferList *)bufferList {
    for (int bufferIndex = 0; bufferIndex < bufferList->mNumberBuffers; bufferIndex++) {
        memset(bufferList->mBuffers[bufferIndex].mData, 0, bufferList->mBuffers[bufferIndex].mDataByteSize);
    }
}

AudioBufferList *AEAllocateAndInitAudioBufferList(AudioStreamBasicDescription audioFormat, NSUInteger frameCount) {
    int numberOfBuffers = audioFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved ? audioFormat.mChannelsPerFrame : 1;
    int channelsPerBuffer = audioFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved ? 1 : audioFormat.mChannelsPerFrame;
    UInt32 bytesPerBuffer = audioFormat.mBytesPerFrame * frameCount;
    AudioBufferList *audio = malloc(sizeof(AudioBufferList) + (numberOfBuffers-1) * sizeof(AudioBuffer));
    if ( !audio ) {
        return NULL;
    }
    audio->mNumberBuffers = numberOfBuffers;
    for ( int i=0; i<numberOfBuffers; i++ ) {
        if ( bytesPerBuffer > 0 ) {
            audio->mBuffers[i].mData = calloc(bytesPerBuffer, 1);
            if ( !audio->mBuffers[i].mData ) {
                for ( int j=0; j<i; j++ ) free(audio->mBuffers[j].mData);
                free(audio);
                return NULL;
            }
        } else {
            audio->mBuffers[i].mData = NULL;
        }
        audio->mBuffers[i].mDataByteSize = bytesPerBuffer;
        audio->mBuffers[i].mNumberChannels = channelsPerBuffer;
    }
    return audio;
}

void AEFreeAudioBufferList(AudioBufferList *bufferList ) {
    for ( int i=0; i<bufferList->mNumberBuffers; i++ ) {
        if ( bufferList->mBuffers[i].mData ) free(bufferList->mBuffers[i].mData);
    }
    free(bufferList);
}

- (ExtAudioFileRef)createAndSetupExtAudioFileWithASBD:(AudioStreamBasicDescription const *)audioDescription
                                          andFilePath:(NSString *)path {
    AudioStreamBasicDescription destinationFormat;
    memset(&destinationFormat, 0, sizeof(destinationFormat));
    destinationFormat.mChannelsPerFrame = audioDescription->mChannelsPerFrame;
    destinationFormat.mSampleRate = audioDescription->mSampleRate;
    destinationFormat.mFormatID = kAudioFormatLinearPCM;
    
    destinationFormat.mFramesPerPacket = 1;
    destinationFormat.mBytesPerFrame = destinationFormat.mChannelsPerFrame * 2;
    destinationFormat.mBytesPerPacket = destinationFormat.mFramesPerPacket * destinationFormat.mBytesPerFrame;
    destinationFormat.mBitsPerChannel = 16;
    destinationFormat.mReserved = 0;
    destinationFormat.mFormatFlags =  kAudioFormatFlagIsSignedInteger | kAudioFormatFlagsNativeEndian | kLinearPCMFormatFlagIsPacked;
    
    ExtAudioFileRef audioFile;
    OSStatus status = ExtAudioFileCreateWithURL(
                                                (__bridge CFURLRef) [NSURL fileURLWithPath:path],
                                                kAudioFileCAFType,
                                                &destinationFormat,
                                                NULL,
                                                kAudioFileFlags_EraseFile,
                                                &audioFile
                                                );
    if (status != noErr) {
        NSLog(@"Can not create ext audio file");
        return nil;
    }
    UInt32 codecManufacturer = kAppleSoftwareAudioCodecManufacturer;
    status = ExtAudioFileSetProperty(
                                     audioFile, kExtAudioFileProperty_CodecManufacturer, sizeof(UInt32), &codecManufacturer
                                     );
    status = ExtAudioFileSetProperty(
                                     audioFile, kExtAudioFileProperty_ClientDataFormat, sizeof(AudioStreamBasicDescription), audioDescription
                                     );
    status = ExtAudioFileWriteAsync(audioFile, 0, NULL);
    if (status != noErr) {
        NSLog(@"Can not setup ext audio file");
        return nil;
    }
    return audioFile;
}

- (NSURL *)applyEffectToFile:(NSURL *)fileURL {
    if ([self.playerNode isPlaying]) {
        [self.playerNode stop];
    }
    
    AVAudioOutputNode *outputNode = self.engine.outputNode;
    AudioStreamBasicDescription const *audioDescription = [outputNode outputFormatForBus:0].streamDescription;
    [self.playerNode scheduleFile:self.audioFile
                           atTime:[AVAudioTime timeWithSampleTime:0 atRate:audioDescription->mSampleRate]
                completionHandler:^{
                }];
    
    [self.playerNode play];
    [self.engine pause];
    
    NSError *error = nil;
    NSURL *outputFileURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:@"mixeroutput.caf"]];
    
    ExtAudioFileRef audioFile = [self createAndSetupExtAudioFileWithASBD:audioDescription andFilePath:outputFileURL.path];
    if (!audioFile)
        return nil;
    AVURLAsset *asset = [AVURLAsset assetWithURL:self.audioFile.url];
    NSTimeInterval duration = CMTimeGetSeconds(asset.duration);
    NSUInteger lengthInFrames = duration * audioDescription->mSampleRate;
    const NSUInteger kBufferLength = 4096;
    AudioBufferList *bufferList = AEAllocateAndInitAudioBufferList(*audioDescription, kBufferLength);
    AudioTimeStamp timeStamp;
    memset (&timeStamp, 0, sizeof(timeStamp));
    timeStamp.mFlags = kAudioTimeStampSampleTimeValid;
    OSStatus status = noErr;
    for (NSUInteger i = kBufferLength; i < lengthInFrames; i += kBufferLength) {
        status = [self renderToBufferList:bufferList writeToFile:audioFile bufferLength:kBufferLength timeStamp:&timeStamp];
        if (status != noErr)
            break;
    }
    if (status == noErr && timeStamp.mSampleTime < lengthInFrames) {
        NSUInteger restBufferLength = (NSUInteger) (lengthInFrames - timeStamp.mSampleTime);
        AudioBufferList *restBufferList = AEAllocateAndInitAudioBufferList(*audioDescription, restBufferLength);
        status = [self renderToBufferList:restBufferList writeToFile:audioFile bufferLength:restBufferLength timeStamp:&timeStamp];
        AEFreeAudioBufferList(restBufferList);
    }
    
    AEFreeAudioBufferList(bufferList);
    ExtAudioFileDispose(audioFile);
    if (status != noErr)
        NSLog(@"An error has occurred");
    else
        NSLog(@"Finished writing to file at path: %@", outputFileURL.path);
    
    [self.engine startAndReturnError:&error];
    return outputFileURL;
}

/*
AudioBufferList *AEAllocateAndInitAudioBufferList(AudioStreamBasicDescription audioFormat, int frameCount) {
    int numberOfBuffers = audioFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved ? audioFormat.mChannelsPerFrame : 1;
    int channelsPerBuffer = audioFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved ? 1 : audioFormat.mChannelsPerFrame;
    int bytesPerBuffer = audioFormat.mBytesPerFrame * frameCount;
    AudioBufferList *audio = malloc(sizeof(AudioBufferList) + (numberOfBuffers-1)*sizeof(AudioBuffer));
    if ( !audio ) {
        return NULL;
    }
    audio->mNumberBuffers = numberOfBuffers;
    for ( int i=0; i<numberOfBuffers; i++ ) {
        if ( bytesPerBuffer > 0 ) {
            audio->mBuffers[i].mData = calloc(bytesPerBuffer, 1);
            if ( !audio->mBuffers[i].mData ) {
                for ( int j=0; j<i; j++ ) free(audio->mBuffers[j].mData);
                free(audio);
                return NULL;
            }
        } else {
            audio->mBuffers[i].mData = NULL;
        }
        audio->mBuffers[i].mDataByteSize = bytesPerBuffer;
        audio->mBuffers[i].mNumberChannels = channelsPerBuffer;
    }
    return audio;
}

void AEFreeAudioBufferList(AudioBufferList *bufferList ) {
    for ( int i=0; i<bufferList->mNumberBuffers; i++ ) {
        if ( bufferList->mBuffers[i].mData ) free(bufferList->mBuffers[i].mData);
    }
    free(bufferList);
}

- (ExtAudioFileRef)createAndSetupExtAudioFileWithASBD:(AudioStreamBasicDescription const *)audioDescription
                                          andFilePath:(NSString *)path {
    AudioStreamBasicDescription destinationFormat;
    memset(&destinationFormat, 0, sizeof(destinationFormat));
    destinationFormat.mSampleRate = audioDescription->mSampleRate;
    destinationFormat.mFormatID = kAudioFormatLinearPCM;
    destinationFormat.mFormatFlags = kAudioFormatFlagIsPacked | kAudioFormatFlagIsSignedInteger;
    destinationFormat.mChannelsPerFrame = 2;
    destinationFormat.mBitsPerChannel = 16;
    destinationFormat.mBytesPerFrame = 4;
    destinationFormat.mBytesPerPacket = 4;
    destinationFormat.mFramesPerPacket = 1;
    
    ExtAudioFileRef audioFile;
    OSStatus status = ExtAudioFileCreateWithURL(
                                                (__bridge CFURLRef) [NSURL fileURLWithPath:path],
                                                kAudioFileCAFType,
                                                &destinationFormat,
                                                NULL,
                                                kAudioFileFlags_EraseFile,
                                                &audioFile
                                                );
    if (status != noErr) {
        NSLog(@"Can not create ext audio file");
        return nil;
    }
    UInt32 codecManufacturer = kAppleSoftwareAudioCodecManufacturer;
    status = ExtAudioFileSetProperty(
                                     audioFile, kExtAudioFileProperty_CodecManufacturer, sizeof(UInt32), &codecManufacturer
                                     );
    status = ExtAudioFileSetProperty(
                                     audioFile, kExtAudioFileProperty_ClientDataFormat, sizeof(AudioStreamBasicDescription), audioDescription
                                     );
    status = ExtAudioFileWriteAsync(audioFile, 0, NULL);
    if (status != noErr) {
        NSLog(@"Can not setup ext audio file");
        return nil;
    }
    return audioFile;
}

- (void)clearBufferList:(AudioBufferList *)bufferList {
    for (int bufferIndex = 0; bufferIndex < bufferList->mNumberBuffers; bufferIndex++) {
        memset(bufferList->mBuffers[bufferIndex].mData, 0, bufferList->mBuffers[bufferIndex].mDataByteSize);
    }
}

- (OSStatus)renderToBufferList:(AudioBufferList *)bufferList
                   writeToFile:(ExtAudioFileRef)audioFile
                  bufferLength:(NSUInteger)bufferLength
                     timeStamp:(AudioTimeStamp *)timeStamp {
    [self clearBufferList:bufferList];
    AudioUnit outputUnit = self.engine.outputNode.audioUnit;
    
    AudioUnitRenderActionFlags flag = kAudioOfflineUnitRenderAction_Render;
    OSStatus status = AudioUnitRender(outputUnit, &flag, timeStamp, 0, bufferLength, bufferList);
    if (status != noErr) {
        NSLog(@"Can not render audio unit");
        return status;
    }
    timeStamp->mSampleTime += bufferLength;
    status = ExtAudioFileWrite(audioFile, bufferLength, bufferList);
    if (status != noErr)
        NSLog(@"Can not write audio to file");
    return status;
}

- (NSURL *)applyEffectToFile:(NSURL *)fileURL {
    if (fileURL == nil || ![fileURL isFileURL]) {
        return nil;
    }
    
    [self.playerNode stop];
    [self.playerNode play];
    [self.engine pause];
    
    NSError *error = nil;
    NSURL *mixerOutputFileURL = [NSURL URLWithString:[NSTemporaryDirectory() stringByAppendingString:@"mixeroutput.caf"]];
    AVAudioOutputNode *outputNode = self.engine.outputNode;
    AudioStreamBasicDescription const *audioDescription = [outputNode outputFormatForBus:0].streamDescription;
    NSString *path = mixerOutputFileURL.path;
    ExtAudioFileRef audioFile = [self createAndSetupExtAudioFileWithASBD:audioDescription andFilePath:path];
    if (!audioFile)
        return nil;
    
//    NSTimeInterval duration = self.audioFile.length;
    NSUInteger lengthInFrames = self.audioFile.length;//duration * audioDescription->mSampleRate;
    const NSUInteger kBufferLength = 4096;
    AudioBufferList *bufferList = AEAllocateAndInitAudioBufferList(*audioDescription, kBufferLength);
    AudioTimeStamp timeStamp;
    memset (&timeStamp, 0, sizeof(timeStamp));
    timeStamp.mFlags = kAudioTimeStampSampleTimeValid;
    OSStatus status = noErr;
    for (NSUInteger i = kBufferLength; i < lengthInFrames; i += kBufferLength) {
        status = [self renderToBufferList:bufferList writeToFile:audioFile bufferLength:kBufferLength timeStamp:&timeStamp];
        if (status != noErr)
            break;
    }
    if (status == noErr && timeStamp.mSampleTime < lengthInFrames) {
        NSUInteger restBufferLength = (NSUInteger) (lengthInFrames - timeStamp.mSampleTime);
        AudioBufferList *restBufferList = AEAllocateAndInitAudioBufferList(*audioDescription, restBufferLength);
        status = [self renderToBufferList:restBufferList writeToFile:audioFile bufferLength:restBufferLength timeStamp:&timeStamp];
        AEFreeAudioBufferList(restBufferList);
    }
    AEFreeAudioBufferList(bufferList);
    ExtAudioFileDispose(audioFile);
    if (status != noErr)
        NSLog(@"An error has occurred");
    else
        NSLog(@"Finished writing to file at path: %@", path);
    
    [self.engine startAndReturnError:&error];
    [self.playerNode stop];
    
    return mixerOutputFileURL;
}

/*
- (instancetype)initWithContentsOfURL:(NSURL *)url error:(NSError **)outError frequencies:(NSArray *)freqs
{
    self = [super init];
    if(self)
    {
        seekNewTimePosition = -1;
        
        _url = url;
        
//        frequencies = freqs;
//        frequencyGains = [[NSMutableArray alloc] init];
//        for(int i=0; i<frequencies.count; i++)
//        {
//            [frequencyGains addObject:@(0)];
//        }
        
        hasInitialized = false;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionInterruptionNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification:) name:
         AVAudioSessionInterruptionNotification object:nil];
        
//        filterPan = 0.0;
//        filterOverallGain = 0.0;
    }
    return self;
}

- (void)notification:(NSNotification*)notificaiton
{
    if( [notificaiton.userInfo[AVAudioSessionInterruptionTypeKey] intValue] == AVAudioSessionInterruptionTypeBegan )
    {
        if (_file != nil)
        {
            fileSampleRate = _file.fileFormat.sampleRate;
            seekNewTimePosition = self.currentTime * fileSampleRate;
            pausedTime = self.currentTime;
        }
        
        wasInterrupted = true;
        isAudioInterruption = true;
        isPaused = true;
        
        _file = nil;
        
        if(_delegate != nil && [_delegate respondsToSelector:@selector(audioPlayerBeginInterruption:)])
        {
            [_delegate audioPlayerBeginInterruption:self];
        }
        
        [_player pause];
        [[self.class sharedEngine] pause];
        
        if(bufferArray != nil)
            [bufferArray removeAllObjects];
        _player = nil;
    }
    
    if( [notificaiton.userInfo[AVAudioSessionInterruptionTypeKey] intValue] == AVAudioSessionInterruptionTypeEnded )
    {
        if(_delegate != nil && [_delegate respondsToSelector:@selector(audioPlayerEndInterruption:withOptions:)])
        {
            NSInteger options = [notificaiton.userInfo[AVAudioSessionInterruptionOptionKey] intValue];
            [_delegate audioPlayerEndInterruption:self withOptions:options];
        }
    }
}

#pragma mark
/*
- (void)setParamIndex:(int)index
{
    if(_player != nil && eq != nil) {
        AVAudioUnitEQFilterParameters* filterParameters = eq.bands[index];
        
        NSNumber* currentFreq = frequencies[index];
        NSNumber* currentFreqGain = frequencyGains[index];
        
        filterParameters.filterType = AVAudioUnitEQFilterTypeParametric;
        filterParameters.frequency = currentFreq.intValue;
        filterParameters.bandwidth = 1.0;
        filterParameters.bypass = false;
        filterParameters.gain = currentFreqGain.floatValue;
    }
}

- (void)setEqGain:(CGFloat)gain forBand:(int)bandIndex
{
    frequencyGains[bandIndex] = @(gain);
    [self setParamIndex:bandIndex];
}

- (void)setPan:(CGFloat)pan {
    filterPan = pan;
    if(_player != nil)
        _player.pan = pan;
}
- (void)setOverallGain:(CGFloat)overallGain {
    filterOverallGain = overallGain;
    if(_player != nil && eq != nil)
        eq.globalGain = overallGain;
}

- (void)setBandPassAtIndex:(int)bandIndex
{
    if(_player != nil && eq != nil) {
        AVAudioUnitEQFilterParameters* filterParameters = eq.bands[bandIndex];
        
        filterParameters.filterType = AVAudioUnitEQFilterTypeBandPass;
        filterParameters.frequency = filterBandPassFreq;
        filterParameters.bandwidth = 0.5;
        filterParameters.bypass = filterBandPassBypass;
    }
}

- (void)setBandPassFrequency:(CGFloat)freq bypass:(BOOL)bypass {
    filterBandPassFreq = freq;
    filterBandPassBypass = bypass;
    [self setBandPassAtIndex:(int)frequencies.count];
}

- (void)setHighPassAtIndex:(int)bandIndex
{
    if(_player != nil && eq != nil) {
        AVAudioUnitEQFilterParameters* filterParameters = eq.bands[bandIndex];
        
        filterParameters.filterType = AVAudioUnitEQFilterTypeHighPass;
        filterParameters.frequency = filterHighPassFreq;
        filterParameters.bandwidth = 0.5;
        filterParameters.bypass = filterHighPassBypass;
    }
}

- (void)setHighPassFrequency:(CGFloat)freq bypass:(BOOL)bypass {
    filterHighPassFreq = freq;
    filterHighPassBypass = bypass;
    [self setHighPassAtIndex:(int)frequencies.count+1];
}

- (void)setLowPassAtIndex:(int)bandIndex
{
    if(_player != nil && eq != nil) {
        AVAudioUnitEQFilterParameters* filterParameters = eq.bands[bandIndex];
        
        filterParameters.filterType = AVAudioUnitEQFilterTypeLowPass;
        filterParameters.frequency = filterLowPassFreq;
        filterParameters.bandwidth = 0.5;
        filterParameters.bypass = filterLowPassBypass;
    }
}

- (void)setLowPassFrequency:(CGFloat)freq bypass:(BOOL)bypass {
    filterLowPassFreq = freq;
    filterLowPassBypass = bypass;
    [self setLowPassAtIndex:(int)frequencies.count+2];
}

- (void)initializePlayerNodeError:(NSError**)outError offset:(AVAudioFramePosition)offset {
    AVAudioEngine* engine = [self.class sharedEngine];
    
    _player = [[AVAudioPlayerNode alloc] init];
    _file = [[AVAudioFile alloc] initForReading:_url error:outError];
    
    [engine attachNode:_player];

    int extraBands = 3;
    
    eq = [[AVAudioUnitEQ alloc] initWithNumberOfBands:frequencies.count + extraBands];
    [engine attachNode:eq];
    
    for( int i=0; i < frequencies.count; i++)
    {
        [self setParamIndex:i];
    }
    
    [self setBandPassAtIndex:(int)frequencies.count];
    [self setHighPassAtIndex:(int)frequencies.count+1];
    [self setLowPassAtIndex:(int)frequencies.count+2];
    
    eq.globalGain = filterOverallGain;
    _player.pan = filterPan;

    [engine connect:_player to:eq format:_file.processingFormat];
    [engine connect:eq to:[engine mainMixerNode] format:_file.processingFormat];

    pitch = [[AVAudioUnitTimePitch alloc] init];
    pitch.pitch = -1000;
    [engine attachNode:pitch];

    [engine connect:_player to:pitch format:_file.processingFormat];
    [engine connect:pitch to:[engine mainMixerNode] format:_file.processingFormat];
    
    @try {
        _file.framePosition = offset;
    }
    @catch (NSException *exception) {
        _file.framePosition = offset;
        NSLog(@"(%s) EXCEPTION: %@",__PRETTY_FUNCTION__,exception.description);
        
        if(kAE_DEBUG)
        {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"EXCEPTION" message:[NSString stringWithFormat:@"(%s) EXCEPTION: %@",__PRETTY_FUNCTION__,exception.description] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        }
    }
    @finally {
    }
}

- (void)startBufferingURL:(NSURL*)url AtOffset:(AVAudioFramePosition)offset error:(NSError**)outError
{
    _url = url;
    
    [self stop];
    
    if(kAE_DEBUG) NSLog(@"Initializing");
    
    AVAudioEngine* engine = [self.class sharedEngine];
    
    [self initializePlayerNodeError:outError offset:offset];
    
    if(outError != nil && *outError != nil)
    {
        NSLog(@"ERROR READING FILE: %@",*outError);
        return;
    }
    
    bufferArray = [NSMutableArray new];
    
    if(!engine.isRunning)
    {
        [engine startAndReturnError:outError];
        if(outError != nil && *outError != nil)
        {
            NSLog(@"START ERROR: %@",*outError);
            return;
        }
    }
    
    seekNewTimePosition = offset;
    
    if(kAE_DEBUG) NSLog(@"(%s) N:%lld / Offset:%lld",__PRETTY_FUNCTION__,seekNewTimePosition,offset);
}

-(void)loadBuffer
{
    while(bufferArray.count < kNumBufferNodes)
    {
        AVAudioPCMBuffer* newBuffer = [self createAndLoadBuffer];
        if(newBuffer != nil)
        {
            [bufferArray addObject:newBuffer];
            [self scheduleBuffer:newBuffer];
        }
        else
        {
            break;
        }
    }
}

- (void)scheduleBuffer:(AVAudioPCMBuffer*)buffer
{
    [_player scheduleBuffer:buffer atTime:nil options:AVAudioPlayerNodeBufferInterruptsAtLoop completionHandler:^{
        
        
        if(bufferArray != nil && bufferArray.count > 0)
            [bufferArray removeObjectAtIndex:0];
        else
            return;

        [self loadBuffer];
        
        if(bufferArray.count == 0 && !wasInterrupted)
        {
            if(kAE_DEBUG) NSLog(@"Song Finished");
            if(_delegate && [_delegate respondsToSelector:@selector(audioPlayerDidFinishPlaying:successfully:)])
            {
                [_delegate audioPlayerDidFinishPlaying:self successfully:true];
            }
        }
    }];
}

- (AVAudioPCMBuffer*)createAndLoadBuffer
{
    if(_file == nil || _player == nil || ![[self.class sharedEngine] isRunning])
    {
        return nil;
    }
    
    AVAudioPCMBuffer* buffer = nil;
    
    @try
    {
        
        AVAudioFormat *format = _file.processingFormat;
        UInt32 frameCount = 1024;
        buffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:format frameCapacity:frameCount];
        NSError* error = nil;
        if(![_file readIntoBuffer:buffer error:&error])
        {
            NSLog(@"failed to read audio file: %@",error);
            return nil;
        }
        if(buffer.frameLength == 0)
            return nil;
        
    }
    @catch (NSException *exception) {
        
        NSLog(@"(%s) EXCEPTION: %@",__PRETTY_FUNCTION__,exception.description);
        
        if(kAE_DEBUG)
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"EXCEPTION" message:[NSString stringWithFormat:@"(%s) EXCEPTION: %@",__PRETTY_FUNCTION__,exception.description] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    
    return buffer;
}

- (void)finished
{
    if(kAE_DEBUG) NSLog(@"Finished Playing");
}

- (void)play
{
    if([self playing])
        return;
    
    _player = nil;
    _file = nil;
    
    isAudioInterruption = false;
    isPaused = false;
    
    if(!hasInitialized)
    {
        [self startBufferingURL:self.url AtOffset:0 error:nil];
        hasInitialized = true;
    }
    
    wasInterrupted = false;
    
    @try
    {
        if( _player == nil ) {
            if(seekNewTimePosition == -1)
                seekNewTimePosition = 0;
            [self initializePlayerNodeError:nil offset:seekNewTimePosition];
        }
        
        if(![[self.class sharedEngine] isRunning])
        {
            NSError* startError = nil;
            [[self.class sharedEngine] startAndReturnError:&startError];
            if(startError)
            {
                NSLog(@"Error Starting Player: %@",startError);
            }
        }
    }
    @catch (NSException *exception) {
    }
    
    if(kAE_DEBUG) NSLog(@"[-(void)play]: seekNewTimeP:%lld SampleRate:%f SampleTime:%lld",seekNewTimePosition, _file.processingFormat.sampleRate, _player.lastRenderTime.sampleTime);
    
    fileSampleRate = _file.fileFormat.sampleRate;
    
    [self startBuffer];
    
    if(seekNewTimePosition != -1)
    {
        if(seekNewTimePosition == 0)
        {
            seekNewTimePosition = self.currentTime;
        }
        
        AVAudioFramePosition curPos = _player.lastRenderTime.sampleTime;
        
        AVAudioTime* newTime = [[AVAudioTime alloc] initWithSampleTime:curPos-seekNewTimePosition atRate:_file.processingFormat.sampleRate];
        [_player playAtTime:newTime];
    }
    else
    {
        [_player play];
    }
    
    seekNewTimePosition = -1;
    wasInterrupted = false;
}

- (void)pause
{
    if(_file == nil && _player == nil) {
        return;
    }
    
    wasInterrupted = true;
    isAudioInterruption = false;
    
    if (_file != nil)
    {
        fileSampleRate = _file.fileFormat.sampleRate;
        seekNewTimePosition = self.currentTime * _file.fileFormat.sampleRate;
        pausedTime = self.currentTime;
    }
    
    [_player pause];
    [[self.class sharedEngine] pause];
    
    isPaused = true;
    
    if(bufferArray != nil)
        [bufferArray removeAllObjects];
    _player = nil;
    _file = nil;
}

- (void)stop
{
    if(kAE_DEBUG) NSLog(@"Stopping");
    wasInterrupted = true;
    isAudioInterruption = false;
    
    seekNewTimePosition = 0;
    
    if(_file.fileFormat.sampleRate != 0) fileSampleRate = _file.fileFormat.sampleRate;
    
    if(_player)
    {
        [_player stop];
    }
    
    if(bufferArray != nil)
        [bufferArray removeAllObjects];
    
    _player = nil;
    _file = nil;
}

- (BOOL)playing
{
    if(kAE_DEBUG) NSLog(@"[-(BOOL)playing]: %d",_player.isPlaying);
    return _player.playing;
}

- (void)dealloc
{
    if(kAE_DEBUG) NSLog(@"Dealloc");
    _delegate = nil;
    
    if(_player)
    {
        [_player stop];
        _player = nil;
    }
}

- (NSTimeInterval)currentTime
{
    if(isPaused)
        return pausedTime;
    
    double sampleRate = _file.fileFormat.sampleRate;
    if(sampleRate == 0)
    {
        return 0;
    }
    
    NSTimeInterval currentTime = ((NSTimeInterval)[_player playerTimeForNodeTime:_player.lastRenderTime].sampleTime / sampleRate);
    
    return currentTime;
}

- (void)setCurrentTime:(NSTimeInterval)currentTime
{
    if(kAE_DEBUG) NSLog(@"Seeking To Location: %f",currentTime);
    
    BOOL isPlaying = [_player isPlaying];
    
    [self pause];
    
    if(fileSampleRate != 0) seekNewTimePosition = currentTime * fileSampleRate;
    pausedTime = currentTime;
    
    /*isPaused = false;
     seekNewTimePosition = -1;
     
     BOOL isPlaying = [_player isPlaying];
     
     //seekOldFramePosition = [_player playerTimeForNodeTime:_player.lastRenderTime].sampleTime;
     
     double sampleRate = _file.fileFormat.sampleRate;
     AVAudioFramePosition framePosition = (long long)(currentTime * sampleRate);
     if(kAE_DEBUG) NSLog(@"FRAME: %lld %lld", _file.framePosition, framePosition);
     
     NSError* startError = nil;
     
     [self startBufferingURL:_url AtOffset:framePosition error:&startError];
     
     if(startError)
     NSLog(@"Start Error: %@",startError);
    
    if(isPlaying)
        [self play];
}

- (NSTimeInterval)duration
{
    double sampleRate = _file.fileFormat.sampleRate;
    if(sampleRate == 0)
        return 0;
    return (((NSTimeInterval)_file.length/sampleRate));// sampleRate;
}

#pragma mark Buffer Loop

- (void)startBuffer
{
    [self loadBuffer];
}


#pragma mark GLOBAL ENGINE

+ (AVAudioEngine*)sharedEngine
{
    static dispatch_once_t once;
    static AVAudioEngine* result = nil;
    dispatch_once(&once, ^{
        result = [[AVAudioEngine alloc] init];
    });
    return result;
}
*/
@end
