//
//  AKUtilities.m
//  Anarko
//
//  Created by Hua Wan on 14/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import "AKUtilities.h"
#import <AssetsLibrary/AssetsLibrary.h>

static int videoIndex = 0;
static int shareIndex = 0;

@implementation AKUtilities

+ (void)setTitleButton:(UIButton *)button attributedString:(NSString *)string attributes:(NSArray *)attributes
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    for (NSDictionary *att in attributes) {
        NSRange range = NSRangeFromString(att[@"range"]);
        NSMutableDictionary *attribute = [NSMutableDictionary dictionaryWithDictionary:att];
        [attribute removeObjectForKey:@"range"];
        UIFont *font = [UIFont fontWithName:@"Gunplay-Regular" size:18];
        [attribute setObject:font forKey:NSFontAttributeName];
        [attributedString addAttributes:attribute range:range];
    }
    
    [button setAttributedTitle:attributedString forState:UIControlStateNormal];
}

+ (void)setTitleLabel:(UILabel *)label attributedString:(NSString *)string attributes:(NSArray *)attributes
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    for (NSDictionary *att in attributes) {
        NSRange range = NSRangeFromString(att[@"range"]);
        NSMutableDictionary *attribute = [NSMutableDictionary dictionaryWithDictionary:att];
        [attribute removeObjectForKey:@"range"];
        UIFont *font = [UIFont fontWithName:@"Gunplay-Regular" size:22];
        [attribute setObject:font forKey:NSFontAttributeName];
        [attributedString addAttributes:attribute range:range];
    }
    
    [label setAttributedText:attributedString];
}

+ (void)setPlaceholder:(UITextField *)textField attributedString:(NSString *)string attributes:(NSDictionary *)attribute fontSize:(CGFloat)fontSize
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:attribute];
//    [dictionary setObject:[UIFont fontWithName:@"Gunplay-Regular" size:fontSize] forKey:NSFontAttributeName];
    [dictionary setObject:[UIFont systemFontOfSize:fontSize] forKey:NSFontAttributeName];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:string attributes:dictionary];
}

+ (NSString *)filenameFromPath:(NSString *)path
{
    if (path == nil)
        return nil;
    NSRange range = [path rangeOfString:@".png"];
    if (range.location == NSNotFound)
        return nil;
    NSString *urlPath = [path substringToIndex:range.location + range.length];
    return urlPath.lastPathComponent;
}

+ (AVAssetExportSession *)exportVideo:(NSURL *)videoURL ratio:(CGFloat)ratio textView:(TVTextView *)textView completion:(void(^)(NSURL *outputURL))completionHandler
{
    if (videoURL == nil)
    {
        if (completionHandler)
            completionHandler(nil);
        return nil;
    }
    
    AVAsset *videoAsset = [AVURLAsset assetWithURL:videoURL];
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    AVMutableVideoCompositionLayerInstruction *layerInstruction = nil;
    AVMutableCompositionTrack *videoTrack = nil;
    
    AVAssetTrack *assetTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize renderSize = assetTrack.naturalSize;
    CGAffineTransform transform = assetTrack.preferredTransform;
    if (transform.b == 1 && transform.c == -1)
        renderSize = CGSizeMake(renderSize.height, renderSize.width);
    else if ((renderSize.width == transform.tx && renderSize.height == transform.ty) || (transform.tx == 0 && transform.ty == 0))
        renderSize = CGSizeMake(renderSize.width, renderSize.height);
    else
        renderSize = CGSizeMake(renderSize.height, renderSize.width);
    CGSize videoSize = CGSizeMake(renderSize.height / ratio, renderSize.height);
    //duration
    if(videoAsset != nil)
    {
        //VIDEO TRACK
        videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        NSArray *arrayVideoDataSources = [NSArray arrayWithArray:[videoAsset tracksWithMediaType:AVMediaTypeVideo]];
        NSError *error = nil;
        [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                            ofTrack:arrayVideoDataSources[0]
                             atTime:kCMTimeZero
                              error:&error];
        if(error)
        {
            NSLog(@"Insertion error: %@", error);
            completionHandler(nil);
            return nil;
        }
        
        // AUDIO TRACK
        NSArray *arrayAudioDataSources = [NSArray arrayWithArray:[videoAsset tracksWithMediaType:AVMediaTypeAudio]];
        if (arrayAudioDataSources.count > 0)
        {
            AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            error = nil;
            [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                ofTrack:arrayAudioDataSources[0]
                                 atTime:kCMTimeZero
                                  error:&error];
            if(error)
            {
                NSLog(@"Insertion error: %@", error);
                completionHandler(nil);
                return nil;
            }
        }
        
        layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
        
        assetTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        
        CGAffineTransform videoTransform = CGAffineTransformIdentity;
        if (transform.b == 1 && transform.c == -1)
            videoTransform = CGAffineTransformTranslate(videoTransform, renderSize.width - (renderSize.width - videoSize.width) / 2.0f, 0);
        else
            videoTransform = CGAffineTransformTranslate(videoTransform, (renderSize.width - videoSize.width) / 2.0f, 0);
        videoTransform = CGAffineTransformConcat(assetTrack.preferredTransform, videoTransform);
        [layerInstruction setTransform:videoTransform atTime:kCMTimeZero];
        [layerInstruction setOpacity:1.0 atTime:kCMTimeZero];
    }
    
    AVMutableVideoCompositionInstruction * mainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
    mainInstruction.layerInstructions = @[layerInstruction];
    
    AVMutableVideoComposition *mainCompositionInst = [AVMutableVideoComposition videoComposition];
    mainCompositionInst.instructions = [NSArray arrayWithObject:mainInstruction];
    mainCompositionInst.frameDuration = CMTimeMake(1, 30);
    mainCompositionInst.renderSize = videoSize;
    
    ///////////////////////// For title overlay //////////////////////////////////////////
    CALayer *overlayLayer = [CALayer layer];
    CGRect frame = CGRectMake(0, 0, videoSize.width, videoSize.height);
    overlayLayer.frame = frame;
    CALayer *layer = [textView layerWithImageRepresentationForSize:frame.size scale:1.0f];
    CFTimeInterval startPosition = AVCoreAnimationBeginTimeAtZero + 0.0f;
    CABasicAnimation *animationStart = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animationStart.fromValue = [NSNumber numberWithFloat:1.0f];
    animationStart.toValue = [NSNumber numberWithFloat:1.0f];
    animationStart.additive = NO;
    animationStart.removedOnCompletion = NO;
    animationStart.fillMode = kCAFillModeBackwards;
    animationStart.beginTime = startPosition;
    animationStart.duration = 0.001f;
    
    CFTimeInterval endPosition = AVCoreAnimationBeginTimeAtZero + videoAsset.duration.value * 1.0f / videoAsset.duration.timescale;
    CABasicAnimation *animationEnd = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animationEnd.fromValue = [NSNumber numberWithFloat:1.0f];
    animationEnd.toValue = [NSNumber numberWithFloat:0.0f];
    animationEnd.additive = NO;
    animationEnd.removedOnCompletion = NO;
    animationEnd.fillMode = kCAFillModeForwards;
    animationEnd.beginTime = endPosition;
    animationEnd.duration = 0.001f;
    
    [overlayLayer addSublayer:layer];
    [overlayLayer setMasksToBounds:YES];
    
    [overlayLayer addAnimation:animationStart forKey:nil];
    [overlayLayer addAnimation:animationEnd forKey:nil];
    
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = frame;
    videoLayer.frame = frame;
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer];
    
    mainCompositionInst.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    /////////////////////////////////////////////////////////////////////////////////////
    
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"videotrim_%d.mp4", videoIndex]];
    videoIndex += 1;
    unlink([path UTF8String]);
    NSURL *videoOutputURL = [NSURL fileURLWithPath:path];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];
    //AVAssetExportSession *exporter = [AVAssetExportSession exportSessionWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];
    exporter.outputURL = videoOutputURL;
    exporter.outputFileType = AVFileTypeMPEG4;
    exporter.videoComposition = mainCompositionInst;
    exporter.timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
    exporter.shouldOptimizeForNetworkUse = YES;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^
     {
         BOOL success = YES;
         switch ([exporter status]) {
             case AVAssetExportSessionStatusCompleted:
                 success = YES;
                 break;
             case AVAssetExportSessionStatusFailed:
                 success = NO;
                 NSLog(@"input videos - failed: %@", [[exporter error] localizedDescription]);
                 break;
             case AVAssetExportSessionStatusCancelled:
                 success = NO;
                 NSLog(@"input videos - canceled");
                 break;
             default:
                 success = NO;
                 break;
         }
         
         dispatch_async(dispatch_get_main_queue(), ^{
             if (completionHandler == nil)
                 return;
             if (success == YES)
                 completionHandler(videoOutputURL);
             else
                 completionHandler(nil);
         });
     }];
    
    return exporter;
}

+ (AVAssetExportSession *)exportVideo:(NSURL *)videoURL ratio:(CGFloat)ratio textView:(TVTextView *)textView audio:(NSURL *)audioURL completion:(void(^)(NSURL *outputURL))completionHandler
{
    if (videoURL == nil)
    {
        if (completionHandler)
            completionHandler(nil);
        return nil;
    }
    
    AVAsset *videoAsset = [AVURLAsset assetWithURL:videoURL];
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    AVMutableVideoCompositionLayerInstruction *layerInstruction = nil;
    AVMutableCompositionTrack *videoTrack = nil;
    
    AVAssetTrack *assetTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize renderSize = assetTrack.naturalSize;
    CGAffineTransform transform = assetTrack.preferredTransform;
    if (transform.b == 1 && transform.c == -1)
        renderSize = CGSizeMake(renderSize.height, renderSize.width);
    else if ((renderSize.width == transform.tx && renderSize.height == transform.ty) || (transform.tx == 0 && transform.ty == 0))
        renderSize = CGSizeMake(renderSize.width, renderSize.height);
    else
        renderSize = CGSizeMake(renderSize.height, renderSize.width);
    CGSize videoSize = CGSizeMake(renderSize.height / ratio, renderSize.height);
    //duration
    if(videoAsset != nil)
    {
        //VIDEO TRACK
        videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        NSArray *arrayVideoDataSources = [NSArray arrayWithArray:[videoAsset tracksWithMediaType:AVMediaTypeVideo]];
        NSError *error = nil;
        [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                            ofTrack:arrayVideoDataSources[0]
                             atTime:kCMTimeZero
                              error:&error];
        if(error)
        {
            NSLog(@"Insertion error: %@", error);
            completionHandler(nil);
            return nil;
        }
        
        // AUDIO TRACK
        NSArray *arrayAudioDataSources = [NSArray arrayWithArray:[videoAsset tracksWithMediaType:AVMediaTypeAudio]];
        if (arrayAudioDataSources.count > 0)
        {
            AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            error = nil;
            [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                ofTrack:arrayAudioDataSources[0]
                                 atTime:kCMTimeZero
                                  error:&error];
            if(error)
            {
                NSLog(@"Insertion error: %@", error);
                completionHandler(nil);
                return nil;
            }
        }
        
        // AUDIO TRACK
        AVAsset *audioAsset = [AVURLAsset URLAssetWithURL:audioURL options:nil];
        arrayAudioDataSources = [NSArray arrayWithArray:[audioAsset tracksWithMediaType:AVMediaTypeAudio]];
        if (arrayAudioDataSources.count > 0)
        {
            AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            error = nil;
            [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration)
                                ofTrack:arrayAudioDataSources[0]
                                 atTime:kCMTimeZero
                                  error:&error];
            if(error)
            {
                NSLog(@"Insertion error: %@", error);
                completionHandler(nil);
                return nil;
            }
        }
        
        layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
        
        assetTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        
        CGAffineTransform videoTransform = CGAffineTransformIdentity;
        if (transform.b == 1 && transform.c == -1)
            videoTransform = CGAffineTransformTranslate(videoTransform, renderSize.width - (renderSize.width - videoSize.width) / 2.0f, 0);
        else
            videoTransform = CGAffineTransformTranslate(videoTransform, (renderSize.width - videoSize.width) / 2.0f, 0);
        videoTransform = CGAffineTransformConcat(assetTrack.preferredTransform, videoTransform);
        [layerInstruction setTransform:videoTransform atTime:kCMTimeZero];
        [layerInstruction setOpacity:1.0 atTime:kCMTimeZero];
    }
    
    AVMutableVideoCompositionInstruction * mainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
    mainInstruction.layerInstructions = @[layerInstruction];
    
    AVMutableVideoComposition *mainCompositionInst = [AVMutableVideoComposition videoComposition];
    mainCompositionInst.instructions = [NSArray arrayWithObject:mainInstruction];
    mainCompositionInst.frameDuration = CMTimeMake(1, 30);
    mainCompositionInst.renderSize = videoSize;
    
    ///////////////////////// For title overlay //////////////////////////////////////////
    CALayer *overlayLayer = [CALayer layer];
    CGRect frame = CGRectMake(0, 0, videoSize.width, videoSize.height);
    overlayLayer.frame = frame;
    CALayer *layer = [textView layerWithImageRepresentationForSize:frame.size scale:1.0f];
    CFTimeInterval startPosition = AVCoreAnimationBeginTimeAtZero + 0.0f;
    CABasicAnimation *animationStart = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animationStart.fromValue = [NSNumber numberWithFloat:1.0f];
    animationStart.toValue = [NSNumber numberWithFloat:1.0f];
    animationStart.additive = NO;
    animationStart.removedOnCompletion = NO;
    animationStart.fillMode = kCAFillModeBackwards;
    animationStart.beginTime = startPosition;
    animationStart.duration = 0.001f;
    
    CFTimeInterval endPosition = AVCoreAnimationBeginTimeAtZero + videoAsset.duration.value * 1.0f / videoAsset.duration.timescale;
    CABasicAnimation *animationEnd = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animationEnd.fromValue = [NSNumber numberWithFloat:1.0f];
    animationEnd.toValue = [NSNumber numberWithFloat:0.0f];
    animationEnd.additive = NO;
    animationEnd.removedOnCompletion = NO;
    animationEnd.fillMode = kCAFillModeForwards;
    animationEnd.beginTime = endPosition;
    animationEnd.duration = 0.001f;
    
    [overlayLayer addSublayer:layer];
    [overlayLayer setMasksToBounds:YES];
    
    [overlayLayer addAnimation:animationStart forKey:nil];
    [overlayLayer addAnimation:animationEnd forKey:nil];
    
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = frame;
    videoLayer.frame = frame;
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer];
    
    mainCompositionInst.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    /////////////////////////////////////////////////////////////////////////////////////
    
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"videotrim_%d.mp4", videoIndex]];
    videoIndex += 1;
    unlink([path UTF8String]);
    NSURL *videoOutputURL = [NSURL fileURLWithPath:path];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];
    //AVAssetExportSession *exporter = [AVAssetExportSession exportSessionWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];
    exporter.outputURL = videoOutputURL;
    exporter.outputFileType = AVFileTypeMPEG4;
    exporter.videoComposition = mainCompositionInst;
    exporter.timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
    exporter.shouldOptimizeForNetworkUse = YES;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^
     {
         BOOL success = YES;
         switch ([exporter status]) {
             case AVAssetExportSessionStatusCompleted:
                 success = YES;
                 break;
             case AVAssetExportSessionStatusFailed:
                 success = NO;
                 NSLog(@"input videos - failed: %@", [[exporter error] localizedDescription]);
                 break;
             case AVAssetExportSessionStatusCancelled:
                 success = NO;
                 NSLog(@"input videos - canceled");
                 break;
             default:
                 success = NO;
                 break;
         }
         
         dispatch_async(dispatch_get_main_queue(), ^{
             if (completionHandler == nil)
                 return;
             if (success == YES)
                 completionHandler(videoOutputURL);
             else
                 completionHandler(nil);
         });
     }];
    
    return exporter;
}

+ (AVAssetExportSession *)addWatermarkVideo:(NSURL *)videoURL completion:(void(^)(NSURL *outputURL))completionHandler
{
    if (videoURL == nil)
    {
        if (completionHandler)
            completionHandler(nil);
        return nil;
    }
    
    AVAsset *videoAsset = [AVURLAsset URLAssetWithURL:videoURL options:nil];
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    AVMutableVideoCompositionLayerInstruction *layerInstruction = nil;
    AVMutableCompositionTrack *videoTrack = nil;
    
    AVAssetTrack *assetTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize renderSize = assetTrack.naturalSize;
    CGAffineTransform transform = assetTrack.preferredTransform;
    if (transform.b == 1 && transform.c == -1)
        renderSize = CGSizeMake(renderSize.height, renderSize.width);
    else if ((renderSize.width == transform.tx && renderSize.height == transform.ty) || (transform.tx == 0 && transform.ty == 0))
        renderSize = CGSizeMake(renderSize.width, renderSize.height);
    else
        renderSize = CGSizeMake(renderSize.height, renderSize.width);
    //duration
    if(videoAsset != nil)
    {
        //VIDEO TRACK
        videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        NSArray *arrayVideoDataSources = [NSArray arrayWithArray:[videoAsset tracksWithMediaType:AVMediaTypeVideo]];
        NSError *error = nil;
        [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                            ofTrack:arrayVideoDataSources[0]
                             atTime:kCMTimeZero
                              error:&error];
        if(error)
        {
            NSLog(@"Insertion error: %@", error);
            completionHandler(nil);
            return nil;
        }
        
        // AUDIO TRACK
        NSArray *arrayAudioDataSources = [NSArray arrayWithArray:[videoAsset tracksWithMediaType:AVMediaTypeAudio]];
        if (arrayAudioDataSources.count > 0)
        {
            AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            error = nil;
            [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                ofTrack:arrayAudioDataSources[0]
                                 atTime:kCMTimeZero
                                  error:&error];
            if(error)
            {
                NSLog(@"Insertion error: %@", error);
                completionHandler(nil);
                return nil;
            }
        }
        
        layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
        
        assetTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        
        CGAffineTransform videoTransform = CGAffineTransformIdentity;
        if (transform.b == 1 && transform.c == -1)
            videoTransform = CGAffineTransformTranslate(videoTransform, renderSize.width - (renderSize.width - renderSize.width) / 2.0f, 0);
        else
            videoTransform = CGAffineTransformTranslate(videoTransform, (renderSize.width - renderSize.width) / 2.0f, 0);
        videoTransform = CGAffineTransformConcat(assetTrack.preferredTransform, videoTransform);
        [layerInstruction setTransform:videoTransform atTime:kCMTimeZero];
        [layerInstruction setOpacity:1.0 atTime:kCMTimeZero];
    }
    
    AVMutableVideoCompositionInstruction * mainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
    mainInstruction.layerInstructions = @[layerInstruction];
    
    AVMutableVideoComposition *mainCompositionInst = [AVMutableVideoComposition videoComposition];
    mainCompositionInst.instructions = [NSArray arrayWithObject:mainInstruction];
    mainCompositionInst.frameDuration = CMTimeMake(1, 30);
    mainCompositionInst.renderSize = renderSize;
    
    ///////////////////////// For the watermark //////////////////////////////////////////
    if (YES)
    {
        CGRect frame = CGRectMake(0, 0, renderSize.width, renderSize.height);
        CALayer *parentLayer = [CALayer layer];
        parentLayer.frame = frame;
        
        UIImage *overlayImage = nil;
        UIImage *imageWatermark = [UIImage imageNamed:@"img-watermark"];
        int width = renderSize.width / 2.4;
        int height = width / imageWatermark.size.width * imageWatermark.size.height;
        CGRect frameWatermark = CGRectMake(renderSize.width - width - 16, renderSize.height - height - 16, width, height);
        UIGraphicsBeginImageContextWithOptions(renderSize, NO, 2.0f);
        [imageWatermark drawInRect:frameWatermark blendMode:kCGBlendModeNormal alpha:1.0f];
        overlayImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        //video layer
        CALayer *videoLayer = [CALayer layer];
        videoLayer.frame = CGRectMake(0, 0, renderSize.width, renderSize.height);
        videoLayer.backgroundColor = [UIColor clearColor].CGColor;
        [parentLayer addSublayer:videoLayer];
        
        CALayer *overlayLayer = [CALayer layer];
        [overlayLayer setContents:(id)[overlayImage CGImage]];
        overlayLayer.frame = CGRectMake(0, 0, renderSize.width, renderSize.height);
        [parentLayer addSublayer:overlayLayer];
        mainCompositionInst.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    }
    /////////////////////////////////////////////////////////////////////////////////////
    
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"videoshare_%d.mp4", shareIndex]];
    shareIndex += 1;
    unlink([path UTF8String]);
    NSURL *videoOutputURL = [NSURL fileURLWithPath:path];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];
    //AVAssetExportSession *exporter = [AVAssetExportSession exportSessionWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];
    exporter.outputURL = videoOutputURL;
    exporter.outputFileType = AVFileTypeMPEG4;
    exporter.videoComposition = mainCompositionInst;
    exporter.timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
    exporter.shouldOptimizeForNetworkUse = YES;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^
     {
         BOOL success = YES;
         switch ([exporter status]) {
             case AVAssetExportSessionStatusCompleted:
                 success = YES;
                 break;
             case AVAssetExportSessionStatusFailed:
                 success = NO;
                 NSLog(@"input videos - failed: %@", [[exporter error] localizedDescription]);
                 break;
             case AVAssetExportSessionStatusCancelled:
                 success = NO;
                 NSLog(@"input videos - canceled");
                 break;
             default:
                 success = NO;
                 break;
         }
         
         dispatch_async(dispatch_get_main_queue(), ^{
             if (completionHandler == nil)
                 return;
             if (success == YES)
                 completionHandler(videoOutputURL);
             else
                 completionHandler(nil);
         });
     }];
    
    return exporter;
}

+ (UIImage *)thumbImage:(NSURL *)videoURL time:(CMTime)time
{
    AVAsset *videoAsset = [AVURLAsset URLAssetWithURL:videoURL options:nil];
    AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:videoAsset];
    generator.appliesPreferredTrackTransform = YES;
    generator.requestedTimeToleranceBefore = kCMTimeZero;
    generator.requestedTimeToleranceAfter = kCMTimeZero;
    
    AVAssetTrack *assetTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize renderSize = assetTrack.naturalSize;
    CGAffineTransform transform = assetTrack.preferredTransform;
    if (transform.b == 1 && transform.c == -1)
        renderSize = CGSizeMake(renderSize.height, renderSize.width);
    else if ((renderSize.width == transform.tx && renderSize.height == transform.ty) || (transform.tx == 0 && transform.ty == 0))
        renderSize = CGSizeMake(renderSize.width, renderSize.height);
    else
        renderSize = CGSizeMake(renderSize.height, renderSize.width);
    generator.maximumSize = renderSize;
    
    CGImageRef imageRef = [generator copyCGImageAtTime:time actualTime:NULL error:nil];
    UIImage *image = [UIImage imageWithCGImage:imageRef];
    
    if (imageRef)
        CFRelease(imageRef);
    
    return image;
}

+ (void)saveVideoFile:(NSURL *)videoURL
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeVideoAtPathToSavedPhotosAlbum:videoURL
                                completionBlock:^(NSURL *assetURL, NSError *error){
                                    if (error) {
                                        //NSString *mssg = [NSString stringWithFormat:@"Error saving the video to the photo library. %@", error];
                                        //[self _showAlertViewWithMessage:mssg];
                                    }
                                    else{
                                        NSString *mssg = @"Saved the video to the photo library.";
                                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                                                            message:mssg
                                                                                           delegate:nil
                                                                                  cancelButtonTitle:@"OK"
                                                                                  otherButtonTitles:nil];
                                            [alert show];
                                        });
                                    }
                                }];
}

@end
