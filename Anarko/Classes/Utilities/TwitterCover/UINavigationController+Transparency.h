//
//  UINavigationController+Transparency.h
//  Anarko
//
//  Created by HeMin on 9/16/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Transparency)

- (void)gsk_setNavigationBarTransparent:(BOOL)transparent
                               animated:(BOOL)animated;

@end
