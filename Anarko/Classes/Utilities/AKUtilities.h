//
//  AKUtilities.h
//  Anarko
//
//  Created by Hua Wan on 14/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "TVTextView.h"

@interface AKUtilities : NSObject

+ (void)setTitleButton:(UIButton *)button attributedString:(NSString *)string attributes:(NSArray *)attributes;
+ (void)setTitleLabel:(UILabel *)label attributedString:(NSString *)string attributes:(NSArray *)attributes;
+ (void)setPlaceholder:(UITextField *)textField attributedString:(NSString *)string attributes:(NSDictionary *)attribute fontSize:(CGFloat)fontSize;
+ (NSString *)filenameFromPath:(NSString *)path;

+ (AVAssetExportSession *)exportVideo:(NSURL *)videoURL ratio:(CGFloat)ratio textView:(TVTextView *)textView completion:(void(^)(NSURL *outputURL))completionHandler;
+ (AVAssetExportSession *)exportVideo:(NSURL *)videoURL ratio:(CGFloat)ratio textView:(TVTextView *)textView audio:(NSURL *)audioURL completion:(void(^)(NSURL *outputURL))completionHandler;
+ (AVAssetExportSession *)addWatermarkVideo:(NSURL *)videoURL completion:(void(^)(NSURL *outputURL))completionHandler;
+ (UIImage *)thumbImage:(NSURL *)videoURL time:(CMTime)time;
+ (void)saveVideoFile:(NSURL *)videoURL;

@end
