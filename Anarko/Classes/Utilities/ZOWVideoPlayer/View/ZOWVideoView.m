//
//  ZOWVideoView.m
//  Example
//
//  Created by stoncle on 12/28/15.
//  Copyright © 2015 stoncle. All rights reserved.
//

#import "ZOWVideoView.h"
#import "ZOWVideoPlayerLayerContainerView.h"

@implementation ZOWVideoView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initVideoPlayer];
    }
    return self;
}

- (void)playVideoWithURL:(NSURL *)url {
    
    
    [self initVideoPlayer];
    [self initVideoLayerContainerView];
    if([self.videoPlayer playVideoWithURL:url])
    {
        _playingURL = url;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(muteCurrentPlayingVideo:) name:@"kNotificationMuteVideo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unMuteCurrentPlayingVideo:) name:@"kNotificationUnMuteVideo" object:nil];

}

- (void)stopVideoPlay {
    [self hideVideoLayer];
    [self.videoPlayer stopVideoPlay];
}

- (void)pause {
    [self.videoPlayer pause];
}

- (void)resume {
    [self.videoPlayer resume];
}

- (void)mute {
    [self.videoPlayer setMute:YES];
}

- (void)unmute {
    [self.videoPlayer setMute:NO];
}

- (void)muteCurrentPlayingVideo:(NSNotification *)notification {
    
    if (self.videoPlayer)
        [self mute];
}

- (void)unMuteCurrentPlayingVideo:(NSNotification *)notification {
    if (self.videoPlayer)
        [self unmute];

}

#pragma mark - Private
- (void)initVideoPlayer
{
    if(!self.videoPlayer)
    {
        self.videoPlayer = [[ZOWVideoPlayer alloc] init];
        self.videoPlayer.dataSource = self;
        self.videoPlayer.delegate = self;
        self.videoPlayer.endAction = ZOWVideoPlayerEndActionRePlay;
    }
}

- (void)initVideoLayerContainerView
{
    if(!self.videoLayerContainerView)
    {
        self.videoLayerContainerView = [[ZOWVideoPlayerLayerContainerView alloc] initWithFrame:self.bounds];
        self.videoLayerContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        [self addSubview:self.videoLayerContainerView];
    }
}

- (void)hideVideoLayer
{
    [self.videoLayerContainerView.layer removeAllAnimations];
    self.videoLayerContainerView.layer.opacity = 0;
}

#pragma mark - Delegate

- (void)videoPlayerDidStartPlayVideo:(ZOWVideoPlayer *)player {
    
}
- (void)videoPlayerDidStartStreamVideo:(ZOWVideoPlayer *)player {
    
    if ([self.delegate respondsToSelector:@selector(videoDidStart)]) {
        
        [self.delegate videoDidStart];
    }
    
}
- (void)videoPlayerDidEndPlayVideo:(ZOWVideoPlayer *)player {
    
    if ([self.delegate respondsToSelector:@selector(videoDidEnd)]) {
        
        [self.delegate videoDidEnd];
    }
    
}
- (void)videoPlayerDidStuck:(ZOWVideoPlayer *)player {
    
}
- (void)videoPlayerDidResume:(ZOWVideoPlayer *)player {
    
    if ([self.delegate respondsToSelector:@selector(videoDidResume)]) {
        
        [self.delegate videoDidResume];
    }
    
}
- (void)videoPlayerDidReset:(ZOWVideoPlayer *)player {
    
}
- (void)videoPlayerDidFailedPlayVideo:(ZOWVideoPlayer *)player {
    
}
- (void)videoPlayer:(ZOWVideoPlayer *)player didMuted:(BOOL)mute {
    
}

#pragma mark ZOWVideoPlayerDataSource
- (UIView<ZOWVideoPlayerProtocol> *)videoPlayerView {
    return self;
}

#pragma mark ZOWVideoPlayerDelegate

#pragma mark ZOWVideoPlayerProtocol
- (void)notifyCancelLoadingVideo {
    
}

- (void)notifyLoadingVideoSuccessed {
    
    NSLog(@"video has been loaded successfully");
    
    if ([self.delegate respondsToSelector:@selector(videoLoadedWithSuccess:contentView:)]) {
        
        [self.delegate videoLoadedWithSuccess:self.videoPlayer contentView:self];
    }
    
}

- (void)notifyLoadingVideoFailed {
    [self hideVideoLayer];
}

- (void)dealloc
{
    [self.videoPlayer stopVideoPlay];
}

@end
