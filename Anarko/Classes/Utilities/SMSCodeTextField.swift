//
//  SMSCodeTextField.swift
//  Anarko
//
//  Created by Khalid on 04/04/2017.
//  Copyright © 2017 Oppous. All rights reserved.
//

import UIKit

protocol SMSCodeTextFieldDelegate {
    func textFieldDidDelete(textField : UITextField) -> Void
}

class SMSCodeTextField: UITextField {

    var codeDelegate: SMSCodeTextFieldDelegate?
    
    override func deleteBackward() {
        //super.deleteBackward()
        codeDelegate?.textFieldDidDelete(textField: self)
    }

}
