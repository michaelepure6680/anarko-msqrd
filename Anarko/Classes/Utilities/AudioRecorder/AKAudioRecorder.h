//
//  AKAudioRecorder.h
//  Anarko
//
//  Created by Hua Wan on 11/11/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AKAudioRecorder : NSObject

+ (AKAudioRecorder *)sharedInstance;

- (NSURL *)audioURL;

- (BOOL)startAudioSession;
- (void)stopAudioSession;

- (BOOL)startRecord;
- (void)stopRecord;
- (void)playAudioRecord;
- (void)pauseAudioRecord;
- (void)continueRecord;
- (void)pauseRecord;

@end
