//
//  AKAudioRecorder.m
//  Anarko
//
//  Created by Hua Wan on 11/11/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import "AKAudioRecorder.h"

#import <AVFoundation/AVFoundation.h>

@interface AKAudioRecorder() <AVAudioRecorderDelegate>
{
    AVAudioRecorder *recorder;
    AVAudioPlayer *audioPlayer;
    
    NSURL *recordURL;
}

@end

@implementation AKAudioRecorder

+ (AKAudioRecorder *)sharedInstance
{
    static dispatch_once_t oncePredicate;
    static AKAudioRecorder *sharedInstance = nil;
    
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[AKAudioRecorder alloc] init];
    });
    return sharedInstance;
}

- (NSURL *)audioURL
{
    return recordURL;
}

- (NSString *)dateString
{
    // return a formatted string for a file name
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"ddMMMYY_hhmmssa";
    return [[formatter stringFromDate:[NSDate date]] stringByAppendingString:@".aif"];
}

- (BOOL)startAudioSession
{
    // Prepare the audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    if (session.inputAvailable == YES)
    {
        NSError *error;
        
        // Recording settings
        NSMutableDictionary *settings = [[NSMutableDictionary alloc] init];
        
        [settings setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
        [settings setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
        [settings setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
        
        [settings setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [settings setValue:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
        [settings setValue:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
        
        // Create a new dated file
        NSString *recorderFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"audioredord.caf"];
        unlink([recorderFilePath UTF8String]);
        
        recordURL = [NSURL fileURLWithPath:recorderFilePath];
        error = nil;
        recorder = [[AVAudioRecorder alloc] initWithURL:recordURL settings:settings error:&error];
        if(!recorder){
            NSLog(@"recorder: %@ %d %@", [error domain], [error code], [[error userInfo] description]);
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle: @"Warning"
                                       message: [error localizedDescription]
                                      delegate: nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
            [alert show];
            return NO;
        }
        
        //prepare to record
        [recorder setDelegate:self];
        [recorder prepareToRecord];
        recorder.meteringEnabled = YES;
    }
    
    return session.inputAvailable;
}

- (void)stopAudioSession
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:NO error:nil];
}

- (BOOL)startRecord
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    if (session.inputAvailable == YES)
    {
        NSError *error;
        
        // Recording settings
        NSMutableDictionary *settings = [[NSMutableDictionary alloc] init];
        
        [settings setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
        [settings setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
        [settings setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
        
        [settings setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [settings setValue:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
        [settings setValue:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
        
        // Create a new dated file
        NSString *recorderFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"audioredord.caf"];
        unlink([recorderFilePath UTF8String]);
        
        recordURL = [NSURL fileURLWithPath:recorderFilePath];
        error = nil;
        recorder = [[AVAudioRecorder alloc] initWithURL:recordURL settings:settings error:&error];
        if(!recorder){
            NSLog(@"recorder: %@ %d %@", [error domain], [error code], [[error userInfo] description]);
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle: @"Warning"
                                       message: [error localizedDescription]
                                      delegate: nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
            [alert show];
            return NO;
        }
        
        //prepare to record
        [recorder setDelegate:self];
        [recorder prepareToRecord];
        recorder.meteringEnabled = YES;
        [recorder record];
        
        return YES;
    }
    
    return NO;
}

- (void)playAudioRecord
{
    if (recordURL == nil)
        return;
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:recordURL error:&error];
    
    if (!audioPlayer)
    {
        NSLog(@"Error establishing player for %@: %@", recorder.url, error.localizedFailureReason);
        return;
    }
    
    audioPlayer.delegate = self;
    
    // Change audio session for playback
    if (![[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error])
    {
        NSLog(@"Error updating audio session: %@", error.localizedFailureReason);
        return;
    }
    
    [audioPlayer prepareToPlay];
    [audioPlayer play];
}

- (void)pauseAudioRecord
{
    [audioPlayer pause];
}

- (void)stopRecord
{
    // This causes the didFinishRecording delegate method to fire
    [recorder stop];
}

- (void)continueRecord
{
    // resume from a paused recording
    [recorder record];
    
}

- (void)pauseRecord
{  // pause an ongoing recording
    [recorder pause];
    
}

#pragma mark - AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    
}

#pragma mark - AVAudioRecorderDelegate

@end
