//
//  AKUser.swift
//  Anarko
//
//  Created by x on 9/22/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKUser: NSObject {

    //SingleTone
    static let me = AKUser()
    
    var userID: String!
    var createdAt: String!
    var clientID: String!
    var isNumberChange: Bool!
    var loginAttempts: NSInteger!
    var market: NSDictionary!
    var phone: String!
    var token: String!
    
    
    override init() {
        
        
    }
    
    init(data:NSDictionary?) {
        
        if let data = data {
        
            self.userID = data["_id"] as? String
            self.createdAt = data["create"] as? String
            self.clientID = data["clientId"] as? String
            self.isNumberChange = data["isNumberChange"] as? NSNumber as? Bool
            self.loginAttempts = data["loginAttempts"] as? NSInteger
            self.market = data["market"] as? NSDictionary
            self.phone = data["phone"] as? String
            self.token = data["token"] as? String
            
        }
    }
    
    
    
}
