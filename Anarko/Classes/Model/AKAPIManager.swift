//
//  AKAppManager.swift
//  Anarko
//
//  Created by x on 9/21/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import Alamofire




class AKAPIManager: NSObject {
    
    
    // SingleTone
    @objc static let sharedManager = AKAPIManager()
    
    // Basic Server Path
    
    //let BASE_URL = "http://testapi.anarko.city:8443/api/v1"
    let BASE_URL = "http://api.anarko.city:8443/api/v1"
    
    
    let API_TOKEN_Q = "?token="
    let API_ONLY_PURCHASED = "&onlyPurchased="
    // GET
    let API_GET_SINGLE_THUMBNAIL            =   "/thumb/:"
    let API_GET_SINGLE_ANARKO               =   "/anarko/"
    let API_GET_ANARKO_LIST                 =   "/explore/"
    let API_GET_UPLOAD_URL                  =   "/uploadUrl"
    let API_GET_STORE_MASK                  =   "/store/mask"
    let API_GET_STORE_EFFECT                =   "/store/effects"
    let API_GET_MASK_MY                     =   "/getMyMasks"
    let API_GET_LOCATION                    =   "/location"
    
    //POST
    let API_REFRESH_TOKEN                   =   "/refresh_token"
    let API_REGISTER_PHONE                  =   "/regphone"
    let API_LOGIN                           =   "/login"
    let API_UPLOAD_ANARKO                   =   "/vidform"
    
    let API_ADD_COMMENT                     =   "/vid/"
    let API_CHANGE_PHONENUMBER              =   "/changeNumber/"
    let API_REPORT_ANARKO                   =   "/vid/report"
    let API_VERIFY_CHANGED_NUMBER           =   "/verifyChangeNumber"
    let API_SEND_FEEDBACK                   =   "/feedback"
    let API_INVITE                          =   "/invites"
    let API_BUY                             =   "/buy/mask"
    let API_VERIFY_APPLE_RECEIPT            =   "/buy/verifyAppleReceipt"
    let API_SEND_RECOVERTY_CODE             =   "/me/send_recovery_code"
    let API_VERIFY_RECOVERTY_CODE           =   "/me/verify_recovery_code"
    let API_POST_INCREMENT_VIDEO_VIEW       =   "/vid/"
    let API_BLOCK_VIDEO                     =   "/usr/blockVideo"
    let API_BLOCK_VIDEO_AUTHOR              =   "/usr/blockVideoAuthor"
    
    //Delete
    let API_DELETE_ACCOUNT                  =   "/me"
    
    //Patch
    let API_ME_UPDATE_DEVICETOKEN           =   "/me"
    
    var request: Alamofire.Request? {
        
        didSet {
            
            oldValue?.cancel()
        }
        
    }
    
    var apiType: String!
    
    //MARK: - Private
    
    // "GET"
    private func GetDataFromService(requestString: String? = nil, completionHandler:@escaping (DataResponse<Any>) -> Void){
        
        let myUser = AKAppManager.sharedInstance.myUser!
        let token = myUser.token!
        let tokenQuery = "\(API_TOKEN_Q)\(token)"

        var baseUrl = "\(BASE_URL)\(apiType!)\(tokenQuery)\(requestString!)"
        //    baseUrlStr = [baseUrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

        baseUrl = baseUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        print("Base Url is **************** \(baseUrl)")
        
        Alamofire.request(baseUrl).responseJSON { (response) in
            
            completionHandler(response)
        }
    }
    // "POST"
    private func SendDataToService(params: [String: AnyObject]? = nil, completionHandler:@escaping (DataResponse<Any>) -> Void){
        

        var baseUrl:String
        
        if self.apiType == API_REGISTER_PHONE || self.apiType == API_LOGIN{
            
            baseUrl = "\(BASE_URL)\(apiType!)"
        }else {
            
            let tokenQuery = "\(API_TOKEN_Q)\(AKAppManager.sharedInstance.myUser.token!)"

            baseUrl = "\(BASE_URL)\(apiType!)\(tokenQuery)"

        }
        
        baseUrl = baseUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!

        Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            completionHandler(response)
        }
    }
    
    // "Delete"
    private func SendDeleteRequestToService(params: [String: AnyObject]? = nil, completionHandler:@escaping (DataResponse<Any>) -> Void){
        
        
        let tokenQuery = "\(API_TOKEN_Q)\(AKAppManager.sharedInstance.myUser.token!)"
            
        var baseUrl = "\(BASE_URL)\(apiType!)\(tokenQuery)"
        
        baseUrl = baseUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        Alamofire.request(baseUrl, method: .delete, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            completionHandler(response)
        }
        
    }
    
    // "PATCH"
    private func SendPatchRequestToService(params: [String: AnyObject]? = nil, completionHandler:@escaping (DataResponse<Any>) -> Void){
        
        
        let tokenQuery = "\(API_TOKEN_Q)\(AKAppManager.sharedInstance.myUser.token!)"
        
        var baseUrl = "\(BASE_URL)\(apiType!)\(tokenQuery)"
        
        baseUrl = baseUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        Alamofire.request(baseUrl, method: .patch, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            completionHandler(response)
        }
        
    }
    
    // "Multipart-Form Data"
    private func UploadDataToService(params: [String: AnyObject]? = nil, videoData: NSData? = nil , imageData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void, progressHandler:  ((_ progress: Float) -> Swift.Void)? = nil) {
        
        let tokenQuery = "\(API_TOKEN_Q)\(AKAppManager.sharedInstance.myUser.token!)"
        // video/mp4, video/quicktime
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if let video = videoData , let image = imageData {
                
                multipartFormData.append(video as Data, withName: "video", fileName: "video.mp4", mimeType: "video/mp4")
                multipartFormData.append(image as Data, withName: "image", fileName: "image.jpg", mimeType: "image/jpeg")
                
                for (key, value) in params! {
                    
                    let strValue = value as! String
                    multipartFormData.append(strValue.data(using:String.Encoding.utf8)!, withName: key)
                }
                
            }
            
        }, to: "\(BASE_URL)\(apiType!)\(tokenQuery)") { (encodingResult) in
            
            
            print("\(self.BASE_URL)\(self.apiType!)\(tokenQuery)")
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    
                    print("Upload progress: \(progress.fractionCompleted)")
                    if progressHandler != nil {
                        progressHandler!(Float(progress.fractionCompleted))
                    }
                })
                
                upload.responseJSON(completionHandler: completionHandler)
//                upload.responseString(completionHandler: { (response) in
//
//                    if let json = response.result.value {
//                        print("JSON: \(json)")
//
//                    }
//                })
                
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    //MARK: - GET Request
    
    //Download image
    func getThumbImageWithName(name: String, completionHandler:@escaping (DataResponse<Any>) -> Void){
    
       self.apiType = API_GET_SINGLE_THUMBNAIL
        
        self.GetDataFromService(requestString: name, completionHandler: completionHandler)
    }
    
    //Get Single Anarko
    
    func getAnarkoWithId(anarkoId: String, completionHandler:@escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = "\(API_GET_SINGLE_ANARKO)\(anarkoId)"
        self.GetDataFromService(requestString: "", completionHandler: completionHandler)
    }
    
    // Get Anarko List

    func getAnarkoList(query: String, completionHandler:@escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = API_GET_ANARKO_LIST
        
        self.GetDataFromService(requestString:query , completionHandler: completionHandler)
    }
    
    //Search Anarko List with Tag
    func searchAnarkoWithQuery(query: String, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = API_GET_ANARKO_LIST
        self.GetDataFromService(requestString:query , completionHandler: completionHandler)

    }
    
    func getUploadUrl(completionHandler:@escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = API_GET_UPLOAD_URL
        self.GetDataFromService(requestString:"", completionHandler: completionHandler)
    }
    
    // Get Store items
    func getStoreMaskList(query: String, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = API_GET_STORE_MASK
        
        self.GetDataFromService(requestString:query, completionHandler: completionHandler)
    }

    func getStoreEffectsList(query: String, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = API_GET_STORE_EFFECT
        
        self.GetDataFromService(requestString:query, completionHandler: completionHandler)
    }
    
    // Get My paid masks
    func getMyMaskList(userId: String, onlyPurchased: String = "false",  completionHandler: @escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = "\(API_GET_MASK_MY)/\(userId)"
        
        self.GetDataFromService(requestString:"\(API_ONLY_PURCHASED)\(onlyPurchased)", completionHandler: completionHandler)
    }
    
    //
    func getStoreList(query: String, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        
        if query == StoreType.Mask.rawValue {
            
            self.apiType = API_GET_STORE_MASK
        }else {
            
            self.apiType = API_GET_STORE_EFFECT
        }
        
        
        self.GetDataFromService(requestString:"", completionHandler: completionHandler)
    }

    
    //Get Location
    
    func getLocation(query: String, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = "\(API_GET_LOCATION)/\(query)"
        
        self.GetDataFromService(requestString: "", completionHandler: completionHandler)
    }
    
    //MARK: - POST Request

    //Increase anarko views count
    
    func increaseVideoViewsCount(anarkoId: String, completionHandler:@escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = "\(API_POST_INCREMENT_VIDEO_VIEW)\(anarkoId)/view"
        self.SendDataToService(params: nil, completionHandler: completionHandler)
    }
    
    //Refresh token when it has been expired
    
    func generateRefreshToken( params:[String: AnyObject], completionHandler:@escaping (DataResponse<Any>) -> Void){
        
        self.apiType = API_REFRESH_TOKEN
        
        self.SendDataToService(params: params, completionHandler: completionHandler)
    }
    
    // Phone number Registration
    
    func registerPhoneNumber(params:[String: AnyObject], completionHandler:@escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = API_REGISTER_PHONE
        
        self.SendDataToService(params: params, completionHandler: completionHandler)
    }
    
    // Submit SMS code
    
    func authorizeWithCode(params:[String: AnyObject], completionHandler:@escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = API_LOGIN
        self.SendDataToService(params: params, completionHandler: completionHandler)

    }
    
    // Upload Anarko
    
    func uploadAnarko(params:[String: AnyObject], videoData: NSData? = nil, imageData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void){
        
        self.apiType = API_UPLOAD_ANARKO
        self.UploadDataToService(params: params, videoData: videoData, imageData: imageData , completionHandler: completionHandler)
    }
    
    func uploadAnarko(params:[String: AnyObject], videoData: NSData? = nil, imageData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void, progressHandler: ((_ progress: Float) -> Swift.Void)? = nil){
        
        self.apiType = API_UPLOAD_ANARKO
        self.UploadDataToService(params: params, videoData: videoData, imageData: imageData , completionHandler: completionHandler, progressHandler: progressHandler!)
    }
    
    //Add Comment
    
    func addComment(params:[String: AnyObject], uploadData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void){
    
        self.apiType = "\(API_ADD_COMMENT)\(params["id"]!)/comments"        
        self.SendDataToService(params: params, completionHandler: completionHandler)
    }
    
    // Change Phone number
    
    func changePhoneNumber(params:[String: AnyObject], uploadData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        
        
        self.apiType = "\(API_CHANGE_PHONENUMBER)\(params["user_id"]!)"
        
        self.SendDataToService(params: params, completionHandler: completionHandler)
    }
    
    // Verify Changed Number
    
    func verifyChangedNumber(params:[String: AnyObject], uploadData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void) {
    
        self.apiType = API_VERIFY_CHANGED_NUMBER
        self.SendDataToService(params: params, completionHandler: completionHandler)
    
    }
    
    // Report vid
    
    func reportAnarko (params:[String: AnyObject], uploadData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void){
    
        self.apiType = API_REPORT_ANARKO
        
        self.SendDataToService(params: params, completionHandler: completionHandler)

    }
    
    // Submit Feedback
    
    func submitFeedback (params:[String: AnyObject], uploadData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void){
        
        self.apiType = API_SEND_FEEDBACK
        
        self.SendDataToService(params: params, completionHandler: completionHandler)
    }
    
    // Invite Friends
    
    func inviteFriends (params:NSString, uploadData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void){
        
        self.apiType = API_INVITE
        
        let tokenQuery = "\(API_TOKEN_Q)\(AKAppManager.sharedInstance.myUser.token!)"
        
        var baseUrl = "\(BASE_URL)\(self.apiType!)\(tokenQuery)"
        baseUrl = baseUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        var request = URLRequest(url: URL(string: baseUrl)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = params.data(using: String.Encoding.utf8.rawValue);
        
        
        Alamofire.request(request).responseJSON { (response) in
            completionHandler(response)

        }
        
    }
    
    // Buy items
    
    func buyMask (params:[String: AnyObject], uploadData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void){
        
        self.apiType = API_BUY
        
        self.SendDataToService(params: params, completionHandler: completionHandler)

    }
    
    // Buy items
    
    func verifyAppleReceipt (params:[String: AnyObject], uploadData: NSData? = nil, completionHandler: @escaping (DataResponse<Any>) -> Void){
        
        self.apiType = API_VERIFY_APPLE_RECEIPT
        
        self.SendDataToService(params: params, completionHandler: completionHandler)
        
    }
    
    //Send Recovery Code to my number
    
    func sendRecoveryCode (completionHandler: @escaping (DataResponse<Any>) -> Void){
        
        self.apiType = API_SEND_RECOVERTY_CODE
        
        self.SendDataToService(completionHandler: completionHandler)
        
    }
    
    //Send Recovery Code to my number
    
    func verifyRecoveryCode (params:[String: AnyObject], completionHandler: @escaping (DataResponse<Any>) -> Void){
        
        self.apiType = API_VERIFY_RECOVERTY_CODE
        
        self.SendDataToService(params: params, completionHandler: completionHandler)
        
    }
    
    //Block Video
    
    func blockVideo(params:[String: AnyObject], completionHandler:@escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = API_BLOCK_VIDEO
        self.SendDataToService(params: params, completionHandler: completionHandler)
        
    }
    
    //Block Video Author
    
    func blockVideoAuthor(params:[String: AnyObject], completionHandler:@escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = API_BLOCK_VIDEO_AUTHOR
        self.SendDataToService(params: params, completionHandler: completionHandler)
        
    }
    
    //MARK: - Delete Request
    
    //Delete my account
    func deleteAccount(completionHandler: @escaping (DataResponse<Any>) -> Void) {
        
        self.apiType = API_DELETE_ACCOUNT
        
        self.SendDeleteRequestToService(completionHandler: completionHandler)
        
    }
    
    
    //MARK: - Patch Request
    
    //Update device token
    @objc func updateDeviceToken(params:[String: AnyObject], completionHandler: @escaping (NSError?) -> Void) {
        
        self.apiType = API_ME_UPDATE_DEVICETOKEN
        
        self.SendPatchRequestToService(params: params) { (response) in
            switch response.result {
            case .success:
                if let json = response.result.value {
                    print("JSON: \(json)")
                    
                    let jsonDic = json as! NSDictionary
                    
                    let error = AKParser.parseErrorMessage(data: jsonDic)
                    
                    completionHandler(error)
                }
            case .failure(let error):
                completionHandler(error as NSError?)
            }
        }
        
    }
}
