//
//  AKMask.swift
//  Anarko
//
//  Created by x on 10/8/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKMask: NSObject {
    
    //        {
    //            "__v" = 0;
    //            "_id" = 57f6a8c1e2aa11666e1c7a82;
    //            active = 1;
    //            creator = "oppous.llc";
    //            description = clow;
    //            isFree = 0;
    //            minVersionReq = 1;
    //            name = clow;
    //            package =             (
    //            );
    //            price = "0.99";
    //            private = 0;
    //            thumbs =             (
    //            {
    //            "_id" = 57f6a8c5e2aa11666e1c7a8b;
    //            action = normal;
    //            url = "https://s3.amazonaws.com/thumbs.anarko.city/57f6a8c1e2aa11666e1c7a82/57f6a8c3e2aa11666e1c7a86.png";
    //        },
    //        {
    //            "_id" = 57f6a8c5e2aa11666e1c7a8a;
    //            action = talk;
    //            url = "https://s3.amazonaws.com/thumbs.anarko.city/57f6a8c1e2aa11666e1c7a82/57f6a8c4e2aa11666e1c7a87.png";
    //        },
    //        {
    //            "_id" = 57f6a8c5e2aa11666e1c7a89;
    //            action = wink;
    //            url = "https://s3.amazonaws.com/thumbs.anarko.city/57f6a8c1e2aa11666e1c7a82/57f6a8c4e2aa11666e1c7a88.png";
    //        }
    //        );
    //        type = mask;
    //    }

    
    var maskId: String!
    var active: Bool!
    var creator: String!
    var descriptions: String!
    var isDefault: Bool!
    var isFree: Bool!
    var minVersionReq: Float!
    var name: String!
    var package: [Any]!
    var price: Float!
    var _private: Bool!
    var thumbs: NSArray!
    var type: String!
    var version: Float!
    var gifs: NSArray?
    
    override init() {
        
        
    }
    
    init(data: NSDictionary) {
        
        self.maskId = data["_id"] as? String
        self.active = data["active"] as? NSNumber as? Bool
        self.creator = data["creator"] as? String
        self.descriptions = data["description"] as? String
        self.isFree = data["isFree"] as? NSNumber as? Bool
        self.minVersionReq = data["minVersionReq"] as? Float
        self.name = data["name"] as? String
        self.package = data["package"] as? [Any]
        self.price = data["price"] as? Float
        self._private = data["private"] as? NSNumber as? Bool
        self.version = data["__v"] as? Float
        self.isDefault = data["is_default"] as? NSNumber as? Bool
        self.type = data["type"] as? String
        
    }
    
    func setResources(resources : NSDictionary) {
        self.thumbs = resources["thumbs"] as? NSArray
        self.gifs = resources["gifs"] as? NSArray
    }

}
