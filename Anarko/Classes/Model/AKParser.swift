//
//  AKParser.swift
//  Anarko
//
//  Created by x on 9/24/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKParser: NSObject {

    
    static var from: Int = 0 {
        
        didSet {
            
        }
    }
    
    static var tag:     String  = ""
    static var count:   Int     = 0
    static var size:    Int     = 20
    static var total:   Int     = 0
    
    static func parseFeedData(data: NSDictionary) -> [AKFeed]{
        
        var feedArray:[AKFeed] = []
        
        AKParser.count = (data["count"] as? Int)!
        AKParser.from = (data["count"] as? Int)! + AKParser.from
//        AKParser.size = (data["size"] as? Int)!
        AKParser.total = (data["total"] as? Int)!
        
        
        let arrayPayload = data["payload"] as! NSArray

        for payload in arrayPayload {
            
            let feed = AKFeed(data: payload as! NSDictionary)
            
            feedArray.append(feed)
        }
        
        return feedArray
    }
    
    static func parseStoreItem(data: NSArray) -> [AKMask] {
//        {
//            "__v" = 0;
//            "_id" = 57f6a8c1e2aa11666e1c7a82;
//            active = 1;
//            creator = "oppous.llc";
//            description = clow;
//            isFree = 0;
//            minVersionReq = 1;
//            name = clow;
//            package =             (
//            );
//            price = "0.99";
//            private = 0;
//            thumbs =             (
//            {
//            "_id" = 57f6a8c5e2aa11666e1c7a8b;
//            action = normal;
//            url = "https://s3.amazonaws.com/thumbs.anarko.city/57f6a8c1e2aa11666e1c7a82/57f6a8c3e2aa11666e1c7a86.png";
//        },
//        {
//            "_id" = 57f6a8c5e2aa11666e1c7a8a;
//            action = talk;
//            url = "https://s3.amazonaws.com/thumbs.anarko.city/57f6a8c1e2aa11666e1c7a82/57f6a8c4e2aa11666e1c7a87.png";
//        },
//        {
//            "_id" = 57f6a8c5e2aa11666e1c7a89;
//            action = wink;
//            url = "https://s3.amazonaws.com/thumbs.anarko.city/57f6a8c1e2aa11666e1c7a82/57f6a8c4e2aa11666e1c7a88.png";
//        }
//        );
//        type = mask;
//    }
        
        var maskArray:[AKMask] = []

        let arrayMask = data

        for mask in arrayMask {
            
            let maskDict = mask as! NSDictionary
            if maskDict["mask"] != nil {
                let feed = AKMask(data: maskDict["mask"] as! NSDictionary)
                maskArray.append(feed)
            } else if(maskDict["maskOrEfectInfo"] != nil){
                let feed = AKMask(data: maskDict["maskOrEfectInfo"] as! NSDictionary)
                maskArray.append(feed)
            }else {
                let feed = AKMask(data:mask as! NSDictionary)
                maskArray.append(feed)
            }
            
            if maskDict["resources"] != nil {
                let feed = maskArray.last
                feed?.setResources(resources: maskDict["resources"] as! NSDictionary)
            }
        }
        
        
        return maskArray
    }

    static func errorParse(data: NSDictionary) -> Int{
        
        if data["status"] as! Int  == 1 {
            
            return 1
        }else {
            
            let errorDic = data["error"] as? NSDictionary
            
            if errorDic?["code"] as! String == "S-1002" {
                
                return -1
            }
            
            return 0
        }
    }
    
    static func parseErrorMessage(data: NSDictionary) -> NSError? {
        
        if data["status"] as! Int  == 1 {
            return nil;
        }else {
            var errorMessage = ""
            var type = ""
            var status = 0
            var code = ""
            
            //Error message
            if let errorDic = data["error"] as? NSDictionary,
                let errorsArray = errorDic["errors"] as? NSArray,
                let error = errorsArray[0] as? NSDictionary
            {
                if let message = error["messages"] as? String {
                    errorMessage = message;
                }
                else if let messages = error["messages"] as? NSArray, let message = messages[0] as? String {
                    errorMessage = message;
                }
                else
                {
                    errorMessage = "User Error"
                }
                
            } else {
                errorMessage = "User Error"
            }
            
            //Error type
            if let errorDic = data["error"] as? NSDictionary, let errorType = errorDic["type"] as? String {
                type = errorType
            }
            else
            {
                if let errorDic = data["error"] as? NSDictionary, let errorType = errorDic["statusText"] as? String {
                    type = errorType
                }
                else
                {
                    type = "Unknown Error!"
                }
            }
            
            //Error Status
            if let errorDic = data["error"] as? NSDictionary,
                let errorStatus = errorDic["status"] as? Int {
                status = errorStatus
            }
            else
            {
                status = 400
            }
            
            //Error Code
            if let errorDic = data["error"] as? NSDictionary,
                let errorCode = errorDic["code"] as? String {
                code = errorCode
            }
            
            if code == "U-0051" {
                type = "Why do you want 2 of the same?"
                errorMessage = "You already have this mask, go show it off"
            }
            
            let userInfo: [AnyHashable : Any] =
                [
                    NSLocalizedDescriptionKey : errorMessage ,
                    NSLocalizedFailureReasonErrorKey : code,
                    NSLocalizedRecoverySuggestionErrorKey : type
            ]
            let err = NSError(domain: "NSURLErrorDomain", code: status, userInfo: userInfo )
            
            return err
        }
    }
    
    static func queryStringOfAnarkoList() ->String {
        
        let queryString = "&\(FROM)=\(AKParser.from)&\(SIZE)=\(AKParser.size)&"
        
        return queryString
    }
    
    static func queryStringOfSearchWith(query: String, from: Int, size: Int) -> String {
        
        let queryString: String
        
        if query == "" {
            
            queryString = "&\(FROM)=\(from)&\(SIZE)=\(size)&"

        }else {
            
            queryString = "&\(TAG)=\(query)&\(FROM)=\(from)&\(SIZE)=\(size)&"

        }
        
        
        return queryString

    }
    
    
}
