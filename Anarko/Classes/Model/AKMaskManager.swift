//
//  AKMaskManager.swift
//  Anarko
//
//  Created by Hua Wan on 27/10/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import CoreData
import MagicalRecord
import Alamofire
import AlamofireImage
import SVProgressHUD

class AKMaskManager: NSObject, NSFetchedResultsControllerDelegate {

    // SingleTone
    static let sharedInstance = AKMaskManager()
    
    lazy private var alamofireManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 60
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        return sessionManager
    }()
    
    private var _fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult> {
        get {
            if _fetchedResultsController == nil {
                _fetchedResultsController = AKMaskObject.mr_fetchAllSorted(by: "isDefault", ascending: false, with: nil, groupBy: nil, delegate: self)
                self.reloadFetchedMasks()
            }
            return _fetchedResultsController
        }
        set (value) {
            _fetchedResultsController = value
        }
    }
    
    func reloadFetchedMasks() -> Void {
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            print("error")
        }
    }
    
    func initDownloadTasks() -> Void {
        let count = self.masksCount()
        
        for i in 0..<count {
            let maskObject = self.fetchedResultsController.object(at: IndexPath(item: i, section: 0)) as! AKMaskObject
            UserDefaults.standard.removeObject(forKey: maskObject.maskId!)
            
            var allObjects = maskObject.thumbs?.allObjects
            for maskThumb in allObjects! {
                let thumb = maskThumb as! AKMaskThumb
                UserDefaults.standard.removeObject(forKey: thumb.filename!)
            }
            
            allObjects = maskObject.resources?.allObjects
            for maskResource in allObjects! {
                let resource = maskResource as! AKMaskResource
                UserDefaults.standard.removeObject(forKey: resource.image!)
            }
        }
    }
    
    var defaultDownloads = 0
    var isDownloadingDefaultMasks = false
    func downloadAvailableMasks() -> Void {
        
        if isDownloadingDefaultMasks == true {
            return
        }
        
        defaultDownloads = 0
        isDownloadingDefaultMasks = true
        AKAPIManager.sharedManager.getMyMaskList(userId: AKAppManager.sharedInstance.myUser.userID) { (response) in
            
            self.isDownloadingDefaultMasks = false
            switch response.result {
                
            case .success:
                
                print("Validation Successful")
                
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let jsonDic = json as! NSDictionary
                    let error = AKParser.parseErrorMessage(data: jsonDic)
                    if error == nil {
                        let data = jsonDic["data"]
                        if data is NSNull {
                            return
                        }
                        let masksArray = data as! [[String: AnyObject]]
                        for maskInfo in masksArray {
                            self.addMask(maskInfo: maskInfo, needsDownload: true)
                        }
                    }
                    else
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                    }
                }
            case .failure(let error):
                
                print(error)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
            }
        }
    }
    
    func masksCount() -> Int {
        var count = 0
        self.reloadFetchedMasks()
        guard (self.fetchedResultsController.sections?.count)! > 0 else {
            return 0
        }
        
        let section = self.fetchedResultsController.sections?[0]
        count = (section?.numberOfObjects)!
        
        return count
    }
    
//    func existMask(maskId: String) -> Bool {
//        let count = self.masksCount()
//        
//        for i in 0..<count {
//            let maskObject = self.fetchedResultsController.object(at: IndexPath(item: i, section: 0)) as! AKMaskObject
//            if maskObject.maskId == maskId {
//                return true
//            }
//        }
//        
//        return false
//    }
    
    func maskObjectAt(index: Int) -> AKMaskObject {
        let count = self.masksCount()
        if index >= count {
            return AKMaskObject()
        }
        
        let maskObject = self.fetchedResultsController.object(at: IndexPath(item: index, section: 0)) as! AKMaskObject
        return maskObject;
    }
    
    func maskObjectFor(maskId: String) -> AKMaskObject? {
        let count = self.masksCount()
        for i in 0..<count {
            let maskObject = self.fetchedResultsController.object(at: IndexPath(item: i, section: 0)) as! AKMaskObject
            if maskObject.maskId == maskId {
                return maskObject
            }
        }
        
        return nil
    }
    
    func deleteMask(maskId: String) -> Void {
        let count = self.masksCount()
        
        for i in 0..<count {
            let maskObject = self.fetchedResultsController.object(at: IndexPath(item: i, section: 0)) as! AKMaskObject
            if maskObject.maskId == maskId {
                maskObject.mr_deleteEntity()
                return
            }
        }
    }
    
    func deleteAllMask() -> Void {
        AKMaskObject.mr_truncateAll()
    }
    
    func thumbFor(maskObject: AKMaskObject, thumbId: String) -> AKMaskThumb? {
        let allThumbs = maskObject.thumbs?.allObjects as! [AKMaskThumb]
        if allThumbs.count == 0 {
            return nil
        }
        
        for thumb in allThumbs {
            if thumb.thumbId == thumbId {
                return thumb
            }
        }
        
        return nil
    }
    
    func resourceFor(maskObject: AKMaskObject, resourceId: String) -> AKMaskResource? {
        let resources = maskObject.resources?.allObjects
        if resources == nil {
            return nil
        }
        
        let allResources = resources as! [AKMaskResource]
        if allResources.count == 0 {
            return nil
        }
        
        for resource in allResources {
            if resource.resourceId == resourceId {
                return resource
            }
        }
        
        return nil
    }
    
    func addMask(maskInfo: [String: AnyObject], needsDownload: Bool) -> Void {
        var maskEffect = maskInfo["maskOrEfectInfo"]
        if maskEffect == nil {
            maskEffect = maskInfo["mask"]
        }
        
        let mask = maskEffect as! [String: AnyObject]
        var maskObject = self.maskObjectFor(maskId: mask["_id"] as! String)
        let managedContext = NSManagedObjectContext.mr_rootSaving()
        if maskObject == nil {
            maskObject = AKMaskObject.mr_createEntity(in: managedContext)
            maskObject?.maskId = mask["_id"] as! String?
            maskObject?.version = (mask["__v"] as! Float?)!
            maskObject?.active = (mask["active"] as! Bool?)!
            maskObject?.creator = mask["creator"] as! String?
            maskObject?.maskDescription = mask["description"] as! String?
            maskObject?.isFree = (mask["isFree"] as! Bool?)!
            maskObject?.isDefault = (mask["is_default"] as! Bool?)!
            maskObject?.isActive = false
            maskObject?.minVersionReq = (mask["minVersionReq"] as! Float?)!
            maskObject?.name = mask["name"] as! String?
            let package = mask["package"]
            maskObject?.package = NSKeyedArchiver.archivedData(withRootObject: package!) as NSObject?
            maskObject?.price = (mask["price"] as! Float?)!
            maskObject?.isPrivate = (mask["private"] as! Bool?)!
            maskObject?.type = mask["type"] as! String?
        }
        
        let resources = maskInfo["resources"] as! [String : AnyObject]
        
        let thumbs = resources["thumbs"] as! [AnyObject]
        self.addThumbnail(maskObject: maskObject!, thumbs: thumbs)
        self.downloadThumbnailImages(maskObject: maskObject!, thumbs: thumbs)
        
        let actions = mask["actions"] as! [[String : AnyObject]]
        self.addActions(maskObject: maskObject!, actions: actions)
        let hdImages = resources["hd"] as! [AnyObject]
        self.downloadHDImages(maskObject: maskObject!, resources: hdImages)
    }
    
    func addThumbnail(maskObject: AKMaskObject, thumbs : [AnyObject]) -> Void {
        
        let managedContext = NSManagedObjectContext.mr_rootSaving()
        
        for thumb in thumbs {
            let thumbDict = thumb as! [String: AnyObject]
            
            let thumbID = self.createThumbId(maskId: maskObject.maskId!, thumb: thumbDict)
            let thumbAction = self.getThumbAction(thumb: thumbDict)
            
            var thumbObject = self.thumbFor(maskObject: maskObject, thumbId: thumbID)
            if thumbObject == nil {
                thumbObject = AKMaskThumb.mr_createEntity(in: managedContext)
                thumbObject?.thumbId = thumbID
                thumbObject?.action = thumbAction
                thumbObject?.url = thumbDict["url"] as! String?
                thumbObject?.filename = "thumb" + maskObject.maskId! + (thumbDict["filename"] as! String)
                maskObject.addToThumbs(thumbObject!)
            }
        }
        
        let context = maskObject.managedObjectContext
        context?.mr_saveToPersistentStoreAndWait()
        
        print("\(self.masksCount())")
    }
    
    func downloadThumbnailImages(maskObject: AKMaskObject, thumbs : [AnyObject]) -> Void {
        
        for thumb in thumbs {
            let thumbDict = thumb as! [String: AnyObject]
            let url = URL(string : thumbDict["url"] as! String)
            let filename = "thumb" + maskObject.maskId! + (thumbDict["filename"] as! String)
            print(filename)
            let path = NSTemporaryDirectory().appending(filename)
            let image = UIImage(contentsOfFile: path)
            let downloading = UserDefaults.standard.object(forKey: filename)
            let notificationName = Notification.Name(kNotificationDidDownloadDefaultMasks)
            if image == nil && downloading == nil {
                UserDefaults.standard.set(filename, forKey: filename)
                Alamofire.request(url!).responseData(completionHandler: { (response: DataResponse<Data>) in
                    if response.result.isSuccess == true {
                        do {
                            unlink(path.cString(using: .utf8))
                            try response.data?.write(to: URL(fileURLWithPath: path))
                            UserDefaults.standard.removeObject(forKey: filename)
                        } catch {
                            print("error to write image")
                        }
                    } else {
                        self.dismissProgressHUD()
                        UserDefaults.standard.removeObject(forKey: filename)
                        NotificationCenter.default.post(name: notificationName, object: maskObject.maskId!)
                        print("error network timeout")
                    }
                })
            }
        }
        
    }
    
    func addActions(maskObject: AKMaskObject, actions : [[String: AnyObject]]) -> Void {
        
        let managedContext = NSManagedObjectContext.mr_rootSaving()
        let context = maskObject.managedObjectContext
        
        for action in actions {
            let actionId = self.createActionId(maskId: maskObject.maskId!, action: action)
            
            var resourceObject = self.resourceFor(maskObject: maskObject, resourceId: actionId )
            if resourceObject == nil {
                resourceObject = AKMaskResource.mr_createEntity(in: managedContext)
                resourceObject?.resourceId = actionId as String?
                resourceObject?.name = action["name"] as! String?
                resourceObject?.type = action["objType"] as! String?
                resourceObject?.image = "hd" + maskObject.maskId! + (action["name"] as! String) + ".png"
                if let eyes = action["eyes"] {
                    let eyeDict = eyes as! [String: AnyObject]
                    let left = eyeDict["left"] as! [String: AnyObject]
                    let leftX = left["x"] as! NSNumber
                    resourceObject?.leftEyeX = leftX.int64Value
                    let leftY = left["y"] as! NSNumber
                    resourceObject?.leftEyeY = leftY.int64Value
                    let right = eyeDict["right"] as! [String: AnyObject]
                    let rightX = right["x"] as! NSNumber
                    resourceObject?.rightEyeX = rightX.int64Value
                    let rightY = right["y"] as! NSNumber
                    resourceObject?.rightEyeY = rightY.int64Value
                }
                if let bounds = action["bounds"] {
                    let boundDict = bounds as! [String: AnyObject]
                    let x = boundDict["x"] as! NSNumber
                    resourceObject?.faceX = x.int64Value
                    let y = boundDict["y"] as! NSNumber
                    resourceObject?.faceY = y.int64Value
                    let w = boundDict["w"] as! NSNumber
                    resourceObject?.faceWidth = w.int64Value
                    let h = boundDict["h"] as! NSNumber
                    resourceObject?.faceHeight = h.int64Value
                }
                if let script = action["script"] {
                    resourceObject?.script = NSKeyedArchiver.archivedData(withRootObject: script) as NSObject?
                }
                maskObject.addToResources(resourceObject!)
                context?.mr_saveToPersistentStoreAndWait()
            }
        }
        
    }
    
    func downloadHDImages(maskObject: AKMaskObject, resources: [AnyObject]) -> Void {
        var downloads = resources.count - 1
        let isDefault = maskObject.isDefault
        if isDefault == true {
            self.defaultDownloads += (resources.count - 1)
        }
        
        // Download resources
        UserDefaults.standard.set(maskObject.maskId, forKey: maskObject.maskId!)
        for resource in resources {
            let resourceDict = resource as! [String: AnyObject]
            let filename = "hd" + maskObject.maskId! + (resourceDict["filename"] as! String)
            print(filename)
            let url = resourceDict["url"] as! String
            
            // Download resource mask
            let path = NSTemporaryDirectory().appending(filename)
            let image = UIImage(contentsOfFile: path)
            let downloading = UserDefaults.standard.object(forKey: filename)
            let notificationName = Notification.Name(kNotificationDidDownloadMaskThumbs)
            if image == nil && downloading == nil {
                if isDefault == true {
                    //showProgressHUD()
                }
                UserDefaults.standard.set(filename, forKey: filename)
                alamofireManager.request(URL(string: url)!).responseData(completionHandler: { (response: DataResponse<Data>) in
                    if response.result.isSuccess == true {
                        downloads -= 1
                        if downloads == 0 {
                            UserDefaults.standard.removeObject(forKey: maskObject.maskId!)
                            NotificationCenter.default.post(name: notificationName, object: maskObject.maskId)
                        }
                        if isDefault == true {
                            self.defaultDownloads -= 1
                            if self.defaultDownloads == 0 {
                                self.dismissProgressHUD()
                                UserDefaults.standard.removeObject(forKey: maskObject.maskId!)
                                NotificationCenter.default.post(name: notificationName, object: maskObject.maskId)
                            }
                        }
                        do {
                            unlink(path.cString(using: .utf8))
                            try response.data?.write(to: URL(fileURLWithPath: path))
                            UserDefaults.standard.removeObject(forKey: filename)
                        } catch {
                            print("error to write image")
                        }
                    } else {
                        self.dismissProgressHUD()
                        UserDefaults.standard.removeObject(forKey: filename)
                        UserDefaults.standard.removeObject(forKey: maskObject.maskId!)
                        //let notificationName = Notification.Name(kNotificationFailedDownloadMaskThumbs)
                        NotificationCenter.default.post(name: notificationName, object: maskObject.maskId)
                        print("error network timeout")
                    }
                })
            } else {
                downloads -= 1
                if downloads == 0 {
                    UserDefaults.standard.removeObject(forKey: maskObject.maskId!)
                    NotificationCenter.default.post(name: notificationName, object: maskObject.maskId)
                }
                if isDefault == true {
                    self.defaultDownloads -= 1
                    if self.defaultDownloads == 0 {
                        self.dismissProgressHUD()
                        UserDefaults.standard.removeObject(forKey: maskObject.maskId!)
                        NotificationCenter.default.post(name: notificationName, object: maskObject.maskId)
                    }
                }
            }
        }
    }
    
    func downloadMask(maskId: String) -> Void {
        
        AKAPIManager.sharedManager.getMyMaskList(userId: AKAppManager.sharedInstance.myUser.userID) { (response) in
            
            switch response.result {
                
            case .success:
                
                print("Validation Successful")
                
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let jsonDic = json as! NSDictionary
                    let error = AKParser.parseErrorMessage(data: jsonDic)
                    if error == nil {
                        let data = jsonDic["data"]
                        if data is NSNull {
                            return
                        }
                        let masksArray = data as! [[String: AnyObject]]
                        for maskInfo in masksArray {
                            var maskEffect = maskInfo["maskOrEfectInfo"]
                            if maskEffect == nil {
                                maskEffect = maskInfo["mask"]
                            }
                            let mask = maskEffect as! [String: AnyObject]
                            let _id = mask["_id"] as! String
                            if _id == maskId {
                                self.addMask(maskInfo: maskInfo, needsDownload: true)
                                return
                            }
                        }
                    }
                    else
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                    }
                }
            case .failure(let error):
                print(error)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
            }
        }
    }

    
    func isDownloadingMask(maskId: String) -> Bool {
        if UserDefaults.standard.object(forKey: maskId) != nil {
            return true
        } else {
            return false
        }
    }
    
    func showProgressHUD() -> Void {
        //SVProgressHUD.show()
        DispatchQueue.main.async {
            AKGlobal.shared.showCustomActivity(UIApplication.shared.keyWindow!)
        }
    }
    
    func dismissProgressHUD() -> Void {
        //SVProgressHUD.dismiss()
        DispatchQueue.main.async {
            AKGlobal.shared.hideCustomActivity()
        }
    }
    
    func createActionId(maskId : String, action : [String : AnyObject]) -> String {
        
        let name = action["name"] as! String;
        let actionId = "hd" + maskId + name
        return actionId
        
    }
    
    func createThumbId(maskId : String, thumb : [String: AnyObject]) -> String {
        let filename = thumb["filename"] as! NSString;
        let thumbId = ("thumb\(maskId)\(filename.deletingPathExtension)")
        return thumbId
    }
    
    func getThumbAction(thumb : [String: AnyObject]) -> String {
        let filename = thumb["filename"] as! NSString
        return filename.deletingPathExtension
    }
    
}
