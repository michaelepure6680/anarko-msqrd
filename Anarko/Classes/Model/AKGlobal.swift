//
//  AKGlobal.swift
//  Anarko
//
//  Created by x on 9/20/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import RMMapper
import AlamofireImage
import Alamofire
import SVProgressHUD


extension UIButton {
    
    public func setGif(_ name: String) {
        
//        if imageView == nil {
//            imageView = UIImageView(frame: refreshButton.bounds)
//            imageView?.image = UIImage.gif(name: "gif-explorer")
//            refreshButton.addSubview(imageView!)
//        }
        
        self.setImage(UIImage.gif(name: name), for: .normal)

    }
}

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}


class AKGlobal: NSObject {
    
    
    static let shared = AKGlobal()
    let activityIndicator: AKCustomActivityIndicatorView = { () -> AKCustomActivityIndicatorView in
        
        let image = UIImage(named: "icon-load")!
        
        return AKCustomActivityIndicatorView(image: image)
        
    }()

    @objc static func showAlertViewController(title:String , message: String, target: UIViewController) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alertController.addAction(alertAction)
        
        target.navigationController?.present(alertController, animated: true, completion: nil)
        
    }
    
    static func showAlertOnTopController(title : String, message : String, target : UIViewController) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alertController.addAction(alertAction)
        
        if let topController = AKGlobal.getTopController(controller: target) {
            topController.navigationController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func getTopController(controller : UIViewController?) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return AKGlobal.getTopController(controller: navigationController.visibleViewController)
        }
        if let presented = controller?.presentedViewController {
            return AKGlobal.getTopController(controller: presented)
        }
        return controller
    }
    // Show Progres
    
    
    @objc static func showWithStatus(status: String) {
        
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setDefaultMaskType(.gradient)
        
        SVProgressHUD.show(withStatus: status)
        
    }
    
    @objc static func hideProgress() {
        
        SVProgressHUD.dismiss()
    }
    
    func showCustomActivity(_ targetView: UIView) {
        
        
        targetView.addSubview(activityIndicator)
        
        activityIndicator.center = targetView.center
        
        activityIndicator.startAnimating()

    }
    
    func hideCustomActivity() {
        
        activityIndicator.stopAnimating()
    }
    
    static func setCliendId(clientID: String) -> Bool {
        
        let userDefault = UserDefaults.standard
        
        userDefault.set(clientID, forKey: "clientID")
        
        return userDefault.synchronize()
    }
    
    static func getClientID() -> String? {
        
        let userDefault = UserDefaults.standard
        let clientID = userDefault.value(forKey: "clientID")        
        userDefault.synchronize()
        
        let cID = clientID as? String
        
        return cID
    }
    
    static func removeClientID() ->Void {
        let userDefault = UserDefaults.standard
        userDefault.removeObject(forKey: "clientID")
        userDefault.synchronize()
    }
    
    static func saveUserData(user: AKUser) {
        
        let userDefault = UserDefaults.standard
        
        userDefault.rm_setCustomObject(user, forKey: "Current_User")
        

    }
    
    static func getUserData() ->AKUser? {
        
        let userDefault = UserDefaults.standard
        let user = userDefault.rm_customObject(forKey: "Current_User") as? AKUser
        
        return user
    }
    
    static func setImageWithUrl(url: String, targetImageView: UIImageView) {
        
        Alamofire.request(url).responseImage { (response) in
            
            if let image = response.result.value {
                
                targetImageView.image = image
            }

        }
        
    }
    
    static func setImageWithUrlWithLoading(url: String, targetImageView: UIImageView) {
        
        let imageLoadingIndicator: AKCustomActivityIndicatorView = { () -> AKCustomActivityIndicatorView in
            let image = UIImage(named: "icon-load")!
            let frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
            return AKCustomActivityIndicatorView(image: image, frame: frame.insetBy(dx: 4.0, dy: 4.0))
        }()
        
        targetImageView.addSubview(imageLoadingIndicator)
        imageLoadingIndicator.center = CGPoint(x: targetImageView.bounds.size.width / 2.0, y: targetImageView.bounds.size.height / 2.0)
        
        imageLoadingIndicator.startAnimating()
        
        Alamofire.request(url).responseImage { (response) in
            
            if let image = response.result.value {
                targetImageView.image = image
            }
            imageLoadingIndicator.stopAnimating()
        }
        
    }
    
    static func getGifWithUrl(maskId : String, url: NSString, filename : String, completionHandler: @escaping (UIImage?) -> Void) {
        
        let imageName = maskId + filename
        let path = NSTemporaryDirectory().appending(imageName)

        if let image = UIImage.gif(url: "file://\(path)") {
            completionHandler(image)
        }
        else
        {
            Alamofire.request(url as String).responseData { (response) in
                
                var image : UIImage?
                if let data = response.data {
                    do {
                        unlink(path.cString(using: .utf8))
                        try data.write(to: URL(fileURLWithPath: path))
                    } catch {
                        print("error to write image")
                    }
                    image = UIImage.gif(data: data)
                }
                completionHandler(image)
            }
        }
    }
    
    static func phoneNumberValidate(value: String) -> Bool {
        
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", argumentArray: [PHONE_REGEX])
        
        let result = phoneTest.evaluate(with: value)
        
        return result
    }
    
    static func downloadFromUrl(url: String, completionHandler: @escaping (_ data: Data?) -> Void) {
        Alamofire.request(URL(string: url)!).responseData(completionHandler: { (response: DataResponse<Data>) in
            if response.result.isSuccess == true {
                completionHandler(response.data)
            } else {
                completionHandler(nil)
            }
        })
    }
}
