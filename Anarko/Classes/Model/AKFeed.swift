//
//  AKFeed.swift
//  Anarko
//
//  Created by x on 9/22/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKFeed: NSObject {
    
    
    var anarkoId: String!
    var xid: String!
    var anarko: String!
    var location : [Double]!
    var nameUrl: String!
    var tags: [String]!
    var title: String!
    var url: String!
    var views: Int!
//    var comments: [AKComment]!
    var comments: [String]!
    var descriptionStr: String!
    
    /***
     "_id" = 575b1b1a27844f052065c45a;
     "_xid" = 0;
     comments =                 (
     );
     createdAt = "2016-06-10T19:55:07.021Z";
     description = "4.- Pruebas en anarko";
     location =                 (
     "-67.0333",
     "10.35"
     );
     "name_url" = 575b1b1a27844f052065c459;
     tags =                 (
     Venezuela,
     LosTeques,
     Anarko,
     Prueba,
     Playfan
     );
     title = "4.- Video Playfan";
     url = "http://anrk.co/vtgthXO";
     views = 30;
     },
     {
     "_id" = 575b1ada3690e77c1ff569d6;
     "_xid" = 0;
     comments =                 (
     "epa que m\U00e1s "
     );
     createdAt = "2016-06-10T19:54:02.250Z";
     description = "4.- Pruebas en anarko";
     location =                 (
     "-67.0333",
     "10.35"
     );
     "name_url" = 575b1ad93690e77c1ff569d5;
     tags =                 (
     Venezuela,
     LosTeques,
     Anarko,
     Prueba,
     Playfan
     );
     title = "4.- Video Playfan";
     url = "http://anrk.co/vtgs-z7";
     views = 19;
 
    */
    override init() {
        
        
    }
    
    init(data: NSDictionary) {
        
        self.anarkoId = data["_id"] as? String
        self.xid = data["_xid"] as? String
        self.anarko = data["anarko"] as? String
        self.location = data["location"] as? [Double]
        self.nameUrl = data["name_url"] as? String
        self.title = data["title"] as? String
        self.url = data["url"] as? String
        self.views = data["views"] as? Int
        self.comments = data["comments"] as? [String]
        self.descriptionStr = data["description"] as? String
        self.tags = data["tags"] as? [String]
//        self.comments = data[]
        
    }
    

}
