//
//  AKMaskThumb+CoreDataProperties.swift
//  
//
//  Created by Hua Wan on 27/10/2016.
//
//

import Foundation
import CoreData


extension AKMaskThumb {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AKMaskThumb> {
        return NSFetchRequest<AKMaskThumb>(entityName: "AKMaskThumb");
    }

    @NSManaged public var thumbId: String?
    @NSManaged public var action: String?
    @NSManaged public var url: String?
    @NSManaged public var filename: String?
}
