//
//  AKMaskObject+CoreDataProperties.swift
//  
//
//  Created by Hua Wan on 27/10/2016.
//
//

import Foundation
import CoreData
//import 

extension AKMaskObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AKMaskObject> {
        return NSFetchRequest<AKMaskObject>(entityName: "AKMaskObject");
    }

    @NSManaged public var version: Float
    @NSManaged public var maskId: String?
    @NSManaged public var active: Bool
    @NSManaged public var creator: String?
    @NSManaged public var maskDescription: String?
    @NSManaged public var isFree: Bool
    @NSManaged public var isDefault: Bool
    @NSManaged public var isActive: Bool
    @NSManaged public var minVersionReq: Float
    @NSManaged public var name: String?
    @NSManaged public var package: NSObject?
    @NSManaged public var price: Float
    @NSManaged public var isPrivate: Bool
    @NSManaged public var type: String?
    @NSManaged public var maskIndex: Int16
    @NSManaged public var thumbs: NSSet?
    @NSManaged public var resources: NSSet?

}

// MARK: Generated accessors for thumbs
extension AKMaskObject {

    @objc(addThumbsObject:)
    @NSManaged public func addToThumbs(_ value: AKMaskThumb)

    @objc(removeThumbsObject:)
    @NSManaged public func removeFromThumbs(_ value: AKMaskThumb)

    @objc(addThumbs:)
    @NSManaged public func addToThumbs(_ values: NSSet)

    @objc(removeThumbs:)
    @NSManaged public func removeFromThumbs(_ values: NSSet)

}

// MARK: Generated accessors for resources
extension AKMaskObject {

    @objc(addResourcesObject:)
    @NSManaged public func addToResources(_ value: AKMaskResource)

    @objc(removeResourcesObject:)
    @NSManaged public func removeFromResources(_ value: AKMaskResource)

    @objc(addResources:)
    @NSManaged public func addToResources(_ values: NSSet)

    @objc(removeResources:)
    @NSManaged public func removeFromResources(_ values: NSSet)

}
