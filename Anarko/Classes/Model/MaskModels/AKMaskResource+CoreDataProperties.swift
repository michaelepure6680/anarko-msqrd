//
//  AKMaskResource+CoreDataProperties.swift
//  
//
//  Created by Hua Wan on 27/10/2016.
//
//

import Foundation
import CoreData


extension AKMaskResource {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AKMaskResource> {
        return NSFetchRequest<AKMaskResource>(entityName: "AKMaskResource");
    }

    @NSManaged public var name: String?
    @NSManaged public var resourceId: String?
    @NSManaged public var type: String?
    @NSManaged public var image: String?
    @NSManaged public var script: NSObject?
    @NSManaged public var leftEyeX: Int64
    @NSManaged public var leftEyeY: Int64
    @NSManaged public var rightEyeX: Int64
    @NSManaged public var rightEyeY: Int64
    @NSManaged public var faceX: Int64
    @NSManaged public var faceY: Int64
    @NSManaged public var faceWidth: Int64
    @NSManaged public var faceHeight: Int64
    @NSManaged public var any: Int32
}
