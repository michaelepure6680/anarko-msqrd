//
//  AKAppManager.swift
//  Anarko
//
//  Created by x on 9/22/16.
//  Copyright © 2016 Oppous. All rights reserved.
//



import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

class AKAppManager: NSObject, AKLocationManagerDelegate {

    // SingleTone
    static let sharedInstance = AKAppManager()
    
    var locationManager = AKLocationManager.sharedInstance
    
    var myUser: AKUser!
    var vid: String!
    
    var maskId: String!
    var amount: Float = 0.0    
    
    override init() {
        
        super.init()
        
        locationManager.autoUpdate = true
        locationManager.delegate = self
        locationManager.startUpdatingLocationWithCompletionHandler { (latitude, longitude, status, verboseMessage, error) in
            
            if error != nil {
                
                print(error)
            }
            
        }
    }
    
    func openWebPageInSafari(_ type: LinkType){
        
        if UIApplication.shared.canOpenURL(URL(string: type.rawValue)!) {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: type.rawValue)!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
                
                UIApplication.shared.openURL(URL(string: type.rawValue)!)
            }
        }
    }
    
    func locationManagerStatus(_ status:NSString) {
        
        print(status)
    }
    
    func locationManagerReceivedError(_ error:NSString) {
        
        print(error)
    }
    
    func locationFound(_ latitude:Double, longitude:Double) {
        
        print(latitude, longitude)
    }


}
