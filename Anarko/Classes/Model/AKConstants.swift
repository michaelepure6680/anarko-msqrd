//
//  AKConstants.swift
//  Anarko
//
//  Created by x on 9/15/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

struct CellIdentifiers {
    
    static let ReportListCellIdentifier = "ReportListCell"
    static let DescriptionCellIdentifier = "DescriptionCell"
    static let ExpandedDescriptionCellIdentifier = "ExpandedDescriptionCell"
    static let TagCellIdentifier = "TagCell"
    static let CommentCellIdentifier = "CommentCell"
    static let CommentCountCellIdentifier = "CommentCountCell"
    static let ProductCellIdentifier = "ProductCell"
    static let CardCellIdentifier = "CardCell"
    static let AddNewCardCellIdentifier = "AddNewCardCell"
    static let ShareListCellIdentifier = "ShareListCell"
    static let ShareHeaderCellIdentifier = "ShareHeaderCell"
    static let FriendListCellIdentifier = "FriendListCell"
    static let FriendListHeaderCellIdentifier = "FriendListHeaderCell"
}

///////*************** API Key ******************
let BASE_URL = "https://s3.amazonaws.com/static.anarko.city/"
//let BASE_URL = "http://api.anarko.city:8443/api/v1"

// Login

let USER_ID      = "user_id"
let COUNTRY_CODE = "en"
let PHONE       = "phone"
let LANGUAGE    = "lang"
let SMS_CODE    = "code"

//Explore

let TAG = "tags"
let FROM = "from"
let SIZE = "size"


//********************Notification **********************

let kNotificationLoadAnarko                  = "kNotificationLoadAnarko"
let kNotificationGoToProductVC               = "kNotificationGoToProductVC"
let kNotificationSendConfirmationCode        = "kNotificationSendConfirmationCode"
let kNotificationStartTimer                  = "kNotificationStartTimer"

let kNotificationDidDownloadDefaultMasks     = "kNotificationDidDownloadDefaultMasks"
let kNotificationDefaultMaskLoaded           = "kNotificationDefaultMaskLoaded"
let kNotificationDefaultMaskLoading          = "kNotificationDefaultMaskLoading"
let kNotificationDidDownloadMaskThumbs       = "kNotificationDidDownloadMaskThumbs"
let kNotificationFailedDownloadMaskThumbs    = "kNotificationFailedDownloadMaskThumbs"
let kNotificationUploadVideo                 = "kNotificationUploadVideo"
let kNotificationHandleError                 = "kNotificationHandleError"
let kNotificationAppWillEnterForeground      = "kNotificationAppWillEnterForeground"
let kNotificationAppWillEnterBackground      = "kNotificationAppWillEnterBackground"
let kNotificationPurchased                   = "kNotificationPurchased"
let kNotificationPurchasingFailed            = "kNotificationPurchasingFailed"
// *********************** App constant

let ANARKO_FONT_SFUI = "SF-UI-Text-Semibold"
let ANARKO_FONT_GUNPLAY = "Gunplay-Regular"
let ANARKO_COLOR    =   0xf5a100

// ******************** Web Links *************

let ANARKO_GUIDELINE        =   "https://www.anarko.city/guidelines"
let ANARKO_FAQ              =   "https://www.anarko.city/faq"
let ANARKO_TERMS            =   "https://www.anarko.city/terms"
let ANARKO_PRIVACY          =   "https://www.anarko.city/policy"
