//
//  AKMaskCollectionCell.swift
//  Anarko
//
//  Created by Jayden Wan on 11/22/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKMaskCollectionCell: UICollectionViewCell {
    @IBOutlet var thumbImageView: UIImageView!
    @IBOutlet var emptyImageView: UIImageView!
    @IBOutlet var captionLabel: UILabel!
    var parentController : AKCameraViewController?
    
    let activityIndicator: AKCustomActivityIndicatorView = { () -> AKCustomActivityIndicatorView in
        
        let image = UIImage(named: "icon-load")!
        let frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)

        return AKCustomActivityIndicatorView(image: image, frame: frame.insetBy(dx: 4.0, dy: 4.0))
        
    }()
    
    func showActivity() -> Void {
        self.addSubview(activityIndicator)
        
        activityIndicator.center = CGPoint(x: self.bounds.size.width / 2.0, y: self.bounds.size.height / 2.0)
        
        activityIndicator.startAnimating()
    }
    
    func stopActivity() -> Void {
        activityIndicator.stopAnimating()
    }
    
    public var maskObject: AKMaskObject? = nil {
        didSet {
            let allThumbs = maskObject?.thumbs?.allObjects as! [AKMaskThumb]
            for thumb in allThumbs {
                let path = NSTemporaryDirectory().appending(thumb.filename!)
                let image = UIImage(contentsOfFile: path)
                if thumb.action == "normal" && image != nil {
                    thumbImageView.image = image
                }
            }
            
            var isExist = true;
            let allResources = maskObject?.resources?.allObjects as! [AKMaskResource]
            if allResources.count == 0 {
                isExist = false
            } else {
                for resource in allResources {
                    let path = NSTemporaryDirectory().appending(resource.image!)
                    let image = UIImage(contentsOfFile: path)
                    if image == nil {
                        isExist = false
                    }
                }
            }
            
            emptyImageView.isHidden = isExist
            if isExist == false && thumbImageView.image != nil {
                let maskImage = thumbImageView.image?.withOverlayColor(.black)
                let maskLayer = CALayer()
                maskLayer.contents = maskImage?.cgImage
                maskLayer.frame = self.bounds
                emptyImageView.layer.mask = maskLayer
            } else {
                emptyImageView.layer.mask = nil
            }
            if isExist == true {
                UserDefaults.standard.removeObject(forKey: (maskObject?.maskId!)!)
            }
            
            //if AKMaskManager.sharedInstance.isDownloadingMask(maskId: (maskObject?.maskId)!) == true
            if isExist == false || thumbImageView.image == nil
            {
                self.showActivity()
                parentController?.downloadMask(self.maskObject)
                if maskObject?.isDefault == true {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationDefaultMaskLoading), object: nil)
                }
            } else {
                self.stopActivity()
                if maskObject?.isDefault == true {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationDefaultMaskLoaded), object: nil)
                }
            }
        }
    }
}
