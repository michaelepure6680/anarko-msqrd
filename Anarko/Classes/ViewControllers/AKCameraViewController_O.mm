//
//  AKCameraViewController.m
//  Anarko
//
//  Created by Hua Wan on 16/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import "AKCameraViewController.h"

#import <AVFoundation/AVFoundation.h>
#import <Math.h>

#import "Anarko-Swift.h"

#import "UIImage+Resize.h"
#import "AKFaceFeature.h"
#import "FrameRateCalculator.h"

#import "AKVideoCamera.h"

#import <opencv2/imgcodecs/ios.h>
#import <opencv2/imgproc.hpp>

static CGFloat totalDuration = 5.0f;

@interface AKCameraViewController () <AKVideoCameraDelegate>
{
    IBOutlet UIView *cameraView;
    IBOutlet UIView *controlView;
    IBOutlet UIButton *flashButton;
    IBOutlet UIScrollView *masksScrollView;
    IBOutlet UIButton *captureButton;
    IBOutlet UIView *recordingView;
    IBOutlet KDCircularProgress *progressView;
    
    AKVideoCamera* videoCamera;
    BOOL isRecording;
    
    NSURL *recordedURL;
    NSTimer *recordTimer;
    CGFloat recordedTime;
    
    FrameRateCalculator *frameRateCalculator;
    
    NSArray *arrayMaskFeatures;
    AKFaceFeature *selectedEyeFeature;
    AKFaceFeature *detectedEyeFeature;
    cv::Mat selectedMask;
    cv::Mat selectedWink;
    cv::Mat selectedTalk;
    cv::Mat alphaMask;
    cv::Mat alphaWink;
    cv::Mat alphaTalk;
    UIImage *selectedImage;
    NSTimer *faceUpdateTimer;
    CGRect lastFaceRect;
    NSDate *lastFaceDate;
    CGRect lastFaceDrawBounds;
    CGRect lastFaceRenderBounds;
    
    BOOL isNeedFaceDetect;
    BOOL isNeedTake;
    BOOL isFirstLayout;
}
@end

@implementation AKCameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    frameRateCalculator = [[FrameRateCalculator alloc] init];
    
    isNeedTake = NO;
    isRecording = NO;
    isFirstLayout = YES;
    isNeedFaceDetect = YES;
    
    progressView.progressColors = @[[UIColor colorWithRed:0.960 green:0.631 blue:0 alpha:1], [UIColor redColor], [UIColor blackColor]];
    progressView.trackColor = [UIColor clearColor];
    progressView.progressInsideFillColor = [UIColor clearColor];
    progressView.progressThickness = 0.22;
    progressView.trackThickness = 0.22;
    progressView.clockwise = true;
    progressView.gradientRotateSpeed = 1;
    progressView.roundedCorners = NO;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (isFirstLayout == NO)
        [self startVideoCamera];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (isFirstLayout == NO)
        return;
    
    isFirstLayout = NO;
    
    [self initialize];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self stopVideoCamera];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initialize
{
    videoCamera = [[AKVideoCamera alloc] initWithParentView:cameraView];
    videoCamera.delegate = self;
    videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
    videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
    videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    videoCamera.gravityMode = kCAGravityResizeAspectFill;
    videoCamera.defaultFPS = 30;
    videoCamera.imageWidth = 640;
    videoCamera.imageHeight = 480;
    [videoCamera createCaptureOutput];
    
    cameraView.backgroundColor = [UIColor blackColor];
    
    [self configFaces];
    
    [self startVideoCamera];
}

- (void)startVideoCamera
{
    if (faceUpdateTimer)
    {
        [faceUpdateTimer invalidate];
        faceUpdateTimer = nil;
    }
    
    faceUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(handleFaceUpdateTimer:) userInfo:nil repeats:YES];
    
    [videoCamera startCamera];
    videoCamera.recordVideo = NO;
}

- (void)stopVideoCamera
{
    [videoCamera stopCamera];
    videoCamera.recordVideo = NO;
}

- (void)configFaces
{
    NSDictionary *root = [NSDictionary dictionaryWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"MaskFeature" withExtension:@"plist"]];
    arrayMaskFeatures = root[@"MaskFeatures"];
    
    [masksScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    float x = 4;
    float height = masksScrollView.frame.size.height - 8;
    for (int i = 0; i < arrayMaskFeatures.count; i++)
    {
        NSDictionary *mask = arrayMaskFeatures[i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(x, 4, height, height);
        button.tag = i;
        button.layer.cornerRadius = 2.0f;
        button.layer.borderColor = [UIColor whiteColor].CGColor;
        [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@-mask-normal", mask[@"Name"]]] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeMaskPressed:) forControlEvents:UIControlEventTouchUpInside];
        x += height + 4;
        [masksScrollView addSubview:button];
    }
    
    selectedImage = [UIImage imageNamed:@"clown-mask-normal"];
    UIImageToMat(selectedImage, selectedMask, true);
    extractAlpha(selectedMask, alphaMask);
    cvtColor(selectedMask, selectedMask, CV_BGRA2RGBA);
    
    UIImage *image = [UIImage imageNamed:@"clown-mask-wink"];
    UIImageToMat(image, selectedWink, true);
    extractAlpha(selectedWink, alphaWink);
    cvtColor(selectedWink, selectedWink, CV_BGRA2RGBA);
    
    image = [UIImage imageNamed:@"clown-mask-talk"];
    UIImageToMat(image, selectedTalk, true);
    extractAlpha(selectedTalk, alphaTalk);
    cvtColor(selectedTalk, selectedTalk, CV_BGRA2RGBA);
    
    //UIImageWriteToSavedPhotosAlbum(MatToUIImage(selectedMask), nil, nil, nil);
    masksScrollView.contentSize = CGSizeMake(x, height + 8);
    
    selectedEyeFeature = [AKFaceFeature faceFeatureFromDictionary:arrayMaskFeatures[0]];
}

- (void)startWriting
{
    [self abortWriting];
    
    videoCamera.rotateVideo = YES;
    [videoCamera startRecord];
    isRecording = YES;
}

- (void)abortWriting
{
    [videoCamera stopRecord];
    videoCamera.recordVideo = NO;
    isRecording = NO;
}

- (void)stopWriting
{
    [videoCamera stopRecord];
    videoCamera.recordVideo = NO;
    isRecording = NO;
    
    recordedURL = videoCamera.videoFileURL;
    
    //[self performSelector:@selector(gotoEditViewController) withObject:nil afterDelay:0.2f];
}

- (void)gotoEditViewController
{
    [self performSegueWithIdentifier:@"AKEditViewController" sender:nil];
}

- (void)handleFaceUpdateTimer:(NSTimer *)timer
{
    isNeedFaceDetect = YES;
}

- (void)handleRecordTimer:(NSTimer *)timer
{
    recordedTime += timer.timeInterval;
    progressView.startAngle = -90;
    progressView.angle = recordedTime / totalDuration * 360;
    
    if (recordedTime >= totalDuration)
    {
        captureButton.hidden = NO;
        recordingView.hidden = YES;
        
        [self stopWriting];
    
        [recordTimer invalidate];
        recordTimer = nil;
    }
}

- (void)showAlertViewWithMessage:(NSString *)message title:(NSString *)title
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:controller animated:YES completion:nil];
    });
}

- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
            return device;
    }
    
    return nil;
}

#pragma mark Device Counts
- (NSUInteger)cameraCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count];
}

- (NSUInteger)micCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] count];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to `the new view controller.
    if ([segue.identifier isEqualToString:@"AKEditViewController"])
    {
        AKEditViewController *controller = segue.destinationViewController;
        controller.videoURL = recordedURL;
    }
}

#pragma mark - IBAction
- (IBAction)changeCameraPressed:(id)sender {
    [videoCamera switchCameras];
}

- (IBAction)explorerPressed:(id)sender {
    
}

- (IBAction)flashPressed:(id)sender {
    
}

- (IBAction)capturePressed:(id)sender {
    isNeedTake = YES;
    return;
    if (isRecording)
    {
        captureButton.hidden = NO;
        recordingView.hidden = YES;
        
        [self stopWriting];
        
        if (recordTimer)
        {
            [recordTimer invalidate];
            recordTimer = nil;
        }
    }
    else
    {
        captureButton.hidden = YES;
        recordingView.hidden = NO;
        
        [self startWriting];
    
        recordedTime = 0;
        recordTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(handleRecordTimer:) userInfo:nil repeats:YES];
    }
}

- (IBAction)storePressed:(id)sender {
    
}

- (IBAction)changeMaskPressed:(UIButton *)sender {
    NSDictionary *dictionary = arrayMaskFeatures[sender.tag];
    selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@-mask-normal", dictionary[@"Name"]]];
    cv::Mat mask1;
    UIImageToMat(selectedImage, mask1, true);
    extractAlpha(mask1, alphaMask);
    cvtColor(mask1, mask1, CV_BGRA2RGBA);
    selectedMask = mask1;
    
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-mask-wink", dictionary[@"Name"]]];
    cv::Mat mask2;
    UIImageToMat(image, mask2, true);
    extractAlpha(mask2, alphaWink);
    cvtColor(mask2, mask2, CV_BGRA2RGBA);
    selectedWink = mask2;
    
    image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-mask-talk", dictionary[@"Name"]]];
    cv::Mat mask3;
    UIImageToMat(image, mask3, true);
    extractAlpha(mask3, alphaTalk);
    cvtColor(mask3, mask3, CV_BGRA2RGBA);
    selectedTalk = mask3;
    
    selectedEyeFeature = [AKFaceFeature faceFeatureFromDictionary:arrayMaskFeatures[sender.tag]];
}

- (IBAction)handleSwipeGesture:(id)sender {
    
}

#pragma mark - Custom Methods
- (CGImageRef)imageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer // Create a CGImageRef from sample buffer data
{
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer,0);        // Lock the image buffer
    
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);   // Get information of the image
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef image = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    
    CGColorSpaceRelease(colorSpace);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    /* CVBufferRelease(imageBuffer); */  // do not call this!
    
    return image;
}

// rotate UIImage by radian value
- (UIImage *)rotateImage:(UIImage *)image byRadian:(CGFloat)radian
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(radian);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContextWithOptions(rotatedSize, NO, [UIScreen mainScreen].scale);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width / 2, rotatedSize.height / 2);
    
    // Rotate the image context
    CGContextRotateCTM(bitmap, radian);
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-image.size.width / 2, -image.size.height / 2, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *rotateImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return rotateImage;
}

// rotate image by radian value
- (CIImage *)rotateImage:(CIImage *)image radian:(float)radian
{
    CIFilter *filter = [CIFilter filterWithName:@"CIAffineTransform"];
    CGAffineTransform transform = CGAffineTransformMakeRotation(radian);
    [filter setValue:[NSValue valueWithCGAffineTransform:transform] forKey:@"inputTransform"];
    [filter setValue:image forKey:@"inputImage"];
    CIImage *rotatedImage = [filter valueForKey:@"outputImage"];
    
    return rotatedImage;
}

// scale image by scale value
- (CIImage *)scaleImage:(CIImage *)image scale:(float)scale
{
    CIFilter *filter = [CIFilter filterWithName:@"CIAffineTransform"];
    CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
    [filter setValue:[NSValue valueWithCGAffineTransform:transform] forKey:@"inputTransform"];
    [filter setValue:image forKey:@"inputImage"];
    CIImage *scaledImage = [filter valueForKey:@"outputImage"];
    
    return scaledImage;
}

#pragma mark - CvVideoCameraDelegate
- (void)processImage:(cv::Mat&)image
{
    AKFaceFeature *feature = detectedEyeFeature;
    if (isNeedFaceDetect == YES)
    {
        feature = [AKFaceFeature faceFeatureFromMatFrame:image];
        detectedEyeFeature = feature;
        if (feature != nil)
        {
            isNeedFaceDetect = NO;
            
//            if (isNeedTake)
//                UIImageWriteToSavedPhotosAlbum(MatToUIImage(image), nil, nil, nil);
//            isNeedTake = NO;
        }
    }
    
    if (feature == nil)
        return;
    
    cv::Rect faceRect(feature.faceBounds.origin.x, feature.faceBounds.origin.y, feature.faceBounds.size.width, feature.faceBounds.size.height);
    //CGFloat scale = feature.faceBounds.size.width / selectedEyeFeature.faceBounds.size.width;
//    cv::Rect maskRect(-selectedEyeFeature.faceBounds.origin.x * scale, -20, feature.faceBounds.size.width + 40, feature.faceBounds.size.height + 40);
    cv::Mat mask = selectedMask;
    cv::Mat alpha = alphaMask;
    if (feature.faceAngle != 0)
    {
        //rotateImage(selectedMask, dst, feature.faceAngle);
        rotate(selectedMask, feature.faceAngle, mask);
    }
    if (feature.mouthBounds.size.width != 0 && feature.mouthBounds.size.height / feature.faceBounds.size.height >= 0.25)
    {
        mask = selectedTalk;
        alpha = alphaTalk;
        NSLog(@"");
    }
    putImage(image, mask, alpha, faceRect, maskRect, -0.1f);
}

- (void)finishedRecord:(NSURL *)outputURL
{
    recordedURL = outputURL;
    
    [self gotoEditViewController];
}

#pragma mark - OpenCV Face methods
void putImage(Mat &frame, const cv::Mat &image, const cv::Mat &alpha, cv::Rect face, cv::Rect feature, float shift)
{
    // Scale animation image
    float scale = 1.1;
    cv::Size size;
    size.width = scale * feature.width;
    size.height = scale * feature.height;
    cv::Size newSz = cv::Size(size.width,
                              float(image.rows) / image.cols * size.width);
    cv::Mat glasses;
    cv::Mat mask;
    resize(image, glasses, newSz);
    resize(alpha, mask, newSz);
    
    // Find place for animation
    float coeff = (scale - 1.) / 2.;
    cv::Point origin(face.x + feature.x - coeff * feature.width,
                     face.y + feature.y - coeff * feature.height +
                     newSz.height * shift);
    cv::Rect roi(origin, newSz);
    if (0 > roi.x || 0 > roi.width || roi.x + roi.width > frame.cols || 0 > roi.y || 0 > roi.height || roi.y + roi.height > frame.rows)
        return;
    
    Mat roi4glass = frame(roi);
    //UIImageWriteToSavedPhotosAlbum(MatToUIImage(glasses), nil, nil, nil);
    alphaBlendC4(glasses, roi4glass, mask);
}

void alphaBlendC4(const Mat& src, Mat& dst, const Mat& alpha)
{
    for (int i = 0; i < src.rows; i++)
        for (int j = 0; j < src.cols; j++)
        {
            uchar alpha_value = alpha.at<uchar>(i, j);
            if (alpha_value != 0)
            {
                float weight = float(alpha_value) / 255.f;
                dst.at<Vec4b>(i, j) = weight * src.at<Vec4b>(i, j) +
                (1 - weight) * dst.at<Vec4b>(i, j);
            }
        }
}

void extractAlpha(cv::Mat& rgbaSrc, cv::Mat& alpha)
{
    std::vector<Mat> channels;
    split(rgbaSrc, channels);
    channels[3].copyTo(alpha);
}

void rotateImage(const Mat& src, Mat& dst, float angle)
{
    Point2f src_center(src.cols/2.0f, src.rows/2.0f);
    Mat rot_mat = getRotationMatrix2D(src_center, angle, 1.0);
    warpAffine(src, dst, rot_mat, src.size());
}

void rotate(cv::Mat& src, double angle, cv::Mat& dst)
{
    int len = max(src.cols, src.rows);
    cv::Point2f pt(len / 2.0f, len / 2.0f);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
    cv::warpAffine(src, dst, r, cv::Size(len, len));
}

@end
