//
//  AKCameraViewController.swift
//  Anarko
//
//  Created by Hua Wan on 14/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKCameraViewController: UIViewController {

    @IBOutlet var cameraView: UIView!
    @IBOutlet var controlView: UIView!
    @IBOutlet var flashButton: UIButton!
    @IBOutlet var masksScrollView: UIScrollView!
    @IBOutlet var captureButton: UIButton!
    
    var captureSessionPreset: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - IBAction
    @IBAction func changeCameraPressed(sender: AnyObject) -> Void {
        
    }
    
    @IBAction func explorerPressed(sender: AnyObject) -> Void {
        
    }
    
    @IBAction func flashPressed(sender: AnyObject) -> Void {
        
    }
    
    @IBAction func capturePressed(sender: AnyObject) -> Void {
        self.performSegue(withIdentifier: "AKEditViewController", sender: nil)
    }
    
    @IBAction func storePressed(sender: AnyObject) -> Void {
        
    }
    
    @IBAction func changeMaskPressed(sender: AnyObject) -> Void {
        
    }
    
    @IBAction func handleSwipeGesture(sender: AnyObject) -> Void {
        
    }
}
