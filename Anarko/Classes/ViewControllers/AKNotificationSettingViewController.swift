//
//  AKNotificationSettingViewController.swift
//  Anarko
//
//  Created by x on 9/19/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var checkButton: UIButton!
    
    @IBAction func checkButtonTapped(_ sender: AnyObject) {
        
        let button = sender as! UIButton
        
        button.isSelected = !button.isSelected
        
    }
    
}

class AKNotificationSettingViewController: UIViewController {
    
    let titleArray = ["Active Audio Notification",
                      "Active Comment Notification",
                      "Active All Notification"]
    

    @IBOutlet var notificationTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        
        let _ = self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func doneButtonTapped(_ sender: AnyObject) {
    }

}
extension AKNotificationSettingViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        
        cell.titleLbl.text = self.titleArray[indexPath.row]
        return cell
    }
    
}

extension AKNotificationSettingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            break
        case 1:
            break
        case 2:
            break
        default:
            break
        }
        
    }
    
}
