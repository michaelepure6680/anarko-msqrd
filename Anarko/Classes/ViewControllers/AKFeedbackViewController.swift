//
//  AKFeedbackViewController.swift
//  Anarko
//
//  Created by x on 9/20/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKFeedbackViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var describeTextField: UITextField!
    
    @IBOutlet var constraintAlignCenterYOfInputTextField: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKFeedbackViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKFeedbackViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Keyboard notification
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        let deltaH = self.view.bounds.height / 2 - self.describeTextField.bounds.height / 2 - keyboardFrame.height
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            self.constraintAlignCenterYOfInputTextField.constant = deltaH - 10
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
        
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            self.constraintAlignCenterYOfInputTextField.constant = 0
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
        
    }


    @IBAction func backButtonTapped(_ sender: AnyObject?) {
        
        describeTextField.resignFirstResponder()

        let _ = self.navigationController?.popViewController(animated: true)

    }
    
    
    @IBAction func sendButtonTapped(_ sender: AnyObject) {
        
        describeTextField.resignFirstResponder()
        
        if describeTextField.text?.characters.count != 0 {
            
            let paramDic = NSMutableDictionary()
            
            
            paramDic.setObject("\(describeTextField.text!)", forKey: "comment" as NSCopying)
            
            if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
                
                print(dict)
                
                AKGlobal.showWithStatus(status: "Sending...")
                
                AKAPIManager.sharedManager.submitFeedback(params: dict) { (response) in
                    
                    AKGlobal.hideProgress()
                    switch response.result {
                        
                    case .success:
                        
                        print("Validation Successful")
                        
                        if let json = response.result.value {
                            print("JSON: \(json)")
                            
                            let jsonDic = json as! NSDictionary
                            
                            if jsonDic["status"] as! Int  == 1 {
                                
                                self.backButtonTapped(nil)
                                
                            }else {
                                
                                AKGlobal.showAlertViewController(title: "Error!", message:jsonDic["error"] as! String , target: self)
                                
                            }
                            
                            
                        }
                    case .failure(let error):
                        
                        print(error)
                        
                        AKGlobal.showAlertViewController(title: "Error!", message: error.localizedDescription, target: self)
                    }
                    
                }
                
            }

        }
    }
    
    @IBAction func privacyButtonTapped(_ sender: AnyObject) {
        
        describeTextField.resignFirstResponder()

        
    }
    
    @IBAction func termsButtonTapped(_ sender: AnyObject) {
        
        describeTextField.resignFirstResponder()

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
