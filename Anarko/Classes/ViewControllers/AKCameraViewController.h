//
//  AKCameraViewController.h
//  Anarko
//
//  Created by Hua Wan on 16/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AKMaskObject;

@interface AKCameraViewController : UIViewController

@property (nonatomic, strong) NSString *captureSessionPreset;

+ (AKCameraViewController *)loadedViewController;

- (void)downloadMask:(AKMaskObject *)maskObject;

@end
