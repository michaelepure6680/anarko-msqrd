//
//  AKReportViewController.swift
//  Anarko
//
//  Created by HeMin on 9/15/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit



class ReportListCell :UITableViewCell {
    
        
    
    @IBOutlet var reportContentLabel: UILabel!
    
    @IBOutlet var checkButton: UIButton!
    
    var checked: Bool = false
    
    var indexPath: IndexPath!
    var delegate: ReportListCellDelegate?
    
    func selectRadioButton(_ selected: Bool) {
        
        checkButton.isSelected = selected
    }
    
    @IBAction func radioButtonTapped(_ sender: AnyObject) {
        
        let button = sender as! UIButton
        
        button.isSelected = !button.isSelected
        
        self.delegate?.cellSelected(self.indexPath, buttonSelected: button.isSelected)
    }
    
}

@objc protocol ReportListCellDelegate {
    
    @objc func cellSelected(_ indexPath: IndexPath , buttonSelected:Bool)
}

class AKReportViewController: UIViewController, ReportListCellDelegate {
    
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var sendButton: UIButton!
    
    @IBOutlet var reportListTableView: UITableView!
    
    
    var selectedReportArray = NSMutableArray()
    var currentVideoId: String!
    
    let titleArray = ["Sexual Content",
                      "Violent or repulsive content",
                      "Hateful or abusive content",
                      "Harmful dangerous acts",
                      "Child abuse",
                      "Spam or misleading",
                      "Other"]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //
    
    func setVideoId() {
        
        
    }
    
    // MARK: - Delegate
    
    func cellSelected(_ indexPath: IndexPath, buttonSelected: Bool) {
        
        if buttonSelected {
            
            if !selectedReportArray.contains(indexPath) {
                
                for path in selectedReportArray {
                    
                    let cell = reportListTableView.cellForRow(at: (path as? IndexPath)!) as! ReportListCell
                    
                    cell.selectRadioButton(false)
                }
                
                selectedReportArray.removeAllObjects()
                
                selectedReportArray.add(indexPath)
            }
        }else {
            
            if selectedReportArray.contains(indexPath) {
                
                selectedReportArray.remove(indexPath)
            }
        }
        if indexPath.row+1 == titleArray.count && buttonSelected == true {
            sendButton.setTitle("", for: .normal)
            sendButton.setImage(UIImage.init(named: "icon-forward"), for: .normal)
        }
        else
        {
            sendButton.setImage(UIImage.init(named: "icon-message-send-white"), for: .normal)
        }
        print("count --- : \(selectedReportArray.count)")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func checkButtonPressed(_ sender: AnyObject) {
    }
    
    @IBAction func cancelButtonPressed(_ sender: AnyObject) {
        
        self.navigationController?.dismiss(animated: true, completion: { 
            
        })
    }
    
    @IBAction func sendButtonPressed(_ sender: AnyObject) {
        
        if selectedReportArray.count == 0 {
            return
        }
        
        let indexPath = selectedReportArray.object(at: 0) as! IndexPath
        
        if indexPath.row == 6 {
            let reportCommentsVC = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "ReportCommentsVC") as! AKReportCommentsViewController
            reportCommentsVC.setReportContentNumber(6, videoid: self.currentVideoId!)
            self.navigationController?.pushViewController(reportCommentsVC, animated: true)

        }else {
            
            let paramDic = NSMutableDictionary()
            
            
            paramDic.setObject(AKAppManager.sharedInstance.vid!, forKey: "vid_id" as NSCopying)
            paramDic.setObject("\(indexPath.row)", forKey: "reason" as NSCopying)
            paramDic.setObject(AKAppManager.sharedInstance.myUser.phone!, forKey: "phone"  as NSCopying)
            
            if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
                
                print(dict)
                
                AKGlobal.showWithStatus(status: "Reporting...")
                
                AKAPIManager.sharedManager.reportAnarko(params: dict) { (response) in
                    
                    AKGlobal.hideProgress()
                    switch response.result {
                        
                    case .success:
                        
                        print("Validation Successful")
                        
                        if let json = response.result.value {
                            print("JSON: \(json)")
                            let jsonDic = json as! NSDictionary
                            let error = AKParser.parseErrorMessage(data: jsonDic)
                            if error == nil {
                                 self.navigationController?.dismiss(animated: true, completion: nil)
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                            }
                        }
                    case .failure(let error):
                        
                        print(error)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                    }
                    
                }
                
            }

        }
                
        
    }
    
}

extension AKReportViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportListCell", for: indexPath) as! ReportListCell
        cell.reportContentLabel.text = self.titleArray[indexPath.row]
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    
}

extension AKReportViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

