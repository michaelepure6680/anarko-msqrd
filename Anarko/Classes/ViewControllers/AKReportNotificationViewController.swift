//
//  AKReportNotificationViewController.swift
//  Anarko
//
//  Created by Muhammad Khalid on 5/27/17.
//  Copyright © 2017 Oppous. All rights reserved.
//

import UIKit
import Alamofire

class AKReportNotificationViewController: UIViewController {
    
    
    @IBOutlet weak var blockAuthurButton: UIButton!
    @IBOutlet weak var removeVideoButton: UIButton!
    @IBOutlet weak var popupView: UIView!
    var currentVideoId: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.popupView.layer.cornerRadius = 4
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    @IBAction func checkBtnPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func okBtnPressed(_ sender: Any) {
        self.view.removeFromSuperview()
        if blockAuthurButton.isSelected {
            AKGlobal.showWithStatus(status: "Blocking...")
            AKAPIManager.sharedManager.blockVideoAuthor(params: ["videoId" : currentVideoId as AnyObject], completionHandler: { (response) in
                AKGlobal.hideProgress()
                self.handleAPIResponse(response: response)
            })
        }
        else if removeVideoButton.isSelected {
            AKGlobal.showWithStatus(status: "Removing...")
            AKAPIManager.sharedManager.blockVideo(params: ["videoId" : currentVideoId as AnyObject], completionHandler: { (response) in
                AKGlobal.hideProgress()
                self.handleAPIResponse(response: response)
            })
        }
        else {
            self.removeFromParentViewController()
        }
    }
    
    private func handleAPIResponse (response : DataResponse<Any>) {
        
        switch response.result {
        case .success:
            
            if let json = response.result.value {
                print("JSON: \(json)")
                let jsonDic = json as! NSDictionary
                let error = AKParser.parseErrorMessage(data: jsonDic)
                if error == nil {

                    let customAlert = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! AKCustomAlertViewController
                    customAlert.alertType = .Confirmation
                    customAlert.show(title: "", message: "Anarko team will moderate objectionable content within 24 hours", cancelButtonTitle: "", confirmButtonTitle: "Close", confirmBlock: {
                        
                        self.removeFromParentViewController()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationLoadAnarko), object: nil)
                    
                    }, cancelBlock: {
                        
                    })
                }
                else
                {
                    self.removeFromParentViewController()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                }
            }
        case .failure(let error):
            print(error)
            self.removeFromParentViewController()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
        }
        
    }
    
}
