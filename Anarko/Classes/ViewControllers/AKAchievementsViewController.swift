//
//  AKAchievementsViewController.swift
//  Anarko
//
//  Created by x on 9/17/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit


class ShareListCell : UITableViewCell {
    
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var featureTitleLabel: UILabel!
    
}

class ShareHeaderCell: UITableViewCell {
    
}

class AKAchievementsViewController: UIViewController {

    @IBOutlet var inviteButton: UIButton!
    @IBOutlet var shareTableView: UITableView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.shareTableView.rowHeight = UITableViewAutomaticDimension
        self.shareTableView.estimatedRowHeight = 90

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func inviteButtonTapped(_ sender: AnyObject) {
        
        
    }
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        
        let _ = self.navigationController?.popViewController(animated: true)
    }

}

extension AKAchievementsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch section {
        case 0:
            return 2
        case 1:
            return 4
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.ShareListCellIdentifier, for: indexPath) as! ShareListCell
        
        if indexPath.section == 1 {
            
            switch indexPath.row {
                
            case 0:
                cell.descriptionLabel.text = "Share this link to 1,000 people and you'll get"
                cell.featureTitleLabel.text = "A Random Free Effect"

                break
            case 1:
                cell.descriptionLabel.text = "Share this link to 1,000 people and you'll get"
                cell.featureTitleLabel.text = "A Random Free Mask"

                break
            case 2:
                cell.descriptionLabel.text = "Share this link to 5,0000 people and you'll get"
                cell.featureTitleLabel.text = "A Unique Effect(Just for you!)"

                break
            case 3:
                cell.descriptionLabel.text = "Share this link to 5,0000 people and you'll get"
                cell.featureTitleLabel.text = "A Unique Mask(Just for you!)"
                
                break
            default:
                break
            }
            
        }else if(indexPath.section == 0) {
            
            switch indexPath.row {
                
            case 0:
                
                cell.descriptionLabel.text = "If you invite 10 friends you will get"
                cell.featureTitleLabel.text = "A Random Free Effect"
                
                break
            case 1:
                cell.descriptionLabel.text = "If you invite 25 friends you will get"
                cell.featureTitleLabel.text = "A Random Free Mask & Effect"
                
                break
                
            default:
                break
            }

        }
        
        
        return cell
    }
}

extension AKAchievementsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            
            let cell =  tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.ShareHeaderCellIdentifier) as! ShareHeaderCell
            
            return cell
            
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            return 0
        }
        
        return 85
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 73
    }
}


