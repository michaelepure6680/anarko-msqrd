//
//  AKMyPurchaseViewController.swift
//  Anarko
//
//  Created by x on 9/21/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKMyPurchaseViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var itemCollectionView: UICollectionView!
    
    @IBOutlet var maskButton: UIButton!
    
    @IBOutlet var effectButton: UIButton!
    
    @IBOutlet weak var noPurchaseView: UIView!
    
    var isEffectTab = false
    
    var maskArray: [AKMask] = []
    
    var effectArray: [AKEffect] = []
    
    var anarkoProducts : AnarkoProducts?
    
    var isVerifying = false
    var isRestoringIAP = false

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadStoreItems(type: StoreType.Mask.rawValue)
        self.noPurchaseView.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKProductListViewController.handlePurchaseNotification(_:)),
                                               name: NSNotification.Name(rawValue: kNotificationPurchased),
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKProductListViewController.handlePurchasingFailedNotification(_:)),
                                               name: NSNotification.Name(rawValue: kNotificationPurchasingFailed),
                                               object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    private func loadStoreItems(type: String) {
        
        
        AKGlobal.showWithStatus(status: "Loading...")
        
        AKAPIManager.sharedManager.getMyMaskList(userId: AKAppManager.sharedInstance.myUser.userID, onlyPurchased: "true", completionHandler: { (response) in
            
            AKGlobal.hideProgress()
            print(response)
            
            switch response.result {
                
            case .success:
                print("Validation Successful")
                if let json = response.result.value {
                    
                    print("JSON: \(json)")
                    let jsonDic = json as! NSDictionary
                    let error = AKParser.parseErrorMessage(data: jsonDic)
                    if error == nil {
                        if jsonDic["data"] is NSNull {
                            
                            self.itemCollectionView.isHidden = true
                            self.noPurchaseView.isHidden = false
                            return
                        }
                        
                        self.itemCollectionView.isHidden = false
                        
                        let maskArr = AKParser.parseStoreItem(data:(jsonDic["data"] as? NSArray)!)
                        print(maskArr)
                        self.itemCollectionView.isHidden = false
                        
                        if type == StoreType.Mask.rawValue {
                            
                            self.maskArray = maskArr
                            
                            self.itemCollectionView.reloadData()
                            
                            if self.maskArray.count == 0 {
                                self.noPurchaseView.isHidden = false
                            }
                            
                            
                        }else {
                            
//                            self.effectArray = maskArr
                        }
                        
                        self.view.setNeedsLayout()
                    }
                    else
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                    }
                }
            case .failure(let error):
                print(error)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
            }
            
        })
        
    }

    
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        
        let _ = self.navigationController?.popViewController(animated: true)

    }

    // MARK: - Actions
    
    @IBAction func maskButtonTapped(_ sender: AnyObject) {
        
        if isEffectTab {
            
            isEffectTab = false
            
            maskButton.isSelected = true
            maskButton.setTitleColor(UIColor(netHex: ANARKO_COLOR), for: .normal)

            effectButton.isSelected = false
            effectButton.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
    @IBAction func effectButtonTapped(_ sender: AnyObject) {
        
        if isEffectTab == false {
            
            isEffectTab = true
            
            maskButton.isSelected = false
            maskButton.setTitleColor(UIColor.white, for: .normal)
            effectButton.isSelected = true
            effectButton.setTitleColor(UIColor(netHex: ANARKO_COLOR), for: .normal)

        }

    }

    @IBAction func breakitButtonTapped(_ sender: UIButton) {
        
        if let controllers = self.navigationController?.viewControllers {
            for vc in controllers {
                if vc is AKProductListViewController {
                    _ = self.navigationController?.popToViewController(vc as! AKProductListViewController, animated: true)
                }
            }
        }
    }
    
    @IBAction func restorePurchasesTapped(_ sender: Any) {
    
        AKGlobal.showWithStatus(status: "Restoring...")
        isRestoringIAP = true
        if anarkoProducts == nil {
            anarkoProducts = AnarkoProducts(productIds: [])
        }
        anarkoProducts?.store?.restorePurchases()
        
    }
    
    //Verify apple receipt
    func verifyAppleReciept(params : [String : AnyObject]) {
        if isVerifying == true {
            return
        }
        isVerifying = true
        AKAPIManager.sharedManager.verifyAppleReceipt(params: params) { (response) in
            AKGlobal.hideProgress()
            self.isVerifying = false
            switch response.result {
            case .success:
                
                print("Validation Successful")
                
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let jsonDic = json as! NSDictionary
                    let error = AKParser.parseErrorMessage(data: jsonDic)
                    if error == nil {
                        self.loadStoreItems(type: StoreType.Mask.rawValue)
                    }
                    else
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                    }
                }
            case .failure(let error):
                print(error)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
            }
        }
    }
    
    func handlePurchaseNotification(_ notification: Notification) {
    
        isRestoringIAP = false
        do {
            let receiptURL = Bundle.main.appStoreReceiptURL
            let receipt = try Data(contentsOf: receiptURL!)
            let receiptString = receipt.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let paramDic = NSMutableDictionary()
            
            paramDic.setObject(AKAppManager.sharedInstance.myUser.userID!, forKey:"id" as NSCopying)
            paramDic.setObject(receiptString , forKey: "appleReceipt" as NSCopying)
            if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
                self.verifyAppleReciept(params: dict)
            }
            
        }
        catch {
            
        }
        
    }
    
    func handlePurchasingFailedNotification(_ notification: Notification) {
        AKGlobal.hideProgress()
        if isRestoringIAP == false {
            return
        }
        isRestoringIAP = false
        if let error = notification.object as? Error {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
        }
    }
    
}

extension AKMyPurchaseViewController: UICollectionViewDataSource {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.isEffectTab {
            
            return self.effectArray.count
            
        }else {
            
            return self.maskArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.ProductCellIdentifier, for: indexPath) as! ProductCell
        if self.isEffectTab {
            
//            cell.currentItem = self.effectArray[indexPath.item]
            
        }else {
            
            cell.currentItem = self.maskArray[indexPath.item]
        }
        
        return cell
    }
    
}

extension AKMyPurchaseViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
}

extension AKMyPurchaseViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: self.itemCollectionView.bounds.width / 3, height: self.itemCollectionView.bounds.width / 3)
        
        return size
    }
}

