//
//  AKSaveViewController.swift
//  Anarko
//
//  Created by Hua Wan on 15/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import AVFoundation
import NVActivityIndicatorView
import AVKit
import SwiftGifOrigin
import UITextView_Placeholder

class AKSaveViewController: UIViewController, UITextFieldDelegate, NVActivityIndicatorViewable, UITextViewDelegate {

    var videoURL: URL!
    
    var videoTitle = ""
    var thumbImage = UIImage()
    
    @IBOutlet var playerView: UIView!
    @IBOutlet var thumbImageView: UIImageView!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var tagsTextField: UITextField!
    @IBOutlet var exploreButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet var locationTextField: UITextField!
    
    @IBOutlet var constraintBottomSpaceOfContentView: NSLayoutConstraint!
    @IBOutlet var constraintTopSpaceOfContentView: NSLayoutConstraint!
    
    var previewVideoPlayerController: AVPlayerViewController!
    var previewVideoPlayer: AVPlayer!
    var previewVideoAsset: AVAsset!
    var previewPlayerItem: AVPlayerItem!
    var playerLayer: AVPlayerLayer!
    var currentLocation: [String: AnyObject]!
    var alertBar : AlertBar?
    
    var latitude = 0.0
    var longitude = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        thumbImageView.image = thumbImage
        exploreButton.setGif("gif-explorer")
        let placeholderColor = UIColor(red: 0.705, green: 0.705, blue: 0.705, alpha: 1)
        descriptionTextView.placeholder = "Describe What's happening"
        descriptionTextView.placeholderColor = placeholderColor
        
        //AKUtilities.setPlaceholder(locationTextField, attributedString: "wynwood design district", attributes: [NSForegroundColorAttributeName: placeholderColor], fontSize: (locationTextField.font?.pointSize)!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKSaveViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKSaveViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        saveButton.isEnabled = false
        self.showCurrentLocation()
        self.setupPreview(frame: UIScreen.main.bounds)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //playerLayer.frame = playerView.bounds
        previewVideoPlayerController.view.frame = playerView.bounds
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if previewVideoPlayer != nil {
            previewVideoPlayer.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(handleVideoPlayEndNotification(notification:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        previewVideoPlayer.pause()
        previewVideoPlayer.removeObserver(self, forKeyPath: "status")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleVideoPlayEndNotification(notification: Notification) -> Void {
        previewVideoPlayer.seek(to: kCMTimeZero, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        previewVideoPlayer.play()
    }
    
    func setupPreview(frame: CGRect) -> Void {
        if videoURL == nil || previewVideoPlayer != nil {
            return
        }
        
        previewVideoAsset = AVURLAsset(url: videoURL as URL)
        previewPlayerItem = AVPlayerItem(asset: previewVideoAsset)
        previewVideoPlayer = AVPlayer(playerItem: previewPlayerItem)
        /*
        playerLayer = AVPlayerLayer(player: previewVideoPlayer)
        //playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.frame = playerView.bounds
        playerView.layer.addSublayer(playerLayer)
        */
        previewVideoPlayerController = AVPlayerViewController()
        previewVideoPlayerController.player = previewVideoPlayer
        previewVideoPlayerController.view.frame = playerView.bounds
        previewVideoPlayerController.showsPlaybackControls = false
        DispatchQueue.main.async {
            self.playerView.addSubview(self.previewVideoPlayerController.view)
            self.previewVideoPlayerController.view.setNeedsDisplay()
        }
        previewVideoPlayer.play()
        thumbImageView.isHidden = true
    }
    
    func setupAudio() -> Void {
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object as! AVPlayer == self.previewVideoPlayer && keyPath == "status" {
            DispatchQueue.main.async {
                self.previewVideoPlayerController.view.setNeedsDisplay()
            }
        }
    }
    // MARK: - Keyboard notification
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            self.constraintBottomSpaceOfContentView.constant = keyboardFrame.size.height / 3
            self.constraintTopSpaceOfContentView.constant = 0 - keyboardFrame.size.height / 3
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
        
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            self.constraintBottomSpaceOfContentView.constant = 0
            self.constraintTopSpaceOfContentView.constant = 0
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
        
    }

    func showCurrentLocation()
    {
        AKGlobal.shared.showCustomActivity(UIApplication.shared.keyWindow!)
        self.loadCurrentLocation { (location: [String : AnyObject]?) in
            DispatchQueue.main.async {
                AKGlobal.shared.hideCustomActivity()
                if let location = location  {
                    self.currentLocation = location
                    self.locationTextField.text = "\(location["locality"] as! String), \(location["administrativeArea"]! as! String), \(location["country"] as! String)"
                }
                
            }
        }
    }
    
    func loadCurrentLocation(completionHandler: @escaping ([String: AnyObject]?) -> Void) -> Void {
        
        latitude = AKLocationManager.sharedInstance.latitude
        longitude = AKLocationManager.sharedInstance.longitude
        AKLocationManager.sharedInstance.reverseGeocodeLocationWithLatLon(latitude: latitude, longitude: longitude, onReverseGeocodingCompletionHandler: { (info: NSDictionary?, placemark: CLPlacemark?, error: String?) in
            if info != nil
            {
                self.saveButton.isEnabled = true
                self.hideRetryAlertBar()
                completionHandler(info as? [String : AnyObject])
            }
            else{
//                if let error = error , error == "No Placemarks Found!" {
//                    completionHandler(["country":"" as AnyObject, "locality":"" as AnyObject, "administrativeArea":"" as AnyObject])
//                }
                completionHandler(nil)
                self.alertBar = AlertBar.show(.notice, otherButtonsHidden: false, message: "TRY AGAIN", duration: -1, completion: {
                    
                    self.hideRetryAlertBar()
                    _ = self.navigationController?.popViewController(animated: true)
                    
                })
                self.alertBar?.retryCompletion = {
                    self.hideRetryAlertBar()
                    self.showCurrentLocation()
                }
                
            }
        })
    }
    
    func hideRetryAlertBar()
    {
        if let alertbar = self.alertBar {
            alertbar.tempCompletion = nil
            alertbar.hide()
        }
    }
    
    // MARK: - IBAction
    @IBAction func cancelPressed(sender: AnyObject) -> Void {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func explorerPressed(sender: AnyObject) -> Void {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationLoadAnarko), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func savePressed(sender: AnyObject) -> Void {
        
        var tags = self.tagsTextField.text
        if let text = tags, text.characters.count > 0 {
            if tags?.characters.first != "#" {
                tags = "#\(tags!)"
            }
        }
        tags = tags?.replacingOccurrences(of: " ", with: ";")
        tags = "\(currentLocation["country"]!);\(currentLocation["locality"]!);\(tags!)"
        
        let params = NSMutableDictionary()
        
        params.setObject(tags!, forKey: "tags" as NSCopying)
        params.setObject(self.videoTitle, forKey: "title" as NSCopying)
        params.setObject(self.descriptionTextView.text, forKey: "description" as NSCopying)
        params.setObject(self.videoURL, forKey: "videoUrl" as NSCopying)
        
        params.setObject("\(latitude)", forKey: "lt" as NSCopying)
        params.setObject("\(longitude)", forKey: "ln" as NSCopying)
        
        let notificationName = Notification.Name(kNotificationUploadVideo)
        NotificationCenter.default.post(name: notificationName, object: params)
        
        _ = self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string != " " {
            return true
        }
        let nsString = textField.text as NSString?
        var newString = nsString?.replacingCharacters(in: range, with: string)
        let tags = (newString?.components(separatedBy: " "))! as Array<String>
        for tag in tags {
            if tag.characters.count > 0 && tag.contains("#") == false {
                let newTag = "#\(tag)"
                newString = newString?.replacingOccurrences(of: "\(newTag) ", with: "")
                newString = newString?.replacingOccurrences(of: " \(tag)", with: " \(newTag)")
            }
        }
        
        newString = newString?.replacingOccurrences(of: "  ", with: " ")
        newString = newString?.replacingOccurrences(of: "# ", with: "")
        textField.text = newString
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        var newString = textField.text
        let tags = (newString?.components(separatedBy: " "))! as Array<String>
        for tag in tags {
            if tag.characters.count > 0 && tag.contains("#") == false {
                let newTag = "#\(tag)"
                newString = newString?.replacingOccurrences(of: "\(newTag) ", with: "")
                newString = newString?.replacingOccurrences(of: " \(tag)", with: " \(newTag)")
                newString = newString?.replacingOccurrences(of: "  ", with: " ")
            }
        }
        
        if newString?.characters.count == 2 {
            newString = newString?.replacingOccurrences(of: " #", with: "")
        }
        textField.text = newString
        
        return true
    }
    
    // MARK: - UITextViewDelegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView .resignFirstResponder()
            return false
        }
        
        return true
    }
}
