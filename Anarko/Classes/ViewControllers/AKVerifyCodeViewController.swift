//
//  AKVerifyCodeViewController.swift
//  Anarko
//
//  Created by x on 9/19/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKVerifyCodeViewController: UIViewController, UITextFieldDelegate, SMSCodeTextFieldDelegate {

    
    
    @IBOutlet var constraintBottomSpaceOfView: NSLayoutConstraint!
    
    @IBOutlet var constraintTopSpaceOfView: NSLayoutConstraint!
    @IBOutlet var contentView: UIView!
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var verifyView: UIView!
    @IBOutlet var verifyInputView: UIView!
    
    @IBOutlet var resendButton: UIButton!
    
    @IBOutlet var codeInputTextFieldCollection: [UITextField]!
    
    private var defaultBottomSpace: CGFloat = 0
    var resendTimer : Timer!
    var resendTimeCounter = 0
    var titleStr:String!
    
    let FINAL_TAG = 17
    
    
    
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        for textField in self.codeInputTextFieldCollection {
            
            textField.delegate = self
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AKPNChangeViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //Register  text change Notification
        NotificationCenter.default.addObserver(self, selector: #selector(AKVerifyCodeViewController.textFieldDidChange(_:)), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKVerifyCodeViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKVerifyCodeViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        for i in 10..<17 {
            if let textField = verifyInputView.viewWithTag(i) as? SMSCodeTextField {
                textField.codeDelegate = self
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.titleLabel.text = self.titleStr!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Private 
    
    func startTimer() {
        
        resendTimeCounter = 60
        resendTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(handleResendTimer(timer:)), userInfo: nil, repeats: true)
    }
    
    func handleResendTimer(timer: Timer) -> Void {
        self.resendTimeCounter -= 1
        
        self.resendButton.setTitle("RESEND CODE IN \(String(format: "%.2d", self.resendTimeCounter / 60)):\(String(format: "%.2d", self.resendTimeCounter % 60))", for: .normal)
        
        if self.resendTimeCounter <= 0 {
            self.resendTimer.invalidate()
            self.resendTimeCounter = 0
            
            self.resendButton.setTitle("RESEND CODE", for: .normal)
            self.resendButton.isEnabled = true
        }
    }
    
    func setTitleString(_ t: String) {
     
        self.titleStr = t
    }
    
    //
    
    func verifyActivationCode() -> Void {
        
        var activationCode = ""
        for i in 10..<17 {
            if let textField = verifyInputView.viewWithTag(i) as? UITextField {
                activationCode += textField.text!
            }
        }
        
        if activationCode.characters.count == 7 {
            self.authorizeWithSMSCode(verifyCode: activationCode)
        }
    }

    private func authorizeWithSMSCode(verifyCode: String) {
        
        if let _ = AKAppManager.sharedInstance.myUser {
            
            let paramDic = NSMutableDictionary()
            
            paramDic.setObject(self.titleLabel.text!, forKey:PHONE as NSCopying)
            paramDic.setObject(verifyCode, forKey: SMS_CODE as NSCopying)
            
            if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
                
                print(dict)
                AKGlobal.showWithStatus(status: "Changing...")
                AKAPIManager.sharedManager.verifyChangedNumber(params: dict, completionHandler: { (response) in
                    AKGlobal.hideProgress()
                    switch response.result {
                        
                    case .success:
                        
                        print("Validation Successful")
                        
                        if let json = response.result.value {
                            print("JSON: \(json)")
                            
                            let jsonDic = json as! NSDictionary
                            
                            if jsonDic["status"] as! Int  == 1 {
                                
                                DispatchQueue.main.async {
                                    AKMaskManager.sharedInstance.deleteAllMask()
                                    AKGlobal.removeClientID()
                                    NotificationCenter.default.removeObserver(self)
                                    let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! AKHomeViewController
                                    self.navigationController?.pushViewController(loginVC, animated: true)
                                    AKGlobal.showAlertViewController(title: "", message: "Number changed successfully.", target: self)
                                }
                                
                            }else {
                                
                                _ = AlertBar.show(.notice, message: "WRONG CODE", duration: 0.5, completion: nil)
                                
                            }                           
                            
                        }
                    case .failure(let error):
                        
                        print(error)
                        
                        AKGlobal.showAlertViewController(title: "Error!", message: error.localizedDescription, target: self)
                    }
                    
                })
                
            }
            
        }
        
        
    }
    @IBAction func resendButtonTapped(_ sender: Any) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSendConfirmationCode), object: true)
    }

    func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    func isBackSpaceTapped(string : String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true;
        }
        else
        {
            return false
        }
    }
    
    func textFieldDidDelete(textField : UITextField) -> Void {
        if let count = textField.text?.characters.count, count > 0 {
            textField.text = ""
        }
        else if let previousTextField = verifyInputView.viewWithTag(textField.tag - 1) as? UITextField {
            previousTextField.text = ""
            previousTextField.becomeFirstResponder()
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidChange(_ notification: Notification) {
        
        let textField = notification.object as! UITextField
        
        let tag = textField.tag + 1
        
        if tag == FINAL_TAG {
            
            
            self.verifyActivationCode()
            self.view.endEditing(true)
            
            return
        }
        
        let nextTextField = self.verifyView.viewWithTag(tag) as? UITextField
        
        if nextTextField != nil {
            
            nextTextField?.becomeFirstResponder()
        }
        
     
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag >= 10 {
            if self.isBackSpaceTapped(string: string) {
                return true
            }
            let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
            if newLength > 1 {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }

    // MARK: - Keyboard notification
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        
        if self.constraintTopSpaceOfView.constant == defaultBottomSpace {
            
            var info = (notification as NSNotification).userInfo!
            
            let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
            let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
            
            let deltaH = self.contentView.bounds.height - self.verifyView.frame.origin.y - self.verifyView.bounds.height
            
            let alphaH = deltaH - keyboardFrame.size.height
            
            UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
                
                self.constraintTopSpaceOfView.constant = alphaH
                self.view.layoutIfNeeded()
                
            }) { (finished) in
                
            }

        }
        
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            self.constraintTopSpaceOfView.constant = self.defaultBottomSpace
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
        
    }

    @IBAction func cancelButtonTapped(_ sender: AnyObject) {
        
        let _ = self.navigationController?.popViewController(animated: true)

    }
    
}
