//
//  AKCameraViewController.m
//  Anarko
//
//  Created by Hua Wan on 16/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import "AKCameraViewController.h"
//#import "AKProductListViewController.swift"
#import <GLKit/GLKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <math.h>

#import <UserNotifications/UserNotifications.h>

#import "Anarko-Swift.h"

#import "UIImage+Resize.h"
#import "AKFaceFeature.h"
#import "FrameRateCalculator.h"

#import <mach/mach_time.h>
#import <sys/time.h>

#import "UIDeviceHardware.h"
#include "visageVision.h"
#import <MagicalRecord/MagicalRecord.h>

#define FACE_TRACKER
#define FRAME_BGRA      1

namespace VisageSDK
{
    void initializeLicenseManager(const char *licenseKeyFileFolder);
}

// enum for image orientation
enum {
    PHOTOS_EXIF_0ROW_TOP_0COL_LEFT          = 1, //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
    PHOTOS_EXIF_0ROW_TOP_0COL_RIGHT         = 2, //   2  =  0th row is at the top, and 0th column is on the right.
    PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT      = 3, //   3  =  0th row is at the bottom, and 0th column is on the right.
    PHOTOS_EXIF_0ROW_BOTTOM_0COL_LEFT       = 4, //   4  =  0th row is at the bottom, and 0th column is on the left.
    PHOTOS_EXIF_0ROW_LEFT_0COL_TOP          = 5, //   5  =  0th row is on the left, and 0th column is the top.
    PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP         = 6, //   6  =  0th row is on the right, and 0th column is the top.
    PHOTOS_EXIF_0ROW_RIGHT_0COL_BOTTOM      = 7, //   7  =  0th row is on the right, and 0th column is the bottom.
    PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM       = 8  //   8  =  0th row is on the left, and 0th column is the bottom.
};

// filtering value for smooth moving of face
static float kFilteringFactor = 0.1f;
static float kOffsetFactor = 48.0f;
static CGFloat totalDuration = 15.0f;
static CGColorSpaceRef sDeviceRgbColorSpace = nil;

//Recording view frame values
static CGFloat rcViewDefaultWidth = 67.0f;
static CGFloat rcViewDefaultHeight = 67.0f;
static CGFloat rcViewRecordingWidth = 100.0f;
static CGFloat rcViewRecordingHeight = 100.0f;

int cam_width;
int cam_height;
static int trackerStatus = TRACK_STAT_OFF;
int _rotated = 0;
int width;
int height;
int _device = 0;

static AKCameraViewController *_loadedViewController = nil;

static CGAffineTransform FCGetTransformForDeviceOrientation(UIDeviceOrientation orientation, BOOL mirrored)
{
    // Internal comment: This routine assumes that the native camera image is always coming from a UIDeviceOrientationLandscapeLeft (i.e. the home button is on the RIGHT, which equals AVCaptureVideoOrientationLandscapeRight!), although in the future this assumption may not hold; better to get video output's capture connection's videoOrientation property, and apply the transform according to the native video orientation
    // Also, it may be desirable to apply the flipping as a separate step after we get the rotation transform
    CGAffineTransform result;
    switch (orientation) {
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationFaceUp:
        case UIDeviceOrientationFaceDown:
            result = CGAffineTransformMakeRotation(M_PI_2);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            result = CGAffineTransformMakeRotation((3 * M_PI_2));
            break;
        case UIDeviceOrientationLandscapeLeft:
            result = mirrored ?  CGAffineTransformMakeRotation(M_PI) : CGAffineTransformIdentity;
            break;
        default:
            result = mirrored ? CGAffineTransformIdentity : CGAffineTransformMakeRotation(M_PI);
            break;
    }
    
    return result;
}

@interface AKCameraViewController () <AVCaptureVideoDataOutputSampleBufferDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, AKProductListViewControllerDelegate>
{
    IBOutlet UIView *cameraView;
    IBOutlet UIView *controlView;
    IBOutlet UIButton *flashButton;
    IBOutlet UICollectionView *masksCollectionView;
    IBOutlet UIScrollView *masksScrollView;
    IBOutlet UIButton *captureButton;
    IBOutlet UIView *recordingView;
    IBOutlet KDCircularProgress *progressView;
    IBOutlet UIButton *logoButton;
    __weak IBOutlet UIView *tipsView1;
    __weak IBOutlet UIView *tipsView2;
    __weak IBOutlet UILabel *maskYourFaceLabel;
    
    IBOutlet NSLayoutConstraint *widthProgressConstraint;
    IBOutlet NSLayoutConstraint *topProgressConstraint;
    IBOutlet NSLayoutConstraint *bottomProgressConstraint;
    __weak IBOutlet NSLayoutConstraint *recordingViewHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *recordingViewWidthConstraint;
    __weak IBOutlet NSLayoutConstraint *collectionViewBottomConstraint;
    
    __weak IBOutlet UILabel *maskNameLabel;
    NSTimer *maskNameAnimationTimer;
    __weak IBOutlet NSLayoutConstraint *maskNameLeadingSpaceConstraint;
    __weak IBOutlet NSLayoutConstraint *maskNameTrailingSpaceConstraint;
    
    GLKView *videoPreviewView;
    CIContext *ciContext;
    EAGLContext *eaglContext;
    CGRect videoPreviewViewBounds;
    
    AVCaptureDevice *videoDevice;
    AVCaptureSession *captureSession;
    
    AVAssetWriter *assetWriter;
    AVAssetWriterInput *assetWriterVideoInput;
    AVAssetWriterInputPixelBufferAdaptor *assetWriterInputPixelBufferAdaptor;
    NSURL *recordedURL;
    NSURL *audioURL;
    NSTimer *recordTimer;
    CGFloat recordedTime;
    
    CIDetector *faceDetector;
    
    dispatch_queue_t captureSessionQueue;
    UIBackgroundTaskIdentifier backgroundRecordingID;
    
    BOOL videoWritingStarted;
    CMTime videoWrtingStartTime;
    CMVideoDimensions currentVideoDimensions;
    CMTime currentVideoTime;
    FrameRateCalculator *frameRateCalculator;
    
    AKFaceFeature *selectedEyeFeature;
    AKMaskObject *selectedMaskObject;
    NSInteger selectedMaskIndex;
    CIImage *selectedFace;
    CIImage *selectedWink;
    CIImage *selectedTalk;
    CIImage *faceImage;
    UIImage *selectedImage;
    NSTimer *faceUpdateTimer;
    CGRect lastFaceRect;
    NSDate *lastFaceDate;
    CGRect lastFaceDrawBounds;
    CGRect lastFaceRenderBounds;
    
    BOOL isNeedFaceDetect;
    BOOL isNeedTake;
    BOOL isFirstLayout;
    BOOL isDownloading;
    BOOL isSelectFirstMask;
    BOOL isFrontCamera;
    BOOL downloadingPopupShowing;
    
    uint8_t *_buffers;
    uint8_t *_bufferR90;
    
    VisageSDK::VisageTracker* tracker;
    VisageSDK::FaceData trackingData;
}

@end

@implementation AKCameraViewController

+ (AKCameraViewController *)loadedViewController
{
    return _loadedViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _loadedViewController = self;
    isFrontCamera = YES;
    flashButton.hidden = YES;
    isSelectFirstMask = YES;
    
    [self initUI];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    
    if (isFirstLayout == NO)
        return;
    
    isFirstLayout = NO;
    
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (isFirstLayout == NO)
        [self startVideoRecorder];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDidFinishDownloadDefaultMasks:) name:@"kNotificationDidDownloadDefaultMasks" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDidFinishDownloadMaskThumbs:) name:@"kNotificationDidDownloadMaskThumbs" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDefaultMaskLoaded:) name:@"kNotificationDefaultMaskLoaded" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDefaultMaskLoading:) name:@"kNotificationDefaultMaskLoading" object:nil];
    
    BOOL tipsShowed = [[NSUserDefaults standardUserDefaults] boolForKey:@"tipsShowed"];
    if (!tipsShowed) {
        tipsView1.hidden = NO;
        tipsView2.hidden = NO;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tipsShowed"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [AKGlobal showWithStatusWithStatus:@"Downloading your new identity"];
        downloadingPopupShowing = YES;
    }
    else
    {
        tipsView1.hidden = YES;
        tipsView2.hidden = YES;
    }
    
    maskNameLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
    maskNameLabel.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    maskNameLabel.layer.shadowRadius = 3.0;
    maskNameLabel.layer.shadowOpacity = 0.8;
    maskNameLabel.layer.masksToBounds = NO;
    maskNameLabel.layer.shouldRasterize = YES;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self stopVideoRecorder];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotificationDidDownloadDefaultMasks" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotificationDidDownloadMaskThumbs" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotificationDefaultMaskLoaded" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotificationDefaultMaskLoading" object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[AKMaskManager sharedInstance] downloadAvailableMasks];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSInteger totalArrayCount = [[AKMaskManager sharedInstance] masksCount];
    if (selectedMaskIndex+1 <= totalArrayCount)
    {
        [masksCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:selectedMaskIndex + 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self collectionView:masksCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:selectedMaskIndex + 1 inSection:0]];
        });
    }
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if (selectedMaskIndex > 1)
    {
        [masksCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:selectedMaskIndex - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self collectionView:masksCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:selectedMaskIndex - 1 inSection:0]];
        });
    }
}

- (void)handleDidFinishDownloadDefaultMasks:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        isDownloading = NO;
        [self loadActiveMask];
        [masksCollectionView reloadData];
    });
}

- (void)handleDidFinishDownloadMaskThumbs:(NSNotification *)notification
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [masksCollectionView reloadData];
    });
}

- (void)handleDefaultMaskLoaded:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (isSelectFirstMask == YES) {
            isSelectFirstMask = NO;
            [self selectFirstMask];
        }
        if(downloadingPopupShowing) {
            downloadingPopupShowing = NO;
            [AKGlobal hideProgress];
        }
    });
}

- (void)handleDefaultMaskLoading:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(!downloadingPopupShowing) {
            [AKGlobal showWithStatusWithStatus:@"Downloading your new identity"];
            downloadingPopupShowing = YES;
        }
    });
}

-(void)selectFirstMask{
    [self collectionView:masksCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
}
- (void)initUI
{
    if (sDeviceRgbColorSpace == nil)
        sDeviceRgbColorSpace = CGColorSpaceCreateDeviceRGB();
    
    frameRateCalculator = [[FrameRateCalculator alloc] init];
    
    isFirstLayout = YES;
    
    progressView.progressColors = @[[UIColor colorWithRed:0.960 green:0.631 blue:0 alpha:1], [UIColor redColor], [UIColor blackColor]];
    progressView.trackColor = [UIColor clearColor];
    progressView.progressInsideFillColor = [UIColor clearColor];
    progressView.progressThickness = 0.20;
    progressView.trackThickness = 0.20;
    progressView.clockwise = true;
    progressView.gradientRotateSpeed = 1;
    progressView.roundedCorners = NO;
    
    if ([UIScreen mainScreen].bounds.size.width > 375)
    {
        widthProgressConstraint.constant = 10;
        topProgressConstraint.constant = -8;
        bottomProgressConstraint.constant = -8;
    }
    
    isDownloading = NO;
    if ([[AKMaskManager sharedInstance] masksCount] == 0)
        isDownloading = YES;
    
    [logoButton setGif:@"gif-explorer"];
    
    [self loadActiveMask];
}

- (void)initialize
{
    if (videoPreviewView)
        return;
    
    cameraView.backgroundColor = [UIColor blackColor];
    
    // setup the GLKView for video/image preview
    eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    videoPreviewView = [[GLKView alloc] initWithFrame:cameraView.bounds context:eaglContext];
    
    videoPreviewView.enableSetNeedsDisplay = NO;
    
    [cameraView insertSubview:videoPreviewView belowSubview:controlView];
    videoPreviewViewBounds = videoPreviewView.bounds;
    
    CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI_2);
    transform = CGAffineTransformTranslate(transform, fabs(videoPreviewViewBounds.size.height - videoPreviewViewBounds.size.width) / 2.0,
                                           fabs(videoPreviewViewBounds.size.height - videoPreviewViewBounds.size.width) / 2.0);
    videoPreviewView.transform = transform;
    videoPreviewView.frame = cameraView.bounds;
    
    // create the CIContext instance, note that this must be done after videoPreviewView is properly set up
    ciContext = [CIContext contextWithEAGLContext:eaglContext options:@{kCIContextWorkingColorSpace : [NSNull null]} ];
    
    // bind the frame buffer to get the frame buffer width and height;
    // the bounds used by CIContext when drawing to a GLKView are in pixels (not points),
    [videoPreviewView bindDrawable];
    videoPreviewViewBounds = CGRectZero;
    videoPreviewViewBounds.size.width = videoPreviewView.drawableWidth;
    videoPreviewViewBounds.size.height = videoPreviewView.drawableHeight;
    
    captureSessionQueue = dispatch_queue_create("capture_session_queue", NULL);
    dispatch_set_target_queue(captureSessionQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0));
    
    NSDictionary * options = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:0] forKey:CIDetectorImageOrientation];
    CIContext *faceContext = [CIContext contextWithOptions:options];
    faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:faceContext options:@{CIDetectorAccuracy : CIDetectorAccuracyLow }];
    
    [self startVideoRecorder];
    
    VisageSDK::initializeLicenseManager("603-021-191-300-792-311-960-224-261-460-922.vlc");
    
    NSString* deviceType = [UIDeviceHardware platform];
#ifdef FACE_TRACKER
    if ([deviceType hasPrefix:@"iPhone3"] ||           // iPhone4
        [deviceType hasPrefix:@"iPhone4"] ||           // iPhone4S
        [deviceType hasPrefix:@"iPad2"]                // iPad2
        )
        tracker = new VisageTracker("Facial Features Tracker - Low.cfg");
    else
        tracker = new VisageTracker("Facial Features Tracker - High.cfg");      // all other devices
#else
    tracker = new VisageTracker("Head Tracker.cfg");
#endif
}

- (void)loadActiveMask
{
    AKMaskObject *maskObject = nil;
    for (int i = 0; i < [[AKMaskManager sharedInstance] masksCount]; i++)
    {
        AKMaskObject *object = [[AKMaskManager sharedInstance] maskObjectAtIndex:i];
        NSArray *allObjects = object.resources.allObjects;
        int imageCount = 0;
        if (object.isActive == YES)
        {
            maskObject = object;
            break;
        }
        else if (maskObject == nil && object.isDefault == YES && allObjects.count != 0)
        {
            for (AKMaskResource *resource in allObjects)
            {
                NSString *filepath = [NSTemporaryDirectory() stringByAppendingPathComponent:resource.image];
                UIImage *image = [UIImage imageWithContentsOfFile:filepath];
                if (image != nil)
                    imageCount += 1;
            }
            
            if (imageCount == allObjects.count)
                maskObject = object;
        }
    }
    
    if (maskObject == nil)
        return;
    
    AKMaskResource *maskResource = nil;
    for (AKMaskResource *resource in maskObject.resources.allObjects)
    {
        if ([resource.name isEqualToString:@"normal"])
        {
            NSString *filepath = [NSTemporaryDirectory() stringByAppendingPathComponent:resource.image];
            UIImage *image = [[UIImage imageWithContentsOfFile:filepath] rotateImageByRadian:-M_PI_2];
            selectedImage = image;
            selectedFace = image.CIImage;
            if (selectedFace == nil)
                selectedFace = [CIImage imageWithCGImage:image.CGImage];
        }
        else if ([resource.name isEqualToString:@"wink"])
        {
            NSString *filepath = [NSTemporaryDirectory() stringByAppendingPathComponent:resource.image];
            UIImage *image = [[UIImage imageWithContentsOfFile:filepath] rotateImageByRadian:-M_PI_2];
            selectedWink = image.CIImage;
            if (selectedWink == nil)
                selectedWink = [CIImage imageWithCGImage:image.CGImage];
        }
        else if ([resource.name isEqualToString:@"talk"])
        {
            NSString *filepath = [NSTemporaryDirectory() stringByAppendingPathComponent:resource.image];
            UIImage *image = [[UIImage imageWithContentsOfFile:filepath] rotateImageByRadian:-M_PI_2];
            selectedTalk = image.CIImage;
            if (selectedTalk == nil)
                selectedTalk = [CIImage imageWithCGImage:image.CGImage];
        }
         maskResource = resource;
    }
    
    selectedMaskObject = maskObject;
    selectedEyeFeature = [AKFaceFeature faceFeatureFromMaskObject:maskResource];
}

- (void)startVideoRecorder
{
    if (faceUpdateTimer)
    {
        [faceUpdateTimer invalidate];
        faceUpdateTimer = nil;
    }
    
    [[AKAudioRecorder sharedInstance] startAudioSession];
    
    if ([[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count] > 0)
    {
        dispatch_async(captureSessionQueue, ^(void) {
            NSError *error = nil;
            
            // get the input device and also validate the settings
            NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
            
            AVCaptureDevicePosition position = AVCaptureDevicePositionFront;
            
            videoDevice = nil;
            for (AVCaptureDevice *device in videoDevices)
            {
                if (device.position == position) {
                    videoDevice = device;
                    break;
                }
            }
            
            if (!videoDevice)
                videoDevice = [videoDevices objectAtIndex:0];
            
            [videoDevice lockForConfiguration:nil];
            [videoDevice unlockForConfiguration];
            // obtain device input
            AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
            if (!videoDeviceInput)
            {
                [self showAlertViewWithMessage:[NSString stringWithFormat:@"Unable to obtain video device input, error: %@", error] title:@"Error"];
                return;
            }
            
            // obtain the preset and validate the preset
            if (_captureSessionPreset == nil || ![videoDevice supportsAVCaptureSessionPreset:_captureSessionPreset])
                _captureSessionPreset = AVCaptureSessionPreset640x480;
//                _captureSessionPreset = AVCaptureSessionPreset352x288;
            
            if (![videoDevice supportsAVCaptureSessionPreset:_captureSessionPreset])
            {
                [self showAlertViewWithMessage:[NSString stringWithFormat:@"Capture session preset not supported by video device: %@", _captureSessionPreset] title:@"Error"];
                return;
            }
            
            // CoreImage wants BGRA pixel format
#if FRAME_BGRA
            NSDictionary *outputSettings = @{ (id)kCVPixelBufferPixelFormatTypeKey : [NSNumber numberWithInteger:kCVPixelFormatType_32BGRA]};
#else
            NSDictionary *outputSettings = @{ (id)kCVPixelBufferPixelFormatTypeKey : [NSNumber numberWithInteger:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange]};
#endif
            // create the capture session
            captureSession = [[AVCaptureSession alloc] init];
            captureSession.sessionPreset = _captureSessionPreset;
            
            // create and configure video data output
            AVCaptureVideoDataOutput *videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
            videoDataOutput.videoSettings = outputSettings;
            videoDataOutput.alwaysDiscardsLateVideoFrames = YES;
            [videoDataOutput setSampleBufferDelegate:self queue:captureSessionQueue];
            
            // begin configure capture session
            [captureSession beginConfiguration];
            
            if (![captureSession canAddOutput:videoDataOutput])
            {
                [self showAlertViewWithMessage:@"Cannot add video data output" title:@"Error"];
                captureSession = nil;
                return;
            }
            
            // connect the video device input and video data and still image outputs
            [captureSession addInput:videoDeviceInput];
            [captureSession addOutput:videoDataOutput];
            
            [captureSession commitConfiguration];
            
            // then start everything
            [frameRateCalculator reset];
            [captureSession startRunning];
            
            isNeedFaceDetect = YES;
            faceUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(handleFaceUpdateTimer:) userInfo:nil repeats:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSLog(@"startVideoRecorder");
            });
        });
    }
}

- (void)stopVideoRecorder
{
    if (!captureSession || !captureSession.running)
        return;
    
//    [[AKAudioRecorder sharedInstance] stopAudioSession];
    
    [captureSession stopRunning];
    
//    dispatch_sync(captureSessionQueue, ^{
//        NSLog(@"waiting for capture session to end");
//    });
    
    [self stopWriting];
    
    captureSession = nil;
    videoDevice = nil;
}

- (void)downloadMask:(AKMaskObject *)maskObject
{
    [[AKMaskManager sharedInstance] downloadMaskWithMaskId:maskObject.maskId];
}

- (void)startWriting
{
//    dispatch_async(captureSessionQueue, ^{
        NSError *error = nil;
        
        // remove the temp file, if any
        recordedURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"videorecord.mov"]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:[recordedURL path]])
            [[NSFileManager defaultManager] removeItemAtURL:recordedURL error:NULL];
        
        AVAssetWriter *newAssetWriter = [AVAssetWriter assetWriterWithURL:recordedURL fileType:AVFileTypeQuickTimeMovie error:&error];
        if (!newAssetWriter || error) {
            [self showAlertViewWithMessage:[NSString stringWithFormat:@"Cannot create asset writer, error: %@", error] title:@"Error"];
            return;
        }
        
        NSDictionary *videoCompressionSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                                  AVVideoCodecH264, AVVideoCodecKey,
                                                  [NSNumber numberWithInteger:currentVideoDimensions.width], AVVideoWidthKey,
                                                  [NSNumber numberWithInteger:currentVideoDimensions.height], AVVideoHeightKey,
                                                  nil];
        
        assetWriterVideoInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoCompressionSettings];
        assetWriterVideoInput.expectsMediaDataInRealTime = YES;
#if FRAME_BGRA
        NSNumber* bufferPixelFormatType = [NSNumber numberWithInteger:kCVPixelFormatType_32BGRA];
#else
        NSNumber* bufferPixelFormatType = [NSNumber numberWithInteger:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange];
#endif
        // create a pixel buffer adaptor for the asset writer; we need to obtain pixel buffers for rendering later from its pixel buffer pool
        assetWriterInputPixelBufferAdaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:assetWriterVideoInput sourcePixelBufferAttributes:
                                              [NSDictionary dictionaryWithObjectsAndKeys:
                                               bufferPixelFormatType, (id)kCVPixelBufferPixelFormatTypeKey,
                                               [NSNumber numberWithUnsignedInteger:currentVideoDimensions.width], (id)kCVPixelBufferWidthKey,
                                               [NSNumber numberWithUnsignedInteger:currentVideoDimensions.height], (id)kCVPixelBufferHeightKey,
                                               (id)kCFBooleanTrue, (id)kCVPixelFormatOpenGLESCompatibility,
                                               nil]];
        
        
        UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
        
        // give correct orientation information to the video
        if (videoDevice.position == AVCaptureDevicePositionFront)
            assetWriterVideoInput.transform = FCGetTransformForDeviceOrientation(orientation, YES);
        else
            assetWriterVideoInput.transform = FCGetTransformForDeviceOrientation(orientation, NO);
        
        BOOL canAddInput = [newAssetWriter canAddInput:assetWriterVideoInput];
        if (!canAddInput) {
            [self showAlertViewWithMessage:@"Cannot add asset writer video input" title:@"Error"];
            assetWriterVideoInput = nil;
            return;
        }
        
        [newAssetWriter addInput:assetWriterVideoInput];
        
        // Make sure we have time to finish saving the movie if the app is backgrounded during recording
        // cf. the RosyWriter sample app from WWDC 2011
        if ([[UIDevice currentDevice] isMultitaskingSupported])
            backgroundRecordingID = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{}];
        
        videoWritingStarted = NO;
        assetWriter = newAssetWriter;
//    });
}

- (void)abortWriting
{
    if (!assetWriter)
        return;
    
    [assetWriter cancelWriting];
    assetWriterVideoInput = nil;
    assetWriter = nil;
    
    // remove the temp file
    NSURL *fileURL = [assetWriter outputURL];
    [[NSFileManager defaultManager] removeItemAtURL:fileURL error:NULL];
    
    void (^resetUI)(void) = ^(void) {
        [captureButton setTitle:@"Start" forState:UIControlStateNormal];
        captureButton.enabled = YES;
        
        // end the background task if it's done there
        // cf. The RosyWriter sample app from WWDC 2011
        if ([[UIDevice currentDevice] isMultitaskingSupported])
            [[UIApplication sharedApplication] endBackgroundTask:backgroundRecordingID];
    };
    
    dispatch_async(dispatch_get_main_queue(), resetUI);
}

- (void)stopWriting
{
    progressView.startAngle = -90;
    progressView.angle = totalDuration / totalDuration * 360;
    
    recordingViewWidthConstraint.constant = rcViewDefaultWidth;
    recordingViewHeightConstraint.constant = rcViewDefaultHeight;
    collectionViewBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.1 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        captureButton.hidden = NO;
        recordingView.hidden = YES;
    }];
    
    if (!assetWriter)
        return;
    
    
    AVAssetWriter *writer = assetWriter;
    
    assetWriterVideoInput = nil;
    assetWriterInputPixelBufferAdaptor = nil;
    assetWriter = nil;
    
    captureButton.enabled = NO;
    
    void (^resetUI)(void) = ^(void) {
        captureButton.hidden = NO;
        recordingView.hidden = YES;
        
        captureButton.enabled = YES;
        
        NSLog(@"Frame Rate = %f", frameRateCalculator.frameRate);
        
        // end the background task if it's done there
        // cf. The RosyWriter sample app from WWDC 2011
        if ([[UIDevice currentDevice] isMultitaskingSupported])
            [[UIApplication sharedApplication] endBackgroundTask:backgroundRecordingID];
    };
    
//    dispatch_async(captureSessionQueue, ^(void){
        [writer finishWritingWithCompletionHandler:^(void){
            if (writer.status == AVAssetWriterStatusFailed)
            {
                dispatch_async(dispatch_get_main_queue(), resetUI);
                NSLog(@"%@, %d", writer.error.localizedDescription, (int)writer.error.code);
                [self showAlertViewWithMessage:@"Cannot complete writing the video, the output could be corrupt." title:@"Error"];
            }
            else if (writer.status == AVAssetWriterStatusCompleted)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    resetUI();
                    recordedURL = [writer outputURL];
                    [self performSegueWithIdentifier:@"AKEditViewController" sender:recordedURL];
                    //[AKUtilities saveVideoFile:recordedURL];
                });
            }
        }];
	//    });
}

- (void)handleFaceUpdateTimer:(NSTimer *)timer
{
    isNeedFaceDetect = YES;
}

- (void)handleRecordTimer:(NSTimer *)timer
{
    recordedTime += timer.timeInterval;
    progressView.startAngle = -90;
    progressView.angle = recordedTime / totalDuration * 360;
    
    if (recordedTime >= totalDuration)
    {
        if (assetWriter)
        {
            captureButton.hidden = NO;
            recordingView.hidden = YES;
            
            [self stopWriting];
        }
    
        [recordTimer invalidate];
        recordTimer = nil;
    }
}

- (void)showAlertViewWithMessage:(NSString *)message title:(NSString *)title
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:controller animated:YES completion:nil];
    });
}

- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
            return device;
    }
    
    return nil;
}

#pragma mark Device Counts
- (NSUInteger)cameraCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count];
}

- (NSUInteger)micCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] count];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to `the new view controller.
    if ([segue.identifier isEqualToString:@"AKEditViewController"])
    {
        AKEditViewController *controller = segue.destinationViewController;
        controller.videoURL = recordedURL;
        controller.audioURL = [[AKAudioRecorder sharedInstance] audioURL];
        
        // Added by Luski Timurovich on 04/15/2017 to identify mask
        controller.maskID = selectedMaskObject.maskId;
    }
}

- (void)adjustFlashButton   
{
    if (videoDevice.flashMode == AVCaptureFlashModeOff)
        [flashButton setImage:[UIImage imageNamed:@"icon-flash-disabled"] forState:UIControlStateNormal];
    else if (videoDevice.flashMode == AVCaptureFlashModeOn)
        [flashButton setImage:[UIImage imageNamed:@"icon-flash-enabled"] forState:UIControlStateNormal];
    else if (videoDevice.flashMode == AVCaptureFlashModeAuto)
        [flashButton setImage:[UIImage imageNamed:@"icon-flash-enabled"] forState:UIControlStateNormal];
}

#pragma mark - IBAction
- (IBAction)changeCameraPressed:(id)sender {
    // Change camera source
    if (captureSession)
    {
        //Indicate that some changes will be made to the session
        [captureSession beginConfiguration];
        
        //Remove existing input
        AVCaptureDeviceInput* currentCameraInput = nil;
        for (AVCaptureDeviceInput *captureInput in captureSession.inputs) {
            if ([captureInput.device hasMediaType:AVMediaTypeVideo])
            {
                currentCameraInput = captureInput;
                break;
            }
        }
        
        [captureSession removeInput:currentCameraInput];
        
        //Get new input
        AVCaptureDevice *newCamera = nil;
        if(currentCameraInput.device.position == AVCaptureDevicePositionBack)
        {
            isFrontCamera = YES;
            flashButton.hidden = YES;
            newCamera = [self cameraWithPosition:AVCaptureDevicePositionFront];
        }
        else
        {
            flashButton.hidden = NO;
            isFrontCamera = NO;
            newCamera = [self cameraWithPosition:AVCaptureDevicePositionBack];
        }
        
        //Add input to session
        NSError *error = nil;
        AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:newCamera error:&error];
        if(!newVideoInput || error)
        {
            NSLog(@"Error creating capture device input: %@", error.localizedDescription);
        }
        else if ([captureSession canAddInput:newVideoInput])
        {
            [captureSession addInput:newVideoInput];
            videoDevice = newCamera;
            
            [self adjustFlashButton];
        }
        
        //Commit all the configuration changes at once
        [captureSession commitConfiguration];
    }
}

- (IBAction)explorerPressed:(id)sender {
    
    [NSNotificationCenter.defaultCenter postNotificationName:@"kNotificationLoadAnarko" object:nil];
    NSString *showExplorerTips = [[NSUserDefaults standardUserDefaults] objectForKey:@"showExplorerTips"];
    if(showExplorerTips == nil)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"showExplorerTips"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)flashPressed:(id)sender {
    if ([videoDevice hasTorch] && [videoDevice hasFlash]){
        [videoDevice lockForConfiguration:nil];
        if (videoDevice.flashMode == AVCaptureFlashModeOff) {
            if ([videoDevice isFlashModeSupported:AVCaptureFlashModeOn])
            {
                [videoDevice setTorchMode:AVCaptureTorchModeOn];
                [videoDevice setFlashMode:AVCaptureFlashModeOn];
                //torchIsOn = YES; //define as a variable/property if you need to know status
            }
        } else if (videoDevice.flashMode == AVCaptureFlashModeOn) {
            /* if ([videoDevice isFlashModeSupported:AVCaptureFlashModeAuto])
            {
                [videoDevice setTorchMode:AVCaptureTorchModeAuto];
                [videoDevice setFlashMode:AVCaptureFlashModeAuto];
                //torchIsOn = YES; //define as a variable/property if you need to know status
            }
        }
        else {*/
            [videoDevice setTorchMode:AVCaptureTorchModeOff];
            [videoDevice setFlashMode:AVCaptureFlashModeOff];
        }
        [videoDevice unlockForConfiguration];
        
        [self adjustFlashButton];
    }
}

- (IBAction)capturePressed:(id)sender {
    if (!captureSession.running)
        [captureSession startRunning];
    
    if (assetWriter)
    {
        [self stopWriting];
        [[AKAudioRecorder sharedInstance] stopRecord];
        
        if (recordTimer)
        {
            [recordTimer invalidate];
            recordTimer = nil;
        }
    }
    else
    {
        tipsView1.hidden = YES;
        tipsView2.hidden = YES;
        captureButton.hidden = YES;
        recordingView.hidden = NO;
        recordingViewWidthConstraint.constant = rcViewRecordingWidth;
        recordingViewHeightConstraint.constant = rcViewRecordingHeight;
        collectionViewBottomConstraint.constant = - masksCollectionView.frame.size.height;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
        [self startWriting];
        [[AKAudioRecorder sharedInstance] startRecord];
    
        recordedTime = 0;
        recordTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(handleRecordTimer:) userInfo:nil repeats:YES];
    }
}

- (IBAction)storePressed:(id)sender {
    UIStoryboard *explorer = [UIStoryboard storyboardWithName:@"Explorer" bundle:nil];
    
    
    UINavigationController *navController = [explorer instantiateViewControllerWithIdentifier:@"ProductsNavigationController"];
    AKProductListViewController *productVC = (AKProductListViewController *)[navController.viewControllers firstObject];
    productVC.delegate = self;
    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)handleSwipeGesture:(id)sender {
    
}

-(void)faceCovered:(BOOL)covered
{
    dispatch_async(dispatch_get_main_queue(), ^{
        maskYourFaceLabel.text = @"";
        if (captureButton.hidden == NO) {
            if(!covered && isFrontCamera) {
                maskYourFaceLabel.text = @"Mask your face before recording";
                captureButton.enabled = NO;
            }
            else  {
                captureButton.enabled = YES;
            }
        }
    });
}

#pragma mark - AVCaptureVideoDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    CMFormatDescriptionRef formatDesc = CMSampleBufferGetFormatDescription(sampleBuffer);
    
    // if not from the audio capture connection, handle video writing
    CMTime timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
    [frameRateCalculator calculateFramerateAtTimestamp:timestamp];
    
    // update the video dimensions information
    currentVideoDimensions = CMVideoFormatDescriptionGetDimensions(formatDesc);
    
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate);
    CIImage *sourceImage = [CIImage imageWithCVPixelBuffer:(CVPixelBufferRef)imageBuffer options:(__bridge NSDictionary *)attachments];
    
    BOOL shouldMirror = (AVCaptureDevicePositionFront == videoDevice.position);
    if (shouldMirror)
    {
        sourceImage = [sourceImage imageByApplyingTransform:CGAffineTransformTranslate(CGAffineTransformMakeScale(1, -1), 0, -sourceImage.extent.size.height)];
        _device = 0;
    }
    else
        _device = 1;
    
    // run the filter through the filter chain
    CGRect sourceExtent = sourceImage.extent;
    
    CGFloat sourceAspect = sourceExtent.size.width / sourceExtent.size.height;
    CGFloat previewAspect = videoPreviewViewBounds.size.width  / videoPreviewViewBounds.size.height;
    
    // we want to maintain the aspect radio of the screen size, so we clip the video image
    CGRect drawRect = sourceExtent;
    if (sourceAspect > previewAspect)
    {
        // use full height of the video image, and center crop the width
        drawRect.origin.x += (drawRect.size.width - drawRect.size.height * previewAspect) / 2.0f;
        drawRect.size.width = drawRect.size.height * previewAspect;
    }
    else
    {
        // use full width of the video image, and center crop the height
        drawRect.origin.y += (drawRect.size.height - drawRect.size.width / previewAspect) / 2.0f;
        drawRect.size.height = drawRect.size.width / previewAspect;
    }
    
    if (eaglContext != [EAGLContext currentContext])
    [EAGLContext setCurrentContext:eaglContext];
    
    [videoPreviewView bindDrawable];
    
    // clear eagl view to grey
    glClearColor(0.5, 0.5, 0.5, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    // set the blend mode to "source over" so that CI will use that
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    
    int exifOrientation;
    UIDeviceOrientation curDeviceOrientation = [[UIDevice currentDevice] orientation];
    switch (curDeviceOrientation) {
        case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
            exifOrientation = PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM;
            break;
        case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
            if (videoDevice.position == AVCaptureDevicePositionFront)
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            break;
        case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
            if (videoDevice.position == AVCaptureDevicePositionFront)
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            break;
        case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
        default:
            exifOrientation = PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP;
            break;
    }
    
    NSDictionary *imageOptions = @{CIDetectorImageOrientation : [NSNumber numberWithInt:exifOrientation]};
    NSArray *features = [NSArray array];
    AKFaceFeature *faceFeature = nil;
    if (isNeedFaceDetect)
    {
        //isNeedFaceDetect = NO;
        features = [faceDetector featuresInImage:sourceImage options:imageOptions];
    }
    
    //if (features != nil && features.count != 0)
    {
        // Visage SDK
        CVPixelBufferLockBaseAddress(imageBuffer,0);
        
        uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
        width = (int)CVPixelBufferGetWidth(imageBuffer);
        height = (int)CVPixelBufferGetHeight(imageBuffer);
        
        unsigned char *src = 0;
    #if FRAME_BGRA
        int size = (width * height) * 4;
        if (_bufferR90 == nil) {
            _bufferR90 = new uint8_t[size];
            _buffers = new uint8_t[size];
        }
        
        src = (unsigned char*) baseAddress;
        // using memcpy
        memcpy(_buffers, (char *)src, sizeof(uint8_t)*size);
        
    #else
        int size = (width * height) * 3 / 2;
        if (_bufferR90 == nil) {
            _bufferR90 = new uint8_t[size];
            _buffers = new uint8_t[size];
        }
        
        src = (unsigned char*)baseAddress;
        // using memcpy
        memcpy(_buffers, (char *)src, sizeof(uint8_t) * size);
    #endif
        
        int pixelFormat = [self getPixelFormat];
        int format = VISAGE_FRAMEGRABBER_FMT_LUMINANCE;
        if (pixelFormat == 1)
            format = VISAGE_FRAMEGRABBER_FMT_LUMINANCE;
        else
            format = VISAGE_FRAMEGRABBER_FMT_BGRA;
        
        getOrientation();
        
        cam_width = height;
        cam_height = width;
        
        unsigned char* pixels = [self getBuffer:_rotated];
        
        if (pixels != nil)
        {
            trackerStatus = tracker->track(cam_width, cam_height, (const char *)pixels, &trackingData, format);
            if (trackingData.isDataInitialized && trackingData.trackingQuality > 0) {
                faceFeature = [AKFaceFeature faceFeatureFromTrackingData:trackingData width:cam_width height:cam_height];
            }
        }
        
        //We unlock the  image buffer
        CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    }
    
    
    CIImage *drawImage = [self drawFaceWithSource:sourceImage drawRect:drawRect faceFeature:faceFeature];
    [videoPreviewView display];
    
    if (isNeedTake == YES)
    {
        isNeedTake = NO;
        CGImageRef imageRef = [ciContext createCGImage:drawImage fromRect:drawImage.extent];
        if (imageRef)
        {
            UIImage *image = [UIImage imageWithCGImage:imageRef];
            image = [self rotateImage:image byRadian:M_PI_2];
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
            CGImageRelease(imageRef);
        }
    }
    
    if (assetWriter != nil)
    {
        // if we need to write video and haven't started yet, start writing
        if (!videoWritingStarted)
        {
            videoWritingStarted = YES;
            BOOL success = [assetWriter startWriting];
            if (!success)
            {
                [self showAlertViewWithMessage:@"Cannot write video data, recording aborted" title:@"Error"];
                [self abortWriting];
                return;
            }
            
            [assetWriter startSessionAtSourceTime:timestamp];
            videoWrtingStartTime = timestamp;
            currentVideoTime = videoWrtingStartTime;
        }
        
        CVPixelBufferRef renderedOutputPixelBuffer = NULL;
        OSStatus status = CVPixelBufferPoolCreatePixelBuffer(nil, assetWriterInputPixelBufferAdaptor.pixelBufferPool, &renderedOutputPixelBuffer);
        if (status)
        {
            NSLog(@"Cannot obtain a pixel buffer from the buffer pool");
            return;
        }
        
        [self renderFaceWithSource:sourceImage pixelBuffer:renderedOutputPixelBuffer faceFeature:faceFeature];
        
        currentVideoTime = timestamp;
        // write the video data
        if (assetWriterVideoInput.readyForMoreMediaData)
            [assetWriterInputPixelBufferAdaptor appendPixelBuffer:renderedOutputPixelBuffer withPresentationTime:timestamp];
        
        CVPixelBufferRelease(renderedOutputPixelBuffer);
    }
}
    
- (void)captureOutput:(AVCaptureOutput *)captureOutput didDropSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    //NSLog(@"didDropSampleBuffer : %@", captureOutput.description);
}
    
- (CGImageRef)imageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer // Create a CGImageRef from sample buffer data
{
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer,0);        // Lock the image buffer
    
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);   // Get information of the image
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef image = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    
    CGColorSpaceRelease(colorSpace);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    /* CVBufferRelease(imageBuffer); */  // do not call this!
    
    return image;
}

// rotate UIImage by radian value
- (UIImage *)rotateImage:(UIImage *)image byRadian:(CGFloat)radian
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(radian);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContextWithOptions(rotatedSize, NO, [UIScreen mainScreen].scale);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width / 2, rotatedSize.height / 2);
    
    // Rotate the image context
    CGContextRotateCTM(bitmap, radian);
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-image.size.width / 2, -image.size.height / 2, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *rotateImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return rotateImage;
}

// rotate image by radian value
- (CIImage *)rotateImage:(CIImage *)image radian:(float)radian
{
    CIFilter *filter = [CIFilter filterWithName:@"CIAffineTransform"];
    CGAffineTransform transform = CGAffineTransformMakeRotation(radian);
    [filter setValue:[NSValue valueWithCGAffineTransform:transform] forKey:@"inputTransform"];
    [filter setValue:image forKey:@"inputImage"];
    CIImage *rotatedImage = [filter valueForKey:@"outputImage"];
    
    return rotatedImage;
}

// scale iamge by scale value
- (CIImage *)scaleImage:(CIImage *)image scale:(float)scale
{
    CIFilter *filter = [CIFilter filterWithName:@"CIAffineTransform"];
    CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
    [filter setValue:[NSValue valueWithCGAffineTransform:transform] forKey:@"inputTransform"];
    [filter setValue:image forKey:@"inputImage"];
    CIImage *scaledImage = [filter valueForKey:@"outputImage"];
    
    return scaledImage;
}

- (CIImage *)drawFaceWithSource:(CIImage *)sourceImage drawRect:(CGRect)drawRect faceFeature:(AKFaceFeature *)feature
{
    CIImage *drawImage = sourceImage;
    if (feature != nil)
    {
        CGRect faceBounds = feature.faceBounds;
        
        AKMaskResource *resource = selectedMaskObject.resources.allObjects[0];
        if (feature.heightBetweenMouth >= 10) {
            resource = selectedMaskObject.resources.allObjects[1];
        } else if(feature.isRaisingBrow == YES) {
            resource = selectedMaskObject.resources.allObjects[2];
        }
        selectedEyeFeature = [AKFaceFeature faceFeatureFromMaskObject:resource];
        CGFloat scale = feature.distanceBetweenEyes / selectedEyeFeature.distanceBetweenEyes;
        faceImage = [self scaleImage:selectedFace scale:(scale / selectedImage.scale)];
        //if (feature.eyeClosureL < 0.3 || feature.eyeClosureR < 0.3)
        if (feature.isRaisingBrow == YES)
            faceImage = [self scaleImage:selectedWink scale:(scale / selectedImage.scale)];
        
        if (feature.heightBetweenMouth >= 10)
            faceImage = [self scaleImage:selectedTalk scale:(scale / selectedImage.scale)];
        
        if (faceImage != nil)
        {
            faceImage = [self rotateImage:faceImage radian:feature.headRotation];

            // Get offset origin for face rotation
            CGFloat iwidth = selectedImage.size.width * scale;
            CGFloat iheight = selectedImage.size.height * scale;
            
            CGFloat radius = sqrt(iwidth * iwidth + iheight * iheight) / 2;
            CGFloat alpha = feature.headRotation / 2;
            CGFloat absalpha = fabs(feature.headRotation / 2);
            
            CGFloat offsetx;
            CGFloat offsety;
            
            if (alpha > 0) {
                offsetx = -2 * radius * sin(absalpha) * sin(absalpha);
                offsety = 2 * radius * sin(absalpha) * cos(absalpha);
            }
            else {
                offsetx = 2 * radius * sin(absalpha) * cos(absalpha);
                offsety = -2 * radius * sin(absalpha) * sin(absalpha);
            }
            // ---------------------------------------------------------------------------------------------------
            
            faceBounds.origin.y = feature.leftEyeCenter.x - selectedEyeFeature.leftEyeCenter.x * scale + offsetx;
            faceBounds.origin.x = feature.leftEyeCenter.y - selectedEyeFeature.leftEyeCenter.y * scale + offsety;
            faceBounds.size.height = selectedImage.size.width * (scale / selectedImage.scale);
            faceBounds.size.width = selectedImage.size.height * (scale / selectedImage.scale);
            
            // Draw the face image to source image
            drawImage = [self applyFace:faceImage faceBounds:faceBounds toSource:sourceImage];
            
            // Save the last face detection bounds and time to use when the face detection is failed.
            lastFaceRect = faceBounds;
            lastFaceDate = [NSDate date];
            [self faceCovered:YES];
        }
    }
    else
    {
        [self faceCovered:NO];
        if (lastFaceDate != nil && [[NSDate date] timeIntervalSinceDate:lastFaceDate] <= 0.4)
        {
            if (CGRectEqualToRect(lastFaceRect, CGRectZero))
                drawImage = sourceImage;
            else if (faceImage != nil)
                drawImage = [self applyFace:faceImage faceBounds:lastFaceRect toSource:sourceImage];
        }
    }
    
    [ciContext drawImage:drawImage inRect:videoPreviewViewBounds fromRect:drawRect];
    
    return drawImage;
}

// render the source image from camera to pixel buffer with face to record
- (void)renderFaceWithSource:(CIImage *)sourceImage pixelBuffer:(CVPixelBufferRef)pixelBuffer faceFeature:(AKFaceFeature *)feature
{
    CIImage *drawImage = sourceImage;
    if (feature != nil)
    {
        CGRect faceBounds = feature.faceBounds;
        AKMaskResource *resource = selectedMaskObject.resources.allObjects[0];
        if (feature.heightBetweenMouth >= 10) {
            resource = selectedMaskObject.resources.allObjects[1];
        } else if(feature.isRaisingBrow == YES) {
            resource = selectedMaskObject.resources.allObjects[2];
        }
        selectedEyeFeature = [AKFaceFeature faceFeatureFromMaskObject:resource];
        CGFloat scale = feature.distanceBetweenEyes / selectedEyeFeature.distanceBetweenEyes;
        faceImage = [self scaleImage:selectedFace scale:(scale / selectedImage.scale)];
        //if (feature.eyeClosureL < 0.3 || feature.eyeClosureR < 0.3)
        if (feature.isRaisingBrow == YES)
            faceImage = [self scaleImage:selectedWink scale:(scale / selectedImage.scale)];
        if (feature.heightBetweenMouth >= 10)
            faceImage = [self scaleImage:selectedTalk scale:(scale / selectedImage.scale)];
        
        if (faceImage != nil)
        {
            faceImage = [self rotateImage:faceImage radian:feature.headRotation];
            
            // Get offset origin for face rotation
            CGFloat iwidth = selectedImage.size.width * scale;
            CGFloat iheight = selectedImage.size.height * scale;
            
            CGFloat radius = sqrt(iwidth * iwidth + iheight * iheight) / 2;
            CGFloat alpha = feature.headRotation / 2;
            CGFloat absalpha = fabs(feature.headRotation / 2);
            
            CGFloat offsetx;
            CGFloat offsety;
            
            if (alpha > 0) {
                offsetx = -2 * radius * sin(absalpha) * sin(absalpha);
                offsety = 2 * radius * sin(absalpha) * cos(absalpha);
            }
            else {
                offsetx = 2 * radius * sin(absalpha) * cos(absalpha);
                offsety = -2 * radius * sin(absalpha) * sin(absalpha);
            }
            // ---------------------------------------------------------------------------------------------------
            
            faceBounds.origin.y = feature.leftEyeCenter.x - selectedEyeFeature.leftEyeCenter.x * scale + offsetx;
            faceBounds.origin.x = feature.leftEyeCenter.y - selectedEyeFeature.leftEyeCenter.y * scale + offsety;
            faceBounds.size.height = selectedImage.size.width * (scale / selectedImage.scale);
            faceBounds.size.width = selectedImage.size.height * (scale / selectedImage.scale);
            
            // Draw the face image to source image
            drawImage = [self applyFace:faceImage faceBounds:faceBounds toSource:sourceImage];
            
            // Save the last face detection bounds and time to use when the face detection is failed.
            lastFaceRect = faceBounds;
            lastFaceDate = [NSDate date];
        }
    }
    else
    {
        if (lastFaceDate != nil && [[NSDate date] timeIntervalSinceDate:lastFaceDate] <= 0.6)
        {
            if (CGRectEqualToRect(lastFaceRect, CGRectZero))
                drawImage = sourceImage;
            else if (faceImage != nil)
                drawImage = [self applyFace:faceImage faceBounds:lastFaceRect toSource:sourceImage];
        }
    }
    
    [ciContext render:drawImage toCVPixelBuffer:pixelBuffer bounds:drawImage.extent colorSpace:sDeviceRgbColorSpace];
}

// adjust face bounds from shake
- (CGRect)filterFaceBoundsForDraw:(CGRect)faceBounds
{
    if (CGRectEqualToRect(lastFaceDrawBounds, CGRectZero))
    return faceBounds;
    
    CGRect offsetBounds;
    
    //high-pass filter to eliminate gravity
    lastFaceDrawBounds.origin.x = faceBounds.origin.x * kFilteringFactor + lastFaceDrawBounds.origin.x * (1.0f - kFilteringFactor);
    lastFaceDrawBounds.origin.y = faceBounds.origin.y * kFilteringFactor + lastFaceDrawBounds.origin.y * (1.0f - kFilteringFactor);
    lastFaceDrawBounds.size.width = faceBounds.size.width * kFilteringFactor + lastFaceDrawBounds.size.width * (1.0f - kFilteringFactor);
    lastFaceDrawBounds.size.height = faceBounds.size.height * kFilteringFactor + lastFaceDrawBounds.size.height * (1.0f - kFilteringFactor);
    offsetBounds.origin.x = faceBounds.origin.x - lastFaceDrawBounds.origin.x;
    offsetBounds.origin.y = faceBounds.origin.y - lastFaceDrawBounds.origin.y;
    offsetBounds.size.width = faceBounds.size.width - lastFaceDrawBounds.size.width;
    offsetBounds.size.height = faceBounds.size.height - lastFaceDrawBounds.size.height;
    
    if (offsetBounds.origin.x > kOffsetFactor)
    return faceBounds;
    
    return lastFaceDrawBounds;
}

// adjust face bounds from shake
- (CGRect)filterFaceBoundsForRender:(CGRect)faceBounds
{
    if (CGRectEqualToRect(lastFaceRenderBounds, CGRectZero))
    return faceBounds;
    
    CGRect offsetBounds;
    
    lastFaceRenderBounds.origin.x = faceBounds.origin.x * kFilteringFactor + lastFaceRenderBounds.origin.x * (1.0f - kFilteringFactor);
    lastFaceRenderBounds.origin.y = faceBounds.origin.y * kFilteringFactor + lastFaceRenderBounds.origin.y * (1.0f - kFilteringFactor);
    lastFaceRenderBounds.size.width = faceBounds.size.width * kFilteringFactor + lastFaceRenderBounds.size.width * (1.0f - kFilteringFactor);
    lastFaceRenderBounds.size.height = faceBounds.size.height * kFilteringFactor + lastFaceRenderBounds.size.height * (1.0f - kFilteringFactor);
    offsetBounds.origin.x = faceBounds.origin.x - lastFaceRenderBounds.origin.x;
    offsetBounds.origin.y = faceBounds.origin.y - lastFaceRenderBounds.origin.y;
    offsetBounds.size.width = faceBounds.size.width - lastFaceRenderBounds.size.width;
    offsetBounds.size.height = faceBounds.size.height - lastFaceRenderBounds.size.height;
    
    if (offsetBounds.origin.x > kOffsetFactor)
    return faceBounds;
    
    return lastFaceRenderBounds;
}

// calculate the face bounds to replace
- (CGRect)boundsWithSourceImage:(CIImage *)sourceImage faceFeature:(AKFaceFeature *)faceFeature
{
    CGFloat sourceDistance = faceFeature.distanceBetweenEyes;
    CGFloat faceDistance = selectedEyeFeature.rightEyeCenter.x - selectedEyeFeature.leftEyeCenter.x;
    CGFloat scale = sourceDistance / faceDistance;
    
    CGPoint leftEyePosition = faceFeature.leftEyeCenter;
    CGRect bounds = CGRectZero;
    bounds.origin.x = leftEyePosition.x - selectedEyeFeature.leftEyeCenter.y * scale;
    bounds.origin.y = leftEyePosition.y - selectedEyeFeature.leftEyeCenter.x * scale;
    bounds.size.width = selectedImage.size.height * scale;
    bounds.size.height = selectedImage.size.width * scale;
    
//    faceImage = [self rotateImage:selectedFace radian:M_PI * 2.0f - backFace.angleBetweenEyes];

//    CGFloat radius = sqrt(bounds.origin.x * bounds.origin.x + bounds.origin.y * bounds.origin.y);
//    CGFloat cos = bounds.origin.y / radius;
//    CGFloat preradian = acos(cos);
//    CGFloat totalradian = backFace.angleBetweenEyes + preradian;

//    bounds.origin.y = cos(totalradian) * radius;
//    bounds.origin.x = sin(M_PI_2 - totalradian) * radius;
    
    scale = bounds.size.height / faceImage.extent.size.height;
    
    faceImage = [self scaleImage:faceImage scale:scale];
    
    return bounds;
}

// draw the face to source image from camera
- (CIImage *)applyFace:(CIImage *)face faceBounds:(CGRect)faceBounds toSource:(CIImage *)sourceImage
{
    CIFilter *filter = [CIFilter filterWithName:@"CISourceOverCompositing"];
    CGAffineTransform transform = CGAffineTransformMakeTranslation(faceBounds.origin.x, faceBounds.origin.y);
    face = [face imageByApplyingTransform:transform];
    [filter setValue:face forKey:@"inputImage"];
    [filter setValue:sourceImage forKey:@"inputBackgroundImage"];
    CIImage *filteredImage = filter.outputImage;
    
    if (filteredImage.extent.size.width != sourceImage.extent.size.width ||
        filteredImage.extent.size.height != sourceImage.extent.size.height)
    {
        filter = [CIFilter filterWithName:@"CICrop"];
        [filter setValue:filteredImage forKey:@"inputImage"];
        CIVector *vector = [CIVector vectorWithCGRect:sourceImage.extent];
        [filter setValue:vector forKey:@"inputRectangle"];
        filteredImage = filter.outputImage;
    }
    
    return filteredImage;
}

#pragma mark -
void YUV_TO_RGBA(unsigned char* yuv, unsigned char* buff, int width, int height)
{
    const int frameSize = width * height;
    
    const int ii = 0;
    const int ij = 0;
    const int di = +1;
    const int dj = +1;
    
    unsigned char* rgba = buff;
    
    for (int i = 0, ci = ii; i < height; ++i, ci += di)
    {
        for (int j = 0, cj = ij; j < width; ++j, cj += dj)
        {
            int y = (0xff & ((int) yuv[ci * width + cj]));
            int v = (0xff & ((int) yuv[frameSize + (ci >> 1) * width + (cj & ~1) + 0]));
            int u = (0xff & ((int) yuv[frameSize + (ci >> 1) * width + (cj & ~1) + 1]));
            y = y < 16 ? 16 : y;
            
            int a0 = 1192 * (y -  16);
            int a1 = 1634 * (v - 128);
            int a2 =  832 * (v - 128);
            int a3 =  400 * (u - 128);
            int a4 = 2066 * (u - 128);
            
            int r = (a0 + a1) >> 10;
            int g = (a0 - a2 - a3) >> 10;
            int b = (a0 + a4) >> 10;
            
            *rgba++ = clamp(r);
            *rgba++ = clamp(g);
            *rgba++ = clamp(b);
            *rgba++ = 255;
        }
    }
}

void getOrientation()
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if (orientation == UIInterfaceOrientationPortrait)
        _rotated = 0;
    else if (orientation == UIInterfaceOrientationLandscapeLeft)
        _rotated = 1;
    else if (orientation == UIInterfaceOrientationLandscapeRight)
        _rotated = 3;
    else
        _rotated = 0;
}


- (unsigned char*)getBuffer:(int)rotated
{
//    struct timespec   ts;
//    struct timeval    tp;
//    
//    int rc = gettimeofday(&tp, NULL);
//    
//    if(rc!=0)
//        return 0;
//    
//    /* Convert from timeval to timespec */
//    ts.tv_sec  = tp.tv_sec + 5;
//    ts.tv_nsec = 0;
//    
//    pthread_mutex_lock(&mutex);
//    
//    int ret = pthread_cond_timedwait(&cond, &mutex, &ts);
//    if (ret == ETIMEDOUT)
//    {
//        NSLog(@"cond timed out\n");
//        pthread_mutex_unlock(&mutex);
//        return 0;
//    }
//    
//    pthread_mutex_unlock(&mutex);
    
    if (_buffers != nil) {
#if FRAME_BGRA
        [self rotateBGRA: _buffers _saveTo:_bufferR90 _width:width _height:height _rotation:rotated _cameraDevice:_device];
#else
        [self rotateYUV: _buffers _saveTo:_bufferR90 _width:width _height:height _rotation:rotated _cameraDevice:_device];
#endif
    }
    
    return (unsigned char*)_bufferR90;
    
}

- (void)rotateYUV:(uint8_t*)yuv _saveTo:(uint8_t*)output _width:(int)width _height:(int)height _rotation:(int)rotation _cameraDevice:(int)device
{
    int frameSize = width * height;
    Boolean swap = false;
    Boolean xflip = false;
    Boolean yflip = false;
    
    if (rotation == 0)
    {
        swap = true;
        
        if (device == 1)
            xflip = true;
    }
    
    else if (rotation == 1)
    {
        xflip = true;
        
        if (device == 1)
            yflip = true;
    }
    
    else if (rotation == 2)
    {
        swap = true;
        xflip = true;
        yflip = true;
        
        if (device ==1)
            xflip = false;
    }
    
    else
    {
        yflip = true;
        
        if (device == 1)
            yflip = false;
    }
    
    for (int j = 0; j < height; j++) {
        for (int i = 0; i < width; i++) {
            int yIn = j * width + i;
            int uIn = frameSize + (j >> 1) * width + (i & ~1);
            int vIn = uIn       + 1;
            
            int wOut     = swap  ? height              : width;
            int hOut     = swap  ? width               : height;
            int iSwapped = swap  ? j                   : i;
            int jSwapped = swap  ? i                   : j;
            int iOut     = xflip ? wOut - iSwapped - 1 : iSwapped;
            int jOut     = yflip ? hOut - jSwapped - 1 : jSwapped;
            
            int yOut = jOut * wOut + iOut;
            int uOut = frameSize + (jOut >> 1) * wOut + (iOut & ~1);
            int vOut = uOut + 1;
            
            output[yOut] = (0xff & yuv[yIn]);
            output[uOut] = (0xff & yuv[uIn]);
            output[vOut] = (0xff & yuv[vIn]);
        }
    }
}


- (void)rotateBGRA:(uint8_t*)rgba _saveTo:(uint8_t*)output _width:(int)width _height:(int)height _rotation:(int)rotation _cameraDevice:(int)device
{
    Boolean swap = false;
    Boolean xflip = false;
    Boolean yflip = false;
    
    if (rotation == 0)
    {
        swap = true;
        
        if (device == 1)
            xflip = true;
    }
    
    else if (rotation == 1)
    {
        xflip = true;
        
        if (device == 1)
            yflip = true;
    }
    
    else if (rotation == 2)
    {
        swap = true;
        xflip = true;
        yflip = true;
        
        if (device ==1)
            xflip = false;
    }
    
    else
    {
        yflip = true;
        
        if (device == 1)
            yflip = false;
    }
    
    for (int j = 0; j < height; j++) {
        for (int i = 0; i < width; i++) {
            int rIn = j * width * 4 + 4*i + 0;
            int gIn = j * width * 4 + 4*i + 1;
            int bIn = j * width * 4 + 4*i + 2;
            int aIn = j * width * 4 + 4*i + 3;
            
            int wOut     = swap  ? height              : width;
            int hOut     = swap  ? width               : height;
            int iSwapped = swap  ? j                   : i;
            int jSwapped = swap  ? i                   : j;
            int iOut     = xflip ? wOut - iSwapped - 1 : iSwapped;
            int jOut     = yflip ? hOut - jSwapped - 1 : jSwapped;
            
            int rOut = jOut * wOut * 4 + 4*iOut + 0;
            int gOut = jOut * wOut * 4 + 4*iOut + 1;
            int bOut = jOut * wOut * 4 + 4*iOut + 2;
            int aOut = jOut * wOut * 4 + 4*iOut + 3;
            
            output[rOut] = (0xff & rgba[rIn]);
            output[gOut] = (0xff & rgba[gIn]);
            output[bOut] = (0xff & rgba[bIn]);
            output[aOut] = (0xff & rgba[aIn]);
        }
    }
}

- (int)getPixelFormat
{
#if FRAME_BGRA
    return 4;
#else
    return 1;
#endif
}

int clamp(int x)
{
    unsigned y;
    return !(y = x >> 8) ? x : (0xff ^ (y >> 24));
}

#pragma mark - AKProductListViewControllerDelegate

- (void)dismissProductListViewController:(AKProductListViewController *)vc
{
    [vc.navigationController dismissViewControllerAnimated:true completion:^{
       
        [NSNotificationCenter.defaultCenter postNotificationName:@"kNotificationLoadAnarko" object:nil];
        [self.navigationController dismissViewControllerAnimated:true completion:^{
            
        }];
        
    }];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[AKMaskManager sharedInstance] masksCount] + 1;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AKMaskCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AKMaskCollectionCell" forIndexPath:indexPath];
    cell.parentController = self;
    cell.thumbImageView.image = nil;
    if (indexPath.item == 0)
    {
        UIImage *image = [UIImage imageNamed:@"icon-store-with-text"];
        cell.thumbImageView.image = image;
        cell.emptyImageView.hidden = YES;
        [cell stopActivity];
    }
    else
    {
        AKMaskObject *maskObject = [[AKMaskManager sharedInstance] maskObjectAtIndex:indexPath.item - 1];
        cell.maskObject = maskObject;
    }
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == 0)
        [self storePressed:nil];
    else
    {
        AKMaskCollectionCell *cell = (AKMaskCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
        AKMaskObject *maskObject = [[AKMaskManager sharedInstance] maskObjectAtIndex:indexPath.item - 1];
        if (cell.thumbImageView.image == nil)
        {
            [self downloadMask:maskObject];
            [cell showActivity];
        }
        else if (cell.emptyImageView.hidden == NO)
        {
            [self downloadMask:maskObject];
            cell.emptyImageView.hidden = YES;
            [cell showActivity];
        }
        else
        {
            if (maskObject.resources.allObjects.count == 0)
            {
                [self downloadMask:maskObject];
                return;
            }
            AKMaskResource *maskResource = nil;
            for (AKMaskResource *resource in maskObject.resources.allObjects)
            {
                if ([resource.name isEqualToString:@"normal"])
                {
                    NSString *filepath = [NSTemporaryDirectory() stringByAppendingPathComponent:resource.image];
                    UIImage *image = [[UIImage imageWithContentsOfFile:filepath] rotateImageByRadian:-M_PI_2];
                    if (image == nil)
                    {
                        [self downloadMask:maskObject];
                        return;
                    }
                    selectedImage = image;
                    selectedFace = image.CIImage;
                    if (selectedFace == nil)
                        selectedFace = [CIImage imageWithCGImage:image.CGImage];
                        
                }
                else if ([resource.name isEqualToString:@"wink"])
                {
                    NSString *filepath = [NSTemporaryDirectory() stringByAppendingPathComponent:resource.image];
                    UIImage *image = [[UIImage imageWithContentsOfFile:filepath] rotateImageByRadian:-M_PI_2];
                    if (image == nil)
                    {
                        [self downloadMask:maskObject];
                        return;
                    }
                    selectedWink = image.CIImage;
                    if (selectedWink == nil)
                        selectedWink = [CIImage imageWithCGImage:image.CGImage];
                }
                else if ([resource.name isEqualToString:@"talk"])
                {
                    NSString *filepath = [NSTemporaryDirectory() stringByAppendingPathComponent:resource.image];
                    UIImage *image = [[UIImage imageWithContentsOfFile:filepath] rotateImageByRadian:-M_PI_2];
                    if (image == nil)
                    {
                        [self downloadMask:maskObject];
                        return;
                    }
                    selectedTalk = image.CIImage;
                    if (selectedTalk == nil)
                        selectedTalk = [CIImage imageWithCGImage:image.CGImage];
                }
                maskResource = resource;
            }
            
            selectedEyeFeature = [AKFaceFeature faceFeatureFromMaskObject:maskResource];
            
            NSManagedObjectContext *context = maskObject.managedObjectContext;
            maskObject.isActive = YES;
            [context MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
                
            }];
            
            context = selectedMaskObject.managedObjectContext;
            selectedMaskObject.isActive = NO;
            [context MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
                
            }];
        
            selectedMaskObject = maskObject;
            maskNameLabel.text = [NSString stringWithFormat:@"%@\n%@",maskObject.name,@"Raise your eyebrows"];
            
            if(maskNameAnimationTimer) {
                [maskNameAnimationTimer invalidate];
                maskNameAnimationTimer = nil;
            }
            if (maskNameLeadingSpaceConstraint.constant != 0) {
                [UIView animateWithDuration:0 animations:^{
                    maskNameLeadingSpaceConstraint.constant = self.view.frame.size.width;
                    maskNameTrailingSpaceConstraint.constant = -self.view.frame.size.width;
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3 animations:^{
                        maskNameLeadingSpaceConstraint.constant = 0;
                        maskNameTrailingSpaceConstraint.constant = 0;
                        [self.view layoutIfNeeded];
                    }];
                }];
            }
            selectedMaskIndex = indexPath.item;
            maskNameAnimationTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hideMaskNameWithAnimation) userInfo:nil repeats:NO];
            NSLog(@"Selected Mask ID = %@, Name = %@", selectedMaskObject.maskId, selectedMaskObject.name);
        }
    }
}

-(void)hideMaskNameWithAnimation
{
    maskNameLeadingSpaceConstraint.constant = -self.view.frame.size.width;
    maskNameTrailingSpaceConstraint.constant = self.view.frame.size.width;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    
    }];
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = collectionView.frame.size.height - 8;
    return CGSizeMake(height, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(4.0, 4.0, 4.0, 4.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 4.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section;
{
    return 4.0;
}

@end
