//
//  AKVerifyPhoneViewController.swift
//  Anarko
//
//  Created by Hua Wan on 13/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftGifOrigin


class AKVerifyPhoneViewController: UIViewController, AKCountryPickerViewControllerDelegate, UIGestureRecognizerDelegate , NVActivityIndicatorViewable, SMSCodeTextFieldDelegate {

    @IBOutlet var verifyLabel: UILabel!
    @IBOutlet var countryButton: UIButton!
    @IBOutlet var phoneNumberTextField: UITextField!
    @IBOutlet var statusImageView: UIImageView!
    
    @IBOutlet var activationButton: UIButton!
    @IBOutlet var resendLabel: UILabel!
    @IBOutlet var verifyView: UIView!
    @IBOutlet var verifyTextsView: UIView!
    
    @IBOutlet var countryPickerView: UIView!
    @IBOutlet var countryPickerOriginY: NSLayoutConstraint!
    @IBOutlet var countryPickerHeight: NSLayoutConstraint!
    @IBOutlet var verifyViewOriginY: NSLayoutConstraint!
    @IBOutlet var logoImageView: UIImageView!
    
    @IBOutlet weak var termsAndConditionsTextView: UITextView!
    var alertBar : AlertBar?
    
    @IBOutlet weak var editMyNumber: UIButton!
    
    var phoneNumberStr: String!
    
    var resendTimer : Timer!
    var resendTimeCounter = 0
    var phoneCode = "1"
    var countryCode = "US"
    var isPhoneValid = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        logoImageView.image = UIImage.gif(name:"logo@2x")
        editMyNumber.isHidden = true
        self.setVerifyViewVisibility(isHidden: true)
        
        for i in 10..<17 {
            if let textField = verifyTextsView.viewWithTag(i) as? SMSCodeTextField {
                textField.codeDelegate = self
            }
        }

    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        countryPickerHeight.constant = termsAndConditionsTextView.frame.origin.y - verifyLabel.frame.origin.y - verifyLabel.frame.size.height - 16
        
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeTextField(notification:)), name: Notification.Name.UITextFieldTextDidChange, object: nil)

        termsAndConditionsTextView.delegate = self
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        let str = "By registering, you agree to Anarko's Terms and Conditions and EULA"
        let attributedString = NSMutableAttributedString(string: str)
        let foundRange = attributedString.mutableString.range(of: "Terms and Conditions and EULA")
        attributedString.addAttribute(NSLinkAttributeName, value: "termsAndConditions", range: foundRange)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1), range: NSMakeRange(0, str.characters.count))
        attributedString.addAttribute(NSFontAttributeName, value: termsAndConditionsTextView.font!, range: NSMakeRange(0, str.characters.count))
        attributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraph, range: NSMakeRange(0, str.characters.count))
        termsAndConditionsTextView.attributedText = attributedString
        termsAndConditionsTextView.linkTextAttributes = [NSForegroundColorAttributeName: UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)]
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func verifyActivationCode() -> Void {
        
        var activationCode = ""
        for i in 10..<17 {
            if let textField = verifyTextsView.viewWithTag(i) as? UITextField {
                activationCode += textField.text!
            }
        }
        
        if activationCode.characters.count == 7 {

            
            self.authorizeWithSMSCode(verifyCode: activationCode)
        }
    }
    
    func handleResendTimer(timer: Timer) -> Void {
        self.resendTimeCounter -= 1
        self.resendLabel.text = "\(String(format: "%.2d", self.resendTimeCounter / 60)):\(String(format: "%.2d", self.resendTimeCounter % 60))"
        if self.resendTimeCounter <= 0 {
            self.setVerifyViewVisibility(isHidden: true)
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "AKCountryPickerViewController" {
            if let controller = segue.destination as? AKCountryPickerViewController {
                controller.delegate = self
            }
        }
    }
    
    func setVerifyViewVisibility(isHidden hidden : Bool) {
        editMyNumber.isHidden = hidden
        activationButton.isEnabled = hidden
        resendLabel.isHidden = hidden
        verifyView.isHidden = hidden
        
        if hidden == true {
            resendTimeCounter = 0
            if let timer = self.resendTimer {
                timer.invalidate()
            }
            if UIScreen.main.bounds.size.height <= 568 {
                verifyViewOriginY.constant = -50
            }
        }
        else
        {
            verifyViewOriginY.constant = 18;
        }
    }
    
    // MARK: - IBAction
    @IBAction func countryPressed(sender: AnyObject) {
        countryPickerView.isHidden = false
        UIView.animate(withDuration: 0.3) { 
            self.countryPickerOriginY.constant = 8.0
        }
        self.view.layoutIfNeeded()
    }
    @IBAction func edityMyNumberTapped(_ sender: UIButton) {
    
        activationButton.setTitle("SEND ACTIVATION CODE", for: UIControlState.normal)
        resendLabel.text = ""
        self.setVerifyViewVisibility(isHidden: true)
    }
    
    @IBAction func sendActivationPressed(sender: AnyObject) {
        
        
        print("\(phoneCode) ----- \(countryCode)")
        
        if phoneNumberTextField.text?.characters.count != 0 && isPhoneValid {
            
            let paramDic = NSMutableDictionary()
            
            if phoneNumberStr == "+1234567890" {
                phoneNumberStr = "+11234567890"
            }
            
            paramDic.setObject(phoneNumberStr!, forKey:PHONE as NSCopying)
            paramDic.setObject(COUNTRY_CODE, forKey: LANGUAGE as NSCopying)
            
            if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
                
                print(dict)
                
                self.requestAuthorizationCode(dic: dict)
            }

        }else {
            
            AKGlobal.showAlertViewController(title: "Error!", message:"Please input your correct phone number." , target: self)

        }
        
    }
    

    @IBAction func handleTapGesture(sender: AnyObject) {
        
        self.view.endEditing(true)
        
    }
    
    func isBackSpaceTapped(string : String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true;
        }
        else
        {
            return false
        }
    }
    
    func textFieldDidDelete(textField : UITextField) -> Void {
        if let count = textField.text?.characters.count, count > 0 {
            textField.text = ""
        }
        else if let previousTextField = verifyTextsView.viewWithTag(textField.tag - 1) as? UITextField {
            previousTextField.text = ""
            previousTextField.becomeFirstResponder()
        }
    }

    // MARK: - AKCountryPickerViewControllerDelegate
    func didSelectCountry(flag: UIImage, name: String, code: String, phcode: String) -> Void {
        countryPickerView.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.countryPickerOriginY.constant = 667;
        }
        self.view.layoutIfNeeded()
        self.view.endEditing(true)
        countryButton.setImage(flag, for: UIControlState.normal)
        countryButton.setTitle(" \(phcode)", for: UIControlState.normal)
        phoneCode = phcode.replacingOccurrences(of: "+", with: "")
        countryCode = code
    }
    
    // MARK: - UIGestureRecognizerDelegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view == countryPickerView || touch.view?.tag == 100 {
            return false
        }
        return true
    }
    
    // MARK: - Request Authorization code by phone number
    
    private func requestAuthorizationCode(dic:[String: AnyObject]) {
        
        
        startAnimating(CGSize(width:80, height: 30), message: "Submitting..", type: NVActivityIndicatorType(rawValue: 14)!)
        
        AKAPIManager.sharedManager.registerPhoneNumber(params: dic) { (response) in
            
            self.stopAnimating()
            
            switch response.result {
                
            case .success:
                print("Validation Successful")
                
                if let json = response.result.value {
                    print("JSON: \(json)")
                    self.alertBar?.hide(completion: {})
                    let jsonDic = json as! NSDictionary
                    let error = AKParser.parseErrorMessage(data: jsonDic)
                    if error == nil {
                    
                        if self.resendTimeCounter == 0 {
                            self.view.endEditing(true)
                            self.activationButton.setTitle("RESEND CODE", for: UIControlState.normal)
                            self.setVerifyViewVisibility(isHidden: false)
                            self.resendTimeCounter = 3 * 60
                            self.resendTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.handleResendTimer(timer:)), userInfo: nil, repeats: true)
                        }
                        
                        let user = AKUser(data: jsonDic["data"] as? NSDictionary)
                        
                        AKAppManager.sharedInstance.myUser = user

                    }else {
                        
                        var title = ""
                        if let type =  error?.localizedRecoverySuggestion {
                            title = type
                        }
                        else
                        {
                            //title = error.domain
                            title = "Error"
                        }
                        AKGlobal.showAlertViewController(title: title, message:error!.localizedDescription , target: self)

                    }
                    
                    
                }
            case .failure(let error):
                print(error)
                self.handleURLError(error: error as NSError)

            }
        }

    }
    
    private func authorizeWithSMSCode(verifyCode: String) {
        
        if let _ = AKAppManager.sharedInstance.myUser {
            
            let paramDic = NSMutableDictionary()
            
            paramDic.setObject(phoneNumberStr!, forKey:PHONE as NSCopying)
            paramDic.setObject(verifyCode, forKey: SMS_CODE as NSCopying)
            
            if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
                
                print(dict)
                
                startAnimating(CGSize(width:80, height: 30), message: "Validating..", type: NVActivityIndicatorType(rawValue: 14)!)

                
                AKAPIManager.sharedManager.authorizeWithCode(params: dict, completionHandler: { (response) in
                    
                    
                    self.stopAnimating()
                    switch response.result {
                        
                    case .success:
                        
                        print("Validation Successful")
                        
                        if let json = response.result.value {
                            print("JSON: \(json)")
                            
                            self.alertBar?.hide(completion: {})
                            let jsonDic = json as! NSDictionary
                            
                            if jsonDic["status"] as! Int  == 1 {
                                
                                
                                let user = AKUser(data: jsonDic["data"] as? NSDictionary)
                                AKAppManager.sharedInstance.myUser = user
                                
                                let _ =  AKGlobal.setCliendId(clientID: user.clientID)
                                
                                AKGlobal.saveUserData(user: user)
                                
                                let delayInSeconds: Double = 0.1
                                let popTime: DispatchTime = DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                
                                DispatchQueue.main.asyncAfter(deadline: popTime, execute: {
                                    
                                    let storyboard = UIStoryboard(name: "Camera", bundle: nil)
                                    let controller = storyboard.instantiateViewController(withIdentifier: "AKCameraViewController")
                                    self.navigationController?.pushViewController(controller, animated: true)
                                    if let delegate = UIApplication.shared.delegate as? AppDelegate {
                                        delegate.registerForPushNotifications()
                                    }
                                    
                                })

                            }else {
                                
                                _ = AlertBar.show(.notice, message: "WRONG CODE", duration: 0.5, completion: nil)
                            }
                            
                            
                            
                            
                        }
                    case .failure(let error):
                        
                        print(error)
                        self.handleURLError(error: error as NSError)
                    }
                    
                })
                
            }

        }
        
        
    }
    
    func handleURLError(error : NSError) {
            
        if error.domain == NSURLErrorDomain &&
            (error.code == NSURLErrorNotConnectedToInternet ||
                error.code == NSURLErrorTimedOut ||
                error.code == NSURLErrorNetworkConnectionLost ) {
            self.alertBar?.hide(completion: {
                
            })
            self.alertBar = AlertBar.show(.error, otherButtonsHidden: true, viewInterActionEnabled: true, message: "NO INTERNET CONNECTION", duration: -1, completion: nil)
        }
        else
        {
            var title = ""
            if let type =  error.localizedRecoverySuggestion {
                title = type
            }
            else
            {
                title = "Error"
            }
            AKGlobal.showAlertOnTopController(title: title, message: error.localizedDescription, target: self)
        }
    }
}

extension AKVerifyPhoneViewController : UITextFieldDelegate {
    
    func didChangeTextField(notification: Notification) {
        if let textField = notification.object as? UITextField {
            if textField == phoneNumberTextField {
                let phoneUtil = NBPhoneNumberUtil()
                
                do {
                    let phoneNumber: NBPhoneNumber = try phoneUtil.parse(textField.text, defaultRegion: countryCode)
                    let formattedString: String = try phoneUtil.format(phoneNumber, numberFormat: .E164)
                    if (phoneUtil.isValidNumber(phoneNumber)) || formattedString == "+1234567890" {
                        statusImageView.image = UIImage(named: "icon-correct")
                        isPhoneValid = true
                        
                        self.phoneNumberStr = formattedString
                    } else {
                        statusImageView.image = UIImage(named: "icon-cancel")
                        isPhoneValid = false
                    }
                    NSLog("[%@]", formattedString)
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            
            if let newTextField = verifyTextsView.viewWithTag(textField.tag + 1) as? UITextField {
                if (textField.text?.characters.count)! > 0 {
                    newTextField.becomeFirstResponder()
                }
            }
            
            self.verifyActivationCode()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag >= 10 {
            if self.isBackSpaceTapped(string: string) {
                return true
            }
            let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
            if newLength > 1 {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}

extension AKVerifyPhoneViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        let settingTextVC = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "SettingTextViewControllerId") as! AKSettingTextViewController
        settingTextVC.controllerType = .TermsAndConditions
        self.navigationController?.pushViewController(settingTextVC, animated: true)
        
        return false
    }
    
}
