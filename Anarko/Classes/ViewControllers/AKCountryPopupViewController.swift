//
//  AKCountryPopupViewController.swift
//  Anarko
//
//  Created by x on 10/11/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

class AKCountryPopupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet var countriesTableView: UITableView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var customAlertView: UIView!
    @IBOutlet var backgroundView: UIView!
    
    @IBOutlet var constraintOfCenterY: NSLayoutConstraint!
    var delegate: AKCountryPickerViewControllerDelegate!

    var countries: [Country]!
    var searchedCountries: [Country]!
    
    var didDismissBlock:((AKCountryPopupViewController) ->Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        countries = countryNamesByCode()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKCountryPopupViewController.textFieldDidChange(_:)), name: NSNotification.Name.UITextFieldTextDidChange, object: self.searchTextField)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKCountryPopupViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKCountryPopupViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AKCountryPopupViewController.backgroundViewTapped(_:))))

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    //Responsible for reacting to the user tapping the visualEffect
    @objc private func backgroundViewTapped(_ gesture: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        
        self.hide()
    }
    // MARK: - Keyboard notification
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        self.updateConstraint(value: -(keyboardFrame.height/4.0))
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
        
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        self.updateConstraint(value: 0)
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
        
    }
    
    //update constraint
    
    private func updateConstraint(value:CGFloat) {
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut, animations: {
            
           self.constraintOfCenterY.constant = value
            self.view.layoutIfNeeded()
            
            }, completion: nil)
    }
    
    func countryNamesByCode() -> [Country] {
        var countries = [Country]()
        
        for code in Locale.isoRegionCodes {
            let countryName = (Locale.current as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: code)
            
            let phoneNumberUtil = NBPhoneNumberUtil.sharedInstance()
            let codeNumber = phoneNumberUtil!.getCountryCode(forRegion: code).intValue
            let phoneCode = "+\(codeNumber)"
            
            if phoneCode != "+0" {
                let country = Country(code: code, name: countryName, phoneCode: phoneCode)
                countries.append(country)
            }
        }
        
        countries = countries.sorted(by: { $0.name! < $1.name! })
        
        return countries
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func show() {
        
        let appDelegate = UIApplication.shared.delegate
        let window = appDelegate?.window
        if let rootViewController = window??.rootViewController {
            
            var topViewController = rootViewController
            
            while topViewController.presentedViewController != nil {
                
                topViewController = topViewController.presentedViewController!
            }
            
            //Add the alert view controller to the top most UIViewController
            
            topViewController.addChildViewController(self)
            topViewController.view.addSubview(self.view)
            self.viewWillAppear(true)
            
            self.didMove(toParentViewController: topViewController)
            self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.alpha = 0.0
            self.view.frame = topViewController.view.bounds
            
            customAlertView.isHidden = false
            customAlertView.alpha = 0.0
            
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                self.view.alpha = 1.0
                }, completion: nil)
            customAlertView.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
            
            customAlertView.center = CGPoint(x: self.view.bounds.size.width / 2, y: self.view.bounds.size.height/2 - 10)
            
            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations: {
                
                self.customAlertView.alpha = 1.0
                self.customAlertView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
                }, completion: { (finished) in
                    
            })
            
        }
    }
    
    func hide() {
        
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            self.customAlertView.alpha = 0.0
            self.customAlertView.transform = CGAffineTransform(scaleX: 1.01, y: 1.01)
            self.customAlertView.center    = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height/2.0)-5)
            }, completion: nil)
        
        UIView.animate(withDuration: 0.3, delay: 0.05, options: .curveEaseInOut, animations: { () -> Void in
            self.view.alpha = 0.0
            
        }) { (completed) -> Void in
            
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
            if let dismissBlock = self.didDismissBlock {
                dismissBlock(self)
            }
        }

    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var country = countries[indexPath.row]
        if searchTextField.text != "" {
            
            country = searchedCountries[indexPath.row]
        }
        
        self.delegate?.didSelectCountry!(flag: UIImage(named: (country.code?.lowercased())!)!, name: country.name!, code: country.code!, phcode: country.phoneCode!)
        
        self.hide()
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.searchTextField.text == "" {
            return countries.count;
        }
        else {
            return searchedCountries.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AKCountryPickerCell", for: indexPath) as! AKCountryPickerCell
        
        var country = countries[indexPath.row]
        if searchTextField.text != "" {
            
            country = searchedCountries[indexPath.row]
            
        }
        cell.flagImageView.image = UIImage(named: (country.code?.lowercased())!)
        cell.countryNameLabel.text = country.name
        cell.countryCodeLabel.text = country.phoneCode
        
        return cell
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidChange(_ notification: Notification) {
        
        
        let textField = notification.object as! UITextField
        
        let key = textField.text?.lowercased()
        
        searchedCountries = countries.filter({ (country: Country) -> Bool in
            return ((country.name?.lowercased().range(of: key!)) != nil)
        })
        
        countriesTableView.reloadData()
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }

}
