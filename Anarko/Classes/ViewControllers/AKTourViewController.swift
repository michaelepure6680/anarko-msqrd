//
//  AKTourViewController.swift
//  Anarko
//
//  Created by Hua Wan on 13/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import AVFoundation

class AKTourViewController: UIViewController {

    @IBOutlet var playerImageView: UIImageView!
    
    var tourPlayer: AVPlayer!
    var playerLayer: AVPlayerLayer!
    
    
    var isPushed:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.playTour()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        playerLayer.frame = playerImageView.bounds;
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        
        tourPlayer.pause()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupPushed(value: Bool) {
        
        self.isPushed = value
    }
    
    func playTour() -> Void {
        let url = Bundle.main.url(forResource: "video", withExtension: "mp4")
        let videoAsset = AVURLAsset(url: url!)
        let playerItem = AVPlayerItem(asset: videoAsset)
        tourPlayer = AVPlayer(playerItem: playerItem)
        playerLayer = AVPlayerLayer(player: tourPlayer)
        playerLayer.frame = playerImageView.bounds;
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerImageView.layer.addSublayer(playerLayer)
        tourPlayer.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    func playerItemDidReachEnd(notification: Notification) -> Void {
        tourPlayer.seek(to: kCMTimeZero)
        tourPlayer.play()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - IBAction
    @IBAction func skipPressed(sender: AnyObject) {
        
        if self.isPushed == true {
            let _ = self.navigationController?.popViewController(animated: true)

        }else {
            self.performSegue(withIdentifier: "AKVerifyPhoneViewController", sender: nil)

        }
        
    }

}
