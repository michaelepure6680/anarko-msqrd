//
//  AKHomeViewController.swift
//  Anarko
//
//  Created by Hua Wan on 13/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import SwiftGifOrigin
import Alamofire

class AKHomeViewController: UIViewController {

    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var toursScrollView: UIScrollView!
    @IBOutlet var logoImageView: UIImageView!
    
    var animationTimer : Timer!
    var autoTimer : Timer!
    
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.setupUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() -> Void {
        
        logoImageView.loadGif(name: "logo")
        
        backgroundImageView.image = UIImage.gif(name: "gif-background")

        print("\(UIFont.fontNames(forFamilyName: "Gunplay"))")
        let strings = ["A N O N Y M O U S\nV I D E O S", "P L A Y  W I T H  C R A Z Y\nM A S K S", "B E  A N\nA N O N Y M O U S  H E R O"];
        let ranges1 = [["range": NSStringFromRange(NSMakeRange(0, 17)), NSForegroundColorAttributeName: UIColor.red], ["range": NSStringFromRange(NSMakeRange(18, 11)), NSForegroundColorAttributeName: UIColor.white]]
        let ranges2 = [["range": NSStringFromRange(NSMakeRange(0, 27)), NSForegroundColorAttributeName: UIColor.white], ["range": NSStringFromRange(NSMakeRange(28, 9)), NSForegroundColorAttributeName: UIColor.red]]
        let ranges3 = [["range": NSStringFromRange(NSMakeRange(0, 9)), NSForegroundColorAttributeName: UIColor.white], ["range": NSStringFromRange(NSMakeRange(9, 26)), NSForegroundColorAttributeName: UIColor.red]]
        let ranges = [ranges1, ranges2, ranges3]
        var index = 0
        for view in toursScrollView.subviews {
            if let label = view as? UILabel {
                let attributes = ranges[index]
                AKUtilities.setTitleLabel(label, attributedString: strings[index], attributes: attributes)
                index += 1
            }
        }
        
        currentIndex = 0;
        autoTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(handleAnimationTimer(timer:)), userInfo: nil, repeats: true)
    }

    func handleAnimationTimer(timer: Timer) -> Void {
        currentIndex += 1
        if currentIndex == 3 {
            currentIndex = 0;
        }
        self.toursScrollView.setContentOffset(CGPoint(x: currentIndex * Int(self.toursScrollView.frame.size.width), y: 0), animated: true)
    }

    // MARK: IBAction
//    @IBAction func anonymousPressed(sender: AnyObject) {
//        animationTimer = Timer(timeInterval: 2.0, target: self, selector: #selector(handleAnimationTimer(timer:)), userInfo: nil, repeats: true)
//    }
    
//    @IBAction func watchTourPressed(sender: AnyObject) {
//        if autoTimer != nil {
//            autoTimer.invalidate()
//            autoTimer = nil
//        }
//        self.performSegue(withIdentifier: "AKTourViewController", sender: nil)
//    }

    @IBAction func breakItPressed(_ sender: Any) {
        if autoTimer != nil {
            autoTimer.invalidate()
            autoTimer = nil
        }
        self.performSegue(withIdentifier: "AKVerifyPhoneViewController", sender: nil)
    }
}

