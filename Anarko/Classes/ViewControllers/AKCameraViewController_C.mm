//
//  AKCameraViewController.m
//  Anarko
//
//  Created by Hua Wan on 16/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

#import "AKCameraViewController.h"

#import <GLKit/GLKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Math.h>

#import "Anarko-Swift.h"

#import "UIImage+Resize.h"
#import "AKFaceFeature.h"
#import "FrameRateCalculator.h"

// enum for image orientation
enum {
    PHOTOS_EXIF_0ROW_TOP_0COL_LEFT          = 1, //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
    PHOTOS_EXIF_0ROW_TOP_0COL_RIGHT         = 2, //   2  =  0th row is at the top, and 0th column is on the right.
    PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT      = 3, //   3  =  0th row is at the bottom, and 0th column is on the right.
    PHOTOS_EXIF_0ROW_BOTTOM_0COL_LEFT       = 4, //   4  =  0th row is at the bottom, and 0th column is on the left.
    PHOTOS_EXIF_0ROW_LEFT_0COL_TOP          = 5, //   5  =  0th row is on the left, and 0th column is the top.
    PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP         = 6, //   6  =  0th row is on the right, and 0th column is the top.
    PHOTOS_EXIF_0ROW_RIGHT_0COL_BOTTOM      = 7, //   7  =  0th row is on the right, and 0th column is the bottom.
    PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM       = 8  //   8  =  0th row is on the left, and 0th column is the bottom.
};

// filtering value for smooth moving of face
static float kFilteringFactor = 0.1f;
static float kOffsetFactor = 48.0f;
static CGFloat totalDuration = 5.0f;
static CGColorSpaceRef sDeviceRgbColorSpace = nil;

static CGAffineTransform FCGetTransformForDeviceOrientation(UIDeviceOrientation orientation, BOOL mirrored)
{
    // Internal comment: This routine assumes that the native camera image is always coming from a UIDeviceOrientationLandscapeLeft (i.e. the home button is on the RIGHT, which equals AVCaptureVideoOrientationLandscapeRight!), although in the future this assumption may not hold; better to get video output's capture connection's videoOrientation property, and apply the transform according to the native video orientation
    // Also, it may be desirable to apply the flipping as a separate step after we get the rotation transform
    CGAffineTransform result;
    switch (orientation) {
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationFaceUp:
        case UIDeviceOrientationFaceDown:
            result = CGAffineTransformMakeRotation(M_PI_2);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            result = CGAffineTransformMakeRotation((3 * M_PI_2));
            break;
        case UIDeviceOrientationLandscapeLeft:
            result = mirrored ?  CGAffineTransformMakeRotation(M_PI) : CGAffineTransformIdentity;
            break;
        default:
            result = mirrored ? CGAffineTransformIdentity : CGAffineTransformMakeRotation(M_PI);
            break;
    }
    
    return result;
}

@interface AKCameraViewController () <AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate>
{
    IBOutlet UIView *cameraView;
    IBOutlet UIView *controlView;
    IBOutlet UIButton *flashButton;
    IBOutlet UIScrollView *masksScrollView;
    IBOutlet UIButton *captureButton;
    IBOutlet UIView *recordingView;
    IBOutlet KDCircularProgress *progressView;
    
    GLKView *videoPreviewView;
    CIContext *ciContext;
    EAGLContext *eaglContext;
    CGRect videoPreviewViewBounds;
    
    AVCaptureDevice *audioDevice;
    AVCaptureDevice *videoDevice;
    AVCaptureSession *captureSession;
    
    AVAssetWriter *assetWriter;
    AVAssetWriterInput *assetWriterAudioInput;
    AVAssetWriterInput *assetWriterVideoInput;
    AVAssetWriterInputPixelBufferAdaptor *assetWriterInputPixelBufferAdaptor;
    NSURL *recordedURL;
    NSTimer *recordTimer;
    CGFloat recordedTime;
    
    CIDetector *faceDetector;
    
    dispatch_queue_t captureSessionQueue;
    UIBackgroundTaskIdentifier backgroundRecordingID;
    
    BOOL videoWritingStarted;
    CMTime videoWrtingStartTime;
    CMFormatDescriptionRef currentAudioSampleBufferFormatDescription;
    CMVideoDimensions currentVideoDimensions;
    CMTime currentVideoTime;
    FrameRateCalculator *frameRateCalculator;
    
    NSArray *arrayEyeFeatures;
    AKFaceFeature *selectedEyeFeature;
    CIImage *selectedFace;
    CIImage *faceImage;
    UIImage *selectedImage;
    NSTimer *faceUpdateTimer;
    CGRect lastFaceRect;
    NSDate *lastFaceDate;
    CGRect lastFaceDrawBounds;
    CGRect lastFaceRenderBounds;
    
    BOOL isNeedFaceDetect;
    BOOL isNeedTake;
    BOOL isFirstLayout;
}
@end

@implementation AKCameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (sDeviceRgbColorSpace == nil)
        sDeviceRgbColorSpace = CGColorSpaceCreateDeviceRGB();
    
    frameRateCalculator = [[FrameRateCalculator alloc] init];
    
    isFirstLayout = YES;
    
    progressView.progressColors = @[[UIColor colorWithRed:0.960 green:0.631 blue:0 alpha:1], [UIColor redColor], [UIColor blackColor]];
    progressView.trackColor = [UIColor clearColor];
    progressView.progressInsideFillColor = [UIColor clearColor];
    progressView.progressThickness = 0.22;
    progressView.trackThickness = 0.22;
    progressView.clockwise = true;
    progressView.gradientRotateSpeed = 1;
    progressView.roundedCorners = NO;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (isFirstLayout == NO)
        return;
    
    isFirstLayout = NO;
    
    [self initialize];
    
    [self startVideoRecorder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (isFirstLayout == NO)
        [self startVideoRecorder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self configFaces];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self stopVideoRecorder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initialize
{
    if (videoPreviewView)
        return;
    
    cameraView.backgroundColor = [UIColor blackColor];
    
    // setup the GLKView for video/image preview
    eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    videoPreviewView = [[GLKView alloc] initWithFrame:cameraView.bounds context:eaglContext];
    
    videoPreviewView.enableSetNeedsDisplay = NO;
    
    [cameraView insertSubview:videoPreviewView belowSubview:controlView];
    videoPreviewViewBounds = videoPreviewView.bounds;
    
    CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI_2);
    transform = CGAffineTransformTranslate(transform, fabs(videoPreviewViewBounds.size.height - videoPreviewViewBounds.size.width) / 2.0,
                                           fabs(videoPreviewViewBounds.size.height - videoPreviewViewBounds.size.width) / 2.0);
    videoPreviewView.transform = transform;
    videoPreviewView.frame = cameraView.bounds;
    
    // create the CIContext instance, note that this must be done after videoPreviewView is properly set up
    ciContext = [CIContext contextWithEAGLContext:eaglContext options:@{kCIContextWorkingColorSpace : [NSNull null]} ];
    
    // bind the frame buffer to get the frame buffer width and height;
    // the bounds used by CIContext when drawing to a GLKView are in pixels (not points),
    [videoPreviewView bindDrawable];
    videoPreviewViewBounds = CGRectZero;
    videoPreviewViewBounds.size.width = videoPreviewView.drawableWidth;
    videoPreviewViewBounds.size.height = videoPreviewView.drawableHeight;
    
    captureSessionQueue = dispatch_queue_create("capture_session_queue", NULL);
    
    NSDictionary * options = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:0] forKey:CIDetectorImageOrientation];
    CIContext *faceContext = [CIContext contextWithOptions:options];
    faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:faceContext options:@{CIDetectorAccuracy : CIDetectorAccuracyLow }];
    
    [self startVideoRecorder];
}

- (void)startVideoRecorder
{
    if (faceUpdateTimer)
    {
        [faceUpdateTimer invalidate];
        faceUpdateTimer = nil;
    }
    
    faceUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(handleFaceUpdateTimer:) userInfo:nil repeats:YES];
    
    if ([[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count] > 0)
    {
        // find the audio device
        NSArray *audioDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio];
        if ([audioDevices count])
            audioDevice = [audioDevices objectAtIndex:0];  // use the first audio device
        
        dispatch_async(captureSessionQueue, ^(void) {
            NSError *error = nil;
            
            // get the input device and also validate the settings
            NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
            
            AVCaptureDevicePosition position = AVCaptureDevicePositionFront;
            
            videoDevice = nil;
            for (AVCaptureDevice *device in videoDevices)
            {
                if (device.position == position) {
                    videoDevice = device;
                    break;
                }
            }
            
            if (!videoDevice)
                videoDevice = [videoDevices objectAtIndex:0];
            
            [videoDevice lockForConfiguration:nil];
            [videoDevice unlockForConfiguration];
            // obtain device input
            AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
            if (!videoDeviceInput)
            {
                [self showAlertViewWithMessage:[NSString stringWithFormat:@"Unable to obtain video device input, error: %@", error] title:@"Error"];
                return;
            }
            
            AVCaptureDeviceInput *audioDeviceInput = nil;
            if (audioDevice)
            {
                audioDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&error];
                if (!audioDeviceInput)
                {
                    [self showAlertViewWithMessage:[NSString stringWithFormat:@"Unable to obtain audio device input, error: %@", error] title:@"Error"];
                    return;
                }
            }
            
            // obtain the preset and validate the preset
            if (_captureSessionPreset == nil || ![videoDevice supportsAVCaptureSessionPreset:_captureSessionPreset])
                _captureSessionPreset = AVCaptureSessionPreset1280x720;
            
            if (![videoDevice supportsAVCaptureSessionPreset:_captureSessionPreset])
            {
                [self showAlertViewWithMessage:[NSString stringWithFormat:@"Capture session preset not supported by video device: %@", _captureSessionPreset] title:@"Error"];
                return;
            }
            
            // CoreImage wants BGRA pixel format
            NSDictionary *outputSettings = @{ (id)kCVPixelBufferPixelFormatTypeKey : [NSNumber numberWithInteger:kCVPixelFormatType_32BGRA]};
            
            // create the capture session
            captureSession = [[AVCaptureSession alloc] init];
            captureSession.sessionPreset = _captureSessionPreset;
            
            // create and configure video data output
            AVCaptureVideoDataOutput *videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
            videoDataOutput.videoSettings = outputSettings;
            videoDataOutput.alwaysDiscardsLateVideoFrames = YES;
            [videoDataOutput setSampleBufferDelegate:self queue:captureSessionQueue];
            
            // configure audio data output
            AVCaptureAudioDataOutput *audioDataOutput = nil;
            if (audioDevice) {
                audioDataOutput = [[AVCaptureAudioDataOutput alloc] init];
                [audioDataOutput setSampleBufferDelegate:self queue:captureSessionQueue];
            }
            
            // begin configure capture session
            [captureSession beginConfiguration];
            
            if (![captureSession canAddOutput:videoDataOutput])
            {
                [self showAlertViewWithMessage:@"Cannot add video data output" title:@"Error"];
                captureSession = nil;
                return;
            }
            
            if (audioDataOutput)
            {
                if (![captureSession canAddOutput:audioDataOutput])
                {
                    [self showAlertViewWithMessage:@"Cannot add still audio data output" title:@"Error"];
                    captureSession = nil;
                    return;
                }
            }
            
            // connect the video device input and video data and still image outputs
            [captureSession addInput:videoDeviceInput];
            [captureSession addOutput:videoDataOutput];
            
            if (audioDevice)
            {
                [captureSession addInput:audioDeviceInput];
                [captureSession addOutput:audioDataOutput];
            }
            
            [captureSession commitConfiguration];
            
            // then start everything
            [frameRateCalculator reset];
            [captureSession startRunning];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                //[self _startLabelUpdateTimer];
            });
        });
    }
}

- (void)stopVideoRecorder
{
    if (!captureSession || !captureSession.running)
        return;
    
    [captureSession stopRunning];
    
    dispatch_sync(captureSessionQueue, ^{
        NSLog(@"waiting for capture session to end");
    });
    
    [self stopWriting];
    
    captureSession = nil;
    videoDevice = nil;
}

- (void)configFaces
{
    NSDictionary *root = [NSDictionary dictionaryWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"MaskFeature" withExtension:@"plist"]];
    arrayEyeFeatures = root[@"MaskFeatures"];
    
    [masksScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    float x = 4;
    float height = masksScrollView.frame.size.height - 8;
    for (int i = 0; i < arrayEyeFeatures.count; i++)
    {
        NSDictionary *mask = arrayEyeFeatures[i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(x, 4, height, height);
        button.tag = i;
        button.layer.cornerRadius = 2.0f;
        button.layer.borderColor = [UIColor whiteColor].CGColor;
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-mask-normal", mask[@"Name"]]];
        [button setImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(changeMaskPressed:) forControlEvents:UIControlEventTouchUpInside];
        x += height + 4;
        [masksScrollView addSubview:button];
    }
    
    selectedImage = [[UIImage imageNamed:@"clown-mask-normal"] rotateImageByRadian:-M_PI_2];
    faceImage = selectedImage.CIImage;
    if (faceImage == nil)
        faceImage = [CIImage imageWithCGImage:selectedImage.CGImage];
    masksScrollView.contentSize = CGSizeMake(x, height + 8);
    
    selectedFace = faceImage;
    selectedEyeFeature = [AKFaceFeature faceFeatureFromDictionary:arrayEyeFeatures[0]];
}

- (void)startWriting
{
    dispatch_async(captureSessionQueue, ^{
        NSError *error = nil;
        
        // remove the temp file, if any
        recordedURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"videorecorder.mov"]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:[recordedURL path]])
            [[NSFileManager defaultManager] removeItemAtURL:recordedURL error:NULL];
        
        AVAssetWriter *newAssetWriter = [AVAssetWriter assetWriterWithURL:recordedURL fileType:AVFileTypeQuickTimeMovie error:&error];
        if (!newAssetWriter || error) {
            [self showAlertViewWithMessage:[NSString stringWithFormat:@"Cannot create asset writer, error: %@", error] title:@"Error"];
            return;
        }
        
        NSDictionary *videoCompressionSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                                  AVVideoCodecH264, AVVideoCodecKey,
                                                  [NSNumber numberWithInteger:currentVideoDimensions.width], AVVideoWidthKey,
                                                  [NSNumber numberWithInteger:currentVideoDimensions.height], AVVideoHeightKey,
                                                  nil];
        
        assetWriterVideoInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoCompressionSettings];
        assetWriterVideoInput.expectsMediaDataInRealTime = YES;
        
        // create a pixel buffer adaptor for the asset writer; we need to obtain pixel buffers for rendering later from its pixel buffer pool
        assetWriterInputPixelBufferAdaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:assetWriterVideoInput sourcePixelBufferAttributes:
                                              [NSDictionary dictionaryWithObjectsAndKeys:
                                               [NSNumber numberWithInteger:kCVPixelFormatType_32BGRA], (id)kCVPixelBufferPixelFormatTypeKey,
                                               [NSNumber numberWithUnsignedInteger:currentVideoDimensions.width], (id)kCVPixelBufferWidthKey,
                                               [NSNumber numberWithUnsignedInteger:currentVideoDimensions.height], (id)kCVPixelBufferHeightKey,
                                               (id)kCFBooleanTrue, (id)kCVPixelFormatOpenGLESCompatibility,
                                               nil]];
        
        
        UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
        
        // give correct orientation information to the video
        if (videoDevice.position == AVCaptureDevicePositionFront)
            assetWriterVideoInput.transform = FCGetTransformForDeviceOrientation(orientation, YES);
        else
            assetWriterVideoInput.transform = FCGetTransformForDeviceOrientation(orientation, NO);
        
        BOOL canAddInput = [newAssetWriter canAddInput:assetWriterVideoInput];
        if (!canAddInput) {
            [self showAlertViewWithMessage:@"Cannot add asset writer video input" title:@"Error"];
            assetWriterAudioInput = nil;
            assetWriterVideoInput = nil;
            return;
        }
        
        [newAssetWriter addInput:assetWriterVideoInput];
        
        if (audioDevice) {
            size_t layoutSize = 0;
            const AudioChannelLayout *channelLayout = CMAudioFormatDescriptionGetChannelLayout(currentAudioSampleBufferFormatDescription, &layoutSize);
            const AudioStreamBasicDescription *basicDescription = CMAudioFormatDescriptionGetStreamBasicDescription(currentAudioSampleBufferFormatDescription);
            
            NSData *channelLayoutData = [NSData dataWithBytes:channelLayout length:layoutSize];
            if (channelLayoutData == nil)
                channelLayoutData = [NSData data];
            
            // record the audio at AAC format, bitrate 64000, sample rate and channel number using the basic description from the audio samples
            NSDictionary *audioCompressionSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                                      [NSNumber numberWithInteger:kAudioFormatMPEG4AAC], AVFormatIDKey,
                                                      [NSNumber numberWithInteger:basicDescription->mChannelsPerFrame], AVNumberOfChannelsKey,
                                                      [NSNumber numberWithFloat:basicDescription->mSampleRate], AVSampleRateKey,
                                                      [NSNumber numberWithInteger:64000], AVEncoderBitRateKey,
                                                      //channelLayoutData, AVChannelLayoutKey,
                                                      nil];
            
            if ([newAssetWriter canApplyOutputSettings:audioCompressionSettings forMediaType:AVMediaTypeAudio]) {
                assetWriterAudioInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeAudio outputSettings:audioCompressionSettings];
                assetWriterAudioInput.expectsMediaDataInRealTime = YES;
                
                if ([newAssetWriter canAddInput:assetWriterAudioInput])
                    [newAssetWriter addInput:assetWriterAudioInput];
                else
                    [self showAlertViewWithMessage:@"Couldn't add asset writer audio input"
                                             title:@"Warning"];
            }
            else
                [self showAlertViewWithMessage:@"Couldn't apply audio output settings."
                                         title:@"Warning"];
        }
        
        // Make sure we have time to finish saving the movie if the app is backgrounded during recording
        // cf. the RosyWriter sample app from WWDC 2011
        if ([[UIDevice currentDevice] isMultitaskingSupported])
            backgroundRecordingID = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{}];
        
        videoWritingStarted = NO;
        assetWriter = newAssetWriter;
    });
}

- (void)abortWriting
{
    if (!assetWriter)
        return;
    
    [assetWriter cancelWriting];
    assetWriterAudioInput = nil;
    assetWriterVideoInput = nil;
    assetWriter = nil;
    
    // remove the temp file
    NSURL *fileURL = [assetWriter outputURL];
    [[NSFileManager defaultManager] removeItemAtURL:fileURL error:NULL];
    
    void (^resetUI)(void) = ^(void) {
        [captureButton setTitle:@"Start" forState:UIControlStateNormal];
        captureButton.enabled = YES;
        
        // end the background task if it's done there
        // cf. The RosyWriter sample app from WWDC 2011
        if ([[UIDevice currentDevice] isMultitaskingSupported])
            [[UIApplication sharedApplication] endBackgroundTask:backgroundRecordingID];
    };
    
    dispatch_async(dispatch_get_main_queue(), resetUI);
}

- (void)stopWriting
{
    if (!assetWriter)
        return;
    
    
    AVAssetWriter *writer = assetWriter;
    
    assetWriterAudioInput = nil;
    assetWriterVideoInput = nil;
    assetWriterInputPixelBufferAdaptor = nil;
    assetWriter = nil;
    
    captureButton.enabled = NO;
    
    void (^resetUI)(void) = ^(void) {
        captureButton.hidden = NO;
        recordingView.hidden = YES;
        
        captureButton.enabled = YES;
        
        NSLog(@"Frame Rate = %f", frameRateCalculator.frameRate);
        
        // end the background task if it's done there
        // cf. The RosyWriter sample app from WWDC 2011
        if ([[UIDevice currentDevice] isMultitaskingSupported])
            [[UIApplication sharedApplication] endBackgroundTask:backgroundRecordingID];
    };
    
    dispatch_async(captureSessionQueue, ^(void){
        [writer finishWritingWithCompletionHandler:^(void){
            if (writer.status == AVAssetWriterStatusFailed)
            {
                dispatch_async(dispatch_get_main_queue(), resetUI);
                [self showAlertViewWithMessage:@"Cannot complete writing the video, the output could be corrupt." title:@"Error"];
            }
            else if (writer.status == AVAssetWriterStatusCompleted)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    recordedURL = [writer outputURL];
                    [self performSegueWithIdentifier:@"AKEditViewController" sender:recordedURL];
                });
            }
            dispatch_async(dispatch_get_main_queue(), resetUI);
        }];
        
    });
}

- (void)handleFaceUpdateTimer:(NSTimer *)timer
{
    isNeedFaceDetect = YES;
}

- (void)handleRecordTimer:(NSTimer *)timer
{
    recordedTime += timer.timeInterval;
    progressView.startAngle = -90;
    progressView.angle = recordedTime / totalDuration * 360;
    
    if (recordedTime >= totalDuration)
    {
        if (assetWriter)
        {
            captureButton.hidden = NO;
            recordingView.hidden = YES;
            
            [self stopWriting];
        }
    
        [recordTimer invalidate];
        recordTimer = nil;
    }
}

- (void)showAlertViewWithMessage:(NSString *)message title:(NSString *)title
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:controller animated:YES completion:nil];
    });
}

- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
            return device;
    }
    
    return nil;
}

#pragma mark Device Counts
- (NSUInteger)cameraCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count];
}

- (NSUInteger)micCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] count];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to `the new view controller.
    if ([segue.identifier isEqualToString:@"AKEditViewController"])
    {
        AKEditViewController *controller = segue.destinationViewController;
        controller.videoURL = recordedURL;
    }
}

#pragma mark - IBAction
- (IBAction)changeCameraPressed:(id)sender {
    if (captureSession)
    {
        [captureSession beginConfiguration];
        
        AVCaptureInput *currentCameraInput = [captureSession.inputs objectAtIndex:0];
        
        [captureSession removeInput:currentCameraInput];
        
        AVCaptureDevice *newCamera = nil;
        
        if (((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionBack)
        {
            newCamera = [self cameraWithPosition:AVCaptureDevicePositionFront];
        }
        else
        {
            newCamera = [self cameraWithPosition:AVCaptureDevicePositionBack];
        }
        
        NSError *error = nil;
        
        AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:newCamera error:&error];
        
        if (!newVideoInput || error)
        {
            NSLog(@"Error creating capture device input: %@", error.localizedDescription);
        }
        else
        {
            if ([captureSession canAddInput:newVideoInput])
            [captureSession addInput:newVideoInput];
            else
            {
                AVCaptureInput *currentCameraInput = [captureSession.inputs objectAtIndex:0];
                [captureSession removeInput:currentCameraInput];
                [captureSession addInput:newVideoInput];
            }
        }
        
        [captureSession commitConfiguration];
    }
}

- (IBAction)explorerPressed:(id)sender {
    
}

- (IBAction)flashPressed:(id)sender {
    
}

- (IBAction)capturePressed:(id)sender {
    if (!captureSession.running)
        [captureSession startRunning];
    
    if (assetWriter)
    {
        captureButton.hidden = NO;
        recordingView.hidden = YES;
        
        [self stopWriting];
        
        if (recordTimer)
        {
            [recordTimer invalidate];
            recordTimer = nil;
        }
    }
    else
    {
        captureButton.hidden = YES;
        recordingView.hidden = NO;
        
        [self startWriting];
    
        recordedTime = 0;
        recordTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(handleRecordTimer:) userInfo:nil repeats:YES];
    }
}

- (IBAction)storePressed:(id)sender {
    
}

- (IBAction)changeMaskPressed:(UIButton *)sender {
    selectedImage = [[sender imageForState:UIControlStateNormal] rotateImageByRadian:-M_PI_2];
    faceImage = selectedImage.CIImage;
    if (faceImage == nil)
    faceImage = [CIImage imageWithCGImage:selectedImage.CGImage];
    selectedFace = faceImage;
    selectedEyeFeature = [AKFaceFeature faceFeatureFromDictionary:arrayEyeFeatures[sender.tag]];
}

- (IBAction)handleSwipeGesture:(id)sender {
    
}

#pragma mark - AVCaptureVideoDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    CMFormatDescriptionRef formatDesc = CMSampleBufferGetFormatDescription(sampleBuffer);
    CMMediaType mediaType = CMFormatDescriptionGetMediaType(formatDesc);
    
    // write the audio data if it's from the audio connection
    if (mediaType == kCMMediaType_Audio)
    {
        CMFormatDescriptionRef tmpDesc = currentAudioSampleBufferFormatDescription;
        currentAudioSampleBufferFormatDescription = formatDesc;
        CFRetain(currentAudioSampleBufferFormatDescription);
        
        if (tmpDesc)
        CFRelease(tmpDesc);
        
        // we need to retain the sample buffer to keep it alive across the different queues (threads)
        if (assetWriter && assetWriterAudioInput.readyForMoreMediaData && ![assetWriterAudioInput appendSampleBuffer:sampleBuffer])
        {
            [self showAlertViewWithMessage:@"Cannot write audio data, recording aborted" title:@"Error"];
            [self abortWriting];
        }
        
        return;
    }
    
    // if not from the audio capture connection, handle video writing
    CMTime timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
    [frameRateCalculator calculateFramerateAtTimestamp:timestamp];
    
    // update the video dimensions information
    currentVideoDimensions = CMVideoFormatDescriptionGetDimensions(formatDesc);
    
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate);
    CIImage *sourceImage = [CIImage imageWithCVPixelBuffer:(CVPixelBufferRef)imageBuffer options:(__bridge NSDictionary *)attachments];
    
    BOOL shouldMirror = (AVCaptureDevicePositionFront == videoDevice.position);
    if (shouldMirror)
    sourceImage = [sourceImage imageByApplyingTransform:CGAffineTransformTranslate(CGAffineTransformMakeScale(1, -1), 0, -sourceImage.extent.size.height)];
    
    // run the filter through the filter chain
    CGRect sourceExtent = sourceImage.extent;
    
    CGFloat sourceAspect = sourceExtent.size.width / sourceExtent.size.height;
    CGFloat previewAspect = videoPreviewViewBounds.size.width  / videoPreviewViewBounds.size.height;
    
    // we want to maintain the aspect radio of the screen size, so we clip the video image
    CGRect drawRect = sourceExtent;
    if (sourceAspect > previewAspect)
    {
        // use full height of the video image, and center crop the width
        drawRect.origin.x += (drawRect.size.width - drawRect.size.height * previewAspect) / 2.0f;
        drawRect.size.width = drawRect.size.height * previewAspect;
    }
    else
    {
        // use full width of the video image, and center crop the height
        drawRect.origin.y += (drawRect.size.height - drawRect.size.width / previewAspect) / 2.0f;
        drawRect.size.height = drawRect.size.width / previewAspect;
    }
    
    if (eaglContext != [EAGLContext currentContext])
    [EAGLContext setCurrentContext:eaglContext];
    
    [videoPreviewView bindDrawable];
    
    // clear eagl view to grey
    glClearColor(0.5, 0.5, 0.5, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    // set the blend mode to "source over" so that CI will use that
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    //    [ciContext drawImage:sourceImage inRect:videoPreviewViewBounds fromRect:drawRect];
    
    /* kCGImagePropertyOrientation values
     The intended display orientation of the image. If present, this key is a CFNumber value with the same value as defined
     by the TIFF and EXIF specifications -- see enumeration of integer constants.
     The value specified where the origin (0,0) of the image is located. If not present, a value of 1 is assumed.
     
     used when calling featuresInImage: options: The value for this key is an integer NSNumber from 1..8 as found in kCGImagePropertyOrientation.
     If present, the detection will be done based on that orientation but the coordinates in the returned features will still be based on those of the image. */
    int exifOrientation;
    UIDeviceOrientation curDeviceOrientation = [[UIDevice currentDevice] orientation];
    switch (curDeviceOrientation) {
        case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
            exifOrientation = PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM;
            break;
        case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
            if (videoDevice.position == AVCaptureDevicePositionFront)
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            break;
        case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
            if (videoDevice.position == AVCaptureDevicePositionFront)
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            break;
        case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
        default:
            exifOrientation = PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP;
            break;
    }
    
    NSDictionary *imageOptions = @{CIDetectorImageOrientation : [NSNumber numberWithInt:exifOrientation]};
    NSArray *features = [NSArray array];
    if (isNeedFaceDetect)
    {
        isNeedFaceDetect = NO;
        features = [faceDetector featuresInImage:sourceImage options:imageOptions];
    }
    
    CIImage *drawImage = [self drawFaceWithSource:sourceImage drawRect:drawRect faces:features];
    [videoPreviewView display];
    
    if (isNeedTake == YES)
    {
        isNeedTake = NO;
        CGImageRef imageRef = [ciContext createCGImage:drawImage fromRect:drawImage.extent];
        if (imageRef)
        {
            UIImage *image = [UIImage imageWithCGImage:imageRef];
            image = [self rotateImage:image byRadian:M_PI_2];
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
            CGImageRelease(imageRef);
        }
    }
    
    if (assetWriter != nil)
    {
        // if we need to write video and haven't started yet, start writing
        if (!videoWritingStarted)
        {
            videoWritingStarted = YES;
            BOOL success = [assetWriter startWriting];
            if (!success)
            {
                [self showAlertViewWithMessage:@"Cannot write video data, recording aborted" title:@"Error"];
                [self abortWriting];
                return;
            }
            
            [assetWriter startSessionAtSourceTime:timestamp];
            videoWrtingStartTime = timestamp;
            currentVideoTime = videoWrtingStartTime;
        }
        
        CVPixelBufferRef renderedOutputPixelBuffer = NULL;
        OSStatus status = CVPixelBufferPoolCreatePixelBuffer(nil, assetWriterInputPixelBufferAdaptor.pixelBufferPool, &renderedOutputPixelBuffer);
        if (status)
        {
            NSLog(@"Cannot obtain a pixel buffer from the buffer pool");
            return;
        }
        
        [self renderFaceWithSource:sourceImage pixelBuffer:renderedOutputPixelBuffer faces:features];
        
        currentVideoTime = timestamp;
        // write the video data
        if (assetWriterVideoInput.readyForMoreMediaData)
        [assetWriterInputPixelBufferAdaptor appendPixelBuffer:renderedOutputPixelBuffer withPresentationTime:timestamp];
        
        CVPixelBufferRelease(renderedOutputPixelBuffer);
    }
}
    
- (void)captureOutput:(AVCaptureOutput *)captureOutput didDropSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    NSLog(@"didDropSampleBuffer : %@", captureOutput.description);
}
    
- (CGImageRef)imageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer // Create a CGImageRef from sample buffer data
{
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer,0);        // Lock the image buffer
    
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);   // Get information of the image
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef image = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    
    CGColorSpaceRelease(colorSpace);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    /* CVBufferRelease(imageBuffer); */  // do not call this!
    
    return image;
}

// rotate UIImage by radian value
- (UIImage *)rotateImage:(UIImage *)image byRadian:(CGFloat)radian
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(radian);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContextWithOptions(rotatedSize, NO, [UIScreen mainScreen].scale);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width / 2, rotatedSize.height / 2);
    
    // Rotate the image context
    CGContextRotateCTM(bitmap, radian);
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-image.size.width / 2, -image.size.height / 2, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *rotateImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return rotateImage;
}

// rotate image by radian value
- (CIImage *)rotateImage:(CIImage *)image radian:(float)radian
{
    CIFilter *filter = [CIFilter filterWithName:@"CIAffineTransform"];
    CGAffineTransform transform = CGAffineTransformMakeRotation(radian);
    [filter setValue:[NSValue valueWithCGAffineTransform:transform] forKey:@"inputTransform"];
    [filter setValue:image forKey:@"inputImage"];
    CIImage *rotatedImage = [filter valueForKey:@"outputImage"];
    
    return rotatedImage;
}

// scale iamge by scale value
- (CIImage *)scaleImage:(CIImage *)image scale:(float)scale
{
    CIFilter *filter = [CIFilter filterWithName:@"CIAffineTransform"];
    CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
    [filter setValue:[NSValue valueWithCGAffineTransform:transform] forKey:@"inputTransform"];
    [filter setValue:image forKey:@"inputImage"];
    CIImage *scaledImage = [filter valueForKey:@"outputImage"];
    
    return scaledImage;
}

// draw the face image to source image width face to display
- (CIImage *)drawFaceWithSource:(CIImage *)sourceImage drawRect:(CGRect)drawRect faces:(NSArray *)features
{
    CIImage *drawImage = sourceImage;
    if (features != nil && features.count > 0)
    {
        for (CIFaceFeature *face in features)
        {
            CGRect faceBounds = face.bounds;
            //NSLog(@"%.2f, %.2f, %.2f, %.2f", faceBounds.origin.x, faceBounds.origin.y, faceBounds.size.width, faceBounds.size.height);
            
            faceBounds = [self boundsWithSourceImage:sourceImage faceFeature:face];
            
            if (CGRectEqualToRect(lastFaceRenderBounds, CGRectZero))
            lastFaceRenderBounds = faceBounds;
            faceBounds = [self filterFaceBoundsForRender:faceBounds];
            
            CGFloat scale = faceBounds.size.height / faceImage.extent.size.height;
            faceImage = [self scaleImage:faceImage scale:scale];
            
            // Draw the face image to source image
            drawImage = [self applyFace:faceImage faceBounds:faceBounds toSource:sourceImage];
            
            // Save the last face detection bounds and time to use when the face detection is failed.
            lastFaceRect = faceBounds;
            lastFaceDate = [NSDate date];
        }
    }
    else
    {
        if (lastFaceDate != nil && [[NSDate date] timeIntervalSinceDate:lastFaceDate] <= 0.6)
        {
            if (CGRectEqualToRect(lastFaceRect, CGRectZero))
            drawImage = sourceImage;
            else
            drawImage = [self applyFace:faceImage faceBounds:lastFaceRect toSource:sourceImage];
        }
    }
    
    [ciContext drawImage:drawImage inRect:videoPreviewViewBounds fromRect:drawRect];
    
    return drawImage;
}

// render the source image from camera to pixel buffer with face to record
- (void)renderFaceWithSource:(CIImage *)sourceImage pixelBuffer:(CVPixelBufferRef)pixelBuffer faces:(NSArray *)features
{
    CIImage *drawImage = sourceImage;
    if (features != nil && features.count > 0)
    {
        for (CIFaceFeature *face in features)
        {
            CGRect faceBounds = face.bounds;
            NSLog(@"%.2f, %.2f, %.2f, %.2f", faceBounds.origin.x, faceBounds.origin.y, faceBounds.size.width, faceBounds.size.height);
            
            faceBounds = [self boundsWithSourceImage:sourceImage faceFeature:face];
            
            if (CGRectEqualToRect(lastFaceRenderBounds, CGRectZero))
            lastFaceRenderBounds = faceBounds;
            faceBounds = [self filterFaceBoundsForRender:faceBounds];
            
            CGFloat scale = faceBounds.size.height / faceImage.extent.size.height;
            faceImage = [self scaleImage:faceImage scale:scale];
            
            // Save the last face detection bounds and time to use when the face detection is failed.
            lastFaceRect = faceBounds;
            lastFaceDate = [NSDate date];
            
            // Draw the face image to source image
            drawImage = [self applyFace:faceImage faceBounds:faceBounds toSource:sourceImage];
        }
    }
    else
    {
        if (lastFaceDate != nil && [[NSDate date] timeIntervalSinceDate:lastFaceDate] <= 0.6)
        {
            if (CGRectEqualToRect(lastFaceRect, CGRectZero))
            drawImage = sourceImage;
            else
            drawImage = [self applyFace:faceImage faceBounds:lastFaceRect toSource:sourceImage];
        }
    }
    
    [ciContext render:drawImage toCVPixelBuffer:pixelBuffer bounds:drawImage.extent colorSpace:sDeviceRgbColorSpace];
}

// adjust face bounds from shake
- (CGRect)filterFaceBoundsForDraw:(CGRect)faceBounds
{
    if (CGRectEqualToRect(lastFaceDrawBounds, CGRectZero))
    return faceBounds;
    
    CGRect offsetBounds;
    
    //high-pass filter to eliminate gravity
    lastFaceDrawBounds.origin.x = faceBounds.origin.x * kFilteringFactor + lastFaceDrawBounds.origin.x * (1.0f - kFilteringFactor);
    lastFaceDrawBounds.origin.y = faceBounds.origin.y * kFilteringFactor + lastFaceDrawBounds.origin.y * (1.0f - kFilteringFactor);
    lastFaceDrawBounds.size.width = faceBounds.size.width * kFilteringFactor + lastFaceDrawBounds.size.width * (1.0f - kFilteringFactor);
    lastFaceDrawBounds.size.height = faceBounds.size.height * kFilteringFactor + lastFaceDrawBounds.size.height * (1.0f - kFilteringFactor);
    offsetBounds.origin.x = faceBounds.origin.x - lastFaceDrawBounds.origin.x;
    offsetBounds.origin.y = faceBounds.origin.y - lastFaceDrawBounds.origin.y;
    offsetBounds.size.width = faceBounds.size.width - lastFaceDrawBounds.size.width;
    offsetBounds.size.height = faceBounds.size.height - lastFaceDrawBounds.size.height;
    
    if (offsetBounds.origin.x > kOffsetFactor)
    return faceBounds;
    
    return lastFaceDrawBounds;
}

// adjust face bounds from shake
- (CGRect)filterFaceBoundsForRender:(CGRect)faceBounds
{
    if (CGRectEqualToRect(lastFaceRenderBounds, CGRectZero))
    return faceBounds;
    
    CGRect offsetBounds;
    
    lastFaceRenderBounds.origin.x = faceBounds.origin.x * kFilteringFactor + lastFaceRenderBounds.origin.x * (1.0f - kFilteringFactor);
    lastFaceRenderBounds.origin.y = faceBounds.origin.y * kFilteringFactor + lastFaceRenderBounds.origin.y * (1.0f - kFilteringFactor);
    lastFaceRenderBounds.size.width = faceBounds.size.width * kFilteringFactor + lastFaceRenderBounds.size.width * (1.0f - kFilteringFactor);
    lastFaceRenderBounds.size.height = faceBounds.size.height * kFilteringFactor + lastFaceRenderBounds.size.height * (1.0f - kFilteringFactor);
    offsetBounds.origin.x = faceBounds.origin.x - lastFaceRenderBounds.origin.x;
    offsetBounds.origin.y = faceBounds.origin.y - lastFaceRenderBounds.origin.y;
    offsetBounds.size.width = faceBounds.size.width - lastFaceRenderBounds.size.width;
    offsetBounds.size.height = faceBounds.size.height - lastFaceRenderBounds.size.height;
    
    if (offsetBounds.origin.x > kOffsetFactor)
    return faceBounds;
    
    return lastFaceRenderBounds;
}

// calculate the face bounds to replace
- (CGRect)boundsWithSourceImage:(CIImage *)sourceImage faceFeature:(CIFaceFeature *)faceFeature
{
    AKFaceFeature *backFace = [AKFaceFeature faceFeatureFromCIFaceFeature:faceFeature];
    
    CGFloat sourceDistance = backFace.distanceBetweenEyes;
    CGFloat faceDistance = selectedEyeFeature.rightEyeCenter.x - selectedEyeFeature.leftEyeCenter.x;
    CGFloat scale = sourceDistance / faceDistance;
    
    CGPoint leftEyePosition = faceFeature.leftEyePosition;
    CGRect bounds = CGRectZero;
    bounds.origin.x = leftEyePosition.x - selectedEyeFeature.leftEyeCenter.y * scale;
    bounds.origin.y = leftEyePosition.y - selectedEyeFeature.leftEyeCenter.x * scale;
    bounds.size.width = selectedImage.size.height * scale;
    bounds.size.height = selectedImage.size.width * scale;
    
//    faceImage = [self rotateImage:selectedFace radian:M_PI * 2.0f - backFace.angleBetweenEyes];

//    CGFloat radius = sqrt(bounds.origin.x * bounds.origin.x + bounds.origin.y * bounds.origin.y);
//    CGFloat cos = bounds.origin.y / radius;
//    CGFloat preradian = acos(cos);
//    CGFloat totalradian = backFace.angleBetweenEyes + preradian;

//    bounds.origin.y = cos(totalradian) * radius;
//    bounds.origin.x = sin(M_PI_2 - totalradian) * radius;
    
    scale = bounds.size.height / faceImage.extent.size.height;
    
    faceImage = [self scaleImage:faceImage scale:scale];
    
    return bounds;
}

// draw the face to source image from camera
- (CIImage *)applyFace:(CIImage *)face faceBounds:(CGRect)faceBounds toSource:(CIImage *)sourceImage
{
    CIFilter *filter = [CIFilter filterWithName:@"CISourceOverCompositing"];
    CGAffineTransform transform = CGAffineTransformMakeTranslation(faceBounds.origin.x, faceBounds.origin.y);
    face = [face imageByApplyingTransform:transform];
    [filter setValue:face forKey:@"inputImage"];
    [filter setValue:sourceImage forKey:@"inputBackgroundImage"];
    CIImage *filteredImage = filter.outputImage;
    
    if (filteredImage.extent.size.width != sourceImage.extent.size.width ||
        filteredImage.extent.size.height != sourceImage.extent.size.height)
    {
        filter = [CIFilter filterWithName:@"CICrop"];
        [filter setValue:filteredImage forKey:@"inputImage"];
        CIVector *vector = [CIVector vectorWithCGRect:sourceImage.extent];
        [filter setValue:vector forKey:@"inputRectangle"];
        filteredImage = filter.outputImage;
    }
    
    return filteredImage;
}
    
@end
