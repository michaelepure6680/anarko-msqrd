//
//  AKEditViewController.swift
//  Anarko
//
//  Created by Hua Wan on 15/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import AVFoundation

extension String {
    func sizeWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGSize {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.size
    }
}

// Changed by Luski Timurovich on 2017/04/15 to Audio Distortion
//class AKEditViewController: UIViewController, TVTextViewDelegate {
class AKEditViewController: UIViewController, TVTextViewDelegate {

    @IBOutlet var playerView: UIView!
    @IBOutlet var editView: UIView!
    @IBOutlet var controlView: UIView!
    @IBOutlet var effectButton: UIButton!
    @IBOutlet var saveButton: UIView!
    @IBOutlet var addTitleLabel: UILabel!
    @IBOutlet var logoButton: UIButton!
    @IBOutlet var labelNote: UILabel!
    
    var videoURL: NSURL!
    var audioURL: NSURL!
    
    // Added by Luski Timurovich on 04/16/2017 to indentify mask from Camera View
    var maskID: String!
    
    var previewVideoPlayer: AVPlayer!
    var previewVideoAsset: AVAsset!
    var playerLayer: AVPlayerLayer!
    
    // Changed by Luski Timurovich on 2017/04/15 to Audio Distortion
//    var audioPlayer: AVAudioPlayer!
    var audioPlayer: AEAudioPlayer!
    
    var titleTextView: TVTextView!
    var previousPoint: CGPoint!
    let S_TEXTVIEW_FRAME = CGRect(x: 0, y: 0, width: 128, height: 88)
    let S_TEXT_OFFSET = CGFloat(20)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupPreview(frame: UIScreen.main.bounds)
        self.setupAudio()
        self.logoButton.setGif("gif-explorer")
        
        // Added by Luski Timurovich to hide Note label.
        self.perform(#selector(hideNoteLabel), with: nil, afterDelay: 2.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleVideoPlayEndNotification(notification:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        playerLayer.frame = playerView.bounds;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleVideoPlayEndNotification(notification: Notification) -> Void {
        previewVideoPlayer.seek(to: kCMTimeZero, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        
        // Marked by Luski Timurovich on 2017/04/15 to Audio Distortion
//        audioPlayer.currentTime = 0
        
        previewVideoPlayer.play()
        audioPlayer.play()
    }
    
    func setupPreview(frame: CGRect) -> Void {
        if videoURL == nil {
            return
        }
        
        previewVideoAsset = AVURLAsset(url: videoURL as URL)
        let playerItem = AVPlayerItem(asset: previewVideoAsset)
        previewVideoPlayer = AVPlayer(playerItem: playerItem)
        playerLayer = AVPlayerLayer(player: previewVideoPlayer)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.frame = playerView.bounds;
        playerView.layer.addSublayer(playerLayer)
        previewVideoPlayer.play()
    }
    
    func setupAudio() -> Void {
        // Marked by Luski Timurovich on 2017/04/15 to Audio Distortion
//        do {
//            try audioPlayer = AVAudioPlayer(contentsOf: audioURL as URL)
//        } catch {
//            return
//        }
//        audioPlayer.prepareToPlay()
//        audioPlayer.play()
        // Added by Luski Timurovich 0n 2017/04/15 to Audio Distortion
        do {
            try audioPlayer = AEAudioPlayer.init(contentsOf: audioURL as URL)
        } catch {
            return
        }
        
        NSLog("[Mask ID = %@]", self.maskID)
        
        if maskID == "57fbaaf3054b8f0356ed7159" { // Doland Trump
            audioPlayer.setEffect(1)
        } else if maskID == "57fbabe0054b8f0356ed7163" { // Hillory Tipton
            audioPlayer.setEffect(2)
        } else if maskID == "57fbac5c054b8f0356ed716d" { // Kimmy K
            audioPlayer.setEffect(3)
        } else if maskID == "58012e5af7ecf428b53b8bbe" { // Satin
            audioPlayer.setEffect(4)
        } else if maskID == "5834650e1e6ccc3b30c553d8" { // Buddhy
            audioPlayer.setEffect(5)
        } else if maskID == "5834664d1e6ccc3b30c553e2" { // Clyde the Monkey
            audioPlayer.setEffect(6)
        } else if maskID == "583466ec1e6ccc3b30c553ec" { // Dollface
            audioPlayer.setEffect(7)
        } else if maskID == "58346a921e6ccc3b30c553f6" { // Jerry the Burglar
            audioPlayer.setEffect(8)
        } else if maskID == "58346c131e6ccc3b30c5540a" { // Sadie the Sadist
            audioPlayer.setEffect(9)
        } else if maskID == "57fbacf1054b8f0356ed7177" { // The Son
            audioPlayer.setEffect(10)
        } else if maskID == "5818adb898cf263a4c50a924" { // Peppy Ventrilo
            audioPlayer.setEffect(11)
        } else if maskID == "583474841e6ccc3b30c5541e" { // Viking
            audioPlayer.setEffect(12)
        } else if maskID == "5834740a1e6ccc3b30c55414" { // Valkyrie
            audioPlayer.setEffect(13)
        } else if maskID == "583478f51e6ccc3b30c55428" { // Bozzo the Clown
            audioPlayer.setEffect(14)
        } else if maskID == "58346b5b1e6ccc3b30c55400" { // Mary
            audioPlayer.setEffect(15)
        } else if maskID == "57fba91385e57f01eb329677" { // buggy man
            audioPlayer.setEffect(16)
        }

        audioPlayer.play()
    }
    
    // Added by Luski Timurovich on 04/16/2017 to fix gesture issue.
    func moveTextView(gesture: UIPanGestureRecognizer) -> Void {
/*
        let translatedPoint = gesture.location(in: editView)
        let thumbRect = editView.bounds
        let imageRect = thumbRect
        
        if gesture.state == UIGestureRecognizerState.began {
            previousPoint = translatedPoint
        }
        
        if imageRect.contains(translatedPoint) {
            let dX = translatedPoint.x - previousPoint.x;
            let dY = translatedPoint.y - previousPoint.y;
            
            previousPoint = translatedPoint;
            
            var textViewCenter = gesture.view?.center;
            textViewCenter?.x += dX;// MIN(imageRect.size.width - pan.view.frame.size.width / 2, translatedPoint.x);
            textViewCenter?.y += dY;// MIN(imageRect.size.height - pan.view.frame.size.height / 2, translatedPoint.y);
            
            gesture.view?.center = textViewCenter!;
        }
 */
        // Replaced by Luski Timurovich on 04/20/2017 to fix error on move.
        if gesture.state == .began || gesture.state == .changed {
            
            let translation = gesture.translation(in: self.view)
            // note: 'view' is optional and need to be unwrapped
            gesture.view!.center = CGPoint(x: gesture.view!.center.x + translation.x, y: gesture.view!.center.y + translation.y)
            gesture.setTranslation(CGPoint.zero, in: self.view)
        }
    }

    func checkContentOffsetForActiveTextView() -> Void {
        
    }

    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "AKSaveViewController" {
            let controller = segue.destination as! AKSaveViewController
            controller.videoURL = sender as! URL
            if titleTextView != nil {
                controller.videoTitle = titleTextView.getText()
            } else {
                controller.videoTitle = ""
            }
            
            //var thumbImage = AKUtilities.thumbImage(controller.videoURL, time: kCMTimeZero)
            //let titleImage = titleTextView.renderImage(for: (titleTextView.superview?.frame.size)!)
            //thumbImage = thumbImage?.withOverlayImage(titleImage, point: CGPoint(x: 0, y: 0))
            let thumbImage = AKUtilities.thumbImage(controller.videoURL, time: kCMTimeZero)
            controller.thumbImage = thumbImage!
        }
    }

    // MARK: - IBAction
    @IBAction func cancelPressed(sender: AnyObject) -> Void {
        
        self.stopVideo()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func explorerPressed(sender: AnyObject) -> Void {
        
        self.stopVideo()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationLoadAnarko), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTitlePressed(sender: AnyObject) -> Void {
        editView.isHidden = false
        
        if titleTextView == nil {
            titleTextView = TVTextView()
            titleTextView.colorIndex = 0
            titleTextView.delegate = self
            titleTextView.frame = S_TEXTVIEW_FRAME
            titleTextView.center = CGPoint(x: editView.frame.size.width / 2.0, y: addTitleLabel.frame.origin.y + addTitleLabel.frame.size.height + 44 + 8)
            
            let panGesture = UIPanGestureRecognizer(target: self, action: #selector(moveTextView(gesture:)))
            panGesture.minimumNumberOfTouches = 1
            panGesture.maximumNumberOfTouches = 2
            titleTextView.addGestureRecognizer(panGesture)
            
            titleTextView.fontSize = 18
            titleTextView.setTextFontWithName("Gunplay-Regular")
            editView.addSubview(titleTextView)
            
            titleTextView.isActive = true
            titleTextView.parentFrame = editView.bounds
            _ = self.textView(titleTextView, shouldChangeText: "")
            titleTextView.showKeyboard()
        } else {
            editView.addSubview(titleTextView)
            
            // Added by Luski Timurovich on 04/20/2017 to reset position/font of the text view.
            if titleTextView.getText().characters.count == 0 {
                titleTextView.transform = CGAffineTransform.identity;
                titleTextView.frame = S_TEXTVIEW_FRAME
                titleTextView.center = CGPoint(x: editView.frame.size.width / 2.0, y: addTitleLabel.frame.origin.y + addTitleLabel.frame.size.height + 44 + 8)
                titleTextView.fontSize = 18
//                titleTextView.setTextFontWithName("Gunplay-Regular")
            }
            titleTextView.isActive = true
            titleTextView.showKeyboard()
        }
    }
    
    // Added by Luski Timurovich on 04/16/2017 to implement Effect ON/OFF
    @IBAction func effectPressed(_ sender: Any) {
        audioPlayer.enableEffect(!audioPlayer.isEffectEnabled)
        
        if audioPlayer.isEffectEnabled {
            self.effectButton.setImage(UIImage.init(named: "icon_Encrypt_ON_State"), for: .normal)
        } else {
            self.effectButton.setImage(UIImage.init(named: "icon_Encrypt_OFF_State"), for: .normal)
        }

        self.labelNote.isHidden = false
        self.effectButton.isEnabled = false
        if self.audioPlayer.isEffectEnabled {
            self.labelNote.text = "Voice Distortion ON"
        } else {
            self.labelNote.text = "Voice Distortion OFF"
        }
        
        self.perform(#selector(hideNoteLabel), with: nil, afterDelay: 2.0)
/*
        UIView.animate(withDuration: 2.0, animations: {
            self.labelNote.alpha = 1.0
        }) { (finished: Bool) in
            self.labelNote.isHidden = true
            self.effectButton.isEnabled = true
        }
*/
    }
    
    func hideNoteLabel(sender: AnyObject) {
        self.labelNote.isHidden = true
        self.effectButton.isEnabled = true
    }
    
    @IBAction func savePressed(sender: AnyObject) -> Void {
        //AKUtilities.exportVideo(videoURL as URL!, size: playerView.bounds.size, textView: titleTextView) { (outputURL: URL?) in
        self.stopVideo()
//        guard titleTextView != nil else {
//            //AKGlobal.showAlertViewController(title: "Error!", message: "Please add the title.", target: self)
//            self.performSegue(withIdentifier: "AKSaveViewController", sender: videoURL)
//            return
//        }
        
        AKGlobal.shared.showCustomActivity(UIApplication.shared.keyWindow!)
        // Added by Luski Timurovich on 17/04/17 to save audio distortion to the video file.
        if audioPlayer.isEffectEnabled {
            let effectURL: URL! = (audioPlayer.applyEffect(toFile: audioURL as URL!))
            
            AKUtilities.exportVideo(videoURL as URL!, ratio: playerView.bounds.size.height / playerView.bounds.size.width, textView: titleTextView, audio: effectURL as URL!) { (outputURL: URL?) in
                AKGlobal.shared.hideCustomActivity()
                self.performSegue(withIdentifier: "AKSaveViewController", sender: outputURL)
            }
        } else {
            AKUtilities.exportVideo(videoURL as URL!, ratio: playerView.bounds.size.height / playerView.bounds.size.width, textView: titleTextView, audio: audioURL as URL!) { (outputURL: URL?) in
                AKGlobal.shared.hideCustomActivity()
                self.performSegue(withIdentifier: "AKSaveViewController", sender: outputURL)
            }
        }
    }
    
    @IBAction func handleTapGesture(sender: AnyObject) {
        if titleTextView != nil && titleTextView.isActive == true {
            titleTextView.isActive = false
        }
        else if previewVideoPlayer.rate == 0.0 {
            previewVideoPlayer.play()
            audioPlayer.play()
        } else {
            previewVideoPlayer.pause()
            audioPlayer.pause()
        }
    }
    
    func stopVideo() {
        previewVideoPlayer.pause()
        audioPlayer.pause()
    }
    
    // MARK: - TVTextViewDelegate
    func textViewDidBeginEditing(_ textView: TVTextView!) {
        editView.isHidden = false
        editView.addSubview(titleTextView)
    }
    
    func textViewDidEndEditing(_ textView: TVTextView!) {
        editView.isHidden = true
        
        // Added by Luski Timurovich on 04/22/2017 to Hide and Remove title text view in the case of empty title.
        if (titleTextView.getText().characters.count == 0) {
            titleTextView.isHidden = true
            titleTextView.removeFromSuperview()
            titleTextView = nil;
        } else {
            playerView.addSubview(titleTextView)
        }
    }
    
    func textViewWillBecomeActive(_ textView: TVTextView!) {
        
    }
    
    func textView(_ textView: TVTextView!, shouldChangeText newText: String!) -> Bool {
        
        if newText.characters.count > 0 {
            addTitleLabel.isHidden = true
        } else {
            addTitleLabel.isHidden = false
        }
        
        // Changed by
//        let transform = textView.transform
        let transform = titleTextView.transform
//        textView.transform = CGAffineTransform.identity
        titleTextView.transform = CGAffineTransform.identity
        
        var shouldChangeText = true
        
        let edgeInsets = textView.textEdgesInsets()
        let contrainedFrame = editView.bounds
        
        let constrainedWidth = contrainedFrame.size.width  - edgeInsets.left - edgeInsets.right
        
        var newTextSize = CGSize()
        if newText.isEmpty {
            newTextSize = CGSize(width: S_TEXTVIEW_FRAME.size.width - 2 * S_TEXT_OFFSET - edgeInsets.left - edgeInsets.right, height: S_TEXTVIEW_FRAME.size.height - 2 * S_TEXT_OFFSET - edgeInsets.top - edgeInsets.bottom)
        } else {
            newTextSize = newText.sizeWithConstrainedWidth(width: constrainedWidth, font: textView.textFont())
        }
        
        if textView.preservedSize != CGSize.zero {
            newTextSize = textView.preservedSize
        }
        
        newTextSize.width += edgeInsets.left + edgeInsets.right
        newTextSize.height += edgeInsets.top + edgeInsets.bottom
        
        shouldChangeText = newTextSize.height * textView.textScale.y < contrainedFrame.size.height;
        
        if shouldChangeText == true {
            if newText.isEmpty {
                textView.textScale = CGPoint(x: 1.0, y: 1.0)
            }
            
            textView.textSize = newTextSize
            
            // Changed by Luski Timurovich on 04/20/2017 to fix gesture issue.
//            var frame = textView.frame
            var frame = textView.bounds
            frame.origin.x = min(frame.origin.x, contrainedFrame.size.width - (frame.size.width - S_TEXT_OFFSET * 2))
            frame.origin.y = min(frame.origin.y, contrainedFrame.size.height - (frame.size.height - S_TEXT_OFFSET * 2));
            
            // Changed by Luski Timurovich on 04/20/2017 to fix gesture issue.
//            textView.frame = frame;
            textView.bounds = frame;

            if textView.getText().characters.count != newText.characters.count {
                self.checkContentOffsetForActiveTextView()
            }
        }

        textView.transform = transform;
        
        return shouldChangeText
    }
    
    func textViewRemovePressed(_ textView: TVTextView!) {
        
    }
    
    func textView(_ textView: TVTextView!, shouldChangeFrame newFrame: CGRect) -> Bool {
        return true
    }
}
