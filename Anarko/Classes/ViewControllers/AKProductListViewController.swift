//
//  AKProductListViewController.swift
//  Anarko
//
//  Created by HeMin on 9/16/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation

enum StoreType: String {
    
    case Mask = "mask", Effect = "effect"
}

class ProductCell: UICollectionViewCell {
    
    @IBOutlet var productThumbImageView: UIImageView!
    @IBOutlet var productNameLabel: UILabel!
    let activityIndicator: AKCustomActivityIndicatorView = { () -> AKCustomActivityIndicatorView in
        let image = UIImage(named: "icon-load")!
        let frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        return AKCustomActivityIndicatorView(image: image, frame: frame.insetBy(dx: 4.0, dy: 4.0))
    }()
    
    var currentItem: AKMask! {
        
        didSet {
            
            DispatchQueue.main.asyncAfter(deadline : DispatchTime.now()) {
                self.displayThumb()
            }
            
        }
    }
    
    func displayThumb() {
        
        activityIndicator.removeFromSuperview()
        self.addSubview(activityIndicator)
        activityIndicator.center = self.productThumbImageView.center
        
        let dic = self.currentItem.thumbs.object(at: 0) as! NSDictionary
        self.productThumbImageView.image = nil
        
        let filename = "thumb" + currentItem.maskId! + (dic["filename"] as! String)
        print(filename)
        let path = NSTemporaryDirectory().appending(filename)
        if let image = UIImage(contentsOfFile: path) {
            self.productThumbImageView.image = image
        }
        else
        {
            activityIndicator.startAnimating()
            Alamofire.request(dic["url"] as! String).responseData { (response) in
                self.activityIndicator.stopAnimating()
                if response.result.isSuccess == true {
                    do {
                        unlink(path.cString(using: .utf8))
                        try response.data?.write(to: URL(fileURLWithPath: path))
                        UserDefaults.standard.removeObject(forKey: filename)
                    } catch {
                        print("error to write image")
                    }
                    self.productThumbImageView.image = UIImage(data: response.data!)
                }
            }
        }
        
        productNameLabel.text = currentItem.name
    }
    
}

@objc protocol AKProductListViewControllerDelegate {
    
    @objc optional func dismissProductListViewController(_ vc: AKProductListViewController)
}

class AKProductListViewController: UIViewController, GSKTabsStretchyHeaderViewDelegate {
    
    @IBOutlet var productCollectionView: UICollectionView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var explorerButton: UIButton!
    @IBOutlet var settingButton: UIButton!
    
    @IBOutlet var earnProductButton: UIButton!
    
    @IBOutlet weak var noMaskView: UIView!
    
    var productHeaderView: AKStoreProductHeaderView!
    
    
    var headerView :AKCustomStoreHeaderView!

    var delegate : AKProductListViewControllerDelegate?
    
    var maskArray:[AKMask] = []
    var effectArray:[AKMask] = []
    
    var isInitialized:Bool = false
    
    
    var isMaskMode: Bool = true
    var isPurchasing = false
    var isVerifying = false
    
    var selectedCardId = ""
    
    
    //AVAudioPlayer
    
    var defaultSoundPlayer: AudioPlayer!
    var sirenSoundPlayer: AudioPlayer!
    var glassSoundPlayer: AudioPlayer!
    
    let companyName = ""
    let paymentCurrency = "usd"
    
    var product = ""
    var selectFirstMaskTimer : Timer?
    var paymentInProgress: Bool = false {
        
        didSet {
            
        }
    }
    
    var currentMask: AKMask!

    var products = [SKProduct]()
    
    var isItunesProductsLoaded = false
    
    var anarkoProducts : AnarkoProducts?
    
    let session =  AVAudioSession.sharedInstance()

    deinit {
        
        self.stopDefaultAudio(true)
        self.stopSuccessAudios(true)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noMaskView.isHidden = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.gsk_setNavigationBarTransparent(true, animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKProductListViewController.handlePurchaseNotification(_:)),
                                               name: NSNotification.Name(rawValue: kNotificationPurchased),
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKProductListViewController.handlePurchasingFailedNotification(_:)),
                                               name: NSNotification.Name(rawValue: kNotificationPurchasingFailed),
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        self.stopDefaultAudio(true)
        self.stopSuccessAudios(true)
        
        NotificationCenter.default.removeObserver(self)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !isInitialized {
            self.initialize()
        }
        
        self.loadStoreItems(type: StoreType.Mask.rawValue)
//        DispatchQueue.main.async(execute: {
//            
//            self.loadStoreItems(type: StoreType.Effect.rawValue)
//        })
        
        self.initializeAudioPlayers()


    }
    
    // MARK: - Stop Audio
    
    func stopDefaultAudio(_ deallocNeeded: Bool){
        
        if defaultSoundPlayer != nil {
            
            defaultSoundPlayer.stop()
            
            if deallocNeeded {
                
                defaultSoundPlayer = nil
            }

        }
    }
   
    //MARK: - Private
    private func initialize() {
        
        let nibViews = Bundle.main.loadNibNamed("AKStoreProductHeaderView", owner: self, options: nil)
        
        self.productHeaderView = nibViews?.first as! AKStoreProductHeaderView!
        self.productHeaderView.tabsDelegate = self
        self.productHeaderView.expansionMode = .immediate
        self.productCollectionView.addSubview(self.productHeaderView)
        
        isInitialized = !isInitialized
        
        self.explorerButton.setGif("gif-explorer")
    }
    
    // Play sound
    
    func initializeAudioPlayers() {
        
        let defaultSoundPath = Bundle.main.path(forResource: "Kevin MacLeod - Local Forecast - Elevator.mp3", ofType: nil)!
        
        let sirenSoundPath = Bundle.main.path(forResource: "audio-police-siren.mp3", ofType: nil)!
        let glassSoundPath = Bundle.main.path(forResource: "audio-breaking-glass.mp3", ofType: nil)!
        
        _ = URL(fileURLWithPath: defaultSoundPath)
        
        _ = URL(fileURLWithPath: sirenSoundPath)
        _ = URL(fileURLWithPath: glassSoundPath)
        
        do {
            
            let sound = try AudioPlayer(fileName: "Kevin MacLeod - Local Forecast - Elevator.mp3")
            let sirenSound = try AudioPlayer(fileName: "audio-police-siren.mp3")
            let glassSound = try AudioPlayer(fileName: "audio-breaking-glass.mp3")
            
            
            if defaultSoundPlayer == nil {
                
                defaultSoundPlayer = sound
                defaultSoundPlayer.numberOfLoops = -1

            }
            
            if sirenSoundPlayer == nil {
                
                sirenSoundPlayer = sirenSound
                
            }
            
            if glassSoundPlayer == nil {
                
                glassSoundPlayer = glassSound
                
            }
            
            defaultSoundPlayer.play()
        }catch {
            
            //Couldn't not load file
            
            print("couldn't not load file")
        }

    }
    func playDefaultSound(){
        
        if defaultSoundPlayer != nil {
            
            defaultSoundPlayer.play()
        }
    }
    
    func playSuccessAudioAfterPayment() {
        
        sirenSoundPlayer.play()
        glassSoundPlayer.play()
    }
    func stopSuccessAudios(_ deallocNeeded: Bool) {
        
        if sirenSoundPlayer != nil {
            
            sirenSoundPlayer.stop()
            
            if deallocNeeded {
                
                sirenSoundPlayer = nil
            }
        }
        
        if glassSoundPlayer != nil {
            
            glassSoundPlayer.stop()
            
            if deallocNeeded {
                
                glassSoundPlayer = nil
            }

        }
    }
    
    func loadItunesProducts() {
        
        AKGlobal.shared.showCustomActivity(self.view)
        var productIds : Set<ProductIdentifier> = []
        for mask in self.maskArray {
            let productId = "com.oppous." + mask.maskId
            productIds.insert(productId)
        }
        anarkoProducts = AnarkoProducts(productIds: productIds)
        anarkoProducts?.store?.requestProducts(completionHandler: { (success, products) in
            AKGlobal.shared.hideCustomActivity()
            if success {
                self.isItunesProductsLoaded = true
                self.products = products!
            }
        })
    }
    
    // load store items
    
    private func loadStoreItems(type: String) {
        
        AKGlobal.shared.showCustomActivity(self.view)
        
        AKAPIManager.sharedManager.getStoreList(query: type, completionHandler: { (response) in
            
            AKGlobal.shared.hideCustomActivity()
            print(response)
            
            switch response.result {
                
            case .success:
                print("Validation Successful")
                if let json = response.result.value {
                    
                    print("JSON: \(json)")
                    let jsonDic = json as! NSDictionary
                    let error = AKParser.parseErrorMessage(data: jsonDic)
                    if error == nil {
                        
                        let maskArr = AKParser.parseStoreItem(data:(jsonDic["data"] as? NSArray)!)
                        print(maskArr)
                        self.productCollectionView.isHidden = false
                        
                        if type == StoreType.Mask.rawValue {
                            self.maskArray = maskArr
                            if (self.maskArray.count > 0) {
                                self.selectCurrentMask()
                                if self.isItunesProductsLoaded == false {
                                    self.loadItunesProducts()
                                }
                                self.productCollectionView.reloadData()
                            }
                            else
                            {
                                self.noMaskView.isHidden = false
                            }
                        }else {
                            self.effectArray = maskArr
                        }
                        self.view.setNeedsLayout()
                    }
                    else
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                    }
                }
            case .failure(let error):
                print(error)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
            }
            
        })

    }
    
    func selectCurrentMask()
    {
        let index = 0
        self.productHeaderView.setMaskImage(url: nil, item: self.maskArray[index])
        self.currentMask = self.maskArray[index]
    }

    //MARK:

    func tabsStretchyHeaderView(headerView: AKStoreProductHeaderView, didSelectTabAtIndex: NSInteger , selectedButton: UIButton) {
        
        if didSelectTabAtIndex == 0 {
            
            isMaskMode = true

        }else {
            
            isMaskMode = false
        }
        
        self.productCollectionView.reloadData()
    }

    //Verify apple receipt
    func verifyAppleReciept(params : [String : AnyObject]) {
        if isVerifying == true {
            AKGlobal.hideProgress()
            return
        }
        isVerifying = true
        AKAPIManager.sharedManager.verifyAppleReceipt(params: params) { (response) in
            AKGlobal.hideProgress()
            self.isVerifying = false
            switch response.result {
            case .success:
                
                print("Validation Successful")
                
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let jsonDic = json as! NSDictionary
                    let error = AKParser.parseErrorMessage(data: jsonDic)
                    if error == nil && self.isPurchasing == true {
                        self.stopDefaultAudio(false)
                        self.playSuccessAudioAfterPayment()
                        self.maskArray.remove(object: self.currentMask)
                        
                        self.productHeaderView.showGlassImage(true)
                        self.productCollectionView.reloadData()
                        
                        self.selectFirstMaskTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.selectFirstMask), userInfo: nil, repeats: false);
                    }
                    else
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                        
                    }
                }
            case .failure(let error):
                print(error)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
            }
            self.isPurchasing = false
        }
    }

    // MARK: - Actions

    @IBAction func backButtonTapped(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: {
            
            self.stopDefaultAudio(true)
            self.stopSuccessAudios(true)
        })
    }
    
    @IBAction func explorerButtonTapped(_ sender: AnyObject) {
        
        self.stopDefaultAudio(true)
        self.stopSuccessAudios(true)
        
        let showExplorerTips = UserDefaults.standard.object(forKey: "showExplorerTips") as? String
        if(showExplorerTips == nil)
        {
            UserDefaults.standard.set("yes", forKey: "showExplorerTips")
            UserDefaults.standard.synchronize()
        }
        
        self.delegate?.dismissProductListViewController!(self)

    }

    @IBAction func settingButtonTapped(_ sender: AnyObject) {
        
        self.stopDefaultAudio(true)
        self.stopSuccessAudios(true)

    }
    
    @IBAction func earnProductButtonTapped(_ sender: AnyObject) {
        
        self.stopDefaultAudio(true)
        self.stopSuccessAudios(true)

    }
    
    func selectFirstMask() {
        if ( self.isMaskMode == false && self.effectArray.count > 0 ) || ( self.isMaskMode == true && self.maskArray.count > 0 ) {
            self.collectionView(self.productCollectionView, didSelectItemAt: IndexPath(item: 0, section: 0))
        }
        else
        {
            self.noMaskView.isHidden = false
        }
    }
    
    func buyMaskTapped() {
        isPurchasing = true
        let productIdToPurchase  = "com.oppous." + currentMask.maskId
        var productToPurchase : SKProduct?
        for product in products {
            if product.productIdentifier == productIdToPurchase {
                productToPurchase = product
            }
        }
        if let product = productToPurchase {
            AKGlobal.showWithStatus(status: "Purchasing...")
            anarkoProducts?.store?.buyProduct(product)
        }
    }
    
    func handlePurchaseNotification(_ notification: Notification) {
        AKGlobal.hideProgress()
        if isPurchasing == false {
            return
        }
        
        do {
            let receiptURL = Bundle.main.appStoreReceiptURL
            let receipt = try Data(contentsOf: receiptURL!)
            let receiptString = receipt.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let paramDic = NSMutableDictionary()
            
            paramDic.setObject(receiptString , forKey: "appleReceipt" as NSCopying)
            if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
                AKGlobal.showWithStatus(status: "Verifying...")
                self.verifyAppleReciept(params: dict)
            }
            
        }
        catch {
            
        }
        
        //guard let productID = notification.object as? String else { return }
    }
    
    func handlePurchasingFailedNotification(_ notification: Notification) {
        AKGlobal.hideProgress()
        if let error = notification.object as? Error {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
        }
    }
}

extension AKProductListViewController: UICollectionViewDataSource {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if self.isMaskMode {
            
            return self.maskArray.count
            
        }else {
            
            return self.effectArray.count
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.ProductCellIdentifier, for: indexPath) as! ProductCell
        
        if self.isMaskMode {
            
            cell.currentItem = self.maskArray[indexPath.item]
            
        }else {
            
            cell.currentItem = self.effectArray[indexPath.item]
        }
        
        return cell
    }
    
}

extension AKProductListViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! ProductCell
        
        if self.isMaskMode {
            
            self.currentMask = cell.currentItem
            
        }else {
            
        }
        
        self.stopSuccessAudios(false)
        
        self.playDefaultSound()

        self.productHeaderView.showGlassImage(false)

        productHeaderView.setMaskImage(url: nil, item: cell.currentItem)
        
        if let timer = self.selectFirstMaskTimer {
            timer.invalidate()
        }
        
    }
    
}

extension AKProductListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: self.productCollectionView.bounds.width / 3, height: self.productCollectionView.bounds.width / 3)
        
        return size
    }
}




