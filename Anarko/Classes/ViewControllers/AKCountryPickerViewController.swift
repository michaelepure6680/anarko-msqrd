//
//  AKCountryPickerViewController.swift
//  Anarko
//
//  Created by Hua Wan on 14/9/2016.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

struct Country {
    var code: String?
    var name: String?
    var phoneCode: String?
    
    init(code: String?, name: String?, phoneCode: String?) {
        self.code = code
        self.name = name
        self.phoneCode = phoneCode
    }
}

class AKCountryPickerCell: UITableViewCell {
    @IBOutlet var flagImageView: UIImageView!
    @IBOutlet var countryNameLabel: UILabel!
    @IBOutlet var countryCodeLabel: UILabel!
}

@objc protocol AKCountryPickerViewControllerDelegate {
    @objc optional func didSelectCountry(flag: UIImage, name: String, code: String, phcode: String) -> Void
}

class AKCountryPickerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UITextFieldDelegate {

    @IBOutlet var countrySearchBar: UISearchBar!
    @IBOutlet var countriesTableView: UITableView!
    @IBOutlet var searchTextField: UITextField!
    
    var delegate: AKCountryPickerViewControllerDelegate!
    
    var countries: [Country]!
    var searchedCountries: [Country]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        countries = countryNamesByCode()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKCountryPickerViewController.textFieldDidChange(_:)), name: NSNotification.Name.UITextFieldTextDidChange, object: self.searchTextField)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func countryNamesByCode() -> [Country] {
        var countries = [Country]()
        
        for code in Locale.isoRegionCodes {
            let countryName = (Locale.current as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: code)
            
            let phoneNumberUtil = NBPhoneNumberUtil.sharedInstance()
            let codeNumber = phoneNumberUtil!.getCountryCode(forRegion: code).intValue
            let phoneCode = "+\(codeNumber)"
            
            if phoneCode != "+0" {
                let country = Country(code: code, name: countryName, phoneCode: phoneCode)
                countries.append(country)
            }
        }
        
        countries = countries.sorted(by: { $0.name! < $1.name! })
        
        return countries
    }
    
    //Show
    
    func show(targetViewController: UIViewController, targetView:UIView) {
        
        let topViewController = targetViewController
        topViewController.addChildViewController(self)
        topViewController.view.addSubview(self.view)
        self.viewWillAppear(true)
        self.didMove(toParentViewController: topViewController)
//        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.alpha = 0.0
        self.view.frame = targetView.frame
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations: {
            
            self.view.alpha = 1.0
//            _customAlertView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
            }, completion: { (finished) in
                
        })


    }
    
    func hide() {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var country = countries[indexPath.row]
        if searchTextField.text != "" {
            country = searchedCountries[indexPath.row]
        }
        
        self.delegate?.didSelectCountry!(flag: UIImage(named: (country.code?.lowercased())!)!, name: country.name!, code: country.code!, phcode: country.phoneCode!)
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchTextField.text == "" {
            return countries.count;
        }
        else {
            return searchedCountries.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AKCountryPickerCell", for: indexPath) as! AKCountryPickerCell
        
        var country = countries[indexPath.row]
        if searchTextField.text != "" {
            country = searchedCountries[indexPath.row]
        }
        cell.flagImageView.image = UIImage(named: (country.code?.lowercased())!)
        cell.countryNameLabel.text = country.name
        cell.countryCodeLabel.text = country.phoneCode
        
        return cell
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidChange(_ notification: Notification) {
        
        
        let textField = notification.object as! UITextField
        
        let key = textField.text?.lowercased()
        searchedCountries = countries.filter({ (country: Country) -> Bool in
            return ((country.name?.lowercased().range(of: key!)) != nil)
        })
        
        countriesTableView.reloadData()

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let key = searchBar.text?.lowercased()
        searchedCountries = countries.filter({ (country: Country) -> Bool in
            return ((country.name?.lowercased().range(of: key!)) != nil)
        })
        
        countriesTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
