//
//  AKCustomReportViewController.swift
//  Anarko
//
//  Created by HeMin on 9/15/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit


@objc enum AKCustomAlertType:Int {

    case Invite
    case Confirmation
}

@objc protocol AKCustomAlertViewControllerDelegate {
    
    @objc func sendReportContent()
}


class AKCustomAlertViewController: UIViewController, UITextViewDelegate{

    @IBOutlet var alertViewCollection: [UIView]!
    
    //Cofirmation alert view IBOutlets
    @IBOutlet weak var confirmationAlertView: UIView!
    @IBOutlet weak var confirmationTitleLabel: UILabel!
    @IBOutlet weak var confirmationMessageLabel: UILabel!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    var titleText:String!
    var messageText:String!
    var cancelButtonText:String!
    var confirmButtonText:String!
    
    //Constraints
    @IBOutlet weak var messageLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var yesButtonLeadingSpaceConstraint: NSLayoutConstraint!
    
    //Invite alert view IBOutlets
    @IBOutlet var inviteAlertView: UIView!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var backgroundView: UIVisualEffectView!
    
    
    var alertType:AKCustomAlertType = .Invite
    var didDismissBlock:((AKCustomAlertViewController) ->Void)?
    var cancelBlock:(() ->Void)?
    var confirmBlock:(() ->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        for subView in self.alertViewCollection {
            
            subView.layer.cornerRadius = 2.0
            subView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            subView.layer.shadowColor = UIColor(white: 0.0, alpha: 1.0).cgColor
            subView.layer.shadowOpacity = 0.3
            subView.layer.shadowRadius = 3.0
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AKCustomAlertViewController.backgroundViewTapped(_:))))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    //Responsible for reacting to the user tapping the visualEffect
    @objc private func backgroundViewTapped(_ gesture: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        
        self.hide()
    }
    
    //update constraint
    
    private func updateConfirmationAlertConstraint() {
        
        confirmationTitleLabel.text = titleText
        confirmationMessageLabel.text = messageText
        noButton.setTitle(cancelButtonText, for: .normal)
        yesButton.setTitle(confirmButtonText, for: .normal)
        
        let titleLabelHeight = confirmationTitleLabel.text?.height(withConstrainedWidth: confirmationTitleLabel.frame.size.width, font: confirmationTitleLabel.font);
        let messageLabelHeight = confirmationMessageLabel.text?.height(withConstrainedWidth: confirmationMessageLabel.frame.size.width, font: confirmationMessageLabel.font)
        
        var messageLabelTopPadding : CGFloat = 30;
        if(titleLabelHeight != 0){
            messageLabelTopPadding = messageLabelTopPadding + titleLabelHeight! + CGFloat(20);
        }
        if(messageLabelHeight == 0 && titleLabelHeight != 0)
        {
            messageLabelTopPadding = messageLabelTopPadding - CGFloat(20);
        }
        
        if cancelButtonText == "" {
            yesButtonLeadingSpaceConstraint.constant = -noButton.frame.size.width
        }
        else
        {
            yesButtonLeadingSpaceConstraint.constant = 0
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut, animations: {
            
            self.messageLabelHeightConstraint.constant = messageLabelHeight! + CGFloat(1)
            self.messageLabelTopConstraint.constant = messageLabelTopPadding
            self.titleLabelHeightConstraint.constant = titleLabelHeight! + CGFloat(1)
            self.view.layoutIfNeeded()
            
            }, completion: nil)
    }
    
    // MARK: - Display or Hide
    
    @objc func show(title : String, message : String, cancelButtonTitle : String, confirmButtonTitle : String, confirmBlock : @escaping () ->Void, cancelBlock : @escaping () ->Void ) {
        titleText = title;
        messageText = message;
        cancelButtonText = cancelButtonTitle
        confirmButtonText = confirmButtonTitle
        self.cancelBlock = cancelBlock
        self.confirmBlock = confirmBlock
        self.show()
    }
    
    func show() {
        
        let appDelegate = UIApplication.shared.delegate
        let window = appDelegate?.window
        if let rootViewController = window??.rootViewController {
            
            var topViewController = rootViewController
            
            while topViewController.presentedViewController != nil {
                
                topViewController = topViewController.presentedViewController!
            }
            
            //Add the alert view controller to the top most UIViewController
            
            topViewController.addChildViewController(self)
            topViewController.view.addSubview(self.view)
            self.viewWillAppear(true)
            
            self.didMove(toParentViewController: topViewController)
            self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.alpha = 0.0
            self.view.frame = topViewController.view.bounds
            
            let _customAlertView: UIView!
            
            switch self.alertType {
            case .Invite:
                _customAlertView = self.inviteAlertView
            case .Confirmation:
                _customAlertView = self.confirmationAlertView
            }
            
            _customAlertView.isHidden = false
            _customAlertView.alpha = 0.0

            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                self.view.alpha = 1.0
                }, completion: nil)
            _customAlertView.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
            
            _customAlertView.center = CGPoint(x: self.view.bounds.size.width / 2, y: self.view.bounds.size.height/2 - 10)
            
            UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveEaseOut, animations: {

                }, completion: { (finished) in
                    if(self.alertType == .Confirmation) {
                        self.updateConfirmationAlertConstraint()
                    }
                    UIView.animate(withDuration: 0.3, animations: { 
                        _customAlertView.alpha = 1.0
                        _customAlertView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    })
            })
            
        }

    }
    
    func hide() {
        
        
        let _customAlertView: UIView!
        
        switch self.alertType {
        case .Invite:
            _customAlertView = self.inviteAlertView
        case .Confirmation:
            _customAlertView = self.confirmationAlertView
        }
        _customAlertView.isHidden = true

        self.view.endEditing(true)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            _customAlertView.alpha = 0.0
            _customAlertView.transform = CGAffineTransform(scaleX: 1.01, y: 1.01)
            _customAlertView.center    = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height/2.0)-5)
            }, completion: nil)
        
        UIView.animate(withDuration: 0.3, delay: 0.05, options: .curveEaseInOut, animations: { () -> Void in
            self.view.alpha = 0.0
            
        }) { (completed) -> Void in
            
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
            if let dismissBlock = self.didDismissBlock {
                dismissBlock(self)
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func doneButtonTapped(_ sender: AnyObject) {
        
        self.hide()

    }

    @IBAction func confirmationNoTapped(_ sender: UIButton) {
        self.hide()
        if let block = cancelBlock {
            block()
        }
    }
    
    @IBAction func confirmationYesTapped(_ sender: UIButton) {
        self.hide()
        if let block = confirmBlock {
            block()
        }
    }
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        if self == "" {
            return CGFloat(0)
        }
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
}
