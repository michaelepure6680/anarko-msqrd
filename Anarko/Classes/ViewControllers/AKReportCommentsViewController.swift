//
//  AKReportCommentsViewController.swift
//  Anarko
//
//  Created by Khalid on 28/03/2017.
//  Copyright © 2017 Oppous. All rights reserved.
//

import UIKit

class AKReportCommentsViewController: UIViewController {

    @IBOutlet weak var reportTextField: UITextField!
    var reportNumber: Int!
    var videoId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        reportTextField.attributedPlaceholder = NSAttributedString(string: "Tell us what's wrong...",
                                                                   attributes: [NSForegroundColorAttributeName: UIColor.init(white: 0.7, alpha: 1)])
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKReportCommentsViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKReportCommentsViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        reportTextField.delegate = self;
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func sendButtonPressed(_ sender: UIButton) {
        
        if reportTextField.text?.characters.count == 0 {
            AKGlobal.showAlertViewController(title: "Error!", message: "You have to input contents.", target: self)
            return
        }
        
        self.reportTextField.resignFirstResponder()
        
        let paramDic = NSMutableDictionary()
        paramDic.setObject(AKAppManager.sharedInstance.vid!, forKey: "vid_id" as NSCopying)
        paramDic.setObject("\(reportNumber!)", forKey: "reason" as NSCopying)
        paramDic.setObject(AKAppManager.sharedInstance.myUser.phone!, forKey: "phone"  as NSCopying)
        paramDic.setObject(reportTextField.text!, forKey: "comment" as NSCopying)
        
        if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
            AKGlobal.showWithStatus(status: "Reporting...")
            AKAPIManager.sharedManager.reportAnarko(params: dict) { (response) in
                AKGlobal.hideProgress()
                switch response.result {
                case .success:
                    
                    print("Validation Successful")
                    
                    if let json = response.result.value {
                        print("JSON: \(json)")
                        let jsonDic = json as! NSDictionary
                        let error = AKParser.parseErrorMessage(data: jsonDic)
                        if error == nil {
                            self.dismiss(animated: true, completion: { 
                                
                            })
//                            let _ = self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                        }
                    }
                case .failure(let error):
                    print(error)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                }
            }
        }
    }
    
    func setReportContentNumber(_ num: Int, videoid: String) {
        
        self.videoId = videoid
        self.reportNumber = num
    }
    
    // MARK: - Keyboard notification
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
//        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
        
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
        
    }
}

extension AKReportCommentsViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}
