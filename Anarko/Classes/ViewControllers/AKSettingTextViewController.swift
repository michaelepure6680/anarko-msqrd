//
//  AKSettingTextViewController.swift
//  Anarko
//
//  Created by Khalid on 03/04/2017.
//  Copyright © 2017 Oppous. All rights reserved.
//

import UIKit

@objc enum AKControllerType:Int {
    
    case FAQ
    case TermsAndConditions
    case PrivacyPolicy
}

class AKSettingTextViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    var controllerType : AKControllerType = .TermsAndConditions
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch controllerType {
        case .FAQ :
            titleLabel.text = "F.A.Q"
            self.showText(withFileName: "AnarkoFAQ")
            break
        case .TermsAndConditions :
            titleLabel.text = "Terms and Conditions"
            self.showText(withFileName: "MobileAppTC-ANARKO-CLEAN")
            break
        case .PrivacyPolicy :
            titleLabel.text = "Privacy Policy"
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func showText(withFileName fileName : String)
    {
        if let filepath = Bundle.main.path(forResource: fileName, ofType: "rtf") {
            let url = URL(fileURLWithPath: filepath)
            do {
                var dict : NSDictionary? = nil
                let attributedString = try NSAttributedString.init(url: url, options: [NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType], documentAttributes: &dict)
                textView.attributedText = attributedString
            }
            catch
            {
                
            }
        } else {
            // example.txt not found!
        }
    }
}
