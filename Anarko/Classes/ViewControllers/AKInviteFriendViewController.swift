//
//  AKInviteFriendViewController.swift
//  Anarko
//
//  Created by x on 9/18/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import APAddressBook
import MessageUI

protocol FriendListCellDelegate {
    func didSelectCell(cell: FriendListCell) -> Void
}

class FriendListCell: UITableViewCell {
   
    @IBOutlet var friendNameLabel: UILabel!
    
    @IBOutlet var checkButton: UIButton!
    
    @IBAction func checkButtonTapped(_ sender: AnyObject) {
        
        let button = sender as! UIButton
        
        button.isSelected = !button.isSelected
        _selection = button.isSelected
        
        delegate?.didSelectCell(cell: self)
    }
    
    var cellIndex = 0
    var delegate: FriendListCellDelegate!
    
    private var _contact: APContact!
    var contact: APContact! {
        get {
            return _contact
        }
        set (value) {
            _contact = value
            
            let firstName = _contact.name?.firstName
            let lastName = _contact.name?.lastName
            if firstName != nil && lastName != nil {
                friendNameLabel.text = "\(lastName!) \(firstName!)"
            } else if firstName != nil {
                friendNameLabel.text = firstName
            } else if lastName != nil {
                friendNameLabel.text = lastName
            } else {
                friendNameLabel.text = "Unknown"
            }
        }
    }
    
    private var _selection: Bool!
    var selection: Bool! {
        get {
            return _selection
        }
        set (value) {
            _selection = value
            
            checkButton.isSelected = _selection
        }
    }
}

class FriendListHeaderCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var inviteButton: UIButton!
    
}

class AKInviteFriendViewController: UIViewController {

    @IBOutlet var friendListTableView: UITableView!
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    
    var addressBook: APAddressBook!
    var contacts: Array<APContact>!
    
    var selectedContacts: [APContact] = []
    
    var selections: NSMutableArray!
    var allSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        selections = NSMutableArray()
        addressBook = APAddressBook()
        addressBook.loadContacts { (array: [APContact]?, error: Error?) in
            if error == nil {
                self.contacts = array
                self.friendListTableView.reloadData()
                
                for _ in 0..<array!.count {
                    self.selections.add(false)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Present Alert
    
    func presentAlertVC() {
        
        let customReportVC = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! AKCustomAlertViewController
        
        customReportVC.alertType = .Invite
        
        customReportVC.show()
    }
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        
        let _ = self.navigationController?.popViewController(animated: true)

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func inviteButtonTapped() {
        
        if self.selectedContacts.count != 0 {
            
            
            var phoneDic: [String : String] = [:]
            let phoneArr = NSMutableArray()
            var i: Int = 0
            for _contact in self.selectedContacts {
                
                if ( _contact.phones != nil && _contact.phones?[0].number?.characters.count != 0 ) {
                    
                    phoneDic["\(i)"] = _contact.phones?[0].number!
                    
                    phoneArr.add((_contact.phones?[0].number)!)

                }
                
                i += 1
            }
            
            //*********
            
            var tempArr = [String]()
            for item in phoneArr {
                
                tempArr.append(item as! String)
            }
            
            if tempArr.count == 0 {
                
                AKGlobal.showAlertViewController(title: "Error!", message: "Your selected contact(s) don't have phone number(s).", target: self)
                return;
            }
            
            //************
            
            let paramDic = NSMutableDictionary()
            
            paramDic.setObject(AKAppManager.sharedInstance.myUser.phone!, forKey:PHONE as NSCopying)
            
            paramDic.setObject(tempArr, forKey: "friends" as NSCopying)
            
            
            if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
                
                let data = try! JSONSerialization.data(withJSONObject: paramDic, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                if let json = json {
                    print(json)
                    
                    AKAPIManager.sharedManager.inviteFriends(params: json, completionHandler: { (response) in
                        
                        switch response.result {
                            
                        case .success:
                            
                            print("Validation Successful")
                            
                            if let json = response.result.value {
                                print("JSON: \(json)")
                                let jsonDic = json as! NSDictionary
                                let error = AKParser.parseErrorMessage(data: jsonDic)
                                if error == nil {
                                    let customReportVC = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! AKCustomAlertViewController
                                    
                                    customReportVC.alertType = .Invite
                                    customReportVC.didDismissBlock = {(alertVC) -> Void  in
                                        
                                        let _ = self.navigationController?.popViewController(animated: true)
                                    }
                                    customReportVC.show()
                                }
                                else
                                {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                                    
                                }
                            }
                        case .failure(let error):
                            print(error)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                        }
                        
                        
                    })
                }

                
                
            }
        }
    }

}

extension AKInviteFriendViewController: UITableViewDataSource, FriendListCellDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch section {
        case 0:
            if contacts == nil {
                return 0
            } else {
                return contacts.count + 1
            }
        case 1:
            return 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var _cell =  UITableViewCell()
        
        switch indexPath.section {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.FriendListCellIdentifier, for: indexPath) as! FriendListCell

            cell.delegate = self
            if indexPath.row > 0 {
                cell.contact = contacts[indexPath.row - 1]
                cell.selection = selections[indexPath.row - 1] as! Bool
            }
            else
            {
                cell.friendNameLabel.text = "Select all"
                cell.selection = allSelected
            }

            cell.cellIndex = indexPath.row
            _cell = cell
            
        default:
            break
        }
        
        return _cell
    }
    
    func didSelectCell(cell: FriendListCell) -> Void {
        if cell.cellIndex == 0 {
            for i in 0..<contacts!.count {
                self.selections[i] = cell.selection
            }
            self.selectedContacts.removeAll()
            if cell.selection == true {
                
                self.selectedContacts.append(contentsOf: self.contacts)
                
            }
            allSelected = cell.selection
            friendListTableView.reloadData()
        } else {
            selections[cell.cellIndex - 1] = cell.selection
            
            if cell.selection == true {
                
                self.selectedContacts.append(cell.contact)
                
            }else {
                
                if let index = self.selectedContacts.index(of: cell.contact) {
                    
                    self.selectedContacts.remove(at: index)

                }
            }
        }
        
        print(self.selectedContacts.count)
    }
}

extension AKInviteFriendViewController: UITableViewDelegate/*, MFMailComposeViewControllerDelegate*/ {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0: break
          
        case 1:
            if indexPath.row == 0 {
                
                if MFMailComposeViewController.canSendMail() {
                    let mailController = MFMailComposeViewController()
//                    mailController.mailComposeDelegate = self
                    mailController.setSubject("Anarko Invitation")
                    
                    let recipients = NSMutableArray()
                    for i in 0..<contacts.count {
                        if selections[i] as! Bool == true {
                            let contact = contacts[i]
                            if (contact.emails?.count)! > 0 {
                                recipients.add(contact.emails?[0])
                            }
                        }
                    }
//                    mailController.setToRecipients(recipients as? Array)
                    self.present(mailController, animated: true, completion: nil)
                }
            }
            break
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            
            let cell =  tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.FriendListHeaderCellIdentifier) as! FriendListHeaderCell
            
            cell.inviteButton.addTarget(self, action: #selector(AKInviteFriendViewController.inviteButtonTapped), for: .touchUpInside)
            
            return cell
            
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            return 0
        }
        
        return 100
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        return 73
//    }
    
//    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
//        if result == MFMailComposeResult.sent {
//            self.presentAlertVC()
//        }
//    }
}
