//
//  AKPNChangeGuideViewController.swift
//  Anarko
//
//  Created by x on 9/19/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKPNChangeGuideViewController: UIViewController {

    
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var sendRecoveryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        proceedButton.titleLabel?.adjustsFontSizeToFitWidth = true
        sendRecoveryButton.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        
        let _ = self.navigationController?.popViewController(animated: true)

    }

    @IBAction func proceedButtonTapped(_ sender: UIButton) {
        
    }
    @IBAction func sendRecoveryButtonTapped(_ sender: UIButton) {
        AKGlobal.shared.showCustomActivity(self.view)
        AKAPIManager.sharedManager.sendRecoveryCode { (response) in
            AKGlobal.shared.hideCustomActivity()
            switch response.result {
            case .success:
                print("Validation Successful")
                
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let jsonDic = json as! NSDictionary
                    let error = AKParser.parseErrorMessage(data: jsonDic)
                    if error == nil {
                        AKGlobal.showAlertViewController(title: "", message: "Recovery code sent to your number.", target: self)
                    }
                    else
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                    }
                }
            case .failure(let error):
                print(error)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
            }
        }
    }
}
