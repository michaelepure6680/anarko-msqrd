//
//  AKPNChangeViewController.swift
//  Anarko
//
//  Created by x on 9/19/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

class AKPNChangeViewController: UIViewController, AKCountryPickerViewControllerDelegate, SMSCodeTextFieldDelegate {

    @IBOutlet var doneButton: UIButton!
    
    @IBOutlet var topNumberInputView: UIView!
    @IBOutlet var bottomNumberInputView: UIView!
    @IBOutlet var countryPickerView: UIView!
    
    @IBOutlet var topInputTextField: UITextField!
    @IBOutlet var bottomInputTextField: UITextField!
    
    @IBOutlet var topVerifyIconImageview: UIImageView!
    
    @IBOutlet var bottomVerifyIconImageView: UIImageView!
    
    @IBOutlet var topFlagButton: UIButton!
    
    @IBOutlet var bottomFlagButton: UIButton!
    @IBOutlet var verifyTextsView: UIView!
    
    @IBOutlet weak var constriantSpaceOfTopView: NSLayoutConstraint!
    
    
    var phoneNumberStr: String!
    
    var phoneCodeTop = "1"
    var countryCodeTop = "US"
    var phoneCodeBottom = "1"
    var countryCodeBottom = "US"
    var topPhoneValid = false
    var bottomPhoneValid = false
    
    var isTopButtonTapped:Bool = false
    var defaultSpace: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AKPNChangeViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKPNChangeViewController.textFieldDidChange(_:)), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(AKPNChangeViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKPNChangeViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKPNChangeViewController.sendConfirmation(_:)), name: NSNotification.Name(rawValue: kNotificationSendConfirmationCode), object: nil)
        
        for i in 10..<16 {
            if let textField = verifyTextsView.viewWithTag(i) as? SMSCodeTextField {
                textField.codeDelegate = self
            }
        }
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Responsible for reacting to the user tapping the visualEffect
    @objc private func backgroundViewTapped(_ gesture: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
        
    }

    // MARK: - Private
    
    private func showCountryPickerView(){
        
        let countryListVC = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "CountryPopupViewController") as! AKCountryPopupViewController
        
        
        countryListVC.delegate = self
        countryListVC.show()
        
    }
    
    // MARK: - Keyboard notification
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        print (self.view.frame.size.height/2 -  keyboardFrame.size.height)
        
        if self.bottomInputTextField.isFirstResponder {
            
            self.constriantSpaceOfTopView.constant = keyboardFrame.size.height - self.view.frame.size.height/2 + defaultSpace

        }
        
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
        
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            self.constriantSpaceOfTopView.constant = self.defaultSpace

            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }
    }

    // MARK: - AKCountryPickerViewControllerDelegate
    func didSelectCountry(flag: UIImage, name: String, code: String, phcode: String) -> Void {
        
        UIView.animate(withDuration: 0.3) {
        }
        self.view.layoutIfNeeded()
        self.view.endEditing(true)
        
        if self.isTopButtonTapped {
            
            topFlagButton.setImage(flag, for: .normal)
            topFlagButton.setTitle("\(phcode)", for: .normal)
            phoneCodeTop = phcode.replacingOccurrences(of: "+", with: "")
            countryCodeTop = code
            
        }else {
            
            bottomFlagButton.setImage(flag, for: .normal)
            bottomFlagButton.setTitle("\(phcode)", for: .normal)
            phoneCodeBottom = phcode.replacingOccurrences(of: "+", with: "")
            countryCodeBottom = code
        }
        
    }
    
    func sendConfirmation(_ notification: Notification?) {
        
        
        let paramDic = NSMutableDictionary()
        
        phoneNumberStr = "+\(phoneCodeBottom)\(bottomInputTextField.text!)"

        paramDic.setObject(AKAppManager.sharedInstance.myUser.userID, forKey: USER_ID as NSCopying)
        paramDic.setObject(phoneNumberStr!, forKey:PHONE as NSCopying)
        paramDic.setObject(COUNTRY_CODE, forKey: LANGUAGE as NSCopying)
        
        if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
            
            print(dict)
            
            AKGlobal.showWithStatus(status: "Sending...")
            AKAPIManager.sharedManager.changePhoneNumber(params: dict, completionHandler: { (response) in
                
                AKGlobal.hideProgress()
                switch response.result {
                    
                case .success:
                    
                    print("Validation Successful")
                    
                    if let json = response.result.value {
                        print("JSON: \(json)")
                        let jsonDic = json as! NSDictionary
                        let error = AKParser.parseErrorMessage(data: jsonDic)
                        if error == nil {
                            if notification == nil {
                                self.performSegue(withIdentifier: "segueToVerifyCodeVC", sender: self)
                            }
                        }
                        else
                        {
                            if AKParser.errorParse(data: jsonDic) == 0 {
                                _ = AlertBar.show(.notice, message: "WRONG CODE", duration: 0.5, completion: nil)
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                            }
                        }
                    }
                case .failure(let error):
                    print(error)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                }


            })
            
        }

    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueToVerifyCodeVC" {
            
            let verifyVC = segue.destination as! AKVerifyCodeViewController
            
            verifyVC.setTitleString(phoneNumberStr)
        }
    }

    //MARK: - IBActions

    @IBAction func cancelButtonTapped(_ sender: AnyObject) {
        
        let _ = self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func doneButtonTapped(_ sender: AnyObject) {
        if let recoveryCode = self.canGoToNextStep() {
            let paramDic = NSMutableDictionary()
            
            phoneNumberStr = "+\(phoneCodeTop)\(topInputTextField.text!)"
            
            paramDic.setObject(phoneNumberStr!, forKey:"phoneNumber" as NSCopying)
            paramDic.setObject(recoveryCode, forKey: "recoveryCode" as NSCopying)
            
            AKGlobal.showWithStatus(status: "Verifying recovery code...")
            
            AKAPIManager.sharedManager.verifyRecoveryCode(params: paramDic as! [String : AnyObject]) { (response) in
                
                AKGlobal.hideProgress()
                switch response.result {
                case .success:
                    if let json = response.result.value {
                        
                        print("JSON: \(json)")
                        let jsonDic = json as! NSDictionary
                        let error = AKParser.parseErrorMessage(data: jsonDic)
                        if error == nil {
                            if let data = jsonDic["data"] as? NSDictionary, let verified = data["verified"] as? Bool, verified == true {
                                self.confirmNumberFromUser()
                            }
                            else
                            {
                                AKGlobal.showAlertViewController(title: "Error!", message: "You entered wrong recovery code.", target: self)
                            }
                        }
                        else
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                        }
                    }
                case .failure(let error):
                    print(error)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                }
            }
        }
    }

    @IBAction func countryNoButtonTapped(_ sender: AnyObject) {
        
        let button = sender as! UIButton
        
        if button.tag == 20 {
            self.isTopButtonTapped = true
        }else {
            self.isTopButtonTapped = false
        }
        
        
        self.showCountryPickerView()
        self.dismissKeyboard()
    }
    
    func dismissKeyboard() {
    
        view.endEditing(true)
    }
    
    func  checkDifferentPhoneNumbers() {
        if topPhoneValid == true && bottomPhoneValid == true {
            let bottomPhoneNumber = "+\(phoneCodeBottom)\(bottomInputTextField.text!)"
            let topPhoneNumber = "+\(phoneCodeTop)\(topInputTextField.text!)"
            if bottomPhoneNumber == topPhoneNumber {
                bottomPhoneValid = false
                bottomVerifyIconImageView.image = UIImage(named: "icon-cancel")
            }
        }
    }
    
    func confirmNumberFromUser() {
        let newNumberString = "+\(phoneCodeBottom)\(bottomInputTextField.text!)"
        
        let customReportVC = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! AKCustomAlertViewController
        
        customReportVC.alertType = .Confirmation
        customReportVC.show(title: "Number confirmation: \n \(newNumberString)", message: "Is your number above correct?", cancelButtonTitle: "EDIT", confirmButtonTitle: "YES", confirmBlock: {
            
            self.sendConfirmation(nil)
            
        }, cancelBlock: {
            
        })
    }
    
    func canGoToNextStep() -> String? {
        var recoveryCode = ""
        for i in 10..<16 {
            if let textField = verifyTextsView.viewWithTag(i) as? UITextField {
                recoveryCode += textField.text!
            }
        }
        
        if recoveryCode.characters.count == 6 && bottomPhoneValid == true && topPhoneValid == true  {
            return recoveryCode
        }
        else
        {
            return nil
        }
    }
    
    func isBackSpaceTapped(string : String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true;
        }
        else
        {
            return false
        }
    }
    
    func textFieldDidDelete(textField : UITextField) -> Void {
        if let count = textField.text?.characters.count, count > 0 {
            textField.text = ""
        }
        else if let previousTextField = verifyTextsView.viewWithTag(textField.tag - 1) as? UITextField {
            previousTextField.text = ""
            previousTextField.becomeFirstResponder()
        }
    }
}

extension AKPNChangeViewController : UITextFieldDelegate {
    
    func textFieldDidChange(_ notification: Notification) {
        
        let textField = notification.object as! UITextField
        
        if textField == topInputTextField {
            let phoneUtil = NBPhoneNumberUtil()
            
            do {
                let phoneNumber: NBPhoneNumber = try phoneUtil.parse(textField.text, defaultRegion: countryCodeTop)
                let formattedString: String = try phoneUtil.format(phoneNumber, numberFormat: .RFC3966)
                if (phoneUtil.isValidNumber(phoneNumber)) {
                    topVerifyIconImageview.image = UIImage(named: "icon-correct")
                    topPhoneValid = true
                    self.checkDifferentPhoneNumbers()
                } else {
                    topVerifyIconImageview.image = UIImage(named: "icon-cancel")
                    topPhoneValid = false
                }
                NSLog("[%@]", formattedString)
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
        }else if textField == bottomInputTextField{
            
            let phoneUtil = NBPhoneNumberUtil()
            
            do {
                let phoneNumber: NBPhoneNumber = try phoneUtil.parse(textField.text, defaultRegion: countryCodeBottom)
                let formattedString: String = try phoneUtil.format(phoneNumber, numberFormat: .RFC3966)
                if (phoneUtil.isValidNumber(phoneNumber)) {
                    bottomVerifyIconImageView.image = UIImage(named: "icon-correct")
                    bottomPhoneValid = true
                    self.checkDifferentPhoneNumbers()
                } else {
                    bottomVerifyIconImageView.image = UIImage(named: "icon-cancel")
                    bottomPhoneValid = false
                }
                NSLog("[%@]", formattedString)
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            
        } else {
            if let newTextField = verifyTextsView.viewWithTag(textField.tag + 1) as? UITextField {
                if (textField.text?.characters.count)! > 0 {
                    newTextField.becomeFirstResponder()
                }
            }
        }
        
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == bottomInputTextField {
            
            if textField.text?.characters.count != 0 {
                
                self.doneButton.isEnabled = true
            }else {
                
                self.doneButton.isEnabled = false
                
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.hasText {
            
        }else {
            
        }
        
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag >= 10 {
            if self.isBackSpaceTapped(string: string) {
                return true
            }
            let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
            if newLength > 1 {
                return false
            }
        }
        return true
    }
}
