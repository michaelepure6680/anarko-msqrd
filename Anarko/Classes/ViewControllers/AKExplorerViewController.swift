//
//  AKExplorerViewController.swift
//  Anarko
//
//  Created by HeMin on 9/14/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftGifOrigin
import AVKit

@objc protocol ExplorerCellDelegate {
    
    @objc optional func showCommentViewForCurrentVideo(_ currentAnarko: AKFeed)
    @objc optional func shareCurrentVideo(_ currentAnarko: AKFeed)
    @objc optional func showRelatedVideos(_ currentAnarko: AKFeed)
    @objc optional func changeViewMode(_ isExpandMode: Bool)
    @objc optional func videoDidStart()
    
}

class DescriptionCell: UITableViewCell {
    @IBOutlet var descriptionLabel: UILabel!
    
    @IBOutlet var readMoreButton: UIButton!
    var delegate: ExplorerCellDelegate?

    @IBAction func readMoreButtonPressed(_ sender: AnyObject) {
        
        self.delegate?.changeViewMode!(true)
    }
    
    
}

class ExpandedDescriptionCell: UITableViewCell {
    
    @IBOutlet var descriptionLbl: UILabel!
    var delegate: ExplorerCellDelegate?

    @IBAction func decreaseButtonTapped(_ sender: AnyObject) {
        
        self.delegate?.changeViewMode!(false)
    }
}

class TagCell: UITableViewCell {
    @IBOutlet var tagLabel: UILabel!
    
}

class CommentCell: UITableViewCell {
    
    @IBOutlet var commentLabel: UILabel!
    
    var currentComment: AKComment! {
        
        didSet {
            
            self.setCommentText()
        }
    }
    
    func setCommentText() {
        
        self.commentLabel.text = self.currentComment.comment
    }
    
}

class CommentCountCell: UITableViewCell {
    
    @IBOutlet var commentCountLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet weak var viewsCountLabel: UILabel!
    
    var currentFeed: AKFeed! {
        
        didSet{
            
            self.showAddressInfo()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    func showAddressInfo(){
        
        let query = "\(String(format: "%.2f",currentFeed.location[0])),\(String(format: "%.2f",currentFeed.location[1]))"
        AKAPIManager.sharedManager.getLocation(query: query) { (response) in
            
            if let json = response.result.value {
                print("JSON: \(json)")
                let jsonDic = json as! NSDictionary
                let error = AKParser.parseErrorMessage(data: jsonDic)
                if error == nil {
                    let addressInfo = jsonDic["data"] as! String
                    self.addressLabel.isHidden = false
                    self.addressLabel.text = addressInfo
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                }
            }
        }
    }
        

}

// MARK - Custom Collectionview Cell

class ExplorerCell: UICollectionViewCell, ZOWVideoViewDelegate {
    
    @IBOutlet var cellContentView: UIView!
    @IBOutlet var commentButton: UIButton!
    @IBOutlet var refreshButton: UIButton!
    @IBOutlet var viewerButton: UIButton!
    @IBOutlet var viewerCountLabel: UILabel!
    @IBOutlet var cameraButton: UIButton!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var relatedButton: UIButton!
    
    @IBOutlet var thumbImageView: UIImageView!
    
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var tipsLabel: UILabel!
    
    @IBOutlet var relatedLabel: UILabel!
    @IBOutlet var relatedView: UIView!
    
    @IBAction func reportBtnPressed(_ sender: Any) {
        
        let reportPopupVC = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "ReportNotificationView") as! AKReportNotificationViewController
        
        reportPopupVC.currentVideoId = self.currentFeed.anarkoId!
        
        self.parentVC.addChildViewController(reportPopupVC)
        self.parentVC.view.addSubview(reportPopupVC.view)
        
//        self.parentVC?.present(navVC, animated: true, completion: {
//            
//            
//        })

    }
    
    var videoView: ZOWVideoView!
    var parentVC : AKExplorerViewController!

//    private var player: Player = Player()

    
    var delegate: ExplorerCellDelegate?
    
    var currentFeed: AKFeed! {
        
        willSet {
            
            
            
        }
        
        didSet {
            
            if self.thumbImageView.image != nil {
                
                self.thumbImageView.image = nil

            }
            self.thumbImageView.isHidden = false
            
            self.viewerCountLabel.text = "\(currentFeed.views!)"
            
            self.commentCountLabel.text = "\(currentFeed.comments.count)"
            self.showTips()
            self.thumbImageView.image = UIImage(named:"placerholder_anarko_640*960.png")
//            if let url = self.currentFeed.nameUrl {
//                
//                self.thumbImageView.af_setImage(withURL: URL(string: "\(BASE_URL)/vid/\(url).jpg")!, placeholderImage: UIImage(named:"placerholder_anarko_640*960.png"), filter: nil, progress: { (process) in
//                    
//                    print(process)
//                    
//                }, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: { (response) in
//                    
//                    print("response is \(response)")
//                    
//                    
//                })
//                
//                
//            }

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.addVideoPlayerView()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(relatedViewTapped))
        tapGesture.numberOfTapsRequired = 1
        self.relatedView.addGestureRecognizer(tapGesture)
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.addVideoPlayerView()
    }
    
    func showTips()
    {
        if let showExplorerTips = UserDefaults.standard.object(forKey: "showExplorerTips") as? String , showExplorerTips == "yes" {
            
            self.tipsLabel.isHidden = false;
            UserDefaults.standard.set("no", forKey: "showExplorerTips")
            UserDefaults.standard.synchronize()
        }
        else
        {
            self.tipsLabel.isHidden = true;
        }
    }
    
    func addVideoPlayerView(){
        
        if self.videoView != nil {
            self.videoView.isHidden = true
            self.videoView.pause()
            return
            //self.videoView.removeFromSuperview()

            //self.videoView = nil
        }
        self.videoView = ZOWVideoView(frame: CGRect(x: -2, y: 0, width: self.cellContentView.bounds.size.width+4, height: self.cellContentView.bounds.size.height+4))
        self.cellContentView.insertSubview(self.videoView, aboveSubview: self.thumbImageView)
        self.videoView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.videoView.isHidden = true
        self.videoView.delegate = self
    }
    //Gesture
    
    func relatedViewTapped(){
        
        self.delegate?.showRelatedVideos!(self.currentFeed!)

    }
    
  
    // MARK: 
    
    func videoLoaded(withSuccess player: ZOWVideoPlayer!, contentView view: ZOWVideoView!) {
        
//        self.thumbImageView.isHidden = true
    }
    
    func videoDidStart() {
        
        let when = DispatchTime.now() + 0.3
        DispatchQueue.main.asyncAfter(deadline: when) { 
            
            self.delegate?.videoDidStart!()
            if self.parentVC.activeCell != nil && self.parentVC.activeCell! == self {
                AKGlobal.shared.hideCustomActivity()
                self.videoView.isHidden = false
            }
        }
    }
    
    func videoDidResume() {
        self.videoDidStart()
    }
    
    func videoDidEnd() {
        
        var viewsCount = 1
        if let views = self.currentFeed.views {
            viewsCount = views + 1;
        }
        self.currentFeed.views = viewsCount
        viewerCountLabel.text = "\(viewsCount)"
        if let anarkoId = self.currentFeed.anarkoId , let myUser = AKAppManager.sharedInstance.myUser , let _ = myUser.token {
            AKAPIManager.sharedManager.increaseVideoViewsCount(anarkoId: anarkoId) { (response) in
                switch response.result {
                case .success:
                    if let json = response.result.value {
                        print("JSON: \(json)")
                    }
                case .failure(let error):
                    print (error)
                    
                }
            }
        }
    }
    
    func stopVideo() {
        
        if self.videoView != nil{
            
            self.videoView.pause()
        }

    }
    
    func muteVideo(_ mute: Bool) {
        
        if mute {
            self.videoView.mute()
        }else {
            self.videoView.unmute()
        }
    }
    
    func resumeVideo() {
        
        if self.videoView != nil{
            self.videoView.resume()
        }
    }
    
    func playvideo() {
        
        print("video view frame is : *** \(self.videoView.frame.size.width) ******* \(self.videoView.frame.size.height)")
        
        if let url = self.currentFeed.url {
            
            print("Video Url is ..... \(url)")
            
            
            
            let videoUrl = url
            
            
            if self.videoView != nil{
                
                self.videoView.playVideo(with: URL(string: videoUrl)!)

            }
            else
            {
                AKGlobal.shared.hideCustomActivity()
            }
            
            
        }
        else
        {
            AKGlobal.shared.hideCustomActivity()
        }

    }
    
    @IBAction func commentButtonPressed(_ sender: AnyObject) {
        
        self.delegate?.showCommentViewForCurrentVideo!(self.currentFeed)
    }
    
    @IBAction func viewerButtonPressed(_ sender: AnyObject) {
    }
    
    @IBAction func shareButtonPressed(_ sender: AnyObject) {
        
        self.delegate?.shareCurrentVideo!(self.currentFeed!)
    }
    
    //
    
}

var isFirstLoad = true
class AKExplorerViewController: UIViewController, UITextFieldDelegate , NVActivityIndicatorViewable, UIScrollViewDelegate{

    @IBOutlet var uploadProgressView: UIProgressView!
    @IBOutlet var explorerCollectionView: UICollectionView!
    @IBOutlet var commentTableView: UITableView!
    
    @IBOutlet var blurView: UIView!
    
    @IBOutlet var statusLabel: UILabel!
    
    @IBOutlet var searchTextField: UITextField!
    //
    @IBOutlet var commentView: UIView!
    
    @IBOutlet var constraintTopspaceOfCommentView: NSLayoutConstraint!
    
    
    @IBOutlet var tempVideoView: InstagramVideoView!
    
    @IBOutlet var logoImageView: UIImageView!
    
    //
    @IBOutlet var inputTextView: UIView!
    @IBOutlet var inputTextField: UITextField!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var constraintBottomSpaceOfInputview: NSLayoutConstraint!
    //
    @IBOutlet var commentEnabledButton: UIButton!
    @IBOutlet var reportButton: UIButton!
    @IBOutlet var refreshButton: UIButton!
    @IBOutlet weak var totalVideosFoundLabel: UILabel!
    @IBOutlet weak var noVideosLabel: UILabel!
    
    let cellIdentifierString = "ExplorerCell"
    
    var defaultTopSpaceOfCommentView: CGFloat!
    var defaultBottomSpaceOfInputview: CGFloat!
    
    var feedArray:[AKFeed] = []
//    var commentArray: [AKComment] = []
    
    var commentArray: [String] = []
    
    var currentAnarko: AKFeed!
    
    var isFirstLoading: Bool = true
    var isExpand: Bool = false
    lazy var documentInteractionController = UIDocumentInteractionController()
    
    var activeCell: ExplorerCell? = nil
    var isHidden = false
    var isViewVisible = false
    var isFirstViewLoad = true
    var alertBar : AlertBar?
    var isRetryingUploading = false
    
    var searchedQuery = ""
    var isEndOfVideos = false
    var inMemoryCells = [ExplorerCell]()
    
    var uploadParams: NSMutableDictionary? = nil
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
        
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isFirstLoad == true && AKGlobal.getClientID() != nil {
            self.cameraButtonTapped(nil)
            isFirstLoad = false
        }
        self.initializeComponents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.defaultTopSpaceOfCommentView = self.constraintTopspaceOfCommentView.constant
        self.defaultBottomSpaceOfInputview = self.constraintBottomSpaceOfInputview.constant
        self.noVideosLabel.layer.opacity = 0
        UIApplication.shared.isStatusBarHidden = true
       
        isViewVisible = true
        isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        isHidden = true
        isViewVisible = false
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationMuteVideo"), object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
      
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationUnMuteVideo"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    

    // MARK: - Private
    
    // load Gif
    
    func loadGifLogo() {
        
        self.refreshButton.setGif("gif-explorer")

    }
    
    func initializeComponents() {
        
        
        self.loadGifLogo()   
        
        self.commentTableView.rowHeight = UITableViewAutomaticDimension
        self.commentTableView.estimatedRowHeight = 90
        
        self.totalVideosFoundLabel.layer.masksToBounds = true
        self.totalVideosFoundLabel.layer.cornerRadius = 8
        self.totalVideosFoundLabel.layer.opacity = 0
        
        self.noVideosLabel.layer.masksToBounds = true
        self.noVideosLabel.layer.cornerRadius = 8
        self.noVideosLabel.layer.opacity = 0

        //intialize long tap gesture to Collection view
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(AKExplorerViewController.collectionViewLongPressed(_:)))
        
        self.explorerCollectionView.addGestureRecognizer(longPressRecognizer)
        
        //set up cursor color to red : 
        
        UITextField.appearance().tintColor = UIColor.red
        
        // Jumping to Store
        NotificationCenter.default.addObserver(self, selector: #selector(AKExplorerViewController.goToStoreVC), name: NSNotification.Name(rawValue: kNotificationGoToProductVC), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKExplorerViewController.handleVideoUpload(notification:)), name: NSNotification.Name(rawValue: kNotificationUploadVideo), object: nil)
        //

        //Register  text change Notification
        NotificationCenter.default.addObserver(self, selector: #selector(AKExplorerViewController.textFieldDidChange(_:)), name: NSNotification.Name.UITextFieldTextDidChange, object: self.searchTextField)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AKExplorerViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKExplorerViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AKExplorerViewController.tableViewScrollToBottom), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadData(_:)), name:NSNotification.Name(rawValue: kNotificationLoadAnarko), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleServerError(_:)), name:NSNotification.Name(rawValue: kNotificationHandleError), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterBackground(_:)), name:NSNotification.Name(rawValue: kNotificationAppWillEnterBackground), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterForeground(_:)), name:NSNotification.Name(rawValue: kNotificationAppWillEnterForeground), object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AKExplorerViewController.blurViewTapped(_:)))
        self.blurView.addGestureRecognizer(tapGesture)
        //
        if let _ = AKGlobal.getClientID(){

        }

    }
    
    // Load videos with search query
    
    func loadFeed(query: String, from: Int, size: Int) {
                
        AKGlobal.shared.showCustomActivity(self.view)
        
        AKAPIManager.sharedManager.getAnarkoList (query: AKParser.queryStringOfSearchWith(query: query, from: from, size: size), completionHandler: { (response) in
            
            AKGlobal.shared.hideCustomActivity()
            switch response.result {
                
            case .success:
                
                print("Validation Successful")
                
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let jsonDic = json as! NSDictionary
                    let error = AKParser.parseErrorMessage(data: jsonDic)
                    if error == nil {
                        let anarkoArray = AKParser.parseFeedData(data: (jsonDic["data"] as? NSDictionary)!)
                        if anarkoArray.count < AKParser.size {
                            self.isEndOfVideos = true
                        }
                        if anarkoArray.count > 0 {
                            self.feedArray.append(contentsOf: anarkoArray)
                            DispatchQueue.main.async {
                                if from == 0 && query != "" {
                                    self.showSearchedVideosCount(totalVideos: AKParser.total)
                                }
                                self.explorerCollectionView.reloadData()
                            }
                        }
                        if self.feedArray.count == 0 {
                            if from == 0 && query != "" {
                                let alertController = UIAlertController(title: "", message: "These aren't the videos you are looking for - meaning, no results found", preferredStyle: .alert)
                                self.navigationController?.present(alertController, animated: true, completion: nil)
                                let when = DispatchTime.now() + 5
                                DispatchQueue.main.asyncAfter(deadline: when) {
                                    alertController.dismiss(animated: true, completion: nil)
                                    self.refreshButtonTapped(nil)
                                }
                            }
                            else
                            {
                                UIView.animate(withDuration: 0.5) {
                                    self.noVideosLabel.layer.opacity = 1
                                }
                            }
                        } else {
                            self.noVideosLabel.layer.opacity = 0
                        }
                    }
                    else
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                    }
                }
            case .failure(let error):
                print(error)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
            }
            
        })

    }
    
    func loadData(_ notification: Notification) {
        
        isEndOfVideos = false
        var query = ""
        let from = 0
        let size = AKParser.size
        
        if let cell = activeCell {
            cell.stopVideo()
        }
        
        self.isFirstLoading = true
        self.feedArray.removeAll()

        self.explorerCollectionView.reloadData()
        if notification.object != nil {
            query = notification.object as! String
        }else {
            
        }
        
        searchedQuery = query
    
        self.loadFeed(query: query, from: from, size: size)

    }
    
    func loadNextPageOfExplorer() {
        
        let query = searchedQuery
        let from = feedArray.count
        let size = AKParser.size
        
        if let cell = activeCell {
            cell.stopVideo()
        }
        
        self.loadFeed(query: query, from: from, size: size)
        
    }
    
    // Search with Tag
    
    func searchAnarko(query:String) {
        
        self.blurViewTapped(nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationLoadAnarko), object: query)

    }
    
    
    // Comment Tableview scroll to bottom
    
    @objc private func tableViewScrollToBottom(){
        
        if self.commentArray.count != 0 {
            
            let indexPath = IndexPath(row: self.commentArray.count + 2, section: 0)
            self.commentTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
        
    }
    
    // Move to Store view
    
    @objc private func goToStoreVC() {
        
        self.performSegue(withIdentifier: "segueToProductListVC", sender: self)
    }
    
    func showSearchedVideosCount(totalVideos : Int) {
        self.totalVideosFoundLabel.text = "\(AKParser.total) videos found"
        if AKParser.total == 1 {
            self.totalVideosFoundLabel.text = "\(AKParser.total) video found"
        }
        UIView.animate(withDuration: 0.5) {
            self.totalVideosFoundLabel.layer.opacity = 1
        }
    
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3) {
            UIView.animate(withDuration: 0.5) {
                self.totalVideosFoundLabel.layer.opacity = 0
            }
        }
    }
    
    // MARK: - Gesture Action
    
    @objc private func collectionViewLongPressed(_ sender: UILongPressGestureRecognizer) {
        
        UIView.animate(withDuration: 0.4, animations: { 
            
            self.blurView.alpha = 1
            self.blurView.isHidden = false
            
            }) { (finished) in
         
                if finished {
                    
                    self.searchTextField.becomeFirstResponder()
                }
        }
    }
    
    @objc private func blurViewTapped(_ sender: UITapGestureRecognizer? = nil) {
    
        self.searchTextField.resignFirstResponder()
        
        UIView.animate(withDuration: 1, animations: {
            
            self.blurView.alpha = 0
            self.blurView.isHidden = true
            
        }) { (finished) in
            
            if finished {
                
            }
        }

    }

    // MARK: - UIScrollView Delegate
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if self.inputTextField.isFirstResponder {
            
            if velocity.y < -0.2 {
                
                self.inputTextField.resignFirstResponder()
            }
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
        
        let visibleRect = CGRect(origin: self.explorerCollectionView.contentOffset, size: self.explorerCollectionView.bounds.size)
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        let indexPath = self.explorerCollectionView.indexPathForItem(at: visiblePoint)
        
        print("current cell row is --- \(indexPath?.row)")
        
        let cell = self.explorerCollectionView.cellForItem(at: indexPath!) as! ExplorerCell
        

        
        if let lastVisibleCell = activeCell {
            lastVisibleCell.stopVideo()
        }
        activeCell = cell
        AKGlobal.shared.showCustomActivity(self.view)
        cell.playvideo()
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.searchTextField.returnKeyType = .search

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == searchTextField {
            
            if textField.hasText {
                
                self.searchAnarko(query: textField.text!)
                
            }else {
                
            }

        }
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidChange(_ notification: Notification) {
        
        let _ = notification.object as! UITextField
        
    }
    
    
    // MARK: - Keyboard notification
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            self.constraintBottomSpaceOfInputview.constant = keyboardFrame.size.height
            self.view.layoutIfNeeded()
            
            }) { (finished) in
                
        }
        
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        
        var info = (notification as NSNotification).userInfo!
        
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: UIViewAnimationOptions(rawValue: UInt(curve)), animations: {
            
            self.constraintBottomSpaceOfInputview.constant = self.defaultBottomSpaceOfInputview
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
        }

    }
    
    //MARK: - Actions
    
    @IBAction func refreshButtonTapped(_ sender: AnyObject?) {
        
        if let cell = activeCell {
            cell.stopVideo()
        }
        
        self.feedArray.removeAll()
        self.isFirstLoading = true
        self.explorerCollectionView.reloadData()
        isEndOfVideos = false
        searchedQuery = ""
        
        self.loadFeed(query: "", from: 0, size: AKParser.size)
    }
    
    @IBAction func cameraButtonTapped(_ sender: AnyObject?) {
        
        let storyboard = UIStoryboard(name: "Camera", bundle: nil)
        var cameraVC = AKCameraViewController.loaded()
        if cameraVC == nil {
            cameraVC = storyboard.instantiateViewController(withIdentifier: "AKCameraViewController") as! AKCameraViewController
        }
        
        let navVC = UINavigationController(rootViewController: cameraVC!)
        navVC.isNavigationBarHidden = true

        if let alertbar = self.alertBar {
            alertbar.tempCompletion = nil
            alertbar.hide()
        }
        self.navigationController?.present(navVC, animated: true, completion: {
            
        })
    }
    
    @IBAction func reportButtonTapped(_ sender: AnyObject) {
        
        let reportVC = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "ReportViewController") as! AKReportViewController
        
        reportVC.currentVideoId = self.currentAnarko.anarkoId!
        AKAppManager.sharedInstance.vid = self.currentAnarko.anarkoId!
        
        let navVC = UINavigationController(rootViewController: reportVC)
        
        self.navigationController?.present(navVC, animated: true, completion: { 
            
            
        })
    }
    
    @IBAction func sendButtonTapped(_ sender: AnyObject) {
        
        if self.inputTextField.text?.characters.count != 0 {
            
            self.inputTextField.resignFirstResponder()
            
            let commentStr = self.inputTextField.text!
            self.inputTextField.text = ""
            let paramDic = NSMutableDictionary()
            
            paramDic.setObject(self.currentAnarko.anarkoId, forKey:"id" as NSCopying)
            paramDic.setObject(commentStr, forKey: "comment" as NSCopying)
            
            if let dict = (paramDic as NSDictionary) as? [String:AnyObject] {
                
                AKGlobal.shared.showCustomActivity(self.view)
                AKAPIManager.sharedManager.addComment(params: dict, completionHandler: { (response) in
                    
                    AKGlobal.shared.hideCustomActivity()
                    switch response.result {
                        
                    case .success:
                        print("Validation Successful")
                        if let json = response.result.value {
                            print("JSON: \(json)")
                            let jsonDic = json as! NSDictionary
                            let error = AKParser.parseErrorMessage(data: jsonDic)
                            if error == nil {
                                //add one cell before send comment
                                self.commentArray.append(commentStr)
                                self.currentAnarko.comments.append(commentStr)
                                self.activeCell?.commentCountLabel.text = "\(self.currentAnarko.comments.count)"
                                self.commentTableView.reloadData()
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                            }
                        }
                    case .failure(let error):
                        
                        print(error)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                    }
                    
                })

            }
            
        }
    }
    
    @IBAction func dismissCommentView(_ sender: AnyObject) {
        
        
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
            
            self.commentView.alpha = 0
            self.constraintTopspaceOfCommentView.constant = self.defaultTopSpaceOfCommentView
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
            
        }

    }
    
}

extension AKExplorerViewController: ExplorerCellDelegate {
    
    func showCommentViewForCurrentVideo(_ currentAnarko: AKFeed) {
        
        
        self.currentAnarko = currentAnarko
        
        self.commentArray = self.currentAnarko.comments
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: { 
            
            self.commentView.alpha = 1
            self.constraintTopspaceOfCommentView.constant = 0
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
            self.commentTableView.reloadData()
        }
    }
    
    func shareCurrentVideo(_ currentAnarko: AKFeed) {
        
        if let url = currentAnarko.url {
            
            print("Video Url is ..... \(url)")

            let shareLink = url
            AKGlobal.shared.showCustomActivity(self.view)
            AKGlobal.downloadFromUrl(url: shareLink, completionHandler: { (data: Data?) in
                DispatchQueue.main.async {
                    if data == nil {
                        AKGlobal.shared.hideCustomActivity()
                        return
                    }
                    let filepath = NSTemporaryDirectory().appending("download.mp4")
                    unlink((filepath as NSString).utf8String)
                    let fileurl = URL(fileURLWithPath: filepath)
                    try! data?.write(to: fileurl)
                    AKUtilities.addWatermarkVideo(fileurl, completion: { (videoURL: URL?) in
                        AKGlobal.shared.hideCustomActivity()
                        if videoURL == nil {
                            return
                        }
                        //let vdata = try! Data(contentsOf: videoURL!)
                        let activityVC = UIActivityViewController(activityItems: [videoURL!], applicationActivities: nil)
                        activityVC.setValue("Video from Anarko", forKey: "subject")
                        activityVC.popoverPresentationController?.sourceView = self.view
                        self.navigationController?.present(activityVC, animated: true, completion: nil)
                        activityVC.completionWithItemsHandler = { activity, success, items, error in
                            
                            if success ==  true && activity == UIActivityType.saveToCameraRoll {
                                let player = AVPlayer(url: videoURL!)
                                let playerController = AVPlayerViewController()
                                playerController.player = player
                                self.present(playerController, animated: true) {
                                    player.play()
                                }
                                print("activity: \(activity), success: \(success), items: \(items), error: \(error)")
                            }
                            
                        }
                    })
                }
            })
        }

    }
    
    func showRelatedVideos(_ currentAnarko: AKFeed) {
        

        var tagQuery: String = ""
        for tag in currentAnarko.tags {
            
            tagQuery.append("\(tag);")
        }
        
        print(tagQuery)
        
        self.searchAnarko(query: tagQuery)
    }
    
    func changeViewMode(_ isExpandMode: Bool) {
        
        self.isExpand = isExpandMode
        
        self.commentTableView.reloadData()
    }
    
    func videoDidStart() {
        for cell in inMemoryCells {
            if cell != activeCell {
                cell.stopVideo()
                cell.videoView.isHidden = true
            }
        }
        if isViewVisible == false {
            print ("View is not visible")
            if let visibleCell = activeCell {
                visibleCell.stopVideo()
            }
        }
    }
    func showLoginViewController() -> Void
    {
        AKGlobal.removeClientID()
        let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! AKHomeViewController
        let loginNavVC = UINavigationController(rootViewController: loginVC)
        loginNavVC.isNavigationBarHidden = true
        self.present(loginNavVC, animated: true) { 
            AKGlobal.showAlertOnTopController(title: "Error!", message: "Your token has been expired. You need to login again.", target: self)
        }
        
    }
    
    func appWillEnterBackground(_ notification: Notification) {
        if isViewVisible {
            for cell in inMemoryCells {
                cell.stopVideo()
                cell.videoView.isHidden = true
            }
        }
    }
    
    func appWillEnterForeground(_ notification: Notification) {
        if isViewVisible {
            self.refreshButtonTapped(nil)
        }
    }
    
    func handleServerError(_ notification: Notification) {
        if let error = notification.object as? NSError {
            if let errorCode = error.localizedFailureReason , errorCode == "S-1002" || errorCode == "U-005" {
                print ("logout here : \(errorCode)")
                if self.presentedViewController == nil {
                    self.showLoginViewController()
                }
                else
                {
                    self.dismiss(animated: true, completion: {
                        self.showLoginViewController()
                    })
                }
            }
            else
            {
                if error.domain == NSURLErrorDomain &&
                    (error.code == NSURLErrorNotConnectedToInternet ||
                     error.code == NSURLErrorTimedOut ||
                     error.code == NSURLErrorNetworkConnectionLost ) {
                    self.alertBar?.hide(completion: {
                
                    })
                    self.alertBar = AlertBar.show(.error, otherButtonsHidden: true, viewInterActionEnabled: true, message: "NO INTERNET CONNECTION", duration: -1, completion: nil)
                }
                else
                {
                    var title = ""
                    if let type =  error.localizedRecoverySuggestion {
                        title = type
                    }
                    else
                    {
                        //title = error.domain
                        title = "Error"
                    }
                    let customAlert = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! AKCustomAlertViewController
                    customAlert.alertType = .Confirmation
                    customAlert.show(title: title, message: error.localizedDescription, cancelButtonTitle: "", confirmButtonTitle: "OK", confirmBlock: { 
                        
                    }, cancelBlock: { 
                        
                    })
                }
            }
        }
    }
}


extension AKExplorerViewController: UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
        
        
    }
}
extension AKExplorerViewController: UICollectionViewDataSource {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.feedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        print("---row is --- \(indexPath.row)")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifierString, for: indexPath) as! ExplorerCell
        
        cell.delegate = self
        cell.parentVC = self
        cell.currentFeed = self.feedArray[indexPath.row]
        
        if self.isFirstLoading {
            AKGlobal.shared.showCustomActivity(self.view)

            if isFirstViewLoad == true {
                cell.muteVideo(true)
            }
            
            activeCell = cell
            cell.playvideo()
            
            isFirstViewLoad = false
            self.isFirstLoading = false
        }
        
        if !isEndOfVideos && indexPath.row == feedArray.count - 1 {
            self.loadNextPageOfExplorer()
        }
        
        inMemoryCells.remove(object: cell)
        inMemoryCells.append(cell)
        
        return cell
    }
    
}

extension AKExplorerViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        print("Display---ros is --- \(indexPath.row)")

        //let _cell = cell as! ExplorerCell
        
        //_cell.showTips()
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        print("Enddisplay---ros is --- \(indexPath.row)")

        
        let _cell = cell as! ExplorerCell
        
        _cell.stopVideo()
        
        if _cell.tipsLabel.isHidden == false {
            _cell.tipsLabel.isHidden =  true
        }
    }
}

extension AKExplorerViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: self.explorerCollectionView.bounds.width, height: self.explorerCollectionView.bounds.height)
        
        return size
    }
}


extension AKExplorerViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let _ = self.currentAnarko {
            
            return self.commentArray.count + 3

        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var _cell = UITableViewCell()
        
        switch indexPath.row {
            
        case 0:
            
            if isExpand {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.ExpandedDescriptionCellIdentifier, for: indexPath) as! ExpandedDescriptionCell
                cell.descriptionLbl.text = self.currentAnarko.descriptionStr
                cell.delegate = self
                _cell = cell
                
            }else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.DescriptionCellIdentifier, for: indexPath) as! DescriptionCell
                cell.descriptionLabel.text = self.currentAnarko.descriptionStr
                cell.delegate = self

                _cell = cell
            }
            
            break

        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.TagCellIdentifier, for: indexPath) as! TagCell
            cell.tagLabel.text = "#tags"
            cell.tagLabel.textColor = UIColor.white.withAlphaComponent(0.6)
            if let tags = self.currentAnarko.tags , tags.count > 0 {
                let filteredTags = tags.filter({ (element) -> Bool in
                    return element.characters.first == "#"
                })
                if filteredTags.count > 0 {
                    cell.tagLabel.text = filteredTags.joined(separator: " ")
                    cell.tagLabel.textColor = UIColor.white
                }
                
            }
            _cell = cell
            break
          
        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.CommentCountCellIdentifier, for: indexPath) as! CommentCountCell
            cell.commentCountLabel.text = "\(self.commentArray.count)"
            cell.viewsCountLabel.text = "\(self.currentAnarko.views!)"
            cell.currentFeed = self.currentAnarko
            _cell = cell
            break
        default:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.CommentCellIdentifier, for: indexPath) as! CommentCell
            cell.commentLabel.text = self.commentArray[indexPath.row - 3]
            _cell = cell
            break
        }
        
        return _cell

    }
    
}

extension AKExplorerViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
    }
    
}

extension AKExplorerViewController {
    func handleVideoUpload(notification: Notification) -> Void {
        let params = notification.object as! NSMutableDictionary
        uploadParams = params
        videoUpload(params: params)
        if feedArray.count == 0 {
            self.refreshButtonTapped(nil)
        }
    }
    
    func videoUpload(params: NSMutableDictionary) -> Void {

        let videoURL = params["videoUrl"] as! URL
        params.removeObject(forKey: "videoUrl")
        let videoData = NSData(contentsOf: videoURL)
        let thumbImage = AKUtilities.thumbImage(videoURL, time: kCMTimeZero)
        let imageData = UIImageJPEGRepresentation(thumbImage!, 0.8)
        if let dict = (params as NSDictionary) as? [String:AnyObject] {
            
            uploadProgressView.isHidden = false
            uploadProgressView.progress = 0.0
            AKAPIManager.sharedManager.uploadAnarko(params: dict, videoData: videoData, imageData: imageData as NSData?, completionHandler: { (response) in
                
                self.isRetryingUploading = false
                switch response.result {
                    
                case .success:
                    
                    print("Validation Successful")
                    
                    if let json = response.result.value {
                        print("JSON: \(json)")
                        let jsonDic = json as! NSDictionary
                        let error = AKParser.parseErrorMessage(data: jsonDic)
                        if error != nil {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                        }
                    }
                    self.uploadProgressView.isHidden = true
                    self.refreshButtonTapped(nil)
                    
                case .failure(let error):
                    
                    params["videoUrl"] = videoURL
                    print(error)
                    self.uploadProgressView.isHidden = true
                    if let alertbar = self.alertBar {
                        alertbar.tempCompletion = nil
                        alertbar.hide()
                    }
                    
                    self.alertBar = AlertBar.show(.notice, otherButtonsHidden: false, message: "TRY AGAIN", duration: -1, completion: {
                        self.cameraButtonTapped(nil)
                        print("Close AlertBar")
                    })
                    self.alertBar?.retryCompletion = {
                        if self.isRetryingUploading == false {
                            self.isRetryingUploading = true
                            self.videoUpload(params: self.uploadParams!)
                        }
                        
                    }
                }
                
            }, progressHandler: { (progress) in
                DispatchQueue.main.async {
                    self.uploadProgressView.progress = progress
                }
            })
        }
    }
}
