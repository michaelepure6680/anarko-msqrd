//
//  AKContactUsViewController.swift
//  Anarko
//
//  Created by Khalid on 24/05/2017.
//  Copyright © 2017 Oppous. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsCell: UITableViewCell {
    @IBOutlet weak var adressLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    
        
}


class AKContactUsViewController: UIViewController, MFMailComposeViewControllerDelegate {
    var alertBar : AlertBar?

    @IBOutlet weak var tableView: UITableView!
    let titleArray = ["For Marketing:",
                      "For Investments:",
                      "For Support:","For Jobs:","For other inquiries:"]
    let emailID = ["marketing@anarko.city","investments@anarko.city","support@anarko.city","jobs@anarko.city","contact@anarko.city"]
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func sendEmail() {
            }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}
extension AKContactUsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactUs", for: indexPath) as! ContactUsCell
        
        cell.adressLbl.text = self.titleArray[indexPath.row]
        cell.emailLbl.text = self.emailID[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 55.0;
    }
    
}

extension AKContactUsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([emailID[indexPath.row]])
            //mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }

    }
    
}
