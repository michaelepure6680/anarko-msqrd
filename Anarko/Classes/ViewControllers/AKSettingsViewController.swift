//
//  AKSettingsViewController.swift
//  Anarko
//
//  Created by x on 9/18/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit

enum LinkType: String {
    
    case Guide = "https://www.anarko.city/guidelines", Faq = "https://www.anarko.city/faq" , Terms = "https://www.anarko.city/terms", Privacy = "https://www.anarko.city/policy"
}
class SettingCell: UITableViewCell {
    
    
    @IBOutlet var titleLbl: UILabel!
    
}


class AKSettingsViewController: UIViewController {
   
    
    let titleArray = ["INVITE FRIENDS",
                      "MY PURCHASES",
                      "CHANGE NUMBER",
                      "F.A.Q",
                      "TERMS & CONDITIONS",
                      "DELETE MY ACCOUNT",
                      "CONTACT US"]
    
    @IBOutlet var settingTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func deleteAccount()
    {        
        let customReportVC = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as! AKCustomAlertViewController

        customReportVC.alertType = .Confirmation
        customReportVC.show(title: "Are you sure you want to delete your account?", message: "Your phone number and payment methods will be deleted", cancelButtonTitle: "NO", confirmButtonTitle: "YES", confirmBlock: { 
            
            print("Confirm")
            AKGlobal.shared.showCustomActivity(self.view)
            AKAPIManager.sharedManager.deleteAccount(completionHandler: { (response) in
                AKGlobal.shared.hideCustomActivity()
                switch response.result {
                    
                case .success:
                    
                    print("Validation Successful")
                    
                    if let json = response.result.value {
                        print("JSON: \(json)")
                        
                        let jsonDic = json as! NSDictionary
                        if let error = AKParser.parseErrorMessage(data: jsonDic) {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                        }
                        else
                        {
                            AKMaskManager.sharedInstance.deleteAllMask()
                            AKGlobal.removeClientID()
                            let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! AKHomeViewController
                            self.navigationController?.pushViewController(loginVC, animated: true)
                        }
                    }
                case .failure(let error):
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationHandleError), object: error)
                }
            })
            
        }, cancelBlock: {
            
            print("Cancel")
        
        })
    }
    func openSettingTextViewController(withType type:AKControllerType) -> Void {
        
        let settingTextVC = UIStoryboard(name: "Explorer", bundle: nil).instantiateViewController(withIdentifier: "SettingTextViewControllerId") as! AKSettingTextViewController
        settingTextVC.controllerType = type
        self.navigationController?.pushViewController(settingTextVC, animated: true)
        
    }
}

extension AKSettingsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        
        cell.titleLbl.text = self.titleArray[indexPath.row]
        return cell
    }
    
}

extension AKSettingsViewController: UITableViewDelegate {
    
    //Options removed from Settings
//    self.performSegue(withIdentifier: "segueToNotificationVC", sender: self)
//    AKAppManager.sharedInstance.openWebPageInSafari(LinkType.Guide)
//    self.performSegue(withIdentifier: "segueToFeedbackVC", sender: self)
//    let tourVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TourViewController") as! AKTourViewController
//    tourVC.setupPushed(value: true)
//    self.navigationController?.pushViewController(tourVC, animated: true)
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: "segueToInviteVC", sender: self)
            break
        case 1:
            self.performSegue(withIdentifier: "segueToMyPurchaseVC", sender: self)
            break
        case 2:
            self.performSegue(withIdentifier: "segueToChangePNVC", sender: self)
            break
        case 3:
            self.openSettingTextViewController(withType: .FAQ)
            break
        case 4:
            self.openSettingTextViewController(withType: .TermsAndConditions)
            break
        case 5:
            self.deleteAccount()
            break
        case 6:
            self.performSegue(withIdentifier: "segueToContactUs", sender: self)
            break
        default:
            break
        }
        
    }
    
}
