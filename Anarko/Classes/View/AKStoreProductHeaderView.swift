//
//  AKStoreProductHeaderView.swift
//  Anarko
//
//  Created by HeMin on 9/16/16.
//  Copyright © 2016 Oppous. All rights reserved.
//

import UIKit
import SwiftGifOrigin
import AlamofireImage
import Alamofire
@objc protocol GSKTabsStretchyHeaderViewDelegate {
    
    func tabsStretchyHeaderView(headerView: AKStoreProductHeaderView, didSelectTabAtIndex: NSInteger, selectedButton: UIButton)
    func buyMaskTapped()
}

class AKStoreProductHeaderView: GSKStretchyHeaderView {

    var tabsCount: NSInteger!
    var tabsDelegate: GSKTabsStretchyHeaderViewDelegate?
    
    @IBOutlet var backgroundImgView: UIImageView!
    @IBOutlet var maskTab: UIButton!
    @IBOutlet var effectTab: UIButton!
    
    @IBOutlet var redImageView: UIImageView!
    
    
    
    @IBOutlet var maskImageView: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    
    @IBOutlet var cloudImageView: UIImageView!
    
    
    @IBOutlet var buttonCollection: [UIButton]!
    
    @IBOutlet var effectPriceBtn: UIButton!
    @IBOutlet var soldLabel: UILabel!
    
    var tabArray: NSMutableArray!
    var selectedIndex = 0
    var selectedMask : AKMask?
    
    let imageLoadingIndicator: AKCustomActivityIndicatorView = { () -> AKCustomActivityIndicatorView in
        let image = UIImage(named: "icon-load")!
        let frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        return AKCustomActivityIndicatorView(image: image, frame: frame.insetBy(dx: 4.0, dy: 4.0))
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clear
        let _ = self.tabs()
        self.soldLabel.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/6)
        self.showCloudImage()
        maskImageView.addSubview(imageLoadingIndicator)
        imageLoadingIndicator.hidesWhenStopped = true
    }
    
    func tabs() ->NSArray {
        
        let buttonArray = [self.maskTab, self.effectTab]
        
        tabArray = NSMutableArray(array: buttonArray)
        
        return buttonArray as NSArray
    }

    @IBAction func tabButtonTapped(_ sender: AnyObject) {
        
        
        for button  in self.tabs() {
            
            let btn = button as! UIButton
            
            btn.titleLabel?.textColor = UIColor.white
//            btn.isEnabled = true
            
            if btn == sender as! UIButton {
                
                btn.setTitleColor(UIColor(netHex: ANARKO_COLOR), for: .normal)
//                btn.isEnabled = false

            }
        }
        
        let _btn = sender as! UIButton
        
        if selectedIndex !=  _btn.tag{
            
            selectedIndex = _btn.tag
            self.tabsDelegate?.tabsStretchyHeaderView(headerView: self, didSelectTabAtIndex: selectedIndex, selectedButton: sender as! UIButton)

        }
        
    }
    
    @IBAction func buyButtonTapped(_ sender: AnyObject) {
        
        if self.maskImageView.image != nil {
            
            self.tabsDelegate?.buyMaskTapped()

        }
        
        
    }
    
    override func didChangeStretchFactor(_ stretchFactor: CGFloat) {
        
//        self.contentView.backgroundColor = UIColor(colorLiteralRed: Float(CGFloatTranslateRange(stretchFactor, 0, 1, 0.2, 0.3)), green: Float(CGFloatTranslateRange(stretchFactor, 0, 1, 0.7, 0.3)), blue: 0.3, alpha: 1)
    }
    
    // Func 
    
    public func setMaskImage(url: String?, item: AKMask) {
        
        var price = "\(String(format: "%.2f", item.price!))$"
        
        if item.isFree == true {
            price = "0.00$"
        }
        selectedMask = item
        effectPriceBtn.setAttributedTitle(self.createBuyButtonString(withPrice: price), for: .normal)
        nameLbl.text = item.name!.stringByReplacingFirstOccurrenceOfString(target: " ", withString: "\n")
        self.maskImageView.image = nil
        
        imageLoadingIndicator.center = CGPoint(x: maskImageView.bounds.size.width / 2.0, y: maskImageView.bounds.size.height / 2.0)
        if let gifs = item.gifs, gifs.count > 0 {
            var index = 0
            if gifs.count > 1 {
                index = 1
            }
            if let gif = gifs.object(at: index) as? NSDictionary {
                self.imageLoadingIndicator.startAnimating()
                AKGlobal.getGifWithUrl(maskId: item.maskId, url: gif["url"] as! NSString, filename: gif["filename"] as! String, completionHandler: { (image) in

                    if self.selectedMask?.maskId == item.maskId {
                        self.imageLoadingIndicator.stopAnimating()
                        if image != nil {
                            self.maskImageView.image = image
                        }
                    }
                    
                })
            }
            
        }
        
    }
    
    func getMaskImage() -> UIImage {
        return self.maskImageView.image!
    }

    func showCloudImage() {
        
        cloudImageView.loadGif(name: "smoke_white")
    }
    
    public func showGlassImage(_ isShow: Bool){
        
        if isShow {
            
            self.backgroundImgView.isHidden = false
            self.redImageView.isHidden = false
            self.maskImageView.isHidden = true
            self.soldLabel.isHidden = false
            
            for button in self.buttonCollection {
                
                button.isHidden = true
            }
            self.cloudImageView.isHidden = true
            
            var alpha = 0
            UIView.animate(withDuration: 2, delay: 0.2, options: .repeat, animations: {
                
                
                self.redImageView.alpha = CGFloat(alpha)
                
            }, completion: { (bool) in
                
                alpha = 1
            })
            
//            Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(showGlassImage(_:)), userInfo: nil, repeats: false)
        }else {
            
            self.backgroundImgView.isHidden = true
            self.redImageView.isHidden = true
            self.redImageView.alpha = 1.0
            self.maskImageView.isHidden = false
            
            for button in self.buttonCollection {
                
                button.isHidden = false
            }
            self.cloudImageView.isHidden = false
            self.soldLabel.isHidden = true
            
        }
    }
    
    func createBuyButtonString(withPrice price:String!) -> NSAttributedString {
        
        let priceString = "BUY  \(price!)"
        let mutableString = NSMutableAttributedString(string: priceString, attributes: nil)
        
        mutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(colorLiteralRed: 246.0/255.0, green: 161.0/255.0, blue: 0, alpha: 1), range: NSRange(location:0,length:3))
        
        mutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(colorLiteralRed: 246.0/255.0, green: 161.0/255.0, blue: 0, alpha: 1), range: NSRange(location:3,length:priceString.characters.count-3))
        
//        mutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location:3,length:priceString.characters.count-3))
        
        return mutableString;
    }
}

extension String
{
    func stringByReplacingFirstOccurrenceOfString(
        target: String, withString replaceString: String) -> String
    {
        if let range = self.range(of: target) {
            return self.replacingCharacters(in: range, with: replaceString)
        }
        return self
    }
}
